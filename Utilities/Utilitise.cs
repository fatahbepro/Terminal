﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Reflection;

namespace UtilClass
{
    public static class Utilitise
    {
        public static string JulianToPersianDate(DateTime date)
        {
            PersianCalendar pc = new PersianCalendar();
            var year = pc.GetYear(date).ToString();
            var month = pc.GetMonth(date).ToString().PadLeft(2, '0');
            var day = pc.GetDayOfMonth(date).ToString().PadLeft(2, '0');
            return string.Format($"{year}-{month}-{day}");
        }
        public static string JulianToPersianDateTime(DateTime date)
        {
            PersianCalendar pc = new PersianCalendar();
            var year = pc.GetYear(date).ToString();
            var month = pc.GetMonth(date).ToString().PadLeft(2, '0');
            var day = pc.GetDayOfMonth(date).ToString().PadLeft(2, '0');
            return string.Format($"{year}-{month}-{day}  {date.ToString("HH:mm")}");
        }

        //public static List<T> ConvertDataTable<T>(DataTable dt)
        //{
        //    List<T> data = new List<T>();
        //    foreach (DataRow row in dt.Rows)
        //    {
        //        T item = GetItem<T>(row);
        //        data.Add(item);
        //    }
        //    return data;
        //}
        //public static T GetItem<T>(DataRow dr)
        //{
        //    Type temp = typeof(T);
        //    T obj = Activator.CreateInstance<T>();

        //    foreach (DataColumn column in dr.Table.Columns)
        //    {
        //        foreach (PropertyInfo pro in temp.GetProperties())
        //        {
        //            if (pro.Name == column.ColumnName)
        //                pro.SetValue(obj, dr[column.ColumnName], null);
        //            else
        //                continue;
        //        }
        //    }
        //    return obj;
        //}

        /// <summary>
        /// تبدیل دیتاتیبل به یک ابجک یا کلاس
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<T> ConvertDataTable<T>(this DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    //in case you have a enum/GUID datatype in your model
                    //We will check field's dataType, and convert the value in it.
                    if (pro.Name == column.ColumnName)
                    {
                        try
                        {
                            var convertedValue = GetValueByDataType(pro.PropertyType, dr[column.ColumnName]);
                            pro.SetValue(obj, convertedValue, null);
                        }
                        catch (Exception e)
                        {
                            //ex handle code                   
                            string err = e.Message;
                        }
                        //pro.SetValue(obj, dr[column.ColumnName], null);
                    }
                    else
                        continue;
                }
            }
            return obj;
        }
        private static object GetValueByDataType(Type propertyType, object o)
        {
            if (propertyType == (typeof(Guid)) || propertyType == typeof(Guid?))
            {
                return Guid.Parse(o.ToString());
            }
            else if (o == null)
            {
                return null;
            }
            else if (propertyType == typeof(int) || propertyType.IsEnum)
            {
                return Convert.ToInt32(o);
            }
            else if (propertyType == typeof(decimal))
            {
                if (o == null) return 0;
                if (string.IsNullOrEmpty(o.ToString())) return 0;
                else
                    return Convert.ToDecimal(o);
            }
            else if (propertyType == typeof(long))
            {
                return Convert.ToInt64(o);
            }
            else if (propertyType == typeof(bool) || propertyType == typeof(bool?))
            {
                return Convert.ToBoolean(o);
            }
            else if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
            {
                return Convert.ToDateTime(o);
            }

            else if (propertyType == typeof(string) || propertyType == typeof(string))
            {
                return Convert.ToString(o);
            }

            else if (DBNull.Value == o) return null;
            else if (o.ToString() == "null")
            {
                return null;
            }

            return o.ToString();
        }

        public static string GetNameOfDayDate(this DateTime ti)
        {
            DayOfWeek nameday = ti.DayOfWeek;
            string Text = "";
            switch (nameday)
            {
                case DayOfWeek.Saturday:
                    Text = "شنبه";
                    break;
                case DayOfWeek.Sunday:

                    Text = "یک شنبه";
                    break;
                case DayOfWeek.Monday:
                    Text = "دوشنبه";

                    break;
                case DayOfWeek.Tuesday:
                    Text = "سه شنبه";
                    break;

                case DayOfWeek.Wednesday:
                    Text = "چهارشنبه";
                    break;
                case DayOfWeek.Thursday:
                    Text = "پنج شنبه";
                    break;

                case DayOfWeek.Friday:
                    Text = "جمعه";
                    break;

            }

            return Text;
        }

        #region Converting
        public static string ToEmptyString(this object obj)
        {
            if (obj == null) return string.Empty;
            if (string.IsNullOrEmpty(obj.ToString())) return string.Empty;
            else
                return obj.ToString();
        }
        public static int ToInt(this object obj)
        {
            try
            {
                if (obj == null) return 0;
                else
                    return Convert.ToInt32(obj);
            }
            catch (Exception)
            {

                return 0;
            }
        }
        public static int? ToIntNullable(this object obj)
        {
            try
            {
                if (obj == null) return null;
                else
                    return Convert.ToInt32(obj);
            }
            catch (Exception)
            {

                return 0;
            }
        }
        public static long ToLong(this object obj)
        {
            try
            {
                if (obj == null) return 0;
                else
                    return Convert.ToInt64(obj);
            }
            catch (Exception)
            {

                return 0;
            }
        }
        public static long? ToLongNullable(this object obj)
        {
            try
            {
                if (obj == null) return null;
                else
                    return Convert.ToInt64(obj);
            }
            catch (Exception)
            {

                return 0;
            }
        } 
        #endregion




    }

}
