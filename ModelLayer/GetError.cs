﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Tulpep.NotificationWindow;
using System.Data;


  public static  class GetError
    {

        public static void WriteError(Exception ex)
        {
            try{

            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
            //   NameSpace.Form
            var NameSpace_Form = trace.GetFrame(0).GetMethod().ReflectedType.FullName;
            // Form Name
            var FormName = trace.GetFrame(0).GetMethod().ReflectedType.Name;
            var LineNumber = trace.GetFrame(0).GetFileLineNumber();
            var Method = trace.GetFrame(0).GetMethod().ToString();

            StreamWriter sw = new StreamWriter(".\\Error.txt",true,Encoding.UTF8);
        //sw.WriteLineAsync($"{FormName} : {Method } : LineNumber = {LineNumber} -> Error ::: " + ex.Message + " >>>" + DateTime.Now.ToString());

         sw.WriteLine("{0} : {1} : LineNumber = {2} -> Error ::: {3} >>> {4}",FormName,Method,LineNumber,ex.Message, DateTime.Now.ToString());
        sw.Close();

            if(ex.Message.Contains("The underlying provider failed on Open."))
            
                ShowError("ارتباط با سرور قطع می باشد",true);
            else

                ShowError("اشکال در انجام عملیات",false);

        }
            catch (Exception)
            {

   
            }
        }


      public static void ShowError(string Error,bool state )
      {
           PopupNotifier pup = new PopupNotifier();
            //pup.Image = Properties.Resources.male1;
            pup.TitleText = "........خطا ";
             pup.ContentText = Error;
        if (state)
        {
            pup.TitleColor = System.Drawing.Color.Red;
            pup.BodyColor = System.Drawing.Color.Yellow;
            pup.BorderColor = System.Drawing.Color.Red;

        }
        pup.Popup();
      }


      public static DataTable ToDataTable<T>(List<T> items)
      {
          DataTable dataTable = new DataTable(typeof(T).Name);
          //Get all the properties by using reflection   
          PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
          foreach (PropertyInfo prop in Props)
          {
              //Setting column names as Property names  
              dataTable.Columns.Add(prop.Name);
          }
          foreach (T item in items)
          {
              var values = new object[Props.Length];
              for (int i = 0; i < Props.Length; i++)
              {

                  values[i] = Props[i].GetValue(item, null);
              }
              dataTable.Rows.Add(values);
          }

          return dataTable;
      }  


    }





