﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilClass;

using Model;

namespace ModelLayer
{
    public class TripRepository : ITrip
    {
        public Trip FindByID(int id, string companyConnection)
        {
            using (testNetEntities db = testNetEntities.Create(companyConnection))
            {
                db.Database.Connection.ConnectionString = companyConnection;
                Trip _Trip = db.Trips.Where(c => c.ID == id).SingleOrDefault();
                return _Trip;
            }
        }

        /// <summary>
        /// لیست سرویس های یک تاریخ
        /// </summary>
        /// <param name="_fromDate">از تاریخ</param>
        /// <param name="_todate">تا تاریخ</param>
        /// <param name="_Path">لیست مسیر های این سرویس ها</param>
        /// <param name="AccessCompanyUser">تعاونی های مورد نظر</param>
        /// <param name="listCities">لیست شهر های بین راهی این سرویس ها</param>
        /// <param name="ServiceOvertime">مدت زمان فروش تا سرویس</param>
        /// <param name="IsLoacConnection">اتصال لوکال یا شبکه</param>
        /// <returns></returns>
        public List<GetTripsReport> GetTripsDateEF(DateTime fromdt, DateTime Todate, out List<PathTrip> listPath, List<Company> AccessCompanyUser,
            out List<Sp_GetCitiesTrip> listCities, int ServiceOvertime, bool IsLoacConnection)
        {
            try
            {

                listPath = new List<PathTrip>();
                List<PathTrip> Pathlist = new List<PathTrip>();
                listCities = new List<Sp_GetCitiesTrip>();
                List<GetTripsReport> listTrips = new List<GetTripsReport>();
                int countCompany = AccessCompanyUser.Count;
                string Connection = string.Empty;
                foreach (Company com in AccessCompanyUser)
                {
                    try
                    {
                        if (IsLoacConnection)
                            Connection = com.LocalConnectionString;
                        else
                            Connection = com.ConnectionString;

                        testNetEntities dbN = testNetEntities.Create(Connection);
                        dbN.Database.Connection.ConnectionString = Connection;

                        #region خواندن اطلاعات تعاونی مورد نظر با استفاده از کانکشن
                        List<GetTripsReport> query = dbN.Sp_GetTrips(fromdt, Todate);

                        string servicesID = string.Join(",", query.Select(c => c.ServiceID).Distinct().ToArray());

                        var CitiesTrip = dbN.Sp_GetCitiesTrip(servicesID).ToList();

                        var citybetween = CitiesTrip.GroupBy(c => c.ServiceID).Select(c => new
                        {
                            Title = string.Join(",", c.Select(x => x.Title).ToArray()),
                            ServiceID = c.Select(x => x.ServiceID).Distinct().SingleOrDefault()
                        }).ToList();

                        if (CitiesTrip.Count > 0)
                        {
                            string Cities = string.Join(",", CitiesTrip.Select(c => c.Title).ToArray());
                            listCities.AddRange(CitiesTrip);
                        }
                        query.Select(c => { c.CompanyCode = com.Code; c.CompanyName = com.Title; return c; }).ToList();
                        query.Select(c => { c.CitiesBetween = (citybetween.Where(x => x.ServiceID == c.ServiceID).Select(r => r.Title).SingleOrDefault()).ToEmptyString(); return c; }).ToList();

                        #region خواندن سرویس هایی که تاریخ حرکت آنها رد نشده است
                        if (DateTime.Parse(DateTime.Now.ToShortDateString()) < DateTime.Parse(fromdt.ToShortDateString()))
                        {
                            listTrips.AddRange(query);
                        }
                        else
                        {
                            if (DateTime.Parse(DateTime.Now.ToShortDateString()) > DateTime.Parse(fromdt.ToShortDateString()))
                            {
                                listTrips.AddRange(query);
                            }
                            else
                            {
                                var litoday = query.Where(c => DateTime.Parse(c.DepartureTime2).AddMinutes(ServiceOvertime) >= DateTime.Now).ToList();
                                if (litoday.Count > 0)
                                    listTrips.AddRange(litoday);
                            }
                        }
                        #endregion

                        List<PathTrip> way = query.Where(c => !string.IsNullOrEmpty(c.PathTitle)).GroupBy(c => c.PathTitle).Select(c => new PathTrip
                        {
                            PathTitle = c.Select(p => p.PathTitle).Distinct().SingleOrDefault(),
                            IDCities = c.Select(p => p.IDCities).Distinct().SingleOrDefault(),
                            IDCity1 = c.Select(p => p.IDCity1).Distinct().SingleOrDefault(),
                            IDCity2 = c.Select(p => p.IDCity2).Distinct().SingleOrDefault()
                        }).Distinct().ToList();

                        var notis = way.Where(c => !Pathlist.Any(x => x.PathTitle == c.PathTitle)).ToList();
                        if (notis.Count > 0)
                            Pathlist.AddRange(notis);
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        string Error = ex.Message;
                        continue;
                    }
                }
                listPath.AddRange(Pathlist);
                listTrips.Select(c => { c.Day = c.Date.GetNameOfDayDate(); return c; }).ToList();
                return listTrips;
            }
            catch (Exception ex)
            {
                GetError.WriteError(ex);
                // GetError.ShowError();
                listPath = new List<PathTrip>();
                listCities = new List<Sp_GetCitiesTrip>();
                return new List<GetTripsReport>();
            }


        }

        public List<GetTripsResultReport> GetTripSummery(DateTime _fromdate, DateTime _todate, out List<PathTrip> listPath, List<Company> AccessCompanyUser, bool IsLoacConnection)
        {

            try
            {
                listPath = new List<PathTrip>();
                List<GetTripsResultReport> li = new List<GetTripsResultReport>();
                int countCompany = AccessCompanyUser.Count;
                string Connection = string.Empty;
                foreach (Company com in AccessCompanyUser)
                {
                    try
                    {
                        if (IsLoacConnection)
                            Connection = com.LocalConnectionString;
                        else
                            Connection = com.ConnectionString;
                        testNetEntities dbN = testNetEntities.Create(Connection);
                        dbN.Database.Connection.ConnectionString = Connection;
                        List<GetTripsResultReport> list = dbN.GetTrips(_fromdate, _todate).ToList();
                        list.Select(c => { c.CompanyCode = com.Code; return c; }).ToList();

                        var summe = list.GroupBy(g => g.PathTitle).Select(x => new GetTripsResultReport
                        {
                            SumPrice = x.Sum(r => r.SumPrice),
                            SumFundPrice = x.Sum(c => c.SumFundPrice),
                            sumNumberchair = x.Sum(c => c.sumNumberchair),
                            SumOnlinePrice = x.Sum(c => c.SumOnlinePrice),
                            Summery = x.Sum(c => c.Summery),
                            CountTrip = x.Sum(c => c.CountTrip),
                            CompanyCode = x.First().CompanyCode,
                            PathTitle = x.First().PathTitle
                        }).ToList();
                        if (summe.Count > 0)
                            li.AddRange(summe);

                        var pathtrip = list.Select(c => new PathTrip { PathTitle = c.PathTitle }).Distinct().ToList();
                        if (pathtrip.Count > 0)
                            listPath.AddRange(pathtrip);
                    }
                    catch (Exception ex)
                    {
                        string Error = ex.Message;
                        continue;
                    }
                }

                var P = listPath.GroupBy(g => g.PathTitle).Select(c => new PathTrip { PathTitle = c.First().PathTitle }).ToList();
                listPath = new List<PathTrip>();
                listPath.AddRange(P);
                return li;
            }
            catch (Exception ex)
            {

                GetError.WriteError(ex);
                listPath = new List<PathTrip>();
                return new List<GetTripsResultReport>();
            }


        }

        /// <summary>
        /// مجموع بلیط های پیش فروش
        /// </summary>
        /// <param name="_fromdate"></param>
        /// <param name="_todate"></param>
        /// <param name="listPath"></param>
        /// <param name="AccessCompanyUser"></param>
        /// <returns></returns>
        public List<GetTripsResultReport> GetTripSummerySaleTicket(DateTime _fromdate, DateTime _todate, out List<PathTrip> listPath, List<Company> AccessCompanyUser, bool IsLoacConnection)
        {

            try
            {
                listPath = new List<PathTrip>();
                List<GetTripsResultReport> li = new List<GetTripsResultReport>();
                int countCompany = AccessCompanyUser.Count;
                string Connection = string.Empty;
                foreach (Company com in AccessCompanyUser)
                {
                    try
                    {

                        if (IsLoacConnection)
                            Connection = com.LocalConnectionString;
                        else
                            Connection = com.ConnectionString;

                        testNetEntities dbN = testNetEntities.Create(Connection);
                        dbN.Database.Connection.ConnectionString = Connection;

                        List<GetTripsResultReport> list = dbN.GetTripsSaleTicket(_fromdate, _todate).ToList();
                        list.Select(c => { c.CompanyCode = com.Code; return c; }).ToList();

                        var summe = list.GroupBy(g => g.PathTitle).Select(x => new GetTripsResultReport
                        {
                            SumPrice = x.Sum(r => r.SumPrice),
                            SumFundPrice = x.Sum(c => c.SumFundPrice),
                            sumNumberchair = x.Sum(c => c.sumNumberchair),
                            SumOnlinePrice = x.Sum(c => c.SumOnlinePrice),
                            Summery = x.Sum(c => c.Summery),
                            CountTrip = x.Sum(c => c.CountTrip),
                            CompanyCode = x.First().CompanyCode,
                            PathTitle = x.First().PathTitle
                        }).ToList();
                        if (summe.Count > 0)
                            li.AddRange(summe);

                        var pathtrip = list.Select(c => new PathTrip { PathTitle = c.PathTitle }).Distinct().ToList();
                        if (pathtrip.Count > 0)
                            listPath.AddRange(pathtrip);

                    }
                    catch (Exception ex)
                    {
                        string Error = ex.Message;
                        continue;
                    }
                }


                var P = listPath.GroupBy(g => g.PathTitle).Select(c => new PathTrip { PathTitle = c.First().PathTitle }).ToList();
                listPath = new List<PathTrip>();
                listPath.AddRange(P);
                return li;
            }
            catch (Exception ex)
            {
                GetError.WriteError(ex);
                listPath = new List<PathTrip>();
                return new List<GetTripsResultReport>();
            }
        }

        public int GetNumberRezervTickets(string ConnectionCompany, int TripID, out int FullChair, int ServiceOvertime)
        {
            try
            {

                testNetEntities dbN = testNetEntities.Create(ConnectionCompany);
                dbN.Database.Connection.ConnectionString = ConnectionCompany;
                var ticket = dbN.Sp_GetCountRezerv(TripID);
                DateTime timeservice = ticket.DepartureTime.AddMinutes(ServiceOvertime);
                if (timeservice >= DateTime.Now)
                {
                    FullChair = ticket.NumTickets;
                    return ticket.NumReserves;
                }
                else
                {
                    FullChair = 0;
                    return 0;
                }

            }
            catch (Exception ex)
            {
                GetError.WriteError(ex);
                FullChair = 0;
                return 0;
            }


        }


    }

}


