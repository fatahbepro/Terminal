﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace ModelLayer
{
    public class CompanyRepository : ICompany
    {
        public List<Company> GetAll(bool Getimg)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                if (Getimg)
                    return db.Companies.ToList();
                else
                {
                    string sql = string.Format(@"
                                    SELECT  [ID]
                                          ,[Title]
                                          ,[Code]
                                          ,[ConnectionString]
                                          ,[Enabled]
                                           ,LocalConnectionString,Logo=null                                
                                            FROM [dbo].[Company] ");
                    return db.Database.SqlQuery<Company>(sql).ToList<Company>();
                }
            }
        }

        public Company FindByID(int id)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                Company _company = db.Companies.Where(c => c.ID == id).SingleOrDefault();
                return _company;
            }
        }

        public Company FindByCode(string Code)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                string sql = string.Format(@"
                                SELECT  [ID]
                                      ,[Title]
                                      ,[Code]
                                      ,[ConnectionString]
                                      ,[Enabled]
                                       ,LocalConnectionString,Logo=null                                
                                        FROM [dbo].[Company]
                                  WHERE [Code]={0}", Code);
                return db.Companies.SqlQuery(sql).SingleOrDefault();
            }
        }
        public List<Company> GetCompaniesByListCode(string Codes)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                string sql = string.Format(@"
                                SELECT  [ID]
                                      ,[Title]
                                      ,[Code]
                                      ,[ConnectionString]
                                      ,[Enabled]
                                       ,LocalConnectionString,Logo=null                                
                                        FROM [dbo].[Company]
                                  WHERE [Enabled]=1 and  [Code] in ({0})", Codes);
                return db.Database.SqlQuery<Company>(sql).ToList<Company>();
            }
        }
        public int Insert(Company company)
        {
            using (UserDBEntities db = new UserDBEntities())
            {

                db.Companies.Add(company);
                return db.SaveChanges();
            }
        }

        public int Update(Company company)
        {
            using (UserDBEntities db = new UserDBEntities())
            {

                db.Companies.Attach(company);
                db.Entry(company).State = System.Data.Entity.EntityState.Modified;
                return db.SaveChanges();
            }
        }

        public int Delete(Company company)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                db.Companies.Attach(company);
                db.Companies.Remove(company);
                return db.SaveChanges();
            }
        }

        public int RemoveByID(int id)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                Company _company = db.Companies.Where(c => c.ID == id).SingleOrDefault();
                db.Companies.Attach(_company);
                db.Companies.Remove(_company);
                return db.SaveChanges();
            }
        }

        public int RemoveByCode(string code)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                Company _company = db.Companies.Where(c => c.Code == code).SingleOrDefault();
                db.Companies.Attach(_company);
                db.Companies.Remove(_company);
                return db.SaveChanges();
            }
        }

        public Company FindByCodeEnable(string Code, bool _Enable = true)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                string sql = string.Format(@"
                                SELECT  [ID]
                                      ,[Title]
                                      ,[Code]
                                      ,[ConnectionString]
                                      ,[Enabled]
                                       ,LocalConnectionString,Logo=null                                
                                        FROM [dbo].[Company]
                                  WHERE [Enabled]={0} AND [Code]={1}
                                ", Convert.ToInt16(_Enable), Code);
                return db.Companies.SqlQuery(sql).SingleOrDefault();
                //Company _company = db.Companies.Where(c => c.Code == Code && c.Enabled == _Enable).SingleOrDefault();
                //return _company;
            }
        }
    }
}
