﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;
namespace ModelLayer
{
  public   class LayoutRipository:ILayout
    {
        public List<Layout> GetAll()
        {
            using (testNetEntities db = new testNetEntities(""))
            {
                return db.Layouts.ToList();
            }
        }

        public Layout FindByID(int id,string _connectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(_connectionCompany))
            {
                db.Database.Connection.ConnectionString = _connectionCompany;
                Layout _Layout = db.Layouts.Where(c => c.ID == id).SingleOrDefault();
                return _Layout;
            }
        }

        public int Insert(Layout _layout)
        {
            using (testNetEntities db = new testNetEntities(""))
            {

                db.Layouts.Add(_layout);
                return db.SaveChanges();
            }
        }

        public int Update(Layout _layout)
        {
            using (testNetEntities db = new testNetEntities(""))
            {

                db.Layouts.Attach(_layout);
                db.Entry(_layout).State = System.Data.Entity.EntityState.Modified;
                return db.SaveChanges();
            }
        }

        public int Delete(Layout _layout)
        {
            using (testNetEntities db = new testNetEntities(""))
            {
                db.Layouts.Attach(_layout);
                db.Layouts.Remove(_layout);
                return db.SaveChanges();
            }
        }

        public int RemoveByID(int ID)
        {
            using (testNetEntities db = new testNetEntities(""))
            {
                Layout _Layout = db.Layouts.Where(c => c.ID == ID).SingleOrDefault();
                db.Layouts.Attach(_Layout);
                db.Layouts.Remove(_Layout);
                return db.SaveChanges();
            }
        }
    }
}
