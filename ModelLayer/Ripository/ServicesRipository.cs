﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace ModelLayer
{
    public class ServicesRipository : IServices
    {
        public Service GetService(int idService,string connectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(connectionCompany))
            {
                return db.Services.Where(c => c.ID == idService).SingleOrDefault();
            }
        }
    }
}
