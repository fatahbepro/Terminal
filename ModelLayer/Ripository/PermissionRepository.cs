﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace ModelLayer
{
    public class PermissionRepository : IPermission
    {
        UserDBEntities db = new UserDBEntities();
        public int Delete(Permission _permission)
        {

            db.Permissions.Remove(_permission);
            return db.SaveChanges();

            
        }

        public Permission FindByID(int ID)
        {
            Permission _Permission = db.Permissions.Where(c => c.ID==ID).SingleOrDefault();
            return _Permission;
        }

        public Permission FindByName(string _SettingName,string usercode)
        {
            Permission _Permission = db.Permissions.Where(c => c.SettingName == _SettingName && c.UserCode == usercode).SingleOrDefault();
            return _Permission;
        }

        public List<Permission> GetAll()
        {
            return db.Permissions.ToList();
        }

        public int Insert(Permission permission)
        {
            db.Permissions.Add(permission);
            return db.SaveChanges();
        }

        public int RemoveByName(string _SettingName, string UserCode)
        {
            Permission _Permission = db.Permissions.Where(c => c.SettingName == _SettingName && c.UserCode == UserCode).SingleOrDefault();
            db.Permissions.Remove(_Permission);
            return db.SaveChanges();
        }

        public int Update(Permission permission)
        {
            db.Entry(permission).State = System.Data.Entity.EntityState.Modified;
            return db.SaveChanges();
        }
    }
}
