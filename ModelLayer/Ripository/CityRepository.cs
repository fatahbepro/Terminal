﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace ModelLayer
{
    public class CityRepository : ICity
    {

        public City FindByID(int id, string _Connection)
        {
            using (testNetEntities db = testNetEntities.Create(_Connection))
            {
                City _City = db.Cities.Where(c => c.ID == id).SingleOrDefault();
                return _City;
            }
        }

        public City FindByCode(string code, string _Connection)
        {
            using (testNetEntities db = testNetEntities.Create(_Connection))
            {
                City _City = db.Cities.Where(c => c.Code == code).FirstOrDefault();
                return _City;
            }
        }

        public List<CitiesServices> FindByIDService(int idService, string Connection)
        {
            using (testNetEntities db = testNetEntities.Create(Connection))
            {
                var aa = db.Database.Connection.ConnectionString;
                string sql = string.Format(@"
                                            SELECT 
                                            [ci].[ID],[ci].[Title],[ci].[Code],[sr].[Price]
                                             FROM Cities AS ci
                                            INNER JOIN ServiceDests AS sr ON [ci].[ID]=[sr].[CityID]
                                            WHERE [sr].[ServiceID]={0}", idService);
                return db.Database.SqlQuery<CitiesServices>(sql).ToList<CitiesServices>();
                //List<CitiesServices> qu = (from Cities in db.Cities
                //                           join Sr in db.ServiceDests on new { CityID = Cities.ID } equals new { CityID = Sr.CityID }
                //                           where
                //                             Sr.ServiceID == idService
                //                           select new CitiesServices
                //                           {
                //                               ID = Cities.ID,
                //                               Title = Cities.Title,
                //                               Code = Cities.Code,
                //                               Price = Sr.Price
                //                           }).ToList();
                //return qu;
            }
        }

    }
}
