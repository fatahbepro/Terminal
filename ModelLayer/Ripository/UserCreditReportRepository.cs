﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace ModelLayer
{
    public class UserCreditReportRepository : IUserCreditReport
    {

        public List<UserCreditReport> GetAll()
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                return db.UserCreditReports.ToList();
            }
        }

        public int Insert(UserCreditReport user)
        {
            using (UserDBEntities db = new UserDBEntities())
            {

                db.UserCreditReports.Add(user);
                return db.SaveChanges();
            }
        }

        public List<UserCreditReport> ReportAllUser(DateTime FromdDate, DateTime Todate)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                return db.UserCreditReports.Where(c => c.ActionDate >= FromdDate && c.ActionDate <= Todate).ToList();

            }
        }

        public List<ReportCreditUser> ReportUser(DateTime FromdDate, DateTime Todate, string UserCode)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                List<ReportCreditUser> li = new List<ReportCreditUser>();

                var countrow = db.UserCreditReports.Where(c => c.ActionDate < FromdDate && c.UserCode == UserCode).ToList().Count;
                long mode = 0;

                if (countrow > 0)
                {
                    mode = db.UserCreditReports.Where(c => c.ActionDate < FromdDate && c.UserCode == UserCode).Sum(c => c.Recived - c.Payment);
                }

                li.Add(new ReportCreditUser() { ActionDate = null, ActionType = "مانده حساب از قبل", ID = 0, Payment = 0, Recived = 0, UserCode = UserCode, Mode = mode });

                var listrpt = (from r in db.UserCreditReports
                               where r.ActionDate >= FromdDate && r.ActionDate <= Todate && r.UserCode == UserCode
                               select new ReportCreditUser
                               {
                                   ID = r.ID,
                                   UserCode = r.UserCode,
                                   Recived = r.Recived,
                                   Payment = r.Payment,
                                   ActionDate = r.ActionDate,
                                   ActionType = r.ActionType.Value == 0 ? "افزایش اعتبار" : (r.ActionType == 1 ? "خرید" : "استرداد"),
                                   Mode = 0
                               }).ToList();
                li.AddRange(listrpt);
                return li;

            }
        }


    }
}
