﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;
using System.Data.Entity.Validation;
using System.Data.Entity.Core.Objects;

namespace ModelLayer
{
    public class TicketRepository : ITicket
    {

        public List<Ticket> GetAll(string connectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(connectionCompany))
            {
                return db.Tickets.ToList();
            }
        }
        public Ticket FindByID(int id, string connectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(connectionCompany))
            {
                Ticket _ticket = db.Tickets.Where(c => c.ID == id).SingleOrDefault();
                return _ticket;
            }
        }
        public Ticket FindTicketByTripInfo(string _no, string codeCompany, string connectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(connectionCompany))
            {
                Ticket _ticket = db.Tickets.Where(c => c.No == _no && c.CompanyCode == codeCompany).SingleOrDefault();
                return _ticket;
            }
        }
        public int Insert(TicketLocal Tickets, string connectionCompany)
        {
            #region Setvalue

            Tickets.Discount = Tickets.Discount.HasValue ? Tickets.Discount.Value : 0;
            Ticket tick = new Ticket()
            {

                ID = Tickets.ID,
                TripID = Tickets.TripID,
                CustomerID = Tickets.CustomerID,
                Man = Tickets.Man,
                Fullname = Tickets.Fullname,
                No = Tickets.No,
                Chairs = Tickets.Chairs,
                CityID = Tickets.CityID,
                SaleType = Tickets.SaleType,
                FundID = Tickets.FundID,
                Expire = Tickets.Expire,
                BranchID = Tickets.BranchID,
                Tel = Tickets.Tel,
                Price = Tickets.Price,
                PaidBack = Tickets.PaidBack,
                PaidBackPercent = Tickets.PaidBackPercent,
                Discount = Tickets.Discount,
                SaleUserID = Tickets.SaleUserID,
                SaleDate = Tickets.SaleDate,
                NumChairs = Tickets.NumChairs,
                Archive = Tickets.Archive,
                Printed = Tickets.Printed,
                FundUserID = Tickets.FundUserID,
                ReturnUserID = Tickets.ReturnUserID,
                PaidBackDate = Tickets.PaidBackDate,
                Address = Tickets.Address,
                CityCode = Tickets.CityCode,
                CashierCode = Tickets.CashierCode,
                CashierName = Tickets.CashierName,
                PayDate = Tickets.PayDate,
                CompanyCode = Tickets.CompanyCode,
                CityName = Tickets.CityName
               ,
                FundPrice = Tickets.FundPrice.Value
               ,
                TypeTicket = Tickets.TypeTicket
               ,
                ReturnUserCode = Tickets.ReturnUserCode
               ,
                ReturnUserName = Tickets.ReturnUserName
               ,
                PayLock = Tickets.PayLock
            };
            #endregion
            int result = 0;
            try
            {

                using (testNetEntities dbn = testNetEntities.Create(connectionCompany))
                {
                    dbn.Database.Connection.ConnectionString = connectionCompany;
                    dbn.Tickets.Add(tick);
                    dbn.SaveChanges();
                    result = tick.ID;

                }
                return result;
            }
            catch (Exception ex)
            {

                return result;


            }


        }
        public int Update(Ticket ticketnet, string ConnectionCompany)
        {
            int result = 0;
            using (testNetEntities dbn = testNetEntities.Create(ConnectionCompany))
            {
                dbn.Database.Connection.ConnectionString = ConnectionCompany;
                dbn.Tickets.Attach(ticketnet);
                dbn.Entry(ticketnet).State = System.Data.Entity.EntityState.Modified;
                result = dbn.SaveChanges();
            }
            return result;
        }
        public int UpdateTicketCompany(Ticket _ticket, string ConnectionCompany)
        {
            int result = 0;
            using (testNetEntities dbn = testNetEntities.Create(ConnectionCompany))
            {
                dbn.Database.Connection.ConnectionString = ConnectionCompany;
                dbn.Tickets.Attach(_ticket);
                dbn.Entry(_ticket).State = System.Data.Entity.EntityState.Modified;
                result = dbn.SaveChanges();
            }
            return result;
        }
        public int Delete(Ticket ticket, string connectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(connectionCompany))
            {
                db.Tickets.Attach(ticket);
                db.Tickets.Remove(ticket);
                return db.SaveChanges();
            }

        }
        public int RemoveByID(int id, string connectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(connectionCompany))
            {
                Ticket _ticket = db.Tickets.Where(c => c.ID == id).SingleOrDefault();
                db.Tickets.Attach(_ticket);
                db.Tickets.Remove(_ticket);
                return db.SaveChanges();
            }
        }
        public List<TicketLocal> GetListByTripID(int _tripID, string ConnectionCompany, string CodeCompany)
        {
            List<TicketLocal> query = new List<TicketLocal>();
            using (testNetEntities db = testNetEntities.Create(ConnectionCompany))
            {
                db.Database.Connection.ConnectionString = ConnectionCompany;
                query = db.Sp_GetTicketsOfTrip(_tripID);
                query.Where(c => string.IsNullOrEmpty(c.CompanyCode)).Select(c => { c.CompanyCode = CodeCompany; return c; }).ToList();
                return query;

            }
        }
        public List<Ticket> FindByIDTrip(int idtrip, string ConnectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(ConnectionCompany))
            {
                db.Database.Connection.ConnectionString = ConnectionCompany;
                List<Ticket> _ticket = db.Tickets.Where(c => c.TripID == idtrip && c.PaidBack == 0).ToList();
                return _ticket;

            }

        }
        public object GetTripbyID(int TripID, string ConnectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(ConnectionCompany))
            {
                db.Database.Connection.ConnectionString = ConnectionCompany;
                var tr = db.Trips.Where(c => c.ID == TripID).Select(c => new { c.ID, c.Closed, c }).SingleOrDefault();
                return tr;

            }
        }
        /// <summary>
        /// خواندن بلیط های فروخته شده توسط صندوقدار از دیتابیس صندوقدار
        /// </summary>
        /// <param name="fromdt">تاریخ شروع</param>
        /// <param name="todate">تاریخ اتمام</param>
        /// <param name="ISAdmin">آیا کاربر مدیر می باشد</param>
        /// <param name="CashierCode">کد صندوقدار</param>
        /// <returns></returns>
        public List<TicketLocal> GetTicketByDate(DateTime fromdt, DateTime todate, bool ISAdmin, string CashierCode, List<Company> Companis, bool localConnection)
        {

            List<TicketLocal> query = new List<TicketLocal>();
            List<TicketLocal> li = new List<TicketLocal>();

            string Connection = string.Empty;
            foreach (Company _company in Companis)
            {

                if (localConnection)
                    Connection = _company.LocalConnectionString;
                else
                    Connection = _company.ConnectionString;

                using (testNetEntities db = testNetEntities.Create(Connection))
                {
                    string cashier = @" AND [tc].[CashierCode]=" + CashierCode;
                    string sql = string.Format(@"
                                                SELECT [tc].* FROM [dbo].[Tickets] AS tc
                                                WHERE [tc].[SaleDate] BETWEEN '{0}' AND '{1}' AND [tc].[SaleType]<>1 AND [tc].[PaidBack]=0 ", fromdt, todate);
                    if (!ISAdmin)
                    {
                        sql += cashier;
                    }
                    li = db.Database.SqlQuery<TicketLocal>(sql).ToList<TicketLocal>();
                    li.Select(c => { c.CompanyCode = _company.Code; return c; }).ToList();
                    if (li.Count > 0)
                        query.AddRange(li);
                }

            }

            return query;

        }

        /// <summary>
        /// خواندن بلیط های برگشتی این کاربر در تعاونی ها یی که دسترسی دارد
        /// </summary>
        /// <param name="fromdt"></param>
        /// <param name="todate"></param>
        /// <param name="CashierCode"></param>
        /// <param name="licompany"></param>
        /// <returns></returns>
        public List<TicketLocal> GetTicketRollBakByDate(DateTime fromdt, DateTime todate, bool ISAdmin, string CashierCode, List<Company> licompany, bool localConnection)
        {

            List<TicketLocal> query = new List<TicketLocal>();
            List<TicketLocal> li = new List<TicketLocal>();
            string Connection = string.Empty;
            foreach (Company companye in licompany)
            {

                if (localConnection)
                    Connection = companye.LocalConnectionString;
                else
                    Connection = companye.ConnectionString;
                #region خواندن اطلاعات
                string user = string.Format(@" WHERE [tc].[PaidBackDate] BETWEEN '{0}' AND '{1}' AND  [tc].[PaidBack]=1 
                                        AND [tc].[ReturnUserCode]={2}
                                        ", fromdt, todate, CashierCode);
                string admin = string.Format(@"  WHERE [tc].[SaleDate] BETWEEN '{0}' AND '{1}' AND  [tc].[PaidBack]=1 ", fromdt, todate);
                using (testNetEntities db = testNetEntities.Create(Connection))
                {
                    string sql;
                    if (ISAdmin)
                        sql = string.Format(@"SELECT [tc].* FROM [dbo].[Tickets] AS tc {0}", admin);
                    else
                        sql = string.Format(@"SELECT [tc].* FROM [dbo].[Tickets] AS tc {0}", user);

                    li = db.Database.SqlQuery<TicketLocal>(sql).ToList<TicketLocal>();
                    li.Select(c => { c.CompanyCode = companye.Code; return c; }).ToList();
                    if (li.Count > 0)
                        query.AddRange(li);
                }
                #endregion
            }

            return query;

        }

        /// <summary>
        /// خواندن بلیطهای اینترنتی از دیتابیسهای تعاونی ها که صندوقدار فروخته است
        /// </summary>
        /// <param name="fromdt"></param>
        /// <param name="todate"></param>
        /// <param name="listcompany"></param>
        /// <returns></returns>
        public List<TicketLocal> GetTicketNetByDate(DateTime fromdt, DateTime todate, List<Company> listcompany, bool localConnection)
        {

            List<TicketLocal> li = new List<TicketLocal>();
            string Connection = string.Empty;
            foreach (Company company in listcompany)
            {
                #region  خواندن بلیطهای تعاونی ها

                if (localConnection)
                    Connection = company.LocalConnectionString;
                else
                    Connection = company.ConnectionString;

                using (testNetEntities db = testNetEntities.Create(Connection))
                {

                    string sql = string.Format(@"SELECT [tc].* FROM [dbo].[Tickets] AS tc
                        WHERE [tc].[SaleDate] BETWEEN '{0}' AND '{0}' AND [tc].[SaleType]=0 AND [tc].[CityID]=0", fromdt, todate);
                    var query = db.Database.SqlQuery<TicketLocal>(sql).ToList<TicketLocal>();
                    if (query.Count > 0)
                    {
                        query.Select(c => { c.CompanyCode = company.Code; return c; }).ToList();
                        li.AddRange(query);
                    }
                }
                #endregion
            }

            return li;

        }

        /// <summary>
        /// دریافت مشخصات بلیط تعاونی مورد نظر
        /// </summary>
        /// <param name="id">کد بلیط</param>
        /// <param name="ConnectionCompany">کانکشنن تعاونی</param>
        /// <returns></returns>
        public Ticket FindByIDNet(int id, string ConnectionCompany)
        {
            using (testNetEntities dbn = testNetEntities.Create(ConnectionCompany))
            {
                dbn.Database.Connection.ConnectionString = ConnectionCompany;
                Ticket _ticket = dbn.Tickets.Where(c => c.ID == id).SingleOrDefault();
                return _ticket;
            }
        }
        public Ticket FindByIDNetSaly(int id, string ConnectionCompany)
        {
            using (testNetEntities dbn = testNetEntities.Create(ConnectionCompany))
            {
                dbn.Database.Connection.ConnectionString = ConnectionCompany;
                Ticket _ticket = dbn.Tickets.Where(c => c.ID == id && c.SaleType == 1 && c.PayLock == null).SingleOrDefault();
                return _ticket;

            }
        }
        public int UpdateticketNet(Ticket Tickets, string ConnectionCompany)
        {
            Tickets.Discount = Tickets.Discount == null ? 0 : Tickets.Discount.Value;
            using (testNetEntities dbn = testNetEntities.Create(ConnectionCompany))
            {
                dbn.Tickets.Attach(Tickets);
                dbn.Entry(Tickets).State = System.Data.Entity.EntityState.Modified;
                return dbn.SaveChanges();
            }

        }
        public int UpSertTickets(Ticket Tickets, string connectionCompany)
        {
            int result = 0;
            int idticketCompany = Tickets.ID;
            Tickets.Discount = Tickets.Discount == null ? 0 : Tickets.Discount.Value;
            try
            {
                using (testNetEntities dbn = testNetEntities.Create(connectionCompany))
                {
                    dbn.Database.Connection.ConnectionString = connectionCompany;
                    Tickets.ID = idticketCompany;
                    dbn.Tickets.Attach(Tickets);
                    dbn.Entry(Tickets).State = System.Data.Entity.EntityState.Modified;
                    dbn.SaveChanges();
                    result = Tickets.ID;

                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }

        }
        public List<TicketLocal> GetListTicketDate(DateTime fromdt, DateTime todate, List<Company> listcompany, bool localConnection, int status = 0)
        {

            List<TicketLocal> listTicket = new List<TicketLocal>();
            string Connection = string.Empty;
            foreach (Company company in listcompany)
            {
                if (localConnection)
                    Connection = company.LocalConnectionString;
                else
                    Connection = company.ConnectionString;
                #region خواندن اطلاعات از دیتابیس مورد نظر
                using (testNetEntities db = testNetEntities.Create(Connection))
                {
                    db.Database.Connection.ConnectionString = Connection;
                    var litick = db.Sp_GetAllTicketsTrips(fromdt, todate, status);
                    litick.Select(c => { c.CompanyCode = company.Code; return c; }).ToList();
                    if (litick.Count > 0)
                        listTicket.AddRange(litick);
                }

                #endregion

            }

            return listTicket;
        }
        public bool SearchTicketByNo(string NO, string ConnectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(ConnectionCompany))
            {
                db.Database.Connection.ConnectionString = ConnectionCompany;
                List<Ticket> _ticket = db.Tickets.Where(c => c.No == NO).ToList();
                if (_ticket.Count == 0)
                    return true;
                else
                    return false;

            }

        }
        public int SuccessPayLockPos(int IDTicketCompany, string ConnectionCompany)
        {

            int result = 0;
            using (testNetEntities dbn = testNetEntities.Create(ConnectionCompany))
            {
                dbn.Database.Connection.ConnectionString = ConnectionCompany;
                Ticket ticket = dbn.Tickets.Where(c => c.ID == IDTicketCompany).SingleOrDefault();

                if (ticket != null)
                {
                    ticket.PayLock = false;
                    ticket.SaleType = 0;
                    dbn.Tickets.Attach(ticket);
                    dbn.Entry(ticket).State = System.Data.Entity.EntityState.Modified;
                    result = dbn.SaveChanges();
                }

            }

            return result;

        }
        public int DeleteTickets(int idticketCompany, string ConnectionCompany)
        {

            int result = 0;
            using (testNetEntities dbn = testNetEntities.Create(ConnectionCompany))
            {
                dbn.Database.Connection.ConnectionString = ConnectionCompany;
                Ticket _ticket = dbn.Tickets.Where(c => c.ID == idticketCompany).SingleOrDefault();
                if (_ticket != null)
                {
                    dbn.Tickets.Remove(_ticket);
                    result = dbn.SaveChanges();
                }
            }


            return result;
        }
        public List<Ticket> GetTicketPaylockRollBack(string ConnectionCompany, string _cashierCode)
        {
            try
            {
                using (testNetEntities db = testNetEntities.Create(ConnectionCompany))
                {
                    return db.SP_GetTicketPaylockRollBack(_cashierCode);
                }
            }
            catch (Exception)
            {
                return new List<Ticket>();
            }

        }
        public Ticket FindTicketPaylockRollBack(Ticket _ticket, string connectionCompany)
        {
            using (testNetEntities db = testNetEntities.Create(connectionCompany))
            {

                Ticket ticket = db.Tickets.Where(c => c.TripID == _ticket.TripID && c.Chairs == _ticket.Chairs && c.CompanyCode == _ticket.CompanyCode && c.PayLock == true && c.SaleType != 1).SingleOrDefault();
                return ticket;

            }
        }
        public List<TicketLocal> GetTicketSales(DateTime fromdt, DateTime todate, List<Company> Companis, bool localConnection)
        {

            List<TicketLocal> query = new List<TicketLocal>();
            string Connection = string.Empty;
            foreach (Company _company in Companis)
            {

                if (localConnection)
                    Connection = _company.LocalConnectionString;
                else
                    Connection = _company.ConnectionString;

                using (testNetEntities db = testNetEntities.Create(Connection))
                {
                    #region ListTicket
                    string sql = string.Format(@"SELECT [tc].* FROM [dbo].[Tickets] AS tc
                        INNER JOIN [dbo].[Trips] AS tr ON [tr].[ID]=[tc].[TripID]
                        WHERE [tc].[SaleDate] BETWEEN '{0}' AND '{0}' AND [tc].[SaleType] <>1 AND [tc].[PaidBack] <> 1 AND  [tr].[Closed]<> 1 ", fromdt, todate);
                    var li = db.Database.SqlQuery<TicketLocal>(sql).ToList<TicketLocal>();
                    if (li.Count > 0)
                    {
                        li.Select(c => { c.CompanyCode = _company.Code; return c; }).ToList();
                        query.AddRange(li);
                    }
                    #endregion

                }

            }

            return query;
        }
    }
}
