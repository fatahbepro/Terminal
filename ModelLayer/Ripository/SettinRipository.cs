﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace ModelLayer
{
   public  class SettinRipository:ISetting
    {
        public List<Setting> GetAll()
        {
            
            using (UserDBEntities db = new UserDBEntities())
            {

                return db.Settings.ToList();
            }
       
          
        }

        public Setting FindByName(string name)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                Setting _setting = db.Settings.Where(c => c.Name == name).SingleOrDefault();
                return _setting;
            }
        }

        public int Insert(Setting setting)
        {
            using (UserDBEntities db = new UserDBEntities())
            {

                db.Settings.Add(setting);
                return db.SaveChanges();
            }
        }

        public int Update(Setting setting)
        {
            using (UserDBEntities db = new UserDBEntities())
            {

                db.Settings.Attach(setting);
                db.Entry(setting).State = System.Data.Entity.EntityState.Modified;
                return db.SaveChanges();
            }
        }

        public int Delete(Setting setting)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                db.Settings.Attach(setting);
                db.Settings.Remove(setting);
                return db.SaveChanges();
            }
        }

        public int RemoveByName(string name)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                Setting _setting = db.Settings.Where(c => c.Name == name).SingleOrDefault();
                db.Settings.Attach(_setting);
                db.Settings.Remove(_setting);
                return db.SaveChanges();
            }
        }
    }
}
