﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;
using ModelLayer;

namespace ModelLayer
{
  public  class PathTripRepository: IPathTrip
    {

      public List<PathTrip> GetAll()
        {
            using (testNetEntities db = new testNetEntities(""))
            {
                List<PathTrip> query =
              (from Trips in db.Trips
               join Services in db.Services on new { ID = Trips.ServiceID } equals new { ID = Services.ID }
               join Paths in db.Paths on new { PathID = Services.PathID } equals new { PathID = Paths.ID }
               join City1 in db.Cities on new { SrcCityID = Paths.SrcCityID } equals new { SrcCityID = City1.ID }
               join City2 in db.Cities on new { DestCityID = Paths.DestCityID } equals new { DestCityID = City2.ID }
               where

                         (Trips.Closed == null || Trips.Closed == false) 
                                 && Trips.Locked != true

               select new PathTrip
               {
                   
                   PathTitle = (City1.Title + " -> " + City2.Title),
                   IDCities = (City1.ID + " -> " + City2.ID),
                  IDCity1= City1.ID,
                   IDCity2 = City2.ID
               }).Distinct().ToList();


                return query.ToList();
            }

        }

        public PathTrip FindByIDCityFirsh(int idcity1)
        {
            using (testNetEntities db = new testNetEntities(""))
            {

                PathTrip query = (from Trips in db.Trips
                             join Services in db.Services on new { ID = Trips.ServiceID } equals new { ID = Services.ID }
                             join Paths in db.Paths on new { PathID = Services.PathID } equals new { PathID = Paths.ID }
                             join City1 in db.Cities on new { SrcCityID = Paths.SrcCityID } equals new { SrcCityID = City1.ID }
                             join City2 in db.Cities on new { DestCityID = Paths.DestCityID } equals new { DestCityID = City2.ID }
                             where
                               (Trips.Closed == null ||
                               Trips.Closed == false) &&
                               City1.ID == idcity1
                                  select new PathTrip
                             {
                                 PathTitle = (City1.Title + " -> " + City2.Title),
                                 IDCity1 = City1.ID,
                                 IDCity2 = City2.ID
                             }).Distinct().SingleOrDefault();


                return query;
            }
        }

        public PathTrip FindByIDCitySecond(int idcity2)
        {
         using (testNetEntities db = new testNetEntities(""))
            {

                PathTrip query = (from Trips in db.Trips
                             join Services in db.Services on new { ID = Trips.ServiceID } equals new { ID = Services.ID }
                             join Paths in db.Paths on new { PathID = Services.PathID } equals new { PathID = Paths.ID }
                             join City1 in db.Cities on new { SrcCityID = Paths.SrcCityID } equals new { SrcCityID = City1.ID }
                             join City2 in db.Cities on new { DestCityID = Paths.DestCityID } equals new { DestCityID = City2.ID }
                             where
                               (Trips.Closed == null ||
                               Trips.Closed == false) &&
                               City2.ID == idcity2
                             select new PathTrip
                             {
                                 PathTitle = (City1.Title + " -> " + City2.Title),
                                 IDCity1 = City1.ID,
                                 IDCity2 = City2.ID
                             }).Distinct().SingleOrDefault();


                return query;
            }
        }

        public PathTrip FindByIDPath(string path)
        {          
            
            using (testNetEntities db = new testNetEntities(""))
            {

                PathTrip query = (from Trips in db.Trips
                             join Services in db.Services on new { ID = Trips.ServiceID } equals new { ID = Services.ID }
                             join Paths in db.Paths on new { PathID = Services.PathID } equals new { PathID = Paths.ID }
                             join City1 in db.Cities on new { SrcCityID = Paths.SrcCityID } equals new { SrcCityID = City1.ID }
                             join City2 in db.Cities on new { DestCityID = Paths.DestCityID } equals new { DestCityID = City2.ID }
                             where
                               (Trips.Closed == null ||
                               Trips.Closed == false) &&
                               (City1.Title + " -> " + City2.Title) == path
                             select new PathTrip
                             {
                                 PathTitle = (City1.Title + " -> " + City2.Title),
                                 IDCity1 = City1.ID,
                                 IDCity2 = City2.ID,
                                 IDCities=(City1.ID + " -> " + City2.ID)
                             }).Distinct().SingleOrDefault();

                return query;
               
            }



        }



    }
}
