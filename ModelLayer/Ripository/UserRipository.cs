﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace ModelLayer
{
 public   class UserRipository:IUser
    {
        public List<User> GetAll()
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                return db.Users.ToList();
            }
        }

        public User FindByID(int id)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                User _User = db.Users.Where(c => c.ID == id).SingleOrDefault();
                return _User;
            }
        }

        public int Insert(User user)
        {
            using (UserDBEntities db = new UserDBEntities())
            {

                db.Users.Add(user);
                return db.SaveChanges();
            }
        }

        public int Update(User user)
        {
            using (UserDBEntities db = new UserDBEntities())
            {

                db.Users.Attach(user);
                db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                return db.SaveChanges();
            }
        }

        public int Delete(User user)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                db.Users.Attach(user);
                db.Users.Remove(user);
                return db.SaveChanges();
            }
        }

        public int RemoveById(int id)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                User _User = db.Users.Where(c => c.ID == id).SingleOrDefault();
                db.Users.Attach(_User);
                db.Users.Remove(_User);
                return db.SaveChanges();
            }
        }



        public User FindByUser(string Uname, string Upass)
        {
            using (UserDBEntities db = new UserDBEntities())
            {   
                User _User = db.Users.Where(c => c.UserName == Uname && c.Password == Upass && c.Enabled == true).SingleOrDefault();
                return _User;
            }
        }

        public bool LoginTime(TimeSpan _now,int _ID)
        {
            try
            {
                bool result = false;
                    using (UserDBEntities db = new UserDBEntities())
                    {

                    TimeSpan HalfNightMor = TimeSpan.Parse("00:00");
                    TimeSpan HalfNight = TimeSpan.Parse("23:59");

                    if (_now >= TimeSpan.Parse("00:00") && _now <= TimeSpan.Parse("06:00:00"))
                    {
                                                 

                        User _User = db.Users.Where(u => u.Starttime.Value >= HalfNightMor && u.Endtime >= _now && u.ID == _ID).SingleOrDefault();
                        if (_User == null)
                         result = false;              
                        else
                            result = true;
                    }
                    else
                    {
                        if (_now >= TimeSpan.Parse("00:00") && _now <= TimeSpan.Parse("23:59:59"))
                        {
                            User _User = db.Users.Where(u => u.Starttime.Value <= _now && u.Endtime >= _now && u.ID == _ID).SingleOrDefault();
                            if (_User == null)
                            {
                                 _User = db.Users.Where(u => u.Starttime.Value <= _now && u.Endtime <= _now && u.ID == _ID).SingleOrDefault();
                                if (_User.Endtime<= TimeSpan.Parse("06:00:00") && _User.Endtime >= TimeSpan.Parse("00:00"))
                                {
                                    result = true;
                                }
                                else
                                {
                                    result = false;
                                }
                            }

                            else
                                result = true;
                        }

                    }

                }
                return result;
            }
       
            catch (Exception ex)
            {
                if (ex.Message.Contains("The underlying provider failed on Open."))
                {
            GetError.WriteError(ex);
                return true;
                }
                else
                {
                    return false;
                }
               
            }


}

        public User FindByCode(string _Code,string _uname)
        {
            using (UserDBEntities db = new UserDBEntities())
            {
                User userResult;
                List<User> _User = db.Users.Where(c => c.Code == _Code || c.UserName == _uname).ToList();
                if (_User.Count > 0)
                {
                    userResult = _User[0];
                    return userResult;
                }
                return null;
            }

        }
    }
}
