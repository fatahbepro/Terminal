﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;


namespace ModelLayer
{
    public interface ITrip
    {
        Trip FindByID(int id, string CompanyConection);
        List<GetTripsReport> GetTripsDateEF(DateTime fromdt, DateTime Todate, out List<PathTrip> listPath, List<Company> AccessCompanyUser,
                out List<Sp_GetCitiesTrip> listCities, int ServiceOvertime, bool IsLoacConnection);
        List<GetTripsResultReport> GetTripSummery(DateTime _fromdate, DateTime _todate, out List<PathTrip> listPath, List<Company> AccessCompanyUser, bool IsLoacConnection);
        List<GetTripsResultReport> GetTripSummerySaleTicket(DateTime _fromdate, DateTime _todate, out List<PathTrip> listPath, List<Company> AccessCompanyUser, bool IsLoacConnection);
        int GetNumberRezervTickets(string ConnectionCompany, int TripID, out int FullChair, int ServiceOvertime);


    }
}
