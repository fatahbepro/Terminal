﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;

namespace ModelLayer
{
    public interface ICity
    {

        City FindByID(int id, string _Connection);
        City FindByCode(string code, string _Connection);
        List<CitiesServices> FindByIDService(int idService, string Connection);

    }
}
