﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace ModelLayer
{
   public interface IPathTrip
    {
        List<PathTrip> GetAll();
        PathTrip FindByIDCityFirsh(int idcity1);
        PathTrip FindByIDCitySecond(int idcity2);
        PathTrip FindByIDPath(string path);

    }
}
