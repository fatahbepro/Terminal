﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;
namespace ModelLayer
{
    public interface IUser
    {
        List<User> GetAll();
        User FindByID(int id);
        int Insert(User user);
        int Update(User user);
        int Delete(User user);
        int RemoveById(int id);
        User FindByCode(string Code, string _Uname);
        User FindByUser(string Uname, string Upass);
        Boolean LoginTime(TimeSpan _now, int _ID);

    }
}
