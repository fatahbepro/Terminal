﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ModelLayer
{
  public  interface IPermission
    {
        List<Permission> GetAll();
        Permission FindByName(string name,string UserCode);
        Permission FindByID(int ID);
        int Insert(Permission permission);
        int Update(Permission permission);
        int Delete(Permission permission);
        int RemoveByName(string name, string UserCode);
    }
}
