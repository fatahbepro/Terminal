﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;

namespace ModelLayer
{
    public interface ITicket
    {

        List<Ticket> GetAll(string connectionCompany);

        Ticket FindByID(int id, string connectionCompany);

        Ticket FindByIDNet(int id, string ConnectionCompany);

        Ticket FindByIDNetSaly(int id, string ConnectionCompany);

        Ticket FindTicketByTripInfo(string _No, string CodeCompany, string connectionCompany);

        List<TicketLocal> GetListByTripID(int idtrip, string ConnectionCompany, string CodeCompany);

        List<TicketLocal> GetListTicketDate(DateTime fromdt, DateTime todate, List<Company> listcompany, bool localConnection, int status = 0);


        List<Ticket> FindByIDTrip(int idtrip, string ConnectionCompany);

        object GetTripbyID(int TripID, string ConnectionCompany);

        /// <summary>
        /// مشاهده بلیطهای صندوقدار
        /// </summary>
        /// <param name="fromdt"></param>
        /// <param name="todate"></param>
        /// <param name="listcompany"></param>
        /// <returns></returns>
        List<TicketLocal> GetTicketByDate(DateTime fromdt, DateTime todate, bool ISAdmin, string CashierCode, List<Company> Companis, bool localConnection);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="fromdt"></param>
        /// <param name="todate"></param>
        /// <param name="Companis"></param>
        /// <param name="localConnection"></param>
        /// <returns></returns>
        List<TicketLocal> GetTicketSales(DateTime fromdt, DateTime todate, List<Company> Companis, bool localConnection);





        List<TicketLocal> GetTicketRollBakByDate(DateTime fromdt, DateTime todate, bool ISAdmin, string CashierCode, List<Company> Companis, bool localConnection);


        /// <summary>
        /// مشاهده بلیطهای اینترنتی صندوقدار
        /// </summary>
        /// <param name="fromdt"></param>
        /// <param name="todate"></param>
        /// <param name="ISAdmin"></param>
        /// <param name="listcompany"></param>
        /// <returns></returns>
        List<TicketLocal> GetTicketNetByDate(DateTime fromdt, DateTime todate, List<Company> listcompany, bool localConnection);

        /// <summary>
        /// ثبت بلیط در سیستم صندوقدار و تعاونی
        /// </summary>
        /// <param name="ticket">بلیط</param>
        /// <param name="connectionCompany">کانکش استرینگ دیتابیس</param>
        /// <param name="id2">کد برگشتی ثبت</param>
        /// <returns></returns>
        int Insert(TicketLocal ticket, string connectionCompany);

        int UpSertTickets(Ticket ticket, string connectionCompany);

        int Update(Ticket ticketnet, string ConnectionCompany);
        int UpdateTicketCompany(Ticket _ticket, string ConnectionCompany);

        int UpdateticketNet(Ticket ticketnet, string ConnectionCompany);

        //int InsertTicketLocal(Ticket ticket,int FundPrice, string connectionCompany);

        Boolean SearchTicketByNo(string NO, string connectionCompany);
        int Delete(Ticket ticket, string connectionCompany);
        int RemoveByID(int id, string connectionCompany);

        int DeleteTickets(int idticketCompany, string ConnectionCompany);

        List<Ticket> GetTicketPaylockRollBack(string ConnectionCompany, string _cashierCode);
        int SuccessPayLockPos(int IDTicketCompany, string ConnectionCompany);
        Ticket FindTicketPaylockRollBack(Ticket _ticket, string connectionCompany);
    }
}
