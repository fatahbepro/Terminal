﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;
namespace ModelLayer
{
   public  interface ILayout
    {

        List<Layout> GetAll();
        Layout FindByID(int id, string _connectionCompany);
        int Insert(Layout _layout);
        int Update(Layout _layout);
        int Delete(Layout _layout);
        int RemoveByID(int ID);


    }
}
