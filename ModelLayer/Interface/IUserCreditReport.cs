﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace ModelLayer
{
  public  interface IUserCreditReport
    {
        List<UserCreditReport> GetAll();
        int Insert(UserCreditReport user);
        //int Update(UserCreditReport user);
        //int Delete(UserCreditReport user);
        List<ReportCreditUser> ReportUser(DateTime FromdDate, DateTime Todate,string UserCode );

    }
}
