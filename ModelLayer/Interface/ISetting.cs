﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;

namespace ModelLayer
{
  public   interface ISetting
    {
      List<Setting> GetAll();
      Setting FindByName(string name);
      int Insert(Setting setting);
      int Update(Setting setting);
      int Delete(Setting setting);
      int RemoveByName(string name);
    }
}
