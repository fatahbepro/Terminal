﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Model;


namespace ModelLayer
{
    public interface ICompany
    {
        /// <summary>
        /// دریافت لیست کلیه شرکت ها
        /// </summary>
        /// <returns>لیست شرکتها</returns>
        List<Company> GetAll(bool Getimg);

        /// <summary>
        /// دریافت اطلاعات یک شرکت با کد اصلی
        /// </summary>
        /// <param name="id">کد اصلی شرکت</param>
        /// <returns>اطلاعات شرکت مورد نظر</returns>
        Company FindByID(int id);

        /// <summary>
        /// دریافت اطلاعات یک شرکت با کد کاری شرکت
        /// 
        /// </summary>
        /// <param name="Code">کد شرکت</param>
        /// <returns>اطلاعات شرکت مورد نظر</returns>
        Company FindByCode(string Code);
        Company FindByCodeEnable(string Code, bool Enable);

        /// <summary>
        /// دریافت لیست تعاونی های مورد نظر از کد آنها
        /// </summary>
        /// <param name="Codes">لیستی از کدهای تعاونی ها</param>
        /// <returns></returns>
        List<Company> GetCompaniesByListCode(string Codes);
        /// <summary>
        /// ثبت شرکت
        /// </summary>
        /// <param name="company">اطلاعات شرکت مورد نظر</param>
        /// <returns>کنترل وضعیت عملیات </returns>
        int Insert(Company company);

        /// <summary>
        /// ویرایش اطلاعات شرکت
        /// </summary>
        /// <param name="company">اطلاعات شرکت</param>
        /// <returns>کنترل وضعیت عملیات</returns>
        int Update(Company company);

        /// <summary>
        /// حذف شرکت
        /// </summary>
        /// <param name="company">اطلاعات شرکت</param>
        /// <returns>کنترل وضعیت عملیات</returns>
        int Delete(Company company);

        /// <summary>
        /// حذف شرکت با کد اصلی
        /// </summary>
        /// <param name="id">کد اصلی شرکت</param>
        /// <returns>کنترل وضعیت عملیات</returns>
        int RemoveByID(int id);

        /// <summary>
        /// حذف شرکت با کد
        /// </summary>
        /// <param name="Code">کد شرکت</param>
        /// <returns>کنترل وضعیت عملیات</returns>
        int RemoveByCode(string Code);


    }
}
