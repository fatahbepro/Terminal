﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BL;
using Model;

namespace Test
{
    public partial class UseridTicket : UserControl
    {


        private bool isonline;

        public bool IsOnline
        {
            get { return isonline; }
            set { isonline = value;

                if (value)
                {
                    lblCaption.Text = "استرداد بلیط اینترنتی";
                    lblCaption.BackColor = Color.DodgerBlue;
                    lblCaption.ForeColor = Color.White;
                    txtIdTicket.Enabled = value;
                }


            }
        }

        /// <summary>
        /// زمان فروش سرویس جهت برگشت
        /// </summary>
        public DateTime Saletime { get; set; }
        private int percent;

        public int TotalTicket { get; set; }

        int PricePercent = 0;

        public UseridTicket()
        {
            InitializeComponent();
        }

        private void UseridTicket_Load(object sender, EventArgs e)
        {
            /// دریافت فرمول زمان
            string PercentDiscount = SettingLogin.GetValue("DefaultPaidbackPercent").ToString();
            string[] val = PercentDiscount.Split(':');
            double time =double.Parse (val[0]); // دقیقه
            double p1 =double.Parse( val[1]);// درصد اول
            double p2 =double.Parse( val[2]);// درصد دوم

            if(DateTime.Now.AddMinutes(time) <= Saletime)
                txtPercent.EditValue = p1;
            else
                txtPercent.EditValue =p2;

            PricePercent = (percent * TotalTicket) / 100;
            txtPricePaidBack.EditValue = RoundPrice(TotalTicket - PricePercent);


            // سطح دسترسی کاربر جهت اعمال درصد تخفیف
            if (!Userlogin.isAdmin)
            {
                Permission _permision = PermissionServices.FindByName("DefaultPaidbackPercent", Userlogin.Code);
                if (_permision != null)
                    txtPercent.Enabled = _permision.Value.Value;
                else
                    txtPercent.Enabled = false;

            }

        }


        private void txtPercent_EditValueChanged(object sender, EventArgs e)
        {
            if (txtPercent.EditValue != null)
            {
                if (txtPercent.EditValue.ToString() == "0")
                {
                    txtPricePaidBack.EditValue = TotalTicket;
                    return;
                }
              
                percent = Convert.ToInt32(txtPercent.EditValue);
                PricePercent = (percent * TotalTicket) / 100;
                txtPricePaidBack.EditValue = RoundPrice(TotalTicket - PricePercent);

                int sumprice = TotalTicket - PricePercent;
                var values = RoundPrice(sumprice);

            }

        }


        int RoundPrice(int Price)
        {
            int PriceSummery = Price;
            int Mode = Price % 10000;
            if (Mode!=0)
            {
                PriceSummery = (10000 - Mode) + Price;
            }

            return PriceSummery;
        }

    }
}
