﻿namespace Test
{
    partial class UseridTicket
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.lblCaption = new DevExpress.XtraEditors.TextEdit();
            this.txtPricePaidBack = new DevExpress.XtraEditors.TextEdit();
            this.txtPercent = new DevExpress.XtraEditors.TextEdit();
            this.txtIdTicket = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lblCaption.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPricePaidBack.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPercent.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdTicket.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.lblCaption);
            this.dataLayoutControl1.Controls.Add(this.txtPricePaidBack);
            this.dataLayoutControl1.Controls.Add(this.txtPercent);
            this.dataLayoutControl1.Controls.Add(this.txtIdTicket);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 0);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(360, 149);
            this.dataLayoutControl1.TabIndex = 0;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // lblCaption
            // 
            this.lblCaption.EditValue = "استرداد بلیط";
            this.lblCaption.Enabled = false;
            this.lblCaption.Location = new System.Drawing.Point(12, 12);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Properties.Appearance.BackColor = System.Drawing.Color.NavajoWhite;
            this.lblCaption.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.lblCaption.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.lblCaption.Properties.Appearance.Options.UseBackColor = true;
            this.lblCaption.Properties.Appearance.Options.UseFont = true;
            this.lblCaption.Properties.Appearance.Options.UseForeColor = true;
            this.lblCaption.Properties.Appearance.Options.UseTextOptions = true;
            this.lblCaption.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblCaption.Properties.AppearanceReadOnly.Options.UseTextOptions = true;
            this.lblCaption.Properties.AppearanceReadOnly.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblCaption.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.lblCaption.Properties.NullValuePrompt = "استرداد بلیط ها";
            this.lblCaption.Properties.ReadOnly = true;
            this.lblCaption.Size = new System.Drawing.Size(336, 24);
            this.lblCaption.StyleController = this.dataLayoutControl1;
            this.lblCaption.TabIndex = 8;
            // 
            // txtPricePaidBack
            // 
            this.txtPricePaidBack.Location = new System.Drawing.Point(12, 104);
            this.txtPricePaidBack.Name = "txtPricePaidBack";
            this.txtPricePaidBack.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.txtPricePaidBack.Properties.Appearance.Options.UseFont = true;
            this.txtPricePaidBack.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPricePaidBack.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPricePaidBack.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.txtPricePaidBack.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtPricePaidBack.Properties.DisplayFormat.FormatString = "c0";
            this.txtPricePaidBack.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPricePaidBack.Properties.EditFormat.FormatString = "c0";
            this.txtPricePaidBack.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPricePaidBack.Properties.Mask.EditMask = "c0";
            this.txtPricePaidBack.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPricePaidBack.Properties.ReadOnly = true;
            this.txtPricePaidBack.Size = new System.Drawing.Size(254, 28);
            this.txtPricePaidBack.StyleController = this.dataLayoutControl1;
            this.txtPricePaidBack.TabIndex = 6;
            // 
            // txtPercent
            // 
            this.txtPercent.Location = new System.Drawing.Point(12, 72);
            this.txtPercent.Name = "txtPercent";
            this.txtPercent.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.txtPercent.Properties.Appearance.Options.UseFont = true;
            this.txtPercent.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPercent.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPercent.Properties.Mask.EditMask = "P";
            this.txtPercent.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPercent.Size = new System.Drawing.Size(254, 28);
            this.txtPercent.StyleController = this.dataLayoutControl1;
            this.txtPercent.TabIndex = 5;
            this.txtPercent.EditValueChanged += new System.EventHandler(this.txtPercent_EditValueChanged);
            // 
            // txtIdTicket
            // 
            this.txtIdTicket.EditValue = "";
            this.txtIdTicket.Enabled = false;
            this.txtIdTicket.Location = new System.Drawing.Point(12, 40);
            this.txtIdTicket.Name = "txtIdTicket";
            this.txtIdTicket.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.txtIdTicket.Properties.Appearance.Options.UseFont = true;
            this.txtIdTicket.Properties.Appearance.Options.UseTextOptions = true;
            this.txtIdTicket.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtIdTicket.Properties.NullText = "0000-00";
            this.txtIdTicket.Properties.NullValuePrompt = "0000-0";
            this.txtIdTicket.Size = new System.Drawing.Size(254, 28);
            this.txtIdTicket.StyleController = this.dataLayoutControl1;
            this.txtIdTicket.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(360, 149);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem1.Control = this.txtIdTicket;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(340, 32);
            this.layoutControlItem1.Text = "شماره بلیط :";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(79, 24);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem2.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem2.Control = this.txtPercent;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 60);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(340, 32);
            this.layoutControlItem2.Text = "درصد کسری :";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(79, 24);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.txtPricePaidBack;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 92);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(340, 37);
            this.layoutControlItem3.Text = " مبلغ برگشتی :";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(79, 24);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.lblCaption;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(340, 28);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // UseridTicket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.dataLayoutControl1);
            this.Name = "UseridTicket";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Size = new System.Drawing.Size(360, 149);
            this.Load += new System.EventHandler(this.UseridTicket_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lblCaption.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPricePaidBack.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPercent.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdTicket.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        public DevExpress.XtraEditors.TextEdit txtPercent;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        public DevExpress.XtraEditors.TextEdit txtPricePaidBack;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        public DevExpress.XtraEditors.TextEdit lblCaption;
        public DevExpress.XtraEditors.TextEdit txtIdTicket;
    }
}
