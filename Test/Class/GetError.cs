﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Tulpep.NotificationWindow;
using System.Data;
using DevExpress.XtraBars.Alerter;
using DevExpress.XtraEditors;
using System.Windows.Forms;

public static class GetError
{
    static DevExpress.XtraBars.Alerter.AlertControl alertControl;
    static Exception _ex;
    static GetError()
    {
        #region Alert
        alertControl = new DevExpress.XtraBars.Alerter.AlertControl();
        alertControl.AppearanceCaption.Font = new System.Drawing.Font("B Nazanin", 11F, System.Drawing.FontStyle.Bold);
        alertControl.AppearanceCaption.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
        alertControl.AppearanceCaption.Options.UseFont = true;
        alertControl.AppearanceCaption.Options.UseForeColor = true;
        alertControl.AppearanceCaption.Options.UseTextOptions = true;
        alertControl.AppearanceCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        alertControl.AppearanceHotTrackedText.Options.UseTextOptions = true;
        alertControl.AppearanceHotTrackedText.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        alertControl.AppearanceText.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
        alertControl.AppearanceText.ForeColor = System.Drawing.Color.Black;
        alertControl.AppearanceText.Options.UseFont = true;
        alertControl.AppearanceText.Options.UseForeColor = true;
        alertControl.AppearanceText.Options.UseTextOptions = true;
        alertControl.AppearanceText.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
        alertControl.AutoFormDelay = 4000;
        alertControl.FormDisplaySpeed = DevExpress.XtraBars.Alerter.AlertFormDisplaySpeed.Fast;
        alertControl.ShowPinButton = false;
        alertControl.AlertClick += new DevExpress.XtraBars.Alerter.AlertClickEventHandler(alertControl_AlertClick);
        #endregion
    }
    public static void WriteError(Exception ex,Form frm=null)
    {
        try
        {
            _ex = ex;
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
            //   NameSpace.Form
            var NameSpace_Form = trace.GetFrame(0).GetMethod().ReflectedType.FullName;
            // Form Name
            var FormName = trace.GetFrame(0).GetMethod().ReflectedType.Name;
            var LineNumber = trace.GetFrame(0).GetFileLineNumber();
            var Method = trace.GetFrame(0).GetMethod().ToString();
            String Message = ex.Message +"------------------- \n "+ex.InnerException.Message;

            StreamWriter sw = new StreamWriter(".\\Error.txt", true, Encoding.UTF8);
            //sw.WriteLineAsync($"{FormName} : {Method } : LineNumber = {LineNumber} -> Error ::: " + ex.Message + " >>>" + DateTime.Now.ToString());
            string errorEF = "An error occurred while executing the command definition. See the inner exception for details";
            if (ex.Message.Contains(errorEF))
            {
                Message = ex.InnerException.Message.ToString();
            }

            sw.WriteLine("{0} : {1} : LineNumber = {2} -> Error ::: {3} >>> {4}", FormName, Method, LineNumber, Message, DateTime.Now.ToString());
            sw.Close();
            if (ex.Message.Contains("The underlying provider failed on Open.") || ex.InnerException.Message.Contains("A network-related or instance-specific error occurred while establishing a connection to SQL Server."))

                ShowError(frm,"اشکال در ارتباط با سرور   ", true);
            else

                ShowError(frm,"اشکال در انجام عملیات", false);

        }
        catch (Exception)
        {


        }
    }

    public static void ShowError(Form frm,string Error, bool state)
    {
        if (state)
            alertControl.AppearanceText.ForeColor = System.Drawing.Color.Red;
        else
            alertControl.AppearanceText.ForeColor = System.Drawing.Color.Black;

        alertControl.Show(frm, "خطا در عملیات", Error);

        //PopupNotifier pup = new PopupNotifier();
        ////pup.Image = Properties.Resources.male1;
        //pup.TitleText = "........خطا ";
        //pup.ContentText = Error;
        //if (state)
        //{
        //    pup.TitleColor = System.Drawing.Color.Red;
        //    pup.BodyColor = System.Drawing.Color.Yellow;
        //    pup.BorderColor = System.Drawing.Color.Red;
        //}
        //pup.Popup();
    }
    private static void alertControl_AlertClick(object sender, AlertClickEventArgs e)
    {
        if (_ex.InnerException == null)
            XtraMessageBox.Show(_ex.InnerException.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
        else
            XtraMessageBox.Show(_ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
    }

    public static DataTable ToDataTable<T>(List<T> items)
    {
        DataTable dataTable = new DataTable(typeof(T).Name);
        //Get all the properties by using reflection   
        PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
        foreach (PropertyInfo prop in Props)
        {
            //Setting column names as Property names  
            dataTable.Columns.Add(prop.Name);
        }
        foreach (T item in items)
        {
            var values = new object[Props.Length];
            for (int i = 0; i < Props.Length; i++)
            {

                values[i] = Props[i].GetValue(item, null);
            }
            dataTable.Rows.Add(values);
        }

        return dataTable;
    }


}





