﻿using DevExpress.XtraSplashScreen;
using System;
using System.Globalization;
using System.Windows.Forms;

public static class Utilitise
{
    public static string JulianToPersianDate(this DateTime date)
    {
        PersianCalendar pc = new PersianCalendar();
        var year = pc.GetYear(date).ToString();
        var month = pc.GetMonth(date).ToString().PadLeft(2, '0');
        var day = pc.GetDayOfMonth(date).ToString().PadLeft(2, '0');
        return string.Format($"{year}-{month}-{day}");
    }
    public static string JulianToPersianDateTime(this DateTime date)
    {
        PersianCalendar pc = new PersianCalendar();
        var year = pc.GetYear(date).ToString();
        var month = pc.GetMonth(date).ToString().PadLeft(2, '0');
        var day = pc.GetDayOfMonth(date).ToString().PadLeft(2, '0');
        return string.Format($"{year}-{month}-{day}  {date.ToString("HH:mm")}");
    }
    public static string GetNameOfDayDate(this DateTime ti)
    {
        DayOfWeek nameday = ti.DayOfWeek;
        string Text = "";
        switch (nameday)
        {
            case DayOfWeek.Saturday:
                Text = "شنبه";
                break;
            case DayOfWeek.Sunday:

                Text = "یک شنبه";
                break;
            case DayOfWeek.Monday:
                Text = "دوشنبه";

                break;
            case DayOfWeek.Tuesday:
                Text = "سه شنبه";
                break;

            case DayOfWeek.Wednesday:
                Text = "چهارشنبه";
                break;
            case DayOfWeek.Thursday:
                Text = "پنج شنبه";
                break;

            case DayOfWeek.Friday:
                Text = "جمعه";
                break;

        }

        return Text;
    }
    public static string ToEmptyString(this object obj)
    {
        if (obj == null) return string.Empty;
        if (string.IsNullOrEmpty(obj.ToString())) return string.Empty;
        else
            return obj.ToString();
    }
    public static int ToInt(this object obj)
    {
        try
        {
            if (obj == null) return 0;
            else
                return Convert.ToInt32(obj);
        }
        catch (Exception)
        {

            return 0;
        }
    }
    public static int? ToIntNullable(this object obj)
    {
        try
        {
            if (obj == null) return null;
            else
                return Convert.ToInt32(obj);
        }
        catch (Exception)
        {

            return 0;
        }
    }
    public static long ToLong(this object obj)
    {
        try
        {
            if (obj == null) return 0;
            else
                return Convert.ToInt64(obj);
        }
        catch (Exception)
        {

            return 0;
        }
    }
    public static long? ToLongNullable(this object obj)
    {
        try
        {
            if (obj == null) return null;
            else
                return Convert.ToInt64(obj);
        }
        catch (Exception)
        {

            return 0;
        }
    }
    public enum StateWating
    {
        Show, Close
    }
    static SplashScreenManager spl;
    /// <summary>
    /// نمایش لودینگ
    /// </summary>
    /// <param name="frm">فرم اصلی</param>
    /// <param name="stateWating">وضعیت نمایش</param>
    public static void ShowWating(Form frm, StateWating stateWating = StateWating.Show)
    {
        if (spl == null)
            spl = new SplashScreenManager(frm, typeof(global::Test.WaitForm1), true, true);
        if (stateWating == StateWating.Close)
        {
            if (spl.IsSplashFormVisible)
            {
                spl.CloseWaitForm();
            }
        }
        else
        {
            if (spl.IsSplashFormVisible)
            {
                spl.CloseWaitForm();
            }
            spl.ShowWaitForm();
        }
    }


}


public class ExtencionFunction
{
    public enum WaiteFromType
    {
        Show = 1,
        Close = 2
    }
    public static void WaiteForm(WaiteFromType Action, SplashScreenManager splash)
    {
        try
        {
            splash.Properties.AllowGlowEffect = true;
            switch (Action)
            {
                case WaiteFromType.Show:

                    if (splash.IsSplashFormVisible)
                    {
                        splash.CloseWaitForm();
                    }
                    splash.ShowWaitForm();
                    break;
                case WaiteFromType.Close:
                    if (splash.IsSplashFormVisible)
                    {
                        splash.CloseWaitForm();
                    }
                    break;
            }

        }
        catch (Exception)
        {
        }

    }
}