﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
   public static class CopyClass
    {
   
    public static T DeepCopy<T>(T obj)

    {

        if (!typeof(T).IsSerializable)

        {

            throw new Exception("The source object must be serializable");

        }

        if (Object.ReferenceEquals(obj, null))

        {

            throw new Exception("The source object must not be null");

        }

        T result = default(T);

        using (var memoryStream = new MemoryStream())

        {

            var formatter = new BinaryFormatter();

            formatter.Serialize(memoryStream, obj);

            memoryStream.Seek(0, SeekOrigin.Begin);

            result = (T)formatter.Deserialize(memoryStream);

            memoryStream.Close();

        }

        return result;

    }

       public static void Copy<T>(T copyFrom, T copyTo)
        {
            if (copyFrom == null || copyTo == null)
                throw new Exception("Must not specify null parameters");

            var properties = copyFrom.GetType().GetProperties();

            foreach (var p in properties.Where(prop => prop.CanRead && prop.CanWrite))
            {
                object copyValue = p.GetValue(copyFrom);
                p.SetValue(copyTo, copyValue);
            }
        }



        public static IList<T> Clone<T>(this IList<T> listToClone) where T : ICloneable
        {
            return listToClone.Select(item => (T)item.Clone()).ToList();
        }

    }
}
