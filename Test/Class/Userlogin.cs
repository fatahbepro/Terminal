﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Test
{
    public static class Userlogin
    {
        public static int ID { get; set; }
        public static string Code { get; set; }
        public static string UserName { get; set; }
        public static string Password { get; set; }
        public static string FullName { get; set; }
        public static string AccessCompanies { get; set; }
        public static bool Enabled { get; set; }
        public static int Credit { get; set; }
        public static bool isAdmin { get; set; }
        public static string PrinterUser { get; set; }
        public static string PoseName { get; set; }
        public static string IpPose { get; set; }

        public static Nullable<System.TimeSpan> Starttime { get; set; }
        public static Nullable<System.TimeSpan> Endtime { get; set; }
          public static Boolean IsLoacConnection { get; set; }
    }
}
