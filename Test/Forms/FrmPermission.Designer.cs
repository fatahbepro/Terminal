﻿namespace Test
{
    partial class FrmPermission
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmPermission));
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnSave = new DevExpress.XtraBars.BarButtonItem();
            this.btnEmpty = new DevExpress.XtraBars.BarButtonItem();
            this.btnDelete = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.permissionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSettingName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reSetting = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.settingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UserCodeLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SettingNameLookUpEdit = new DevExpress.XtraEditors.LookUpEdit();
            this.ValueToggleSwitch = new DevExpress.XtraEditors.ToggleSwitch();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForUserCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForSettingName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForValue = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxValidationProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.permissionBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reSetting)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserCodeLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SettingNameLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueToggleSwitch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSettingName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnClose,
            this.btnRefresh,
            this.btnSave,
            this.btnEmpty,
            this.btnDelete});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 6;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(819, 147);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // btnClose
            // 
            this.btnClose.Caption = "بستن";
            this.btnClose.Glyph = ((System.Drawing.Image)(resources.GetObject("btnClose.Glyph")));
            this.btnClose.Id = 1;
            this.btnClose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnClose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnClose.LargeGlyph")));
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "بروز رسانی";
            this.btnRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Glyph")));
            this.btnRefresh.Id = 2;
            this.btnRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btnRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.LargeGlyph")));
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnSave
            // 
            this.btnSave.Caption = "ذخیره";
            this.btnSave.Glyph = ((System.Drawing.Image)(resources.GetObject("btnSave.Glyph")));
            this.btnSave.Id = 3;
            this.btnSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnSave.LargeGlyph")));
            this.btnSave.Name = "btnSave";
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSave_ItemClick);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Caption = "خالی کردن";
            this.btnEmpty.Glyph = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Glyph")));
            this.btnEmpty.Id = 4;
            this.btnEmpty.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btnEmpty.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnEmpty.LargeGlyph")));
            this.btnEmpty.Name = "btnEmpty";
            this.btnEmpty.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEmpty_ItemClick);
            // 
            // btnDelete
            // 
            this.btnDelete.Caption = "حذف";
            this.btnDelete.Glyph = ((System.Drawing.Image)(resources.GetObject("btnDelete.Glyph")));
            this.btnDelete.Id = 5;
            this.btnDelete.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Delete);
            this.btnDelete.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnDelete.LargeGlyph")));
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnDelete_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup3,
            this.ribbonPageGroup2});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ذخیره اطلاعات";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnSave);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnEmpty);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "ذخیره";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnRefresh);
            this.ribbonPageGroup3.ItemLinks.Add(this.btnDelete);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "بروزرسانی داده ها";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnClose);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 597);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(819, 23);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.UserCodeLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.SettingNameLookUpEdit);
            this.dataLayoutControl1.Controls.Add(this.ValueToggleSwitch);
            this.dataLayoutControl1.DataSource = this.permissionBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 147);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(566, 257, 250, 350);
            this.dataLayoutControl1.OptionsFocus.MoveFocusRightToLeft = true;
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(819, 450);
            this.dataLayoutControl1.TabIndex = 2;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.permissionBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(12, 49);
            this.gridControl1.MainView = this.grd;
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.reSetting});
            this.gridControl1.Size = new System.Drawing.Size(795, 389);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grd});
            // 
            // permissionBindingSource
            // 
            this.permissionBindingSource.DataSource = typeof(Model.Permission);
            // 
            // grd
            // 
            this.grd.ColumnPanelRowHeight = 25;
            this.grd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colUserName,
            this.colSettingName,
            this.colValue});
            this.grd.GridControl = this.gridControl1;
            this.grd.Name = "grd";
            this.grd.OptionsBehavior.Editable = false;
            this.grd.OptionsView.ShowGroupPanel = false;
            this.grd.DoubleClick += new System.EventHandler(this.grd_DoubleClick);
            // 
            // colID
            // 
            this.colID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colID.AppearanceCell.Options.UseFont = true;
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colID.AppearanceHeader.Options.UseFont = true;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            // 
            // colUserName
            // 
            this.colUserName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colUserName.AppearanceCell.Options.UseFont = true;
            this.colUserName.AppearanceCell.Options.UseTextOptions = true;
            this.colUserName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colUserName.AppearanceHeader.Options.UseFont = true;
            this.colUserName.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserName.Caption = "کاربر";
            this.colUserName.FieldName = "UserName";
            this.colUserName.Name = "colUserName";
            this.colUserName.Visible = true;
            this.colUserName.VisibleIndex = 0;
            this.colUserName.Width = 240;
            // 
            // colSettingName
            // 
            this.colSettingName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSettingName.AppearanceCell.Options.UseFont = true;
            this.colSettingName.AppearanceCell.Options.UseTextOptions = true;
            this.colSettingName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSettingName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSettingName.AppearanceHeader.Options.UseFont = true;
            this.colSettingName.AppearanceHeader.Options.UseTextOptions = true;
            this.colSettingName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSettingName.Caption = "سطح دسترسی";
            this.colSettingName.ColumnEdit = this.reSetting;
            this.colSettingName.FieldName = "SettingName";
            this.colSettingName.Name = "colSettingName";
            this.colSettingName.Visible = true;
            this.colSettingName.VisibleIndex = 1;
            this.colSettingName.Width = 335;
            // 
            // reSetting
            // 
            this.reSetting.AutoHeight = false;
            this.reSetting.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.reSetting.DataSource = this.settingBindingSource;
            this.reSetting.DisplayMember = "Title";
            this.reSetting.Name = "reSetting";
            this.reSetting.NullText = "نامشخص";
            this.reSetting.ValueMember = "Name";
            // 
            // settingBindingSource
            // 
            this.settingBindingSource.DataSource = typeof(Model.Setting);
            // 
            // colValue
            // 
            this.colValue.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colValue.AppearanceCell.Options.UseFont = true;
            this.colValue.AppearanceCell.Options.UseTextOptions = true;
            this.colValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colValue.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colValue.AppearanceHeader.Options.UseFont = true;
            this.colValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colValue.Caption = "وضعیت دسترسی";
            this.colValue.FieldName = "Value";
            this.colValue.Name = "colValue";
            this.colValue.Visible = true;
            this.colValue.VisibleIndex = 2;
            this.colValue.Width = 147;
            // 
            // UserCodeLookUpEdit
            // 
            this.UserCodeLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.permissionBindingSource, "UserCode", true));
            this.UserCodeLookUpEdit.Location = new System.Drawing.Point(547, 12);
            this.UserCodeLookUpEdit.MenuManager = this.ribbon;
            this.UserCodeLookUpEdit.Name = "UserCodeLookUpEdit";
            this.UserCodeLookUpEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.UserCodeLookUpEdit.Properties.Appearance.Options.UseFont = true;
            this.UserCodeLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.UserCodeLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "کد", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FullName", "کاربر", 85, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center)});
            this.UserCodeLookUpEdit.Properties.DataSource = this.userBindingSource;
            this.UserCodeLookUpEdit.Properties.DisplayMember = "FullName";
            this.UserCodeLookUpEdit.Properties.NullText = "";
            this.UserCodeLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.UserCodeLookUpEdit.Properties.ValueMember = "Code";
            this.UserCodeLookUpEdit.Size = new System.Drawing.Size(196, 28);
            this.UserCodeLookUpEdit.StyleController = this.dataLayoutControl1;
            this.UserCodeLookUpEdit.TabIndex = 4;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "لطفا کاربر را مشخص نمایید";
            this.dxValidationProvider1.SetValidationRule(this.UserCodeLookUpEdit, conditionValidationRule1);
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(Model.User);
            // 
            // SettingNameLookUpEdit
            // 
            this.SettingNameLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.permissionBindingSource, "SettingName", true));
            this.SettingNameLookUpEdit.Location = new System.Drawing.Point(230, 12);
            this.SettingNameLookUpEdit.MenuManager = this.ribbon;
            this.SettingNameLookUpEdit.Name = "SettingNameLookUpEdit";
            this.SettingNameLookUpEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.SettingNameLookUpEdit.Properties.Appearance.Options.UseFont = true;
            this.SettingNameLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.SettingNameLookUpEdit.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Name", "نام", 80, DevExpress.Utils.FormatType.None, "", false, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Title", "عنوان", 80, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center)});
            this.SettingNameLookUpEdit.Properties.DataSource = this.settingBindingSource;
            this.SettingNameLookUpEdit.Properties.DisplayMember = "Title";
            this.SettingNameLookUpEdit.Properties.NullText = "";
            this.SettingNameLookUpEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.SettingNameLookUpEdit.Properties.ValueMember = "Name";
            this.SettingNameLookUpEdit.Size = new System.Drawing.Size(249, 28);
            this.SettingNameLookUpEdit.StyleController = this.dataLayoutControl1;
            this.SettingNameLookUpEdit.TabIndex = 5;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "لطفا تنظیمات مورد نظر را انتخاب نمایید";
            this.dxValidationProvider1.SetValidationRule(this.SettingNameLookUpEdit, conditionValidationRule2);
            // 
            // ValueToggleSwitch
            // 
            this.ValueToggleSwitch.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.permissionBindingSource, "Value", true));
            this.ValueToggleSwitch.Location = new System.Drawing.Point(12, 12);
            this.ValueToggleSwitch.MenuManager = this.ribbon;
            this.ValueToggleSwitch.Name = "ValueToggleSwitch";
            this.ValueToggleSwitch.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ValueToggleSwitch.Properties.Appearance.Options.UseFont = true;
            this.ValueToggleSwitch.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.ValueToggleSwitch.Properties.OffText = "ندارد";
            this.ValueToggleSwitch.Properties.OnText = "دارد";
            this.ValueToggleSwitch.Size = new System.Drawing.Size(129, 33);
            this.ValueToggleSwitch.StyleController = this.dataLayoutControl1;
            this.ValueToggleSwitch.TabIndex = 6;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "لطفا نوع دسترسی را انتخاب نمایید";
            this.dxValidationProvider1.SetValidationRule(this.ValueToggleSwitch, conditionValidationRule3);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(819, 450);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForUserCode,
            this.layoutControlItem1,
            this.ItemForSettingName,
            this.ItemForValue});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(799, 430);
            // 
            // ItemForUserCode
            // 
            this.ItemForUserCode.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForUserCode.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForUserCode.Control = this.UserCodeLookUpEdit;
            this.ItemForUserCode.Location = new System.Drawing.Point(535, 0);
            this.ItemForUserCode.Name = "ItemForUserCode";
            this.ItemForUserCode.Size = new System.Drawing.Size(264, 37);
            this.ItemForUserCode.Text = "    کاربران :";
            this.ItemForUserCode.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ItemForUserCode.TextSize = new System.Drawing.Size(59, 24);
            this.ItemForUserCode.TextToControlDistance = 5;
            // 
            // ItemForSettingName
            // 
            this.ItemForSettingName.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForSettingName.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForSettingName.Control = this.SettingNameLookUpEdit;
            this.ItemForSettingName.Location = new System.Drawing.Point(218, 0);
            this.ItemForSettingName.Name = "ItemForSettingName";
            this.ItemForSettingName.Size = new System.Drawing.Size(317, 37);
            this.ItemForSettingName.Text = " تنظیمات :";
            this.ItemForSettingName.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForSettingName.TextSize = new System.Drawing.Size(59, 24);
            this.ItemForSettingName.TextToControlDistance = 5;
            // 
            // ItemForValue
            // 
            this.ItemForValue.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForValue.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForValue.Control = this.ValueToggleSwitch;
            this.ItemForValue.Location = new System.Drawing.Point(0, 0);
            this.ItemForValue.Name = "ItemForValue";
            this.ItemForValue.Size = new System.Drawing.Size(218, 37);
            this.ItemForValue.Text = " نوع دسترسی :";
            this.ItemForValue.TextSize = new System.Drawing.Size(82, 24);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 37);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(799, 393);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // FrmPermission
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 620);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Name = "FrmPermission";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "دسترسی کاربران";
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.permissionBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reSetting)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.settingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserCodeLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SettingNameLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ValueToggleSwitch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSettingName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.BarButtonItem btnSave;
        private DevExpress.XtraBars.BarButtonItem btnEmpty;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraEditors.LookUpEdit UserCodeLookUpEdit;
        private System.Windows.Forms.BindingSource permissionBindingSource;
        private DevExpress.XtraEditors.LookUpEdit SettingNameLookUpEdit;
        private DevExpress.XtraEditors.ToggleSwitch ValueToggleSwitch;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSettingName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForValue;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView grd;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colSettingName;
        private DevExpress.XtraGrid.Columns.GridColumn colValue;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit reSetting;
        private System.Windows.Forms.BindingSource settingBindingSource;
        private System.Windows.Forms.BindingSource userBindingSource;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private DevExpress.XtraBars.BarButtonItem btnDelete;
    }
}