﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Model;
using BL;
using DevExpress.XtraEditors;

namespace Test
{
    public partial class FrmPermission : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public FrmPermission()
        {
            InitializeComponent();

            LoadData();
            _RefreshFrm();
        }

        bool EditMode;
        void _RefreshFrm()
        {
            permissionBindingSource.DataSource = new Permission();
            EditMode = false;
            ValueToggleSwitch.EditValue = false;
        }
        void LoadData()
        {
            try
            {

                List<Permission> li= PermissionServices.GetAll();

                gridControl1.DataSource = PermissionServices.GetAll().Select(c => new { ID = c.ID, UserCode = c.UserCode, Value = c.Value== true ? "دسترسی دارد":"دسترسی ندارد" , UserName = c.UserName, SettingName = c.SettingName }).ToList();
                
                reSetting.DataSource = SettingServices.GetAll();
            SettingNameLookUpEdit.Properties.DataSource = SettingServices.GetAll();
            UserCodeLookUpEdit.Properties.DataSource = UserServices.GetAll();

            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }

        }


        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void btnEmpty_ItemClick(object sender, ItemClickEventArgs e)
        {
            _RefreshFrm();
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
        }

        private void btnSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {

                if (!dxValidationProvider1.Validate())
                return;
                permissionBindingSource.EndEdit();
            Permission _permission = permissionBindingSource.Current as Permission;
                _permission.UserName = UserCodeLookUpEdit.Text;
            if (EditMode)
                PermissionServices.Update(_permission);
            else
                PermissionServices.Insert(_permission);

            LoadData();
            _RefreshFrm();
            XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);

           
            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
            }

        }

        private void grd_DoubleClick(object sender, EventArgs e)
        {
            _RefreshFrm();
            int id = Convert.ToInt32(grd.GetFocusedRowCellValue(colID));
            permissionBindingSource.DataSource = PermissionServices.FindByID(id);
                EditMode = true;

        }

        private void btnDelete_ItemClick(object sender, ItemClickEventArgs e)
        {

            try
            {
                if (XtraMessageBox.Show("آیا مایل به حذف دسترسی کاربر مورد نظر می باشید؟", "پیام", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {


                    int id = Convert.ToInt32(grd.GetFocusedRowCellValue(colID));
                    Permission _permission = PermissionServices.FindByID(id);
                    PermissionServices.Delete(_permission);
                    LoadData();
                    _RefreshFrm();
                    XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
            }

        }
    }
}