﻿namespace Test
{
    partial class FrmTicketsTrips
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTicketsTrips));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject9 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject11 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject12 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject13 = new DevExpress.Utils.SerializableAppearanceObject();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.Btn_Del = new DevExpress.XtraBars.BarButtonItem();
            this.BtnbackSale = new DevExpress.XtraBars.BarButtonItem();
            this.btnCancelPose = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barCreditUser = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.ticketLocalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTripID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSumDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaleDateSH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumChairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStateticket = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCityName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCashierName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReturnUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusPaidBack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReturnUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidBackTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTicket = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFundPriceValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMoveTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaleType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCodeCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnlinePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColPayback = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ReBtnPayback = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPrintFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reBtnPrintticket = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPayPose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reBtnPayPose = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPaycredit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reBtnPayCredit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPaidBackDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolTip = new DevExpress.Utils.ToolTipController(this.components);
            this.cityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.companyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.bgpose = new System.ComponentModel.BackgroundWorker();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Test.WaitForm1), true, true);
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.TripInfo = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketLocalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReBtnPayback)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPrintticket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPayPose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPayCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TripInfo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnReport,
            this.barButtonItem1,
            this.barButtonItem2,
            this.Btn_Del,
            this.BtnbackSale,
            this.btnCancelPose,
            this.btnRefresh,
            this.barCreditUser,
            this.barButtonItem4});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ribbon.MaxItemId = 10;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1372, 146);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // btnReport
            // 
            this.btnReport.Caption = "گزارش";
            this.btnReport.Glyph = ((System.Drawing.Image)(resources.GetObject("btnReport.Glyph")));
            this.btnReport.Id = 1;
            this.btnReport.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6);
            this.btnReport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnReport.LargeGlyph")));
            this.btnReport.Name = "btnReport";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "چاپ";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "بستن";
            this.barButtonItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.Glyph")));
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.LargeGlyph")));
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // Btn_Del
            // 
            this.Btn_Del.Caption = "کنسل بلیط";
            this.Btn_Del.Glyph = ((System.Drawing.Image)(resources.GetObject("Btn_Del.Glyph")));
            this.Btn_Del.Id = 4;
            this.Btn_Del.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("Btn_Del.LargeGlyph")));
            this.Btn_Del.Name = "Btn_Del";
            // 
            // BtnbackSale
            // 
            this.BtnbackSale.Caption = "استرداد";
            this.BtnbackSale.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnbackSale.Glyph")));
            this.BtnbackSale.Id = 5;
            this.BtnbackSale.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F9);
            this.BtnbackSale.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnbackSale.LargeGlyph")));
            this.BtnbackSale.Name = "BtnbackSale";
            toolTipTitleItem1.Text = "استرداد";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "بازگشت هزینه بلیط به موجودی حساب صندوقدار";
            toolTipTitleItem2.LeftIndent = 6;
            toolTipTitleItem2.Text = "F9";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipSeparatorItem1);
            superToolTip1.Items.Add(toolTipTitleItem2);
            this.BtnbackSale.SuperTip = superToolTip1;
            this.BtnbackSale.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnbackSale_ItemClick);
            // 
            // btnCancelPose
            // 
            this.btnCancelPose.Caption = "لغو ارسال";
            this.btnCancelPose.Enabled = false;
            this.btnCancelPose.Id = 6;
            this.btnCancelPose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F8);
            this.btnCancelPose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnCancelPose.LargeGlyph")));
            this.btnCancelPose.Name = "btnCancelPose";
            this.btnCancelPose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancelPose_ItemClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "بروز رسانی";
            this.btnRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Glyph")));
            this.btnRefresh.Id = 7;
            this.btnRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btnRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.LargeGlyph")));
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // barCreditUser
            // 
            this.barCreditUser.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barCreditUser.Caption = "اعتبار حساب کاربر";
            this.barCreditUser.Edit = this.repositoryItemTextEdit1;
            this.barCreditUser.EditWidth = 200;
            this.barCreditUser.Id = 8;
            this.barCreditUser.Name = "barCreditUser";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.repositoryItemTextEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "c0";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.EditFormat.FormatString = "c0";
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "c0";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.ReadOnly = true;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 9;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "عملیات";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.BtnbackSale);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnCancelPose);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "عملیات";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnRefresh);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "بروز رسانی داده ها";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barCreditUser);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 715);
            this.ribbonStatusBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1372, 21);
            // 
            // dgv
            // 
            this.dgv.DataSource = this.ticketLocalBindingSource;
            this.dgv.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.Location = new System.Drawing.Point(12, 40);
            this.dgv.MainView = this.grd;
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MenuManager = this.ribbon;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ReBtnPayback,
            this.reBtnPrintticket,
            this.reBtnPayPose,
            this.reBtnPayCredit});
            this.dgv.Size = new System.Drawing.Size(1348, 517);
            this.dgv.TabIndex = 31;
            this.dgv.ToolTipController = this.toolTip;
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grd});
            // 
            // ticketLocalBindingSource
            // 
            this.ticketLocalBindingSource.DataSource = typeof(Model.TicketLocal);
            // 
            // grd
            // 
            this.grd.Appearance.FocusedCell.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedCell.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grd.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grd.Appearance.FocusedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grd.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grd.Appearance.FooterPanel.Font = new System.Drawing.Font("Tornado Tahoma", 8.25F);
            this.grd.Appearance.FooterPanel.Options.UseFont = true;
            this.grd.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grd.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.GroupFooter.Font = new System.Drawing.Font("Tornado Tahoma", 8.25F);
            this.grd.Appearance.GroupFooter.Options.UseFont = true;
            this.grd.Appearance.GroupFooter.Options.UseTextOptions = true;
            this.grd.Appearance.GroupFooter.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.SelectedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grd.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grd.ColumnPanelRowHeight = 30;
            this.grd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colTripID,
            this.colMan,
            this.colFullname,
            this.colNo,
            this.colChairs,
            this.colSumDiscount,
            this.colSaleDateSH,
            this.colNumChairs,
            this.colStateticket,
            this.colCityName,
            this.colCashierName,
            this.colReturnUserID,
            this.colStatusPaidBack,
            this.colReturnUserName,
            this.colPaidBackTime,
            this.colTotalTicket,
            this.colTimeSale,
            this.colFundPriceValue,
            this.colMoveTime,
            this.colSaleType,
            this.colCodeCompany,
            this.colOnlinePrice,
            this.colPrice,
            this.col1,
            this.col2,
            this.ColPayback,
            this.colPrintFactor,
            this.colPayPose,
            this.colPaycredit,
            this.colPaidBackDate,
            this.colCityID});
            this.grd.GridControl = this.dgv;
            this.grd.Name = "grd";
            this.grd.OptionsFind.AlwaysVisible = true;
            this.grd.OptionsFind.FindNullPrompt = "جست و جو ..";
            this.grd.OptionsView.ShowFooter = true;
            this.grd.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grd_RowStyle);
            this.grd.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.grd_CustomRowCellEdit);
            this.grd.MouseMove += new System.Windows.Forms.MouseEventHandler(this.grd_MouseMove);
            // 
            // colID
            // 
            this.colID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colID.AppearanceCell.Options.UseFont = true;
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colID.AppearanceHeader.Options.UseFont = true;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.Caption = "#";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.Width = 50;
            // 
            // colTripID
            // 
            this.colTripID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTripID.AppearanceCell.Options.UseFont = true;
            this.colTripID.AppearanceCell.Options.UseTextOptions = true;
            this.colTripID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTripID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTripID.AppearanceHeader.Options.UseFont = true;
            this.colTripID.AppearanceHeader.Options.UseTextOptions = true;
            this.colTripID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTripID.Caption = "کد سرویس";
            this.colTripID.FieldName = "TripID";
            this.colTripID.Name = "colTripID";
            this.colTripID.OptionsColumn.ReadOnly = true;
            this.colTripID.Width = 94;
            // 
            // colMan
            // 
            this.colMan.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colMan.AppearanceCell.Options.UseFont = true;
            this.colMan.AppearanceCell.Options.UseTextOptions = true;
            this.colMan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMan.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colMan.AppearanceHeader.Options.UseFont = true;
            this.colMan.AppearanceHeader.Options.UseTextOptions = true;
            this.colMan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMan.Caption = "جنسیت";
            this.colMan.FieldName = "Man";
            this.colMan.Name = "colMan";
            this.colMan.OptionsColumn.ReadOnly = true;
            this.colMan.Width = 62;
            // 
            // colFullname
            // 
            this.colFullname.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colFullname.AppearanceCell.Options.UseFont = true;
            this.colFullname.AppearanceCell.Options.UseTextOptions = true;
            this.colFullname.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullname.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colFullname.AppearanceHeader.Options.UseFont = true;
            this.colFullname.AppearanceHeader.Options.UseTextOptions = true;
            this.colFullname.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullname.Caption = "نام مسافر";
            this.colFullname.FieldName = "Fullname";
            this.colFullname.Name = "colFullname";
            this.colFullname.OptionsColumn.ReadOnly = true;
            this.colFullname.Visible = true;
            this.colFullname.VisibleIndex = 0;
            this.colFullname.Width = 122;
            // 
            // colNo
            // 
            this.colNo.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colNo.AppearanceCell.Options.UseFont = true;
            this.colNo.AppearanceCell.Options.UseTextOptions = true;
            this.colNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colNo.AppearanceHeader.Options.UseFont = true;
            this.colNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.Caption = "شماره بلیط";
            this.colNo.FieldName = "No";
            this.colNo.Name = "colNo";
            this.colNo.OptionsColumn.ReadOnly = true;
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 1;
            this.colNo.Width = 90;
            // 
            // colChairs
            // 
            this.colChairs.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colChairs.AppearanceCell.Options.UseFont = true;
            this.colChairs.AppearanceCell.Options.UseTextOptions = true;
            this.colChairs.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colChairs.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colChairs.AppearanceHeader.Options.UseFont = true;
            this.colChairs.AppearanceHeader.Options.UseTextOptions = true;
            this.colChairs.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colChairs.Caption = "صندلی ها";
            this.colChairs.FieldName = "Chairs";
            this.colChairs.Name = "colChairs";
            this.colChairs.OptionsColumn.ReadOnly = true;
            this.colChairs.Visible = true;
            this.colChairs.VisibleIndex = 2;
            this.colChairs.Width = 91;
            // 
            // colSumDiscount
            // 
            this.colSumDiscount.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSumDiscount.AppearanceCell.Options.UseFont = true;
            this.colSumDiscount.AppearanceCell.Options.UseTextOptions = true;
            this.colSumDiscount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumDiscount.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSumDiscount.AppearanceHeader.Options.UseFont = true;
            this.colSumDiscount.AppearanceHeader.Options.UseTextOptions = true;
            this.colSumDiscount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumDiscount.Caption = "تخفیف";
            this.colSumDiscount.DisplayFormat.FormatString = "c0";
            this.colSumDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSumDiscount.FieldName = "SumDiscount";
            this.colSumDiscount.Name = "colSumDiscount";
            this.colSumDiscount.OptionsColumn.ReadOnly = true;
            this.colSumDiscount.Visible = true;
            this.colSumDiscount.VisibleIndex = 3;
            this.colSumDiscount.Width = 62;
            // 
            // colSaleDateSH
            // 
            this.colSaleDateSH.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSaleDateSH.AppearanceCell.Options.UseFont = true;
            this.colSaleDateSH.AppearanceCell.Options.UseTextOptions = true;
            this.colSaleDateSH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSaleDateSH.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSaleDateSH.AppearanceHeader.Options.UseFont = true;
            this.colSaleDateSH.AppearanceHeader.Options.UseTextOptions = true;
            this.colSaleDateSH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSaleDateSH.Caption = "ت فروش";
            this.colSaleDateSH.FieldName = "SaleDateSH";
            this.colSaleDateSH.Name = "colSaleDateSH";
            this.colSaleDateSH.OptionsColumn.ReadOnly = true;
            this.colSaleDateSH.Visible = true;
            this.colSaleDateSH.VisibleIndex = 5;
            this.colSaleDateSH.Width = 80;
            // 
            // colNumChairs
            // 
            this.colNumChairs.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colNumChairs.AppearanceCell.Options.UseFont = true;
            this.colNumChairs.AppearanceCell.Options.UseTextOptions = true;
            this.colNumChairs.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumChairs.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colNumChairs.AppearanceHeader.Options.UseFont = true;
            this.colNumChairs.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumChairs.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumChairs.Caption = "ت صندلی";
            this.colNumChairs.FieldName = "NumChairs";
            this.colNumChairs.Name = "colNumChairs";
            this.colNumChairs.OptionsColumn.ReadOnly = true;
            this.colNumChairs.Visible = true;
            this.colNumChairs.VisibleIndex = 7;
            this.colNumChairs.Width = 109;
            // 
            // colStateticket
            // 
            this.colStateticket.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colStateticket.AppearanceCell.Options.UseFont = true;
            this.colStateticket.AppearanceCell.Options.UseTextOptions = true;
            this.colStateticket.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateticket.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colStateticket.AppearanceHeader.Options.UseFont = true;
            this.colStateticket.AppearanceHeader.Options.UseTextOptions = true;
            this.colStateticket.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateticket.Caption = "وضعیت بلیط";
            this.colStateticket.FieldName = "Stateticket";
            this.colStateticket.Name = "colStateticket";
            this.colStateticket.OptionsColumn.ReadOnly = true;
            this.colStateticket.Visible = true;
            this.colStateticket.VisibleIndex = 8;
            this.colStateticket.Width = 68;
            // 
            // colCityName
            // 
            this.colCityName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCityName.AppearanceCell.Options.UseFont = true;
            this.colCityName.AppearanceCell.Options.UseTextOptions = true;
            this.colCityName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCityName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCityName.AppearanceHeader.Options.UseFont = true;
            this.colCityName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCityName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCityName.Caption = "شهر مقصد";
            this.colCityName.FieldName = "CityName";
            this.colCityName.Name = "colCityName";
            this.colCityName.OptionsColumn.ReadOnly = true;
            this.colCityName.Visible = true;
            this.colCityName.VisibleIndex = 9;
            this.colCityName.Width = 100;
            // 
            // colCashierName
            // 
            this.colCashierName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCashierName.AppearanceCell.Options.UseFont = true;
            this.colCashierName.AppearanceCell.Options.UseTextOptions = true;
            this.colCashierName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCashierName.AppearanceHeader.Options.UseFont = true;
            this.colCashierName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCashierName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierName.Caption = "کاربر صندوق";
            this.colCashierName.FieldName = "CashierName";
            this.colCashierName.Name = "colCashierName";
            this.colCashierName.OptionsColumn.ReadOnly = true;
            this.colCashierName.Visible = true;
            this.colCashierName.VisibleIndex = 10;
            this.colCashierName.Width = 83;
            // 
            // colReturnUserID
            // 
            this.colReturnUserID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colReturnUserID.AppearanceCell.Options.UseFont = true;
            this.colReturnUserID.AppearanceCell.Options.UseTextOptions = true;
            this.colReturnUserID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnUserID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colReturnUserID.AppearanceHeader.Options.UseFont = true;
            this.colReturnUserID.AppearanceHeader.Options.UseTextOptions = true;
            this.colReturnUserID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnUserID.Caption = "کد کاربر";
            this.colReturnUserID.FieldName = "ReturnUserID";
            this.colReturnUserID.Name = "colReturnUserID";
            this.colReturnUserID.OptionsColumn.ReadOnly = true;
            this.colReturnUserID.Width = 64;
            // 
            // colStatusPaidBack
            // 
            this.colStatusPaidBack.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colStatusPaidBack.AppearanceCell.Options.UseFont = true;
            this.colStatusPaidBack.AppearanceCell.Options.UseTextOptions = true;
            this.colStatusPaidBack.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStatusPaidBack.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colStatusPaidBack.AppearanceHeader.Options.UseFont = true;
            this.colStatusPaidBack.AppearanceHeader.Options.UseTextOptions = true;
            this.colStatusPaidBack.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStatusPaidBack.Caption = "وضعیت برگشت";
            this.colStatusPaidBack.FieldName = "StatusPaidBack";
            this.colStatusPaidBack.Name = "colStatusPaidBack";
            this.colStatusPaidBack.OptionsColumn.ReadOnly = true;
            this.colStatusPaidBack.Visible = true;
            this.colStatusPaidBack.VisibleIndex = 11;
            this.colStatusPaidBack.Width = 66;
            // 
            // colReturnUserName
            // 
            this.colReturnUserName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colReturnUserName.AppearanceCell.Options.UseFont = true;
            this.colReturnUserName.AppearanceCell.Options.UseTextOptions = true;
            this.colReturnUserName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnUserName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colReturnUserName.AppearanceHeader.Options.UseFont = true;
            this.colReturnUserName.AppearanceHeader.Options.UseTextOptions = true;
            this.colReturnUserName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnUserName.Caption = "کاربر برگشت دهنده";
            this.colReturnUserName.FieldName = "ReturnUserName";
            this.colReturnUserName.Name = "colReturnUserName";
            this.colReturnUserName.OptionsColumn.ReadOnly = true;
            this.colReturnUserName.Width = 61;
            // 
            // colPaidBackTime
            // 
            this.colPaidBackTime.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPaidBackTime.AppearanceCell.Options.UseFont = true;
            this.colPaidBackTime.AppearanceCell.Options.UseTextOptions = true;
            this.colPaidBackTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackTime.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPaidBackTime.AppearanceHeader.Options.UseFont = true;
            this.colPaidBackTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colPaidBackTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackTime.Caption = "س برگشت";
            this.colPaidBackTime.DisplayFormat.FormatString = "HH:mm:tt";
            this.colPaidBackTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPaidBackTime.FieldName = "PaidBackDate";
            this.colPaidBackTime.Name = "colPaidBackTime";
            this.colPaidBackTime.Width = 53;
            // 
            // colTotalTicket
            // 
            this.colTotalTicket.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTotalTicket.AppearanceCell.Options.UseFont = true;
            this.colTotalTicket.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalTicket.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalTicket.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTotalTicket.AppearanceHeader.Options.UseFont = true;
            this.colTotalTicket.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalTicket.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalTicket.Caption = "جمع کل";
            this.colTotalTicket.DisplayFormat.FormatString = "c0";
            this.colTotalTicket.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalTicket.FieldName = "TotalTicket";
            this.colTotalTicket.Name = "colTotalTicket";
            this.colTotalTicket.OptionsColumn.ReadOnly = true;
            this.colTotalTicket.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalTicket", "ج: {0:c0}")});
            this.colTotalTicket.Visible = true;
            this.colTotalTicket.VisibleIndex = 4;
            this.colTotalTicket.Width = 99;
            // 
            // colTimeSale
            // 
            this.colTimeSale.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTimeSale.AppearanceCell.Options.UseFont = true;
            this.colTimeSale.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeSale.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeSale.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTimeSale.AppearanceHeader.Options.UseFont = true;
            this.colTimeSale.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeSale.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeSale.Caption = "س فروش";
            this.colTimeSale.FieldName = "TimeSale";
            this.colTimeSale.Name = "colTimeSale";
            this.colTimeSale.OptionsColumn.ReadOnly = true;
            this.colTimeSale.Visible = true;
            this.colTimeSale.VisibleIndex = 6;
            this.colTimeSale.Width = 77;
            // 
            // colFundPriceValue
            // 
            this.colFundPriceValue.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colFundPriceValue.AppearanceCell.Options.UseFont = true;
            this.colFundPriceValue.AppearanceCell.Options.UseTextOptions = true;
            this.colFundPriceValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFundPriceValue.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colFundPriceValue.AppearanceHeader.Options.UseFont = true;
            this.colFundPriceValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colFundPriceValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFundPriceValue.Caption = "سهم صندوق";
            this.colFundPriceValue.DisplayFormat.FormatString = "c0";
            this.colFundPriceValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFundPriceValue.FieldName = "FundPriceValue";
            this.colFundPriceValue.Name = "colFundPriceValue";
            this.colFundPriceValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FundPrice", " ج {0:c0}")});
            this.colFundPriceValue.Width = 48;
            // 
            // colMoveTime
            // 
            this.colMoveTime.Caption = "ساعت حرکت";
            this.colMoveTime.FieldName = "MoveTime";
            this.colMoveTime.Name = "colMoveTime";
            // 
            // colSaleType
            // 
            this.colSaleType.FieldName = "SaleType";
            this.colSaleType.Name = "colSaleType";
            // 
            // colCodeCompany
            // 
            this.colCodeCompany.Caption = "CodeCompany";
            this.colCodeCompany.FieldName = "CompanyCode";
            this.colCodeCompany.Name = "colCodeCompany";
            // 
            // colOnlinePrice
            // 
            this.colOnlinePrice.FieldName = "OnlinePrice";
            this.colOnlinePrice.Name = "colOnlinePrice";
            // 
            // colPrice
            // 
            this.colPrice.FieldName = "Price";
            this.colPrice.Name = "colPrice";
            // 
            // col1
            // 
            this.col1.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.col1.AppearanceCell.Options.UseFont = true;
            this.col1.AppearanceCell.Options.UseTextOptions = true;
            this.col1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col1.MaxWidth = 90;
            this.col1.Name = "col1";
            this.col1.Visible = true;
            this.col1.VisibleIndex = 12;
            this.col1.Width = 52;
            // 
            // col2
            // 
            this.col2.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.col2.AppearanceCell.Options.UseFont = true;
            this.col2.AppearanceCell.Options.UseTextOptions = true;
            this.col2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col2.MaxWidth = 90;
            this.col2.Name = "col2";
            this.col2.Visible = true;
            this.col2.VisibleIndex = 13;
            this.col2.Width = 73;
            // 
            // ColPayback
            // 
            this.ColPayback.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.ColPayback.AppearanceCell.Options.UseFont = true;
            this.ColPayback.AppearanceCell.Options.UseTextOptions = true;
            this.ColPayback.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColPayback.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ColPayback.AppearanceHeader.Options.UseFont = true;
            this.ColPayback.AppearanceHeader.Options.UseTextOptions = true;
            this.ColPayback.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColPayback.ColumnEdit = this.ReBtnPayback;
            this.ColPayback.MaxWidth = 60;
            this.ColPayback.MinWidth = 50;
            this.ColPayback.Name = "ColPayback";
            this.ColPayback.Width = 60;
            // 
            // ReBtnPayback
            // 
            this.ReBtnPayback.AutoHeight = false;
            this.ReBtnPayback.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "استرداد", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F9), serializableAppearanceObject1, "", null, null, true)});
            this.ReBtnPayback.Name = "ReBtnPayback";
            this.ReBtnPayback.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.ReBtnPayback.Click += new System.EventHandler(this.ReBtnPayback_Click);
            // 
            // colPrintFactor
            // 
            this.colPrintFactor.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPrintFactor.AppearanceCell.Options.UseFont = true;
            this.colPrintFactor.AppearanceCell.Options.UseTextOptions = true;
            this.colPrintFactor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrintFactor.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPrintFactor.AppearanceHeader.Options.UseFont = true;
            this.colPrintFactor.AppearanceHeader.Options.UseTextOptions = true;
            this.colPrintFactor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrintFactor.ColumnEdit = this.reBtnPrintticket;
            this.colPrintFactor.MaxWidth = 68;
            this.colPrintFactor.MinWidth = 50;
            this.colPrintFactor.Name = "colPrintFactor";
            this.colPrintFactor.Width = 50;
            // 
            // reBtnPrintticket
            // 
            this.reBtnPrintticket.AutoHeight = false;
            serializableAppearanceObject2.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject2.Options.UseFont = true;
            serializableAppearanceObject3.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject3.Options.UseFont = true;
            serializableAppearanceObject4.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject4.Options.UseFont = true;
            serializableAppearanceObject5.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject5.Options.UseFont = true;
            this.reBtnPrintticket.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "چاپ", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, "", new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F10), serializableAppearanceObject2, serializableAppearanceObject3, serializableAppearanceObject4, serializableAppearanceObject5, "", null, null, true)});
            this.reBtnPrintticket.Name = "reBtnPrintticket";
            this.reBtnPrintticket.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.reBtnPrintticket.Click += new System.EventHandler(this.reBtnPrintticket_Click);
            // 
            // colPayPose
            // 
            this.colPayPose.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPayPose.AppearanceCell.Options.UseFont = true;
            this.colPayPose.AppearanceCell.Options.UseTextOptions = true;
            this.colPayPose.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPayPose.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPayPose.AppearanceHeader.Options.UseFont = true;
            this.colPayPose.AppearanceHeader.Options.UseTextOptions = true;
            this.colPayPose.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPayPose.ColumnEdit = this.reBtnPayPose;
            this.colPayPose.MaxWidth = 75;
            this.colPayPose.MinWidth = 50;
            this.colPayPose.Name = "colPayPose";
            this.colPayPose.Width = 50;
            // 
            // reBtnPayPose
            // 
            this.reBtnPayPose.AutoHeight = false;
            serializableAppearanceObject6.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject6.Options.UseFont = true;
            serializableAppearanceObject7.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject7.Options.UseFont = true;
            serializableAppearanceObject8.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject8.Options.UseFont = true;
            serializableAppearanceObject9.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject9.Options.UseFont = true;
            this.reBtnPayPose.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "پ POS", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, "", new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F7), serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, serializableAppearanceObject9, "", null, null, true)});
            this.reBtnPayPose.Name = "reBtnPayPose";
            this.reBtnPayPose.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.reBtnPayPose.Click += new System.EventHandler(this.reBtnPayPose_Click);
            // 
            // colPaycredit
            // 
            this.colPaycredit.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPaycredit.AppearanceCell.Options.UseFont = true;
            this.colPaycredit.AppearanceCell.Options.UseTextOptions = true;
            this.colPaycredit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaycredit.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPaycredit.AppearanceHeader.Options.UseFont = true;
            this.colPaycredit.AppearanceHeader.Options.UseTextOptions = true;
            this.colPaycredit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaycredit.ColumnEdit = this.reBtnPayCredit;
            this.colPaycredit.MaxWidth = 88;
            this.colPaycredit.MinWidth = 70;
            this.colPaycredit.Name = "colPaycredit";
            this.colPaycredit.Width = 70;
            // 
            // reBtnPayCredit
            // 
            this.reBtnPayCredit.AutoHeight = false;
            serializableAppearanceObject10.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject10.Options.UseFont = true;
            serializableAppearanceObject11.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject11.Options.UseFont = true;
            serializableAppearanceObject12.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject12.Options.UseFont = true;
            serializableAppearanceObject13.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            serializableAppearanceObject13.Options.UseFont = true;
            this.reBtnPayCredit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "پ اعتباری", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, "", new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F2), serializableAppearanceObject10, serializableAppearanceObject11, serializableAppearanceObject12, serializableAppearanceObject13, "", null, null, true)});
            this.reBtnPayCredit.Name = "reBtnPayCredit";
            this.reBtnPayCredit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.reBtnPayCredit.Click += new System.EventHandler(this.reBtnPayCredit_Click);
            // 
            // colPaidBackDate
            // 
            this.colPaidBackDate.Caption = "PaidBackDate";
            this.colPaidBackDate.FieldName = "PaidBackDate";
            this.colPaidBackDate.Name = "colPaidBackDate";
            // 
            // colCityID
            // 
            this.colCityID.FieldName = "CityID";
            this.colCityID.Name = "colCityID";
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 20;
            this.toolTip.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            this.toolTip.InitialDelay = 20;
            this.toolTip.KeepWhileHovered = true;
            this.toolTip.ReshowDelay = 10;
            this.toolTip.Rounded = true;
            this.toolTip.RoundRadius = 3;
            this.toolTip.ShowBeak = true;
            this.toolTip.ToolTipLocation = DevExpress.Utils.ToolTipLocation.BottomCenter;
            this.toolTip.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController1_GetActiveObjectInfo);
            // 
            // cityBindingSource
            // 
            this.cityBindingSource.DataSource = typeof(Model.City);
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataSource = typeof(Model.Company);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "چاپ";
            this.barButtonItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.Glyph")));
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.LargeGlyph")));
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // bgpose
            // 
            this.bgpose.WorkerReportsProgress = true;
            this.bgpose.WorkerSupportsCancellation = true;
            this.bgpose.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgpose_DoWork);
            this.bgpose.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgpose_RunWorkerCompleted);
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.TripInfo);
            this.dataLayoutControl1.Controls.Add(this.dgv);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 146);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(199, 358, 250, 350);
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1372, 569);
            this.dataLayoutControl1.TabIndex = 34;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // TripInfo
            // 
            this.TripInfo.Location = new System.Drawing.Point(687, 12);
            this.TripInfo.MenuManager = this.ribbon;
            this.TripInfo.Name = "TripInfo";
            this.TripInfo.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.TripInfo.Properties.Appearance.Options.UseFont = true;
            this.TripInfo.Properties.Appearance.Options.UseTextOptions = true;
            this.TripInfo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TripInfo.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.TripInfo.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.TripInfo.Properties.ReadOnly = true;
            this.TripInfo.Size = new System.Drawing.Size(673, 24);
            this.TripInfo.StyleController = this.dataLayoutControl1;
            this.TripInfo.TabIndex = 32;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem1});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1372, 569);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dgv;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1352, 521);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.TripInfo;
            this.layoutControlItem2.Location = new System.Drawing.Point(675, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(677, 28);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(675, 28);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // FrmTicketsTrips
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1372, 736);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmTicketsTrips";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "بلیط ها";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmReserve_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketLocalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReBtnPayback)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPrintticket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPayPose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPayCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TripInfo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem btnReport;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView grd;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colTripID;
        private DevExpress.XtraGrid.Columns.GridColumn colMan;
        private DevExpress.XtraGrid.Columns.GridColumn colFullname;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Columns.GridColumn colChairs;
        private DevExpress.XtraGrid.Columns.GridColumn colSaleDateSH;
        private DevExpress.XtraGrid.Columns.GridColumn colNumChairs;
        private DevExpress.XtraGrid.Columns.GridColumn colSumDiscount;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem Btn_Del;
        private DevExpress.XtraGrid.Columns.GridColumn ColPayback;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ReBtnPayback;
        private System.Windows.Forms.BindingSource ticketLocalBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colStateticket;
        private DevExpress.XtraGrid.Columns.GridColumn colCityName;
        private DevExpress.XtraGrid.Columns.GridColumn colCashierName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintFactor;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reBtnPrintticket;
        private DevExpress.XtraGrid.Columns.GridColumn colPayPose;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reBtnPayPose;
        private DevExpress.XtraGrid.Columns.GridColumn colPaycredit;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reBtnPayCredit;
        private DevExpress.XtraBars.BarButtonItem BtnbackSale;
        private System.ComponentModel.BackgroundWorker bgpose;
        private DevExpress.XtraBars.BarButtonItem btnCancelPose;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusPaidBack;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTicket;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeSale;
        private DevExpress.XtraGrid.Columns.GridColumn col1;
        private DevExpress.XtraGrid.Columns.GridColumn col2;
        private DevExpress.XtraGrid.Columns.GridColumn colCodeCompany;
        private DevExpress.XtraBars.BarEditItem barCreditUser;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit TripInfo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidBackTime;
        private DevExpress.XtraGrid.Columns.GridColumn colFundPriceValue;
        private System.Windows.Forms.BindingSource cityBindingSource;
        private DevExpress.Utils.ToolTipController toolTip;
        private DevExpress.XtraGrid.Columns.GridColumn colOnlinePrice;
        private DevExpress.XtraGrid.Columns.GridColumn colSaleType;
        private DevExpress.XtraGrid.Columns.GridColumn colMoveTime;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidBackDate;
        private DevExpress.XtraGrid.Columns.GridColumn colCityID;
    }
}