﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Model;
using BL;
using DevExpress.XtraEditors;

namespace Test
{
    public partial class frmticketUser : DevExpress.XtraBars.Ribbon.RibbonForm
    {


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    barButtonItem1_ItemClick(null, null);
                    break;
                case Keys.F2:
                    BtnSave_ItemClick(null, null);
                    break;
                case Keys.F3:
                    BtnNew_ItemClick(null, null);
                    break;
                case Keys.F4:
                    barButtonItem1_ItemClick(null, null);
                    break;
                case Keys.F5:
                    BtnRefesh_ItemClick(null, null);
                    break;
                case Keys.Delete:
                    BtnDelete_ItemClick(null, null);
                    break;

            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


  bool EditValue;

        public frmticketUser(Boolean editvalue = false)
        {
            InitializeComponent();

            GetList();
            EditValue = editvalue; 

            if (!editvalue)
            {
                Refresh_frmticket();
            }
        }

      
        public void FillPrinterSystem(ComboBoxEdit _ComboboxName)
        {
            try { 
            _ComboboxName.Properties.Items.Clear();
            _ComboboxName.Properties.Items.AddRange(System.Drawing.Printing.PrinterSettings.InstalledPrinters);

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
            
            }


        /// <summary>
        /// تابع بروز رسانی 
        /// </summary>
        void GetList()
        {
            if (Userlogin.isAdmin)
                gridControl1.DataSource = UserServices.GetAll();
            else
                gridControl1.DataSource = UserServices.FindByID(Userlogin.ID);


            AccessCompaniesTextEdit.Properties.DataSource = CompanysServices.GetAll();



            FillPrinterSystem(PrinterUserComboBoxEdit);

   
        }

        /// <summary>
        /// تابع خالی کردن فرم
        /// </summary>
        void Refresh_frmticket()
        { 
            try{
            
            EditValue = false;
            userBindingSource.DataSource = new User();
            UserNameTextEdit.EditValue = string.Empty;
            PasswordTextEdit.EditValue = string.Empty;
            FullNameTextEdit.EditValue = string.Empty;
              EnabledCheckEdit.EditValue = false;
              CodeTextEdit.EditValue = string.Empty;
              AccessCompaniesTextEdit.EditValue = string.Empty;
                StarttimeTimeEdit.EditValue = null;
                EndtimeTimeEdit.EditValue = null;

            grdcompany.ClearSelection();
               }
            catch (Exception ex)
            {
                
              GetError.WriteError(ex,this);
                
            }

        }



        /// <summary>
        /// عملیات ثبت کاربر و ویرایش کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {

                if (!dxValidationProvider1.Validate())
                    return;
                CodeTextEdit.Focus();
                    userBindingSource.EndEdit();
                    User user = (User)userBindingSource.Current;

                if(!EditValue)
                {
    User searchuser = UserServices.FindByCode(user.Code,user.UserName);
                if(searchuser != null)
                {
                    XtraMessageBox.Show("کاربری با این مشخصات ثبت شده است", "پیام", MessageBoxButtons.OK);
                    return;
                }
                }
            




                user.Starttime = TimeSpan.Parse(StarttimeTimeEdit.Time.ToString("HH:mm"));
                user.Endtime = TimeSpan.Parse(EndtimeTimeEdit.Time.ToString("HH:mm"));


                int[] dr = grdcompany.GetSelectedRows();
    
                              string code="" ;
                            foreach (var item in dr)
	                            {
                                   
                               code +=";"+ grdcompany.GetRowCellValue(item,colCodeCompany).ToString();

	                            }
                   
    

                 
                    user.AccessCompanies = code;

                int Resut = 0;
                if (EditValue)
                {
            
                    Resut = UserServices.Update(user);
                }
                else
                {
                    Resut = UserServices.Insert(user);
                }

                if (Resut == 1)
                {
                      XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    GetList();
                    Refresh_frmticket();
                    if (user.ID == Userlogin.ID)
                    {
                    Userlogin.Code = user.Code;
                    Userlogin.FullName = user.FullName;
                    Userlogin.Credit = user.Credit;
                    Userlogin.AccessCompanies = user.AccessCompanies;
                    Userlogin.Password = user.Password;
                    Userlogin.UserName = user.UserName;
                    Userlogin.isAdmin = user.isAdmin;
                    Userlogin.PrinterUser = user.PrinterUser;
                    Userlogin.PoseName = user.PoseName;
                    Userlogin.IpPose = user.IpPose;
                    }

                }
                else
                {
                   XtraMessageBox.Show("اشکال در اجرای عملیات", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }



            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }


        private void BtnNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            Refresh_frmticket();
        }

        private void BtnRefesh_ItemClick(object sender, ItemClickEventArgs e)
        {
            GetList();
        }


        /// <summary>
        /// حذف یا غیر فعال کردن کاربر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            try { 

            if (XtraMessageBox.Show("آیا مایل به حذف کاربر می باشید؟", "پیام", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                int id = (int)grd.GetFocusedRowCellValue(colID);
                User us=UserServices.FindByID(id);
                us.Enabled = false;
                int Resut = UserServices.Update(us);

                if (Resut == 1)
                {

          Refresh_frmticket();
                  GetList();
                    XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
               

                    }
                else
                {
                   XtraMessageBox.Show("اشکال در اجرای عملیات", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }


            }

        }
            catch (Exception ex)
            {


              GetError.WriteError(ex,this);
                
            }

}

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();

        }

        private void grd_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {



 


        }

        private void FullNameTextEdit_Validated(object sender, EventArgs e)
        {
            dxValidationProvider1.Validate();
        }

        private void frmticketUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            EditValue = false;
            e.Cancel = true;
            this.Hide();
        }

        private void grd_DoubleClick(object sender, EventArgs e)
        {
            try{
                AccessCompaniesTextEdit.ShowPopup();

            int id = (int)grd.GetFocusedRowCellValue(colID);
            User _user = UserServices.FindByID(id);
            userBindingSource.DataSource = _user;

              

                EditValue = true;
      
            if ( !string.IsNullOrEmpty(_user.AccessCompanies))
            {
                List<string> _CodeCompanyUser = _user.AccessCompanies.Split(';').ToList();

                for (int i = 0; i < grdcompany.RowCount; i++)
                {
                    string code = grdcompany.GetRowCellValue(i, colCodeCompany).ToString();
                    if (_CodeCompanyUser.Contains(code))
                    {
                        grdcompany.SelectRow(i);
                    }

                }
            }

                AccessCompaniesTextEdit.ShowPopup();
  StarttimeTimeEdit.Time =Convert.ToDateTime(_user.Starttime.Value.ToString());
                EndtimeTimeEdit.Time = Convert.ToDateTime(_user.Endtime.Value.ToString());

            }
            catch (Exception ex)
            {
                
              GetError.WriteError(ex,this);
                
            }
        }

        private void AccessCompaniesTextEdit_Click(object sender, EventArgs e)
        {
            AccessCompaniesTextEdit.ShowPopup();
        }

        private void PrinterUserComboBoxEdit_Click(object sender, EventArgs e)
        {
            PrinterUserComboBoxEdit.ShowPopup();
        }

        private void PoseNameTextEdit_Click(object sender, EventArgs e)
        {
            PoseNameTextEdit.ShowPopup();
        }
    }
}