﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;


using Model;
using BL;
using DevExpress.XtraEditors.Repository;
using DevExpress.Utils;
using DevExpress.XtraEditors;


using SSP1126.PcPos;
using PcPosDll;
using PosInterface;
using SSP1126.PcPos.Infrastructure;
using SSP1126.PcPos.BaseClasses;

using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.IO.Ports;
using DevExpress.XtraReports.UI;
using System.Threading.Tasks;
using Syncfusion.Windows.Forms.Grid;
using System.Threading;
using System.Drawing.Printing;
using System.Net.NetworkInformation;

namespace Test
{
    public partial class FrmTickets : DevExpress.XtraBars.Ribbon.RibbonForm, PosInterface.ITransactionDoneHandler
    {

        public FrmTickets()
        {
            InitializeComponent();

            Refresh_frmticket();

            if (!Userlogin.isAdmin)
            {
                Permission _permision = PermissionServices.FindByName("EnableDiscount", Userlogin.Code);
                //DiscountTextEdit.Enabled = Convert.ToBoolean(SettingLogin.GetValue("EnableDiscount"));
                if (_permision != null)
                    DiscountTextEdit.Enabled = _permision.Value.Value;
                else
                    DiscountTextEdit.Enabled = false;

            }



        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    btnClose_ItemClick(null, null);
                    break;
                case Keys.F2:
                    btnSave_ItemClick(null, null);
                    break;
                case Keys.F3:
                    btnEmpty_ItemClick(null, null);
                    break;
                case Keys.F4:
                    btnClose_ItemClick(null, null);
                    break;
                case Keys.F7:
                    btnPose_ItemClick(null, null);
                    break;
                case Keys.F8:
                    btnCancelPose_ItemClick(null, null);
                    break;

            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        #region محاسبه سهم صندوقدار از  فروش بلیط فعلی
        /// <summary>
        /// سهم صندوقدار
        /// </summary>
        private int FundPriceCash { get; set; }

        void CalculateFundPrice()
        {
            Service _service = ServicesServiceeee.GetService(ServicesID, ConnectionCompany);
            if (_service != null)
            {
                if (_service.FundShare != null)
                {
                    Double FundShareTrip = _service.FundShare.Value;

                    if (FundShareTrip < 0) /// محاسبه سهم صندوق در صورت منفی بودن این فیلد
                    {
                        FundShareTrip = FundShareTrip * -1;
                        FundPriceCash = (Convert.ToInt32(FundShareTrip) * (Convert.ToInt32(PriceTextEdit.EditValue) - Convert.ToInt32(DiscountTextEdit.EditValue))) / 100;
                    }
                    else if (FundShareTrip >= 0) ///  اگر مقدار این فیلد از صفر بزرگتر بود سهم صندوق 0 شود
                        FundPriceCash = 0;
                    return;
                }


            }

            string fund = SettingLogin.GetValue("FundPrice");
            fund = fund == string.Empty ? "0" : fund;
            if (fund.Contains("%"))
            {
                fund = fund.Replace("%", string.Empty);
                if (DiscountTextEdit.EditValue == DBNull.Value)
                    DiscountTextEdit.EditValue = 0;

                FundPriceCash = (int.Parse(fund) * (Convert.ToInt32(PriceTextEdit.EditValue) - Convert.ToInt32(DiscountTextEdit.EditValue))) / 100;
            }
            else
            {
                FundPriceCash = int.Parse(fund);
            }



        }
        #endregion

        #region متغییرها و پروپرتی ها
        /// <summary>
        /// مسیر سرویس
        /// </summary>
        public string TripWay { get; set; }
        public string CodeTrip { get; set; }
        public string DayTrip { get; set; }
        public string DateTripPersian { get; set; }
        public string TimeTrip { get; set; }
        /// <summary>
        /// نوع سرویس
        /// </summary>
        public string TypeTrip { get; set; }
        public string CountChair { get; set; }
        /// <summary>
        /// صندلی های پر شده
        /// </summary>
        public int FullChair { get; set; }
        /// <summary>
        /// صندلی های رزرو شده
        /// </summary>
        public int RezervChair { get; set; }
        public string ConnectionCompany { get; set; }
        public byte[] Logo { get; set; }

        public string CompanyName { get; set; }
        public int LayoutID { get; set; }
        public int ServicesID { get; set; }
        /// <summary>
        /// کد شهر مقصد
        /// </summary>
        public int IDEndCity { get; set; }
        /// <summary>
        /// کد شهر مقصد
        /// </summary>
        public string CodeEndCity { get; set; }

        /// <summary>
        /// تاریخ حرکت سرویس
        /// </summary>
        public DateTime MoveTimeDate { get; set; }
        private int emptychair;
        public int EmptyChair
        {
            get
            {
                int full = FullChair + RezervChair;
                return emptychair = int.Parse(CountChair) - full;
            }
        }


        public static int floors, rows, columns, columnSpace, chairs;
        public static string layout;
        int cnt;

        public string CodeCompany { get; set; }

        string _namechairs = ";", GenderCustomre;

        /// <summary>
        /// صندلی هایی که انتخاب شده
        /// </summary>
        List<string> _selectchairs = new List<string>();

        /// <summary>
        /// نام شهرهای مبدا - مقصد نام اتوبوس یا سرویس و عنوان شرکت تعاونی
        /// </summary>
        /// 
        string EndCity, StartCity, BusNameService, companyName;


        /// خواندل صندلی های رزروی و فروخته شده این سرویس    
        List<Ticket> _chairTicket;
        /// <summary>
        /// کلیه صندلی های رزور شده این سرویس
        /// </summary>
        List<Ticket> _chairRezervTicket = new List<Ticket>();
        /// <summary>
        ///فروش صندلی های رزرو شده که انتخاب شده اند
        /// </summary>
        List<Ticket> _SalechairRezerve = new List<Ticket>();


        /// <summary>
        /// لیست صندلی های رزرو شده
        /// </summary>
        Dictionary<string, string> chaireisRezerv = new Dictionary<string, string>();
        /// <summary>
        /// لیست صندلی های فروخته شده
        /// </summary>
        Dictionary<string, string> chaireisSale = new Dictionary<string, string>();

        bool PayCash;

        String IpPos, typePos;

        #endregion


        /// <summary>
        /// نمایش اطلاعات مربوط به سرویس مورد نظر هنگام فروش بلیط
        /// </summary>
        void ShowInformationTrip()
        {
            try
            {

                txtWay.EditValue = CompanyName + " - " + TripWay + "  ( " + CodeTrip + " )   " + DayTrip + "  -  " + DateTripPersian + "  -  " + TimeTrip + "   " + TypeTrip;
                txtCountChair.EditValue = CountChair;
                txtFullChair.EditValue = FullChair;
                txtRezervChair.EditValue = RezervChair;
                txtEmptyChair.EditValue = EmptyChair;

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }

        }


        /// <summary>
        /// خالی کردن فرم
        /// </summary>
        public void Refresh_frmticket()
        {

            try
            {
                Paylock = false;
                TypeTicket = string.Empty;
                txtStatusPayment.EditValue = string.Empty;
                PayCash = false;
                codecity = string.Empty;
                ticketBindingSource.DataSource = new TicketLocal();
                PayDateDateEdit.EditValue = DateTime.Now;
                _namechairs = ";";
                EndCity = string.Empty;

                BusNameService = string.Empty;
                companyName = string.Empty;
                CreditUsertxt.EditValue = Userlogin.Credit;
                txtStatusPayment.EditValue = "وضعیت پرداخت کارتخوان";
                txtStatusPayment.ForeColor = Color.Green;
                GenderCustomre = string.Empty;

                CityIDTextEdit.EditValue = IDEndCity;


                _chairTicket = new List<Ticket>();
                _selectchairs = new List<string>();
                selectchair = new List<string>();
                _SalechairRezerve = new List<Ticket>();
            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }

        }


        /// <summary>
        /// لود کردن اطلاعات لازم برای فروش بلیط  سرویس مورد نظر
        /// </summary>
        /// <param name="idservice">کد سرویس</param>
        /// <param name="_TripID">کد سفر</param>
        /// <param name="_way">مسیر سفر</param>
        /// <param name="_layoutId">کد چیدمان صندلی ها</param>
        public void ShowEdit()
        {
            try
            {

                FullnameTextEdit.EditValue = GenderCustomre;

                CreditUsertxt.EditValue = Userlogin.Credit;
                BusNameService = TypeTrip;
                TripIDTextEdit.EditValue = CodeTrip;
                string cit = TripWay.Replace("->", "-");
                String[] cities = cit.Split('-');
                StartCity = cities[0];
                EndCity = cities[1];

                //خواندن قیمت این سرویس
                PriceTextEdit.EditValue = TripServices.FindByID(int.Parse(CodeTrip), ConnectionCompany).Price;
                /// خواندن شهر های بین راهی
                List<CitiesServices> licity = CityService.FindByIDService(ServicesID, ConnectionCompany);
                licity.Add(new CitiesServices() { Title = EndCity, Code = CodeEndCity, ID = IDEndCity, Price = Convert.ToInt32(PriceTextEdit.EditValue) });
                CityIDTextEdit.Properties.DataSource = licity;

                citiesServicesBindingSource.DataSource = licity;

                /// خواندل صندلی های رزروی و فروخته  شده این سرویس    
                _chairTicket = TicketServices.FindByIDTrip(int.Parse(CodeTrip), ConnectionCompany);
                chaireisRezerv.Clear();
                chaireisSale.Clear();

                NoTextEdit.EditValue = TripIDTextEdit.Text.Trim() + "-" + (_chairTicket.Count + 1);

                foreach (Ticket item in _chairTicket)
                {
                    if (item.SaleType == 1)
                    {

                        _chairRezervTicket.Add(item);
                        string nchairs = item.Chairs;
                        string[] lchair = nchairs.Split(';');
                        for (int i = 0; i < lchair.Length; i++)
                        {
                            if (lchair[i].Contains("f"))
                            {
                                string namechair = lchair[i].Replace("f", string.Empty).Replace(";", string.Empty);
                                if (!chaireisRezerv.ContainsKey(namechair))
                                    chaireisRezerv.Add(namechair, "خانم");

                            }
                            if (lchair[i].Contains("m"))
                            {
                                string namechair = lchair[i].Replace("m", string.Empty).Replace(";", string.Empty);
                                if (!chaireisRezerv.ContainsKey(namechair))
                                    chaireisRezerv.Add(namechair, "آقا");

                            }

                        }


                        chaireisRezerv.Remove(";");
                        chaireisRezerv.Remove(string.Empty);

                    }
                    else
                    {
                        string nchairs = item.Chairs;
                        string[] lchair = nchairs.Split(';');
                        for (int i = 0; i < lchair.Length; i++)
                        {
                            if (lchair[i].Contains("f"))
                            {
                                string namechair = lchair[i].Replace("f", string.Empty).Replace(";", string.Empty);
                                if (!chaireisSale.ContainsKey(namechair))
                                    chaireisSale.Add(namechair, "خانم");

                            }
                            if (lchair[i].Contains("m"))
                            {
                                string namechair = lchair[i].Replace("m", string.Empty).Replace(";", string.Empty);
                                if (!chaireisSale.ContainsKey(namechair))
                                    chaireisSale.Add(namechair, "آقا");

                            }

                        }


                        chaireisSale.Remove(";");
                        chaireisSale.Remove(string.Empty);

                    }


                }


                ///  دریافت الگوریتم صندلی های سرویس
                Layout lay = LayoutServices.FindByID(LayoutID, ConnectionCompany);
                floors = lay.Floors;
                columns = lay.Columns;
                columnSpace = lay.ColumnSpace;
                rows = lay.Rows;
                var nch = lay.NumChairs;
                chairs = (int)nch;
                CountChair = chairs.ToString();
                layout = lay.LayoutText;
                SetChair();

                ShowInformationTrip();

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
            finally
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }
        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void btnEmpty_ItemClick(object sender, ItemClickEventArgs e)
        {
            Refresh_frmticket();
            ShowEdit();

            int _fullChair = 0;

            RezervChair = TripServices.GetNumberRezervTickets(ConnectionCompany, int.Parse(CodeTrip), out _fullChair, SettingLogin.GetValue("ServiceOvertime").ToInt());
            txtFullChair.EditValue = FullChair = _fullChair;
            txtRezervChair.EditValue = RezervChair;
            txtEmptyChair.EditValue = EmptyChair;

            ShowNumberChairGraphic();

            txtSumPrice.EditValue = _selectchairs.Count * Convert.ToInt32(PriceTextEdit.EditValue);
            DiscountTextEdit_Leave(null, null);
            NoTextEdit.EditValue = (TripIDTextEdit.Text.Trim() + "-" + (_selectchairs.Count + _chairTicket.Count)).Replace(",", "").ToString();
            ChairsTextEdit.EditValue = _namechairs;
            NumChairsTextEdit.EditValue = _selectchairs.Count;


        }


        /// <summary>
        /// چاپ بلیط
        /// </summary>
        /// <param name="_tic">اطلاعات بلیط</param>
        /// <param name="_totalPrice">جمع کل بلیط ها</param>
        /// <returns>عملیات چاپ انجام شد</returns>
        bool PrintFactor(TicketLocal _tic, int _totalPrice)
        {
            try
            {
                if (Paylock)
                {
                    return false;
                }


                Trip _trip = TripServices.FindByID(_tic.TripID, ConnectionCompany);
                DateTime saledt = _trip != null ? _trip.Date : _tic.SaleDate.Value;
                string nameday = saledt.GetNameOfDayDate();
                string saledate = (saledt.JulianToPersianDate() + nameday + "  " + saledt.ToShortTimeString());

                TicketReport _ticprint = new TicketReport()
                {
                    ID = _tic.ID,
                    TripID = _tic.TripID,
                    Man = _tic.Man,
                    Fullname = _tic.Fullname,
                    No = _tic.No,
                    Chairs = _tic.Chairs,
                    CityID = _tic.CityID,
                    Tel = _tic.Tel,
                    Price = _tic.Price + _tic.FundPrice.Value,
                    NumChairs = _tic.NumChairs,
                    CashierCode = _tic.CashierCode,
                    CashierName = _tic.CashierName,
                    CompanyCode = _tic.CompanyCode,
                    SaleDate = _trip != null ? nameday + " " + _trip.Date.JulianToPersianDate() + "  " + _trip.DepartureTime : "",
                    PrintDate = DateTime.Now.JulianToPersianDateTime(),
                    CompanyTitle = CompanyName,
                    TotalPrice = _totalPrice,
                    CityNameEnd = EndCity,
                    CityNameStart = StartCity,
                    TypeBus = BusNameService,
                    Logo = Logo

                };


                List<TicketReport> li = new List<TicketReport>();
                li.Add(_ticprint);

                RptTicket rpt = new RptTicket();

                rpt.DataSource = li;

                rpt.DescriptionTicket.Value = SettingLogin.GetValue("DescriptionReport").ToString();

                //rpt.DataMember = dtvw.TableName;
                //rpt.DataAdapter = _ticprint;
                rpt.PriceString.Value = JntNum2Text.Num2Text.ToFarsi(_ticprint.TotalPrice) + " ریال";
                rpt.RequestParameters = false;
                rpt.ShowPrintStatusDialog = false;
                rpt.PrintingSystem.ShowMarginsWarning = false;
                ReportPrintTool rp = new ReportPrintTool(rpt);
                rp.PrinterSettings.Copies = 1;
                rp.PrinterSettings.PrinterName = Userlogin.PrinterUser;
                //rp.ShowPreview();
                rp.Print();

                return true;


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
                return false;
            }


        }

        int IDTicketcompany;

        TicketLocal _TicketSalyNow;

        /// <summary>
        /// شماره پرداخت کارتخوان یک بلیط که فقط دفعه اول مقدار بلیط را دریافت نمایید
        /// </summary>
        int counterGetTicketRezerv = 0;


        /// <summary>
        /// ثبت یا فروش بلیط
        /// </summary>
        void InsertTicket()
        {

            try
            {

                if (!FullnameTextEdit.Text.Contains(GenderCustomre))
                {
                    FullnameTextEdit.EditValue = GenderCustomre + FullnameTextEdit.Text;
                }
                ticketBindingSource.EndEdit();
                TicketLocal _ticket = ticketBindingSource.Current as TicketLocal;
                _TicketSalyNow = new TicketLocal();
                var trip = TicketServices.GetTripbyID(Convert.ToInt32(TripIDTextEdit.EditValue), ConnectionCompany) as Trip;
                if (trip != null && trip.Closed)
                {
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("صورت وضعیت بسته شده است لطفا سرویس را مجددا بررسی نمایید", "", MessageBoxButtons.OK);
                    return;
                }
                if (_SalechairRezerve.Count > 1)
                {
                    CheckingPay = true;
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("کاربر گرامی تعداد بلیط های رزرو شده شما بیش از یک عدد می باشد", "", MessageBoxButtons.OK);
                    return;
                    //  ویرایش بلیط رزور شده به عنوان بلیط فروخته شده در سیستم تعاونی و ثبت در سیستم صندوقدار
                }


                #region دریافت اطلاعات بلیط

                // int rezerSale = _SalechairRezerve.Count;
                int TotalPrice = Convert.ToInt32(txtSumPrice.EditValue);

                _ticket.NumChairs = _selectchairs.Count;


                #region چک کردن اعتبار فعلی کاربر قبل از عملیات ثبت
                User ulogin = UserServices.FindByUser(Userlogin.UserName, Userlogin.Password);
                Userlogin.Credit = ulogin.Credit;
                if (TotalPrice > Userlogin.Credit && PayCash == true)
                {
                    CheckingPay = true;
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("کاربر گرامی میزان اعتبار شما کمتر از حسابتان می باشد" + " \n  میزان اعتبار شما  " + Userlogin.Credit.ToString() + "  میباشد", "", MessageBoxButtons.OK);
                    return;
                }
                if (TotalPrice < 0)
                {
                    CheckingPay = true;
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("تخفیف بیشتر از مبلغ بلیط می باشد", "", MessageBoxButtons.OK);
                    return;
                }
                #endregion


                _ticket.PayDate = PayDateDateEdit.DateTime;
                _ticket.TripID = Convert.ToInt32(TripIDTextEdit.EditValue);
                _ticket.SaleType = 2;
                _ticket.FundID = 100;
                _ticket.PaidBack = 0;
                _ticket.PaidBackPercent = 0;
                _ticket.SaleDate = DateTime.Now;
                _ticket.CompanyCode = CodeCompany;
                _ticket.CashierCode = Userlogin.Code;
                _ticket.CashierName = Userlogin.FullName;
                _ticket.Chairs = _namechairs;
                _ticket.Printed = false;

                _ticket.No = (TripIDTextEdit.Text.Trim() + "-" + (_selectchairs.Count + _chairTicket.Count)).Replace(",", "").ToString();

                if (_ticket.CityID != 0)
                {
                    _ticket.CityName = CityIDTextEdit.Text.Trim();
                    if (String.IsNullOrEmpty(codecity))
                        codecity = CodeEndCity;

                    _ticket.CityCode = codecity;
                }

                else
                {
                    _ticket.CityID = IDEndCity;
                    _ticket.CityCode = CodeEndCity;
                    _ticket.CityName = EndCity;
                }

                if (_ticket.Tel == null)
                    _ticket.Tel = "";

                _ticket.PayLock = Paylock;  // مشخص کردن پرداخت کارتخوان یا اعتباری


                CalculateFundPrice();
                _ticket.FundPrice = FundPriceCash;
                _ticket.TypeTicket = TypeTicket;
                _ticket.Price = Convert.ToInt32(PriceTextEdit.EditValue) - FundPriceCash;
                #endregion

                if (_SalechairRezerve.Count == 1) /// چنانچه کاربر قصد فروش بلیط رزرو شده را داشته باشد بلیط رزرو شده ویرایش و ثبت می شود
                {
                    #region ثبت بلیط در سیستم صندوقدار و ویرایش اطلاعات بلیط رزرو شده در سیستم تعاونی


                    if (ticketRezervPayPose.SaleType == 0 && ticketRezervPayPose.ID == 0)
                        CopyClass.Copy<Ticket>(_SalechairRezerve[0], ticketRezervPayPose);


                    Ticket _ticktNet = TicketServices.FindByIDNetSaly(ticketRezervPayPose.ID, ConnectionCompany);
                    if (_ticktNet == null)
                    {
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                        XtraMessageBox.Show("این بلیط فروخته شده است.لطفا در انتخاب خود دقت نمایید", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        return;
                    }

                    Ticket ticketRezerv = _SalechairRezerve[0];

                    ticketRezerv.TripID = _ticket.TripID;
                    ticketRezerv.CustomerID = _ticket.CustomerID;
                    ticketRezerv.Man = _ticket.Man;
                    ticketRezerv.Fullname = _ticket.Fullname;
                    ticketRezerv.No = _ticket.No;
                    ticketRezerv.Chairs = _ticket.Chairs;
                    ticketRezerv.CityID = _ticket.CityID;
                    ticketRezerv.SaleType = _ticket.SaleType;
                    ticketRezerv.FundID = _ticket.FundID;
                    ticketRezerv.Expire = _ticket.Expire;
                    ticketRezerv.BranchID = _ticket.BranchID;
                    ticketRezerv.Tel = _ticket.Tel;
                    ticketRezerv.Price = _ticket.Price;       //   کسر مبلغ سهم صندوقدار از قیمت بلیط فقط در سیستم تعاونی
                    ticketRezerv.PaidBack = _ticket.PaidBack;
                    ticketRezerv.PaidBackPercent = _ticket.PaidBackPercent;
                    ticketRezerv.Discount = _ticket.Discount;
                    ticketRezerv.SaleUserID = _ticket.SaleUserID;
                    ticketRezerv.SaleDate = _ticket.SaleDate;
                    ticketRezerv.NumChairs = _ticket.NumChairs;
                    ticketRezerv.Archive = _ticket.Archive;
                    ticketRezerv.Printed = _ticket.Printed;
                    ticketRezerv.FundUserID = _ticket.FundUserID;
                    ticketRezerv.ReturnUserID = _ticket.ReturnUserID;
                    ticketRezerv.PaidBackDate = _ticket.PaidBackDate;
                    ticketRezerv.Address = _ticket.Address;
                    ticketRezerv.CityCode = _ticket.CityCode;
                    ticketRezerv.CashierCode = _ticket.CashierCode;
                    ticketRezerv.CashierName = _ticket.CashierName;
                    ticketRezerv.PayDate = _ticket.PayDate;
                    ticketRezerv.CompanyCode = _ticket.CompanyCode;
                    ticketRezerv.CityName = _ticket.CityName;
                    ticketRezerv.FundPrice = _ticket.FundPrice.Value;
                    ticketRezerv.TypeTicket = TypeTicket;
                    ticketRezerv.PayLock = Paylock;


                    IDTicketcompany = TicketServices.UpSertTickets(ticketRezerv, ConnectionCompany);

                    #endregion
                }
                else
                {
                    #region ثبت بلیط در سیستم تعاونی و صندوقدار

                    int count = (_selectchairs.Count + _chairTicket.Count);

                SearchExistsNO:
                    if (TicketServices.SearchTicketByNo(_ticket.No, ConnectionCompany))
                    {
                        //      ثبت بلیط در سیستم صندوقدار و تعاونی
                        IDTicketcompany = TicketServices.Insert(_ticket, ConnectionCompany);

                    }
                    else
                    {
                        count++;
                        _ticket.No = (TripIDTextEdit.Text.Trim() + "-" + count).Replace(",", "").ToString();
                        goto SearchExistsNO;
                    }

                    #endregion

                }



                if (IDTicketcompany > 0)
                {
                    _TicketSalyNow = _ticket;

                    #region کسر مبلغ از اعتبار صندوقدار

                    if (PayCash) // اگر پرداخت اعتباری بود از حساب کاربر کسر کند
                    {
                        /// کسر مبلغ بلیط از حساب کاربر
                        User us = UserServices.FindByID(Userlogin.ID);
                        us.Credit -= TotalPrice;
                        Userlogin.Credit = us.Credit;
                        UserServices.Update(us);

                        AddCreditUserreport(TotalPrice); // ثبت مبلغ دریافتی در جدول گزارشات کاربر
                    }

                    #endregion

                    if (!PrinterExists(Userlogin.PrinterUser))
                    {
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                        XtraMessageBox.Show("پرینتر مورد نظر یافت نشد . لطفا تنظیمات خود را بررسی نمایید", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        #region چاپ بلیط- در صورت چاپ شدن بلیط فیلد چاپ فعال می شود در غیر این صورت پیامی به کاربر نمایش داده میشود

                        if (PrintFactor(_ticket, TotalPrice))
                        {

                            #region فعال کردن چاپ بلیط در تعاونی و سیستم صندوقدار

                            //  اطلاعات بلیط سیستم تعاونی
                            if (IDTicketcompany > 0)
                            {
                                Ticket tickupdateNet = TicketServices.FindByIDNet(IDTicketcompany, ConnectionCompany);
                                tickupdateNet.Printed = true;
                                IDTicketcompany = TicketServices.Update(tickupdateNet, ConnectionCompany);
                            }
                            #endregion

                        }
                        else
                        {
                            if (!Paylock)
                            {
                                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                                XtraMessageBox.Show("عملیات ثبت  موفقیت انجام شد.اما چاپ  بلیط ناموفق لطفا نام پرینتر را چک نمایید", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                        }

                        #endregion
                    }

                    #region خالی کردن فرم جهت ثبت جدید

                    if (!Paylock)
                    {
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                        XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Refresh_frmticket();
                        ShowEdit();

                        int fullchair;
                        RezervChair = TripServices.GetNumberRezervTickets(ConnectionCompany, int.Parse(CodeTrip), out fullchair, SettingLogin.GetValue("ServiceOvertime").ToInt());
                        txtFullChair.EditValue = FullChair = fullchair;
                        txtRezervChair.EditValue = RezervChair;
                        txtEmptyChair.EditValue = EmptyChair;

                        ShowNumberChairGraphic();

                    }


                    #endregion

                }
                else
                {
                    if (IDTicketcompany == 0)
                    {
                        ChangyPayLockPosFailed();
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                        XtraMessageBox.Show("عملیات با مشکل مواجه شد لطفا مجددا سعی نمایید", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    CheckingPay = true;
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("ثبت بلیط ناموفق", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }


            }
            catch (Exception ex)
            {
                CheckingPay = true;

               GetError.WriteError(ex,this);
                
            }

        }

        /// <summary>
        /// ثبت در جدول گزارشات اعتبار کاربر
        /// </summary>
        void AddCreditUserreport(int _TotalPrice)
        {
            UserCreditReport _uc = new UserCreditReport();
            _uc.ActionDate = DateTime.Now;
            _uc.ActionType = 1;
            _uc.UserCode = Userlogin.Code;
            _uc.Recived = 0;
            _uc.Payment = _TotalPrice;
            _uc.Status = 1;
            int result = UserCreditReportServices.Insert(_uc);
        }

        /// <summary>
        /// تغییر مقدار فیلد کنترل پرداخت کارتخوان هنگام پرداخت موفق در دیتابیس های مورد نظر
        /// </summary>
        void ChangyPayLockPosSuccess()
        {

            CheckingClickPose = false;

            Paylock = false;
            int TotalPrice = Convert.ToInt32(txtSumPrice.EditValue);
            int Result = TicketServices.SuccessPayLockPos(IDTicketcompany, ConnectionCompany);

            if (Result == 1)
            {



                #region چاپ بلیط در صورت موفق بودن عملیات

                if (!PrinterExists(Userlogin.PrinterUser))
                {
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("پرینتر مورد نظر یافت نشد . لطفا تنظیمات خود را بررسی نمایید", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                else
                {

                    if (PrintFactor(_TicketSalyNow, TotalPrice))
                    {

                        #region فعال کردن چاپ بلیط در تعاونی و سیستم صندوقدار

                        //  اطلاعات بلیط سیستم تعاونی
                        Ticket tickupdateNet = TicketServices.FindByIDNet(IDTicketcompany, ConnectionCompany);

                        tickupdateNet.Printed = true;
                        Result = TicketServices.Update(tickupdateNet, ConnectionCompany);

                        #endregion

                    }

                }
                #endregion

                #region خالی کردن فرم بعد از ثبت

                IDTicketcompany = 0;

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Refresh_frmticket();
                ShowEdit();

                int fullchair;
                RezervChair = TripServices.GetNumberRezervTickets(ConnectionCompany, int.Parse(CodeTrip), out fullchair, SettingLogin.GetValue("ServiceOvertime").ToInt()).ToInt();
                txtFullChair.EditValue = FullChair = fullchair;
                txtRezervChair.EditValue = RezervChair;
                txtEmptyChair.EditValue = EmptyChair;

                ShowNumberChairGraphic();


                #endregion
            }


        }

        /// <summary>
        /// حذف بلیط فروخته شده و ویرایش بلیط رزور شده به حالت قبل هنگام اشکال در انجام عملیات پرداخت کارتخوان یا مشکل دیتابیس
        /// </summary>
        void ChangyPayLockPosFailed()
        {

            if (_SalechairRezerve.Count > 0)
            {
                //  بلیط رزور شده را ویرایش کن و به حالت قبل برگردان
                int result = TicketServices.UpdateTicketCompany(ticketRezervPayPose, ConnectionCompany);

            }
            else
            {
                /// بلیط ها را با استفاده از کدشان حذف کن
                /// 
                int result = TicketServices.DeleteTickets(IDTicketcompany, ConnectionCompany);

            }

            IDTicketcompany = 0;

            CheckingClickPose = false;
            ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
        }


        public static bool PrinterExists(string printerName)
        {
            if (String.IsNullOrEmpty(printerName)) { throw new ArgumentNullException("printerName"); }
            return PrinterSettings.InstalledPrinters.Cast<string>().Any(name => printerName.ToUpper().Trim() == name.ToUpper().Trim());
        }


        string TypeTicket;
        // دکمه ثبت بلیط
        private void btnSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            FullnameTextEdit.Focus();
            if (dxValidationProvider1.Validate())
            {
                if (_selectchairs.Count == 0)
                {
                    XtraMessageBox.Show("کاربر گرامی لطفا صندلی مورد نظر را انتخاب نمایید.", "", MessageBoxButtons.OK);
                    return;
                }
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                PayCash = true;
                Paylock = false;
                TypeTicket = "پرداخت اعتباری";
                IDTicketcompany = 0;
                InsertTicket();//پرداخت اعتباری
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }
            else
            {
                XtraMessageBox.Show("لطفا داده های مورد نیاز را وارد نمایید", "پیام", MessageBoxButtons.OK);
            }

        }


        string codecity;

        //محاسبه هزینه شهر های بین راهی
        private void CityIDTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (CityIDTextEdit.EditValue != DBNull.Value)
                {

                    if (Convert.ToInt32(CityIDTextEdit.EditValue) != 0)
                    {

                        string pric = grdCitiesServices.GetFocusedRowCellValue(colPrice).ToString();
                        string code = grdCitiesServices.GetFocusedRowCellValue(colCode).ToString();


                        var obj = citiesServicesBindingSource.List.OfType<CitiesServices>().ToList().Find(c => c.ID == Convert.ToInt32(CityIDTextEdit.EditValue));
                        var pos = citiesServicesBindingSource.IndexOf(obj);

                        PriceTextEdit.EditValue = obj.Price;
                        codecity = obj.Code;
                        EndCity = CityIDTextEdit.Text;
                    }

                }


            }
            catch (Exception)
            {
                CityIDTextEdit.EditValue = IDEndCity;


            }
            finally
            {
                if (CityIDTextEdit.EditValue == DBNull.Value || Convert.ToInt32(CityIDTextEdit.EditValue) == 0)
                {

                    CityIDTextEdit.EditValue = IDEndCity;

                }

            }


        }



        /// <summary>
        /// طراحی الگوریتم صندلی ها
        /// </summary>

        void SetChair()
        {
            try
            {

                DataTable dt = new DataTable();

                for (int i = 0; i < columns; i++)
                {
                    dt.Columns.Add("ستون" + (i + 1).ToString(), typeof(string));
                }

                for (int i = 0; i < rows; i++)
                {
                    dt.Rows.Add(new Object[] { });
                }

                cnt = 0;
                int loc = 0;


                this.dgv.RowCount = columns;
                this.dgv.ColCount = rows + 1;

                for (int i = 0; i < rows && cnt < chairs; i++)
                {
                    for (int j = 0; j < columns; j++)
                    {

                        loc++;
                        cnt++;
                        if (cnt > chairs)
                            break;
                        string tempStr = "0_" + i.ToString() + "_" + j.ToString();
                        if (layout.Contains(tempStr))
                        {

                            string numberchair = cnt < 10 ? cnt.ToString() : cnt.ToString();
                            if (chaireisRezerv.ContainsKey(numberchair))
                            {
                                dt.Rows[i][j] = numberchair;
                            }
                            else
                            {
                                if (chaireisSale.ContainsKey(numberchair))
                                {
                                    dt.Rows[i][j] = numberchair;
                                }
                                else
                                {
                                    dt.Rows[i][j] = numberchair;
                                }


                            }
                            if (!nameChairtext.ContainsKey(loc))
                            {
                                /// مشخص کردن مکان شماره صندلی در سلول های سطر گرید
                                nameChairtext.Add(loc, numberchair);
                            }


                        }
                        else
                        {

                            if (!nameChairtext.ContainsKey(loc))
                            {
                                /// مشخص کردن مکان شماره صندلی در سلول های سطر گرید
                                //nameChairtext.Add(loc, "---");
                                dt.Rows[i][j] = "---";

                            }


                            cnt--;
                        }



                    }

                }


                DataTable dtrotate = GenerateTransposedTable(dt);

                this.dgv.PopulateValues(GridRangeInfo.Cells(1, 1, dgv.RowCount, dgv.ColCount), dtrotate);
                dgv.Cols.Hidden[0] = true;
                dgv.Cols.Hidden[1] = true;
                dgv.Rows.Hidden[0] = true;

                setimage();


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        CheckBox newCheckBox()
        {
            CheckBox che = new CheckBox();
            che.BackgroundImage = global::Test.Properties.Resources.Unchecked;
            che.BackgroundImageLayout = ImageLayout.Stretch;
            che.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            che.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft;
            che.Name = "ch";
            che.Size = new System.Drawing.Size(80, 50);
            che.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            che.UseVisualStyleBackColor = true;
            che.Checked = false;
            che.Click += new EventHandler(OnClickCh);
            return che;
        }
        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();
            outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());
            string newColName = string.Empty;
            foreach (DataRow inRow in inputTable.Rows)
            {
                try
                {
                    newColName = inRow[0].ToString();
                    outputTable.Columns.Add(newColName);
                }
                catch (System.Data.DuplicateNameException)
                {
                    newColName += " ";
                    outputTable.Columns.Add(newColName);
                }
            }

            for (int rCount = 0; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();
                int rowcount = outputTable.Rows.Count;
                int colcount = outputTable.Columns.Count;
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }
        void setimage()
        {

            CheckBox che = newCheckBox();
            for (int i = 0; i < dgv.RowCount + 1; i++)
            {
                this.dgv.RowHeights[i] = 50;

                for (int j = 0; j < dgv.ColCount + 1; j++)
                {
                    che.Name = "ch" + i + "-" + j;
                    this.dgv[i, j].CellType = "Control";
                    this.dgv[i, j].Control = che;
                    this.dgv.ColWidths[j] = 50;
                }

            }


        }

        /// <summary>
        /// نمایش شماره صندلی ها
        /// </summary>
        async void ShowNumberChairGraphic()
        {

            await Task.Run(() =>
             {

                 if (this.InvokeRequired)
                 {
                     dgv.BeginInvoke(new MethodInvoker(() =>
                     {
                         SetNewchair();
                         ShowSelectedChairs();

                     }));
                 }
                 else
                 {
                     SetNewchair();
                     ShowSelectedChairs();

                 }

             });


        }

        void SetNewchair()
        {
            for (int row = 0; row < columns + 1; row++)
            {

                for (int col = 0; col < rows + 2; col++)
                {
                    CheckBox newchecke = newCheckBox();
                    newchecke.Text = dgv[row, col].Text;

                    if (newchecke.Text == "")
                        newchecke.Text = "---";

                    dgv[row, col].Control = newchecke;
                    dgv[row, col].CellType = "Control";
                    if (!LocationChairinGRD.ContainsKey(newchecke.Text))
                        LocationChairinGRD.Add(newchecke.Text, row.ToString() + ";" + col.ToString());
                }
            }
        }
        void RefreshCellChairs(int row, int col, Image img, bool state)
        {
            CheckBox newchecke = newCheckBox();
            CheckBox che = (CheckBox)dgv[row, col].Control;
            newchecke.BackgroundImage = img;
            newchecke.Text = dgv[row, col].Text;
            newchecke.Enabled = state;
            dgv[row, col].Control = newchecke;
            dgv[row, col].CellType = "Control";

        }

        /// <summary>
        /// مشخص کردن نوع تصویر و جنسیت و وضعیت بلیط که ایا در چه حالت فروش -رزرو و یا ازاد وجود دارد
        /// </summary>
        void ShowSelectedChairs()
        {
            try
            {
                for (int row = 0; row < columns + 1; row++)
                {
                    for (int col = 0; col < rows + 2; col++)
                    {
                        string value = dgv[row, col].Text.ToString();
                        value = value.Replace("آقا", string.Empty).Replace("خانم", string.Empty);

                        if (string.IsNullOrEmpty(value))
                            value = "---";

                        #region چک کردن بلیط که آیا رزرو شده می باشد یا خیر
                        if (chaireisRezerv.ContainsKey(value))
                        {
                            string general = chaireisRezerv[value];
                            if (general.Contains("خانم"))
                            {
                                // آدمک رزروی خانم
                                RefreshCellChairs(row, col, Properties.Resources.rezerv_zan, false);

                            }
                            else
                            {
                                //      آدمک رزروی آقا
                                RefreshCellChairs(row, col, Properties.Resources.rezerv_mard, false);
                            }
                        }
                        #endregion

                        #region چک کردن این که ایا این بلیط فروخته شده است یا خیر
                        else
                        {
                            if (chaireisSale.ContainsKey(value))
                            {
                                string general = chaireisSale[value];
                                if (general.Contains("خانم"))
                                {
                                    //      آدمک فروخته شده خانم
                                    RefreshCellChairs(row, col, Properties.Resources.frosh_zan, false);
                                }
                                else
                                {
                                    RefreshCellChairs(row, col, Properties.Resources.frosh_mard, false);
                                    //      آدمک فروخته شده آقا

                                }
                            }

                            #region این خونه یا صندلی خالی می باشد
                            else
                            {
                                if (value == "---")
                                {
                                    //    صندلی غیر فعال

                                    CheckBox newchecke = newCheckBox();
                                    PictureBox pic = new PictureBox();
                                    pic.BackgroundImage = global::Test.Properties.Resources.dash;
                                    pic.BackgroundImageLayout = ImageLayout.Stretch;
                                    pic.Size = new System.Drawing.Size(80, 50);
                                    //newchecke.Text = dgv[row, col].Text;
                                    newchecke.Enabled = false;
                                    dgv[row, col].Control = pic;
                                    dgv[row, col].CellType = "Control";


                                }


                            }
                            #endregion

                        }
                        #endregion

                        #region اجرای رویداد مربوط به هر صندلی جهت یک بار کلیک کردن
                        //if (value != "---")
                        //{
                        //    /////  برای چک باکس
                        //  //  CheckBox che = (CheckBox)dgv[row, col].Control;
                        //  //  CheckBox newchecke = newCheckBox();
                        //  ////  newchecke.Checked = che.Checked;
                        //  //  newchecke.BackgroundImage = che.BackgroundImage;
                        //  //  newchecke.Text = dgv[row, col].Text;
                        //  //  dgv[row, col].Control = newchecke;
                        //  //  dgv[row, col].CellType = "Control";
                        //}

                        #endregion

                    }
                }

            }
            catch (Exception)
            {


            }
        }

        List<string> selectchair = new List<string>();
        private void OnClickCh(object sender, EventArgs e)
        {
            CheckBox che = (CheckBox)sender;
            Image img = che.BackgroundImage;

            if (che.Checked)
            {
                if (selectchair.Contains(che.Text))
                {
                    che.BackgroundImage = global::Test.Properties.Resources.Unchecked;
                    che.Checked = false;
                    selectchair.Remove(che.Text);

                    _namechairs = _namechairs.Replace(";f" + che.Text, string.Empty);
                    _namechairs = _namechairs.Replace(";m" + che.Text, string.Empty);
                    _selectchairs.Remove(che.Text);
                }
                else
                {

                    che.Checked = true;
                    if (!_selectchairs.Contains(che.Text))
                    {
                        _namechairs += "m" + che.Text + ";";
                        _selectchairs.Add(che.Text);
                        GenderCustomre = "آقای ";

                    }
                    if (che.AccessibleDescription != "Rezerv")
                    {
                        che.BackgroundImage = Properties.Resources.entekhab_mard;
                    }
                    else { GetRezerTicketsSelectChair(che.Text); }

                }


            }
            else
            {
                if (selectchair.Contains(che.Text))
                {
                    che.BackgroundImage = global::Test.Properties.Resources.Unchecked;
                    che.Checked = false;
                    selectchair.Remove(che.Text);
                    _namechairs = _namechairs.Replace(";f" + che.Text, string.Empty);
                    _namechairs = _namechairs.Replace(";m" + che.Text, string.Empty);
                    _selectchairs.Remove(che.Text);
                    GenderCustomre = String.Empty;
                }
                else
                {
                    if (che.AccessibleDescription != "Rezerv")
                    {
                        che.BackgroundImage = global::Test.Properties.Resources.entekhab_zan;
                        che.Checked = true;
                        selectchair.Add(che.Text);
                        _namechairs = _namechairs.Replace(";m" + che.Text, string.Empty);
                        _namechairs += "f" + che.Text + ";";
                        if (!_selectchairs.Contains(che.Text))
                        {
                            _selectchairs.Add(che.Text);
                        }
                        GenderCustomre = "خانم ";
                    }
                    else
                    {
                        selectchair.Remove(che.Text);
                        _namechairs = _namechairs.Replace(";f" + che.Text, string.Empty);
                        _namechairs = _namechairs.Replace(";m" + che.Text, string.Empty);
                        _selectchairs.Remove(che.Text);
                        uncheckedChair = true;
                        GetRezerTicketsSelectChair(che.Text);
                    }



                }

            }


            ChairsTextEdit.EditValue = _namechairs;
            NumChairsTextEdit.EditValue = _selectchairs.Count;
            txtSumPrice.EditValue = _selectchairs.Count * Convert.ToInt32(PriceTextEdit.EditValue);
            DiscountTextEdit_Leave(null, null);
            NoTextEdit.EditValue = (TripIDTextEdit.Text.Trim() + "-" + (_selectchairs.Count + _chairTicket.Count)).Replace(",", "").ToString();

            if (String.IsNullOrEmpty(FullnameTextEdit.Text) || String.IsNullOrWhiteSpace(FullnameTextEdit.Text))
                FullnameTextEdit.Text = GenderCustomre;
            else
            {

                if (String.IsNullOrEmpty(GenderCustomre) || String.IsNullOrWhiteSpace(GenderCustomre))
                {
                    FullnameTextEdit.Text = FullnameTextEdit.Text.Replace("آقای ", GenderCustomre);
                    FullnameTextEdit.Text = FullnameTextEdit.Text.Replace("خانم ", GenderCustomre);
                    return;
                }

                if (FullnameTextEdit.Text.Contains("خانم"))
                    FullnameTextEdit.Text = FullnameTextEdit.Text.Replace("خانم ", GenderCustomre);
                else
                    //valuefullname= valuefullname.Remove(0, 4);
                    FullnameTextEdit.Text = FullnameTextEdit.Text.Replace("آقای ", GenderCustomre);

            }

            if (!FullnameTextEdit.Text.Contains(GenderCustomre))
                FullnameTextEdit.Text = FullnameTextEdit.Text.Insert(0, GenderCustomre);

        }
        bool uncheckedChair = false;
        private void dgv_CellClick(object sender, GridCellClickEventArgs e)
        {
            try
            {

                //int row = e.RowIndex;
                //int col = e.ColIndex;

                //////  برای چک باکس
                //CheckBox che = (CheckBox)dgv[row, col].Control;

                //if (chaireisRezerv.ContainsKey(dgv[row, col].Text))
                //{
                //    #region با کلیک بر روی صندلی رزور شده سایر نام مشتری نمایش داده می شود و هزینه محاسبه می گردد
                //foreach (Ticket item in _chairTicket)
                //{
                //    if (item.SaleType == 1)
                //    {
                //        List<string> chaires = item.Chairs.Replace("f", string.Empty).Replace("m", string.Empty).Split(';').ToList();
                //        foreach (var ich in chaires)
                //        {
                //            if (ich == che.Text)
                //            {
                //                FullnameTextEdit.EditValue = item.Fullname;
                //                TelTextEdit.EditValue = item.Tel;

                //                    if (_SalechairRezerve.Contains(item))
                //                    {

                //                        _SalechairRezerve.Remove(item);

                //                        _namechairs = _namechairs.Replace(";m" + che.Text, string.Empty);
                //                        _namechairs = _namechairs.Replace(";f" + che.Text, string.Empty);
                //                        if (_selectchairs.Contains(che.Text))
                //                        {
                //                            _selectchairs.Remove(che.Text);
                //                             uncheckedChair = true;

                //                            che.AccessibleDescription = "Rezerv";
                //                            che.Checked = false;
                //                            dgv[row, col].Control = che;
                //                            dgv[row, col].CellType = "Control";

                //                        }
                //                    }
                //                    else
                //                    {
                //                        che.AccessibleDescription = "Rezerv";
                //                        che.Checked = true;
                //                        dgv[row, col].Control = che;
                //                        dgv[row, col].CellType = "Control";
                //                        _SalechairRezerve.Add(item);

                //                    }

                //            }

                //        }
                //    }


                //}
                //    #endregion

                //if(!uncheckedChair)
                //    {

                //    string General = chaireisRezerv[dgv[row, col].Text];

                //    if (General.Contains("خانم"))
                //    {
                //        _namechairs = _namechairs.Replace(";f" + che.Text, string.Empty);
                //        _namechairs = _namechairs.Replace(";m" + che.Text, string.Empty);
                //                _namechairs += "f" + che.Text + ";";
                //                if (!_selectchairs.Contains(che.Text))
                //                {
                //                    _selectchairs.Add(che.Text);
                //                }
                //    }
                //    else
                //    {
                //        _namechairs = _namechairs.Replace(";m" + che.Text, string.Empty);
                //        _namechairs = _namechairs.Replace(";f" + che.Text, string.Empty);
                //        _namechairs += "m" + che.Text + ";";
                //        if (!_selectchairs.Contains(che.Text))
                //        {
                //            _selectchairs.Add(che.Text);
                //        }
                //    }

                //    }

                //txtSumPrice.EditValue = _selectchairs.Count * Convert.ToInt32(PriceTextEdit.EditValue);
                //DiscountTextEdit_Leave(null, null);
                //NoTextEdit.EditValue = (TripIDTextEdit.Text.Trim() + "-" + (_selectchairs.Count + _chairTicket.Count)).Replace(",", "").ToString();
                //GetRezerTicketsSelectChair(dgv[row, col].Text);
                //ChairsTextEdit.EditValue = _namechairs;
                //NumChairsTextEdit.EditValue = _selectchairs.Count;




                //    if (_selectchairs.Count==0)
                //    {
                //        FullnameTextEdit.EditValue = string.Empty;
                //        TelTextEdit.EditValue = string.Empty;
                //    }

                //}


            }
            catch (Exception)
            {


            }

        }


        /// <summary>
        /// لیست مکان شماره صندلی ها در سلولهای گرید
        /// </summary>
        Dictionary<int, string> nameChairtext = new Dictionary<int, string>();

        bool CheckingPingPose()
        {
            Ping p1 = new Ping();
            PingReply PR = p1.Send(Userlogin.IpPose);
            if (PR.Status == IPStatus.Success)
                return true;
            return false;
        }


        bool CheckingPay = false;
        bool CheckingClickPose = false;
        Ticket ticketRezervPayPose = new Ticket();

        /// <summary>
        /// این متغییر برای چک کردن فروش بلیط درحالت پرداخت کارتخوان می باشد . که کاربران دیگر نتوانند این بلیط هارا بفروشند .جهت کنترل در حالت پرداخت کارتخوان
        /// </summary>
        bool Paylock = false;
        // ارسال مبلغ به دستگاه
        private void btnPose_ItemClick(object sender, ItemClickEventArgs e)
        {

            try
            {
                FullnameTextEdit.Focus();
                if (dxValidationProvider1.Validate())
                {

                    if (_selectchairs.Count == 0)
                    {
                        XtraMessageBox.Show("کاربر گرامی لطفا صندلی مورد نظر را انتخاب نمایید.", "", MessageBoxButtons.OK);
                        return;
                    }

                    if (!CheckingPingPose())
                    {
                        XtraMessageBox.Show("اتصال به کارتخوان مقدور نمی باشد . لطفا آدرس دستگاه یا وضعیت اتصال را بررسی نمایید", "", MessageBoxButtons.OK);
                        return;
                    }

                    IDTicketcompany = 0;

                    ///     شناسایی اطلاعات دستگاه کارتخوان مورد نظر
                    int TotalPrice = Convert.ToInt32(txtSumPrice.EditValue);

                    Mablagh = TotalPrice.ToString();

                    IpPos = Userlogin.IpPose;
                    typePos = Userlogin.PoseName;

                    PayCash = false;
                    Paylock = true;

                    CheckingPay = false;
                    TypeTicket = "پرداخت کارتخوان";

                    InsertTicket();// ثبت بلیط قبل از عملیات پرداخت کارتخوان

                    if (CheckingPay)
                    {
                        return;
                    }

                    CheckingClickPose = true;

                    if (!BgPose.IsBusy)
                    {
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);

                        ////Start work
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(() =>
                            {

                                txtStatusPayment.EditValue = "ارسال اطلاعات به دستگاه ....";
                                txtStatusPayment.ForeColor = Color.Purple;
                                // load the control with the appropriate data

                            }));
                        }
                        else
                        {
                            txtStatusPayment.EditValue = "ارسال اطلاعات به دستگاه ....";
                            txtStatusPayment.ForeColor = Color.Purple;
                            // load the control with the appropriate data
                        }

                        btnPose.Enabled = false;
                        BgPose.RunWorkerAsync();

                    }
                    else
                    {
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                        XtraMessageBox.Show("سیستم در حالت ارتباط با دستگاه کارتخوان می باشد .لطفا چند لحظه  شکیبا باشید", "پیام", MessageBoxButtons.OK);
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                    }

                }
                else
                {
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("لطفا داده های مورد نیاز را وارد نمایید", "پیام", MessageBoxButtons.OK);
                }


            }
            catch (Exception ex)
            {

                ChangyPayLockPosFailed();

               GetError.WriteError(ex,this);
                
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }
        }


        private void BgPose_DoWork(object sender, DoWorkEventArgs e)
        {

            /// توابع ارسال
            PayPosePrice();

        }

        private async void btnCancelPose_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);


                BgPose.Dispose();
                BgPose.CancelAsync();

                if (CheckingClickPose)
                {

                    ChangyPayLockPosFailed();

                    await Task.Run(() =>
                    {
                        CancelPay();
                    });
                }


            }
            catch (Exception ex)
            {

                ChangyPayLockPosFailed();
               GetError.WriteError(ex,this);
                
            }


        }




        //***************************************************************                کدهای پرداخت کارتخوان               ***********************************************************************************
        PcPosFactory _PcPosFactory;
        MediaType _mediaType = MediaType.Network;
        AccountType _accountType = AccountType.Single;
        ResponseLanguage _responseLanguage;
        AsyncType _asyncType = AsyncType.Sync;
        ReportAction _reportAction;
        ReportFilterType _reportFilterType;
        TransactionType _tracsactionType;
        PcPosDll.PcPosBusiness PcPos = new PcPosDll.PcPosBusiness();
        PcPosDll.PcPosDiscovery searchPos = new PcPosDll.PcPosDiscovery();
        public string ShomreErja { get; set; }
        public string ShomarePeygiri { get; set; }
        public string ParentIDKart { get; set; }
        public bool PaymentPostActive { get; set; }
        public string Mablagh { get; set; }
        private void FrmTickets_FormClosing(object sender, FormClosingEventArgs e)
        {

            btnCancelPose_ItemClick(null, null);
            //e.Cancel = true;
            //this.Hide();
        }

        public void CancelPay()
        {
            try
            {

                if (!BgPose.IsBusy)
                {
                    BgPose.CancelAsync();
                }

                // Fwait.Close();


                if (typePos != null)
                {
                    if (typePos.Contains("سداد"))
                    {
                        if (PcPos == null)
                            PcPos = new PcPosBusiness();

                        if (InvokeRequired)
                        {
                            // after we've done all the processing, 
                            this.Invoke(new MethodInvoker(delegate
                            {

                                txtStatusPayment.EditValue = "عملیات دریافت از کارتخوان لغو گردید";
                                txtStatusPayment.ForeColor = Color.Red;
                                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                                // load the control with the appropriate data
                            }));

                        }


                        // لغو عملیات پرداخت
                        PcPos.AbortPcPosOperation();
                    }
                    if (typePos.Contains("سامان کیش"))
                    {
                        if (_PcPosFactory == null)
                        {
                            _PcPosFactory = new PcPosFactory();
                        }

                        _PcPosFactory.Dispose();

                        if (InvokeRequired)
                        {
                            // after we've done all the processing, 
                            this.Invoke(new MethodInvoker(delegate
                            {
                                txtStatusPayment.EditValue = "عملیات دریافت از کارتخوان لغو گردید";
                                txtStatusPayment.ForeColor = Color.Red;
                                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                                // load the control with the appropriate data
                            }));

                        }







                    }


                    if (typePos.Contains("آسان پرداخت"))
                    {

                        if (PcposAsanPardakht == null)
                        {
                            PcposAsanPardakht = new PosInterface.PCPos();
                        }

                        if (InvokeRequired)
                        {
                            // after we've done all the processing, 
                            this.Invoke(new MethodInvoker(delegate
                            {
                                txtStatusPayment.EditValue = "عملیات دریافت از کارتخوان لغو گردید";
                                txtStatusPayment.ForeColor = Color.Red;
                                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                                // load the control with the appropriate data
                            }));

                        }

                        PcposAsanPardakht.Stop();
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    }
                    //         if (typePos.Contains("ملت")) {
                    //             mellat.StopProcessing();
                    //txtStatusPayment.EditValue="عملیات دریافت از کارتخوان لغو گردید";

                    //         }
                }

                PaymentPostActive = false;


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
            finally
            {
                CheckingClickPose = false;
                btnPose.Enabled = true;
            }


        }


        /// <summary>
        ///  ارسال مبلغ به دستگاه مورد نظر کاربر
        /// </summary>
        public void PayPosePrice()
        {
            try
            {

                //OperationSaveFact = true;


                if (typePos.Contains("سداد"))
                {
                    PaymentPostActive = true;
                    if (PcPos == null)
                        PcPos = new PcPosDll.PcPosBusiness();

                    {
                        PcPos.PcPosIp = IpPos;
                        PcPos.PcPosPort = 8888;
                        PcPos.PcPosMode = PcPosConnectionType.Lan;
                        PcPos.Pc2PosAmount = Mablagh;
                        PcPos.PcPosRetryTimeOut = new int[] {
                5000,
                5000,
                5000
            };
                        PcPos.PcPosResponseTimeout = new int[] {
                180000,
                5000,
                5000
            };
                        PcPos.OnPcPosResult += PcPosSaleResult;
                        PcPos.AsyncPcPosSaleTransaction();

                    }
                }

                if (typePos.Contains("سامان کیش"))
                {
                    PaymentPostActive = true;
                    _PcPosFactory = new PcPosFactory();
                    if (PurchaseInitialization())
                    {
                        return;
                    }

                    List<string> lstAmnts = new List<string>();
                    SSP1126.PcPos.Infrastructure.PosResult posResult = new SSP1126.PcPos.Infrastructure.PosResult();
                    string TID = null;
                    if (_accountType == AccountType.Single)
                    {
                        posResult = _PcPosFactory.PcStarterPurchase(Mablagh, string.Empty, null, "", TID);
                    }
                    if (_asyncType == AsyncType.Sync && posResult != null)
                    {
                        PurchaseResultReceived(posResult);
                    }
                    else
                    {
                        ChangyPayLockPosFailed(); // لغو عملیات سامان کیش
                    }
                }



                if (typePos.Contains("آسان پرداخت"))
                {
                    PcposAsanPardakht.InitLAN(IpPos, 17000);
                    PcposAsanPardakht.DoASyncPayment(Mablagh, "", "", DateTime.Now, this);


                }

                //if (typePos.Contains("ملت"))
                //{
                //    string ErrorMessage;
                //    string NumberError;
                //    string NumberPeygiri;
                //    string NumberErja;
                //    string ReasonCode;

                //    mellat = new MellatPose();
                //    bool Response = mellat.PeymentFunc(Mablagh, IpPos, 1024, ShomarePeygiri, ShomreErja, NumberError, ReasonCode);

                //    switch (Response)
                //    {

                //        case true:
                //            Invoke(new MethodInvoker(() =>
                //            {
                //                System.Threading.Thread.Sleep(1000);
                // txtStatusPayment.EditValue= "پرداخت با موفقیت انجام شد";

                //                //ShomarePeygiri = NumberPeygiri
                //                //ShomreErja = NumberErja
                //                PaymentPostActive = false;
                //               // Fwait.Close();

                //                   /// ثبت فاکتور

                //            }));

                //            break;
                //        case false:
                //            PaymentPostActive = false;
                //            btnCancelPose.Enabled = false;
                //        
                //  txtStatusPayment.EditValue=   "عملیات پرداخت ناموفق بود" ;

                //           // Fwait.Close();
                //            /// عملیات پرداخت ناموفق
                //            break;


                //    }

                //}

            }
            catch (Exception ex)
            {
                ChangyPayLockPosFailed();

               GetError.WriteError(ex,this);
                
            }
        }

        //  نتیجه عملیات پرداخت دستگاه سداد
        private void PcPosSaleResult(object sender, PcPosDll.PosResult e)
        {
            //AdUPos.ShowGuides = DevExpress.Utils.DefaultBoolean.True


            Invoke(new MethodInvoker(() =>
            {
                System.Threading.Thread.Sleep(1000);

                if (e.ResponseCode == "00")
                {
                    TypeTicket = "پرداخت کارتخوان";

                    ChangyPayLockPosSuccess();// پرداخت کارتخوان سداد

                    txtStatusPayment.EditValue = "پرداخت با موفقیت انجام شد";
                    txtStatusPayment.ForeColor = Color.Green;
                    ShomarePeygiri = e.ApprovalCode.ToString();
                    ShomreErja = e.TransactionNo;
                    PaymentPostActive = false;
                    /// عملیات ثبت فاکتور

                }
                else
                {
                    ChangyPayLockPosFailed();// لغو عملیات سداد

                    txtStatusPayment.EditValue = "عملیات پرداخت ناموفق بود";
                    txtStatusPayment.ForeColor = Color.Red;
                    btnPose.Enabled = true;
                    /// عملیات پرداخت ناموفق
                }

                PcPos.OnPcPosResult -= PcPosSaleResult;
                PcPos = null;

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);

            }));

        }

        private void TripIDTextEdit_Validated(object sender, EventArgs e)
        {
            dxValidationProvider1.Validate();
        }

        private void DiscountTextEdit_Leave(object sender, EventArgs e)
        {


            if (txtSumPrice.EditValue != null)
            {
                if (DiscountTextEdit.EditValue == DBNull.Value || DiscountTextEdit.EditValue == null)
                    DiscountTextEdit.EditValue = 0;

                int percent = Convert.ToInt32(DiscountTextEdit.EditValue) * _selectchairs.Count;
                int price = Convert.ToInt32(PriceTextEdit.EditValue);

                int sumprice = price * _selectchairs.Count;

                //double _percentPrice = (sumprice / 100) * Convert.ToInt32(percent);                  // مبلغ درصد به ریال
                txtSumPrice.EditValue = sumprice - percent;

            }



        }

        private void DiscountTextEdit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DiscountTextEdit_Leave(null, null);
            }
        }

        private void FrmTickets_Load(object sender, EventArgs e)
        {
            CityIDTextEdit.EditValue = IDEndCity;

            timerGraphic.Start();
        }

        private void FullnameTextEdit_Enter(object sender, EventArgs e)
        {
            try
            {



                System.Globalization.CultureInfo fa = new System.Globalization.CultureInfo("fa-IR");
                InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(fa);
            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
            }

        }

        private void FullnameTextEdit_Leave(object sender, EventArgs e)
        {
            try
            {

                System.Globalization.CultureInfo fa = new System.Globalization.CultureInfo("fa-IR");
                InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(fa);

                if (string.IsNullOrEmpty(FullnameTextEdit.Text) || string.IsNullOrEmpty(FullnameTextEdit.Text))
                {

                    FullnameTextEdit.Text = GenderCustomre;

                }
                else
                {
                    if (!FullnameTextEdit.Text.Contains(GenderCustomre))
                    {
                        FullnameTextEdit.Text = FullnameTextEdit.Text.Insert(0, GenderCustomre);
                    }
                }

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
            }

        }

        private void timerGraphic_Tick(object sender, EventArgs e)
        {
            ShowNumberChairGraphic();
            timerGraphic.Stop();
        }

        private void FullnameTextEdit_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            try
            {

                //System.Globalization.CultureInfo fa = new System.Globalization.CultureInfo("fa-IR");
                //InputLanguage.CurrentInputLanguage = InputLanguage.FromCulture(fa);
            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
            }

        }

        private void btnListTickets_ItemClick(object sender, ItemClickEventArgs e)
        {
            ShowTicketService();
        }

        /// <summary>
        /// تابع مشاهده بلیط های یک سرویس
        /// </summary>
        void ShowTicketService()
        {
            try
            {


                Trip _trip = TripServices.FindByID(int.Parse(CodeTrip), ConnectionCompany);
                string _way = TripWay;

                if (_trip.Locked == true)
                {
                    XtraMessageBox.Show("کاربر گرامی سرویس مورد نظر قفل می باشد", "پیام", MessageBoxButtons.OK);
                    return;
                }

                Form frmticket = Application.OpenForms["FrmReserve"];
                if (frmticket == null)
                {
                    FrmTicketsTrips fr = new FrmTicketsTrips();
                    fr.TripID = int.Parse(CodeTrip);
                    fr.CodeCompany = CodeCompany;
                    fr.ConnectionCompany = ConnectionCompany;
                    fr.TitleCompany = CompanyName;
                    fr.Logo = Logo;
                    fr.Owner = this;
                    fr.MoveTime = MoveTimeDate;
                    fr.CodeTrip = CodeTrip;
                    fr.TripWay = _way;
                    fr.DayTrip = DayTrip;
                    fr.DateTripPersian = DateTripPersian;
                    fr.TimeTrip = TimeTrip;
                    fr.TypeTrip = TypeTrip;

                    fr.LoadList();
                    //fr.ShowDialog(this);


                    fr.Show();
                    fr.BringToFront();
                }
                else
                {
                    FrmTicketsTrips fm = (FrmTicketsTrips)frmticket;
                    fm.TripID = int.Parse(CodeTrip);
                    fm.CodeCompany = CodeCompany;
                    fm.ConnectionCompany = ConnectionCompany;
                    fm.TitleCompany = CompanyName;
                    fm.Logo = Logo;
                    fm.Owner = this;
                    fm.MoveTime = MoveTimeDate;

                    fm.CodeTrip = CodeTrip;
                    fm.TripWay = _way;
                    fm.DayTrip = DayTrip;
                    fm.DateTripPersian = DateTripPersian;
                    fm.TimeTrip = TimeTrip;
                    fm.TypeTrip = TypeTrip;
                    fm.LoadList();
                    //fm.ShowDialog(this);

                    fm.Show();
                    fm.BringToFront();
                }


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }

        }

        private void FullnameTextEdit_EditValueChanged(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(FullnameTextEdit.Text) || string.IsNullOrEmpty(FullnameTextEdit.Text))
            {

                FullnameTextEdit.Text = GenderCustomre;

            }
            else
            {
                if (!FullnameTextEdit.Text.Contains(GenderCustomre))
                {
                    FullnameTextEdit.Text = GenderCustomre + FullnameTextEdit.Text;
                }
            }
        }

        private void PriceTextEdit_EditValueChanged(object sender, EventArgs e)
        {
            txtSumPrice.EditValue = _selectchairs.Count * Convert.ToInt32(PriceTextEdit.EditValue);
        }


        //   دریافت نتیجه پرداخت سامان کیش
        private void PurchaseResultReceived(SSP1126.PcPos.Infrastructure.PosResult posResult)
        {

            if (posResult == null)
            {

                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        ChangyPayLockPosFailed(); // لغو عملیات سامان کیش
                        txtStatusPayment.EditValue = "عملیات پرداخت ناموفق بود";
                        txtStatusPayment.ForeColor = Color.Red;
                        btnPose.Enabled = true;

                    }));
                }
                else
                {
                    ChangyPayLockPosFailed(); // لغو عملیات سامان کیش
                    txtStatusPayment.EditValue = "عملیات پرداخت ناموفق بود";
                    txtStatusPayment.ForeColor = Color.Red;
                    btnPose.Enabled = true;
                }
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }
            //Successful result
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(() =>
                {
                    if (posResult.ResponseCode == "00")
                    {
                        TypeTicket = "پرداخت کارتخوان";

                        ChangyPayLockPosSuccess();   // پرداخت کارتخوان سامان کیش

                        txtStatusPayment.EditValue = "پرداخت با موفقیت انجام شد";
                        txtStatusPayment.ForeColor = Color.Green;

                        ShomarePeygiri = posResult.TraceNumber;
                        ShomreErja = posResult.RRN;
                        PaymentPostActive = false;
                        /// عملیات ثبت فاکتور
                    }
                    else
                    {
                        ChangyPayLockPosFailed(); // لغو عملیات سامان کیش

                        txtStatusPayment.EditValue = "عملیات پرداخت ناموفق بود";
                        txtStatusPayment.ForeColor = Color.Red;
                        btnPose.Enabled = true;
                    }
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                }));
            }
        }

        public void OnFinish(string message)
        {
            throw new NotImplementedException();
        }

        // نتیجه پرداخت آسان پرداخت
        public void OnTransactionDone(TransactionResult result)
        {
            {
                if (this.InvokeRequired)
                {

                    this.Invoke(new MethodInvoker(() =>
                    {
                        if (result.ErrorCode == 0)
                        {
                            TypeTicket = "پرداخت کارتخوان";

                            ChangyPayLockPosSuccess(); // پرداخت کارتخوان آسان پرداخت

                            txtStatusPayment.EditValue = "پرداخت با موفقیت انجام شد";
                            txtStatusPayment.ForeColor = Color.Green;
                            ShomarePeygiri = result.Stan;
                            ShomreErja = result.RRN;
                            PaymentPostActive = false;
                            ///  عملیات ثبت فاکتور
                        }
                        else
                        {

                            ChangyPayLockPosFailed();

                            txtStatusPayment.EditValue = "عملیات پرداخت ناموفق بود";
                            txtStatusPayment.ForeColor = Color.Red;

                            btnPose.Enabled = true;
                        }
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    }));
                }

            }
        }


        //MellatPose mellat = new MellatPose();
        PosInterface.PCPos PcposAsanPardakht = new PosInterface.PCPos();
        private bool PurchaseInitialization()
        {

            _tracsactionType = TransactionType.Purchase;

            if (TransactionMediaInitialization())
            {
                return false;
            }

            _PcPosFactory.Initialization(_responseLanguage, 0, _asyncType);
            return false;
        }
        private bool TransactionMediaInitialization()
        {
            //If _mediaType = MediaType.Network Then
            if (string.IsNullOrEmpty(IpPos))
            {
                //XtraMessageBox.Show("There is no value for Pos IP in configurations.", "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
                txtStatusPayment.EditValue = "ای پی دستگاه اشتباه می باشد";
                txtStatusPayment.ForeColor = Color.Red;
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                return true;
            }
            _PcPosFactory.SetLan(IpPos);
            //End If
            _PcPosFactory.Initialization(_responseLanguage, 0, _asyncType);
            return false;
        }

        //***************************************************************************************************************************************************************************
        Dictionary<string, string> LocationChairinGRD = new Dictionary<string, string>();

        /// <summary>
        /// مشخص کردن کلیه بلیط های رزوری با انتخاب یک بلیط
        /// </summary>
        /// <param name="chair">صندلی رزوری</param>
        void GetRezerTicketsSelectChair(string chair)
        {

            try
            {
                return;

                foreach (var item in _chairRezervTicket)
                {
                    string chairs = item.Chairs.Replace("m", string.Empty).Replace("f", string.Empty).Trim(';');
                    List<string> listchair = chairs.Split(';').ToList();


                    foreach (var ch in listchair)
                    {
                        if (ch == chair)
                        {

                            foreach (var ch2 in listchair)
                            {
                                if (ch2 != chair)
                                {

                                    for (int rowdgv = 0; rowdgv < columns + 1; rowdgv++)
                                    {
                                        for (int col = 0; col < rows + 2; col++)
                                        {
                                            string value = dgv[rowdgv, col].Text.ToString();
                                            value = value.Replace("آقا", string.Empty).Replace("خانم", string.Empty);
                                            if (value == ch2)
                                            {

                                                lock (_selectchairs)
                                                {
                                                    foreach (string c in _selectchairs.ToList())
                                                    {
                                                        if (!_selectchairs.Contains(ch2) && uncheckedChair == false)
                                                        {
                                                            _selectchairs.Add(ch2);
                                                            string san = item.Chairs;
                                                            List<string> lstchair = san.Split(';').ToList();
                                                            _namechairs = _namechairs.Replace(";m" + ch2, string.Empty);
                                                            _namechairs = _namechairs.Replace(";f" + ch2, string.Empty);

                                                            if (lstchair.Contains("f" + ch2 + ""))
                                                            {
                                                                _namechairs += "f" + ch2 + ";";

                                                            }
                                                            else
                                                            {

                                                                _namechairs += "m" + ch2 + ";";
                                                            }



                                                            #region  تیک سایر صندلی های این بلیط

                                                            if (LocationChairinGRD.ContainsKey(ch2))
                                                            {
                                                                string locate = LocationChairinGRD[ch2];
                                                                string[] loc = locate.Split(';');
                                                                int row = int.Parse(loc[0]);
                                                                int column = int.Parse(loc[1]);

                                                                CheckBox newchecke = (CheckBox)dgv[row, column].Control;
                                                                newchecke.Text = dgv[row, column].Text;
                                                                newchecke.Checked = true;
                                                                dgv[row, column].Control = newchecke;
                                                                dgv[row, column].CellType = "Control";
                                                            }
                                                            #endregion


                                                        }
                                                        else
                                                        {
                                                            if (uncheckedChair)
                                                            {
                                                                if (_selectchairs.Contains(ch2))
                                                                {

                                                                    _selectchairs.Remove(ch2);
                                                                    _namechairs = _namechairs.Replace(";m" + ch2, string.Empty);
                                                                    _namechairs = _namechairs.Replace(";f" + ch2, string.Empty);

                                                                    #region برداشتن تیک سایر صندلی های این بلیط

                                                                    if (LocationChairinGRD.ContainsKey(ch2))
                                                                    {
                                                                        string locate = LocationChairinGRD[ch2];
                                                                        string[] loc = locate.Split(';');
                                                                        int row = int.Parse(loc[0]);
                                                                        int column = int.Parse(loc[1]);

                                                                        CheckBox newchecke = (CheckBox)dgv[row, column].Control;
                                                                        newchecke.Text = dgv[row, column].Text;
                                                                        newchecke.Checked = false;
                                                                        dgv[row, column].Control = newchecke;
                                                                        dgv[row, column].CellType = "Control";
                                                                    }
                                                                    #endregion
                                                                }
                                                            }
                                                        }

                                                    }
                                                }






                                            }
                                        }
                                    }


                                }
                            }


                        }
                    }

                }

                NumChairsTextEdit.EditValue = _selectchairs.Count;
                txtSumPrice.EditValue = _selectchairs.Count * Convert.ToInt32(PriceTextEdit.EditValue);
                DiscountTextEdit_Leave(null, null);
                NoTextEdit.EditValue = (TripIDTextEdit.Text.Trim() + "-" + (_selectchairs.Count + _chairTicket.Count)).Replace(",", "").ToString();

                uncheckedChair = false;
            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);

            }


        }

    }

}


