﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraGrid;

using Model;
using BL;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using System.Threading.Tasks;

namespace Test
{
    public partial class FrmReportUserTicket : DevExpress.XtraBars.Ribbon.RibbonForm
    {


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    btnClose_ItemClick(null, null);
                    break;
                case Keys.F4:
                    btnClose_ItemClick(null, null);
                    break;
                case Keys.F5:
                    btnRefresh_ItemClick(null, null);
                    break;
                case Keys.F6:
                    btnReport_ItemClick(null, null);
                    break;


            }

            return base.ProcessCmdKey(ref msg, keyData);
        }



        public FrmReportUserTicket()
        {
            InitializeComponent();

            if (Userlogin.isAdmin)
                layUsers.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            LoadData();

        }


        List<Company> AccessCompanyUser;


        /// <summary>
        /// نمایش  اطلاعات کاربر یا شرکتها
        /// </summary>
        async void LoadData()
        {
            try
            {
                List<Company> listcompanys = new List<Company>();
                List<User> listuser = new List<User>();

                listcompanys = CompanysServices.GetAll();
                if (Userlogin.isAdmin)
                    listuser = UserServices.GetAll();


                reConpany.DataSource = listcompanys;
                ChCompany.DataSource = listcompanys;
                cmbUsers.Properties.DataSource = listuser;

                txtFromDate.DateTime = DateTime.Now;
                txtTodate.DateTime = DateTime.Now;

                User user = UserServices.FindByID(Userlogin.ID);

                if (user.AccessCompanies == null)
                {

                    XtraMessageBox.Show("کاربر گرامی سطح دسترسی شما تعریف نشده است", "پیام", MessageBoxButtons.OK);
                    return;
                }

                List<string> _CodeCompanyUser = user.AccessCompanies.Split(';').ToList();    //لیست کد های شرکت های سطح دسترسی کاربر
                AccessCompanyUser = new List<Company>();  //  لیست شرکت های سطح دسترسی


                await Task.Run(() =>
                {
                    foreach (string item in _CodeCompanyUser)
                    {
                        if (!string.IsNullOrEmpty(item) || !string.IsNullOrWhiteSpace(item))
                        {
                            Company co = CompanysServices.FindByCodeEnable(item, true);
                            if (co != null)
                            {
                                AccessCompanyUser.Add(CompanysServices.FindByCodeEnable(item, true));
                            }

                        }
                    }
                });


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }

        }


        /// <summary>
        /// گزارش
        /// </summary>
        /// <param name="_fromdate">تاریخ شروع</param>
        /// <param name="_todate">تاریخ اتمام</param>

        async void Report(DateTime _fromdate, DateTime _todate)
        {
            try
            {

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);

                _todate = Convert.ToDateTime(_todate.ToShortDateString());

                _todate = _todate.AddHours(23).AddMinutes(59).AddSeconds(59);
                List<TicketLocal> li = new List<TicketLocal>();
                List<TicketLocal> lirollback = new List<TicketLocal>();

                await Task.Run(() =>
                 {
                     if (Userlogin.isAdmin)
                     {
                         string userCode = "";
                         if (cmbUsers.EditValue == null)
                             userCode = Userlogin.Code;

                         li = TicketServices.GetTicketByDate(_fromdate, _todate, Userlogin.isAdmin, userCode, AccessCompanyUser, Userlogin.IsLoacConnection);
                         lirollback = TicketServices.GetTicketRollBakByDate(_fromdate, _todate, Userlogin.isAdmin, Userlogin.Code, AccessCompanyUser, Userlogin.IsLoacConnection);
                         li.AddRange(lirollback);
                     }
                     else
                     {

                         li = TicketServices.GetTicketByDate(_fromdate, _todate, Userlogin.isAdmin, Userlogin.Code, AccessCompanyUser, Userlogin.IsLoacConnection);
                         lirollback = TicketServices.GetTicketRollBakByDate(_fromdate, _todate, Userlogin.isAdmin, Userlogin.Code, AccessCompanyUser, Userlogin.IsLoacConnection);
                         li.AddRange(lirollback);


                     }

                 });

                gridControl1.DataSource = li;


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
            finally
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }
        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();

        }

        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }




        private string GetFilterStringBindedColumns(string column, CheckedListBoxControl CheckListBox)
        {



            CheckedListBoxControl clbc = (CheckedListBoxControl)CheckListBox;
            string str = string.Empty;

            for (int i = 0; i <= clbc.CheckedItems.Count - 1; i++)
            {
                if (str != string.Empty)
                {
                    str = str + "OR";
                }
                str = str + "[" + column + "] = '" + clbc.CheckedItems[i].ToString() + "' ";
            }

            return str;


        }


        // نماش سرویس های شرکت
        private void ChCompany_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            try
            {
                string str = GetFilterStringBindedColumns("CompanyCode", ChCompany);
                if (string.IsNullOrEmpty(str))
                {
                    grd.ActiveFilter.Clear();
                }
                else
                {
                    grd.Columns["CompanyCode"].FilterInfo = new DevExpress.XtraGrid.Columns.ColumnFilterInfo(str);
                }

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        private void btnReport_ItemClick(object sender, ItemClickEventArgs e)
        {

            Report(Convert.ToDateTime(txtFromDate.DateTime.ToShortDateString()), Convert.ToDateTime(txtTodate.DateTime.ToShortDateString()));
        }

        private void grd_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {

            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void frmticketListTicketUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void cmbUsers_EditValueChanged(object sender, EventArgs e)
        {
            string str = "[CashierCode] = '" + cmbUsers.EditValue + "' ";
            grd.Columns["CashierCode"].FilterInfo = new ColumnFilterInfo(str);

        }


        // گزارش بر اساس زمان
        private void timeEdit2_EditValueChanged(object sender, EventArgs e)
        {
            //string str = "[TimeSale] >='" + timeEdit1.EditValue + "' and [TimeSale] <='"+ timeEdit2.EditValue +"'";
            //grd.Columns["TimeSale"].FilterInfo = new ColumnFilterInfo(str);


            grd.ActiveFilterString = "[TimeSale] >='" + Convert.ToDateTime(timeEdit1.EditValue).ToShortTimeString() + "' and [TimeSale] <='" + Convert.ToDateTime(timeEdit2.EditValue).ToShortTimeString() + "'";

        }

        private void txtFromDate_Click(object sender, EventArgs e)
        {
            txtFromDate.ShowPopup();
        }

        private void txtTodate_Click(object sender, EventArgs e)
        {
            txtTodate.ShowPopup();
        }

        private void cmbUsers_Click(object sender, EventArgs e)
        {
            cmbUsers.ShowPopup();
        }


        // چاپ گزارش
        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!gridControl1.IsPrintingAvailable)
            {
                XtraMessageBox.Show("امکان چاپ وجود ندارد", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            // Open the Preview window. 
            gridControl1.ShowPrintPreview();

        }

        private void ribbon_Click(object sender, EventArgs e)
        {

        }

        private void BtnTicketInternet_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);


                DateTime _from = DateTime.Parse(txtFromDate.DateTime.ToShortDateString());
                DateTime dtto = Convert.ToDateTime(txtTodate.DateTime.ToShortDateString());
                DateTime _todate = dtto.AddHours(23).AddMinutes(59).AddSeconds(59);

                if (Userlogin.isAdmin)
                {
                    string userCode = "";
                    if (cmbUsers.EditValue == null)
                        userCode = Userlogin.Code;

                    gridControl1.DataSource = TicketServices.GetTicketNetByDate(_from, _todate, AccessCompanyUser, Userlogin.IsLoacConnection);
                }
                else
                {
                    gridControl1.DataSource = TicketServices.GetTicketNetByDate(_from, _todate, AccessCompanyUser, Userlogin.IsLoacConnection);

                }





            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
            finally
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }


        }

        private void frmticketListTicketUser_Load(object sender, EventArgs e)
        {
            Report(Convert.ToDateTime(DateTime.Now.ToShortDateString()), Convert.ToDateTime(DateTime.Now.ToShortDateString()));

        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void grd_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName != "Time") return;
            int row = e.ListSourceRowIndex;
            string time = grd.GetListSourceRowCellValue(row, colTimeSale).ToString();
            DateTime date = DateTime.Parse(grd.GetListSourceRowCellValue(row, colSaleDate).ToString());
            e.Value = date.JulianToPersianDate() + "  " + time;




        }
    }
}