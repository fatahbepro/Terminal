﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Model;
using BL;
using DevExpress.XtraEditors;


using SSP1126.PcPos;
using PcPosDll;
using PosInterface;
using SSP1126.PcPos.Infrastructure;
using SSP1126.PcPos.BaseClasses;

using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.IO.Ports;
using DevExpress.XtraReports.UI;
using System.Threading.Tasks;
using System.Drawing.Printing;
using System.Net.NetworkInformation;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.Utils;


//using Newtonsoft.Json;
//using System.ServiceProcess;

namespace Test
{
    public partial class FrmTicketList : DevExpress.XtraBars.Ribbon.RibbonForm, PosInterface.ITransactionDoneHandler
    {


        public FrmTicketList()
        {
            InitializeComponent();
            txtFromDate.DateTime = DateTime.Now;
        }

        #region Property


        public int TicketIdFund;
        List<TicketLocal> listticket;
        public int TripID { get; set; }
        public string ConnectionCompany { get; set; }
        public string CodeCompany { get; set; }
        public string TitleCompany { get; set; }
        public byte[] Logo { get; set; }
        public int IDService { get; set; }

        public List<Company> AccessCompanyUser;
        DateTime FilterTimeUser;
        #endregion

        #region محاسبه سهم صندوقدار از  فروش بلیط فعلی
        /// <summary>
        /// سهم صندوقدار
        /// </summary>
        private int FundPriceCash { get; set; }

        void CalculateFundPrice(int PriceTicket, int countchair)
        {

            Service _service = ServicesServiceeee.GetService(IDService, ConnectionCompany);
            if (_service != null)
            {
                if (_service.FundShare != null)
                {
                    Double FundShareTrip = _service.FundShare.Value;

                    if (FundShareTrip < 0) /// محاسبه سهم صندوق در صورت منفی بودن این فیلد
                    {
                        FundShareTrip = FundShareTrip * -1;
                        FundPriceCash = (Convert.ToInt32(FundShareTrip) * PriceTicket) / 100;
                    }
                    else if (FundShareTrip >= 0) ///  اگر مقدار این فیلد از صفر بزرگتر بود سهم صندوق 0 شود
                        FundPriceCash = 0;
                    return;
                }


            }


            string fund = SettingLogin.GetValue("FundPrice");
            fund = fund == string.Empty ? "0" : fund;
            if (fund.Contains("%"))
            {
                fund = fund.Replace("%", string.Empty);
                FundPriceCash = (int.Parse(fund) * PriceTicket) / 100;
            }
            else
            {

                FundPriceCash = int.Parse(fund);
            }

        }
        #endregion


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    barButtonItem2_ItemClick(null, null);
                    break;
                case Keys.F9:
                    BtnbackSale_ItemClick(null, null);
                    break;

                case Keys.F8:
                    btnCancelPose_ItemClick(null, null);
                    break;
                case Keys.F4:
                    barButtonItem2_ItemClick(null, null);
                    break;

            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        public void LoadData()
        {
            reCompany.DataSource = ChCompany.DataSource = AccessCompanyUser;
            barCreditUser.EditValue = Userlogin.Credit;
        }

        public int StatusTickets { get; set; }

        /// <summary>
        /// نمایش بلیط های سرویس مورد نظر
        /// </summary>
        /// <param name="fromdate">بلیط های این تاریخ را فیلتر میکند</param>
        /// <param name="statusTicket"> وضعیت بلیط که ایا رزروی می باشد یا فروخته شده</param>
        public void LoadListTicket(DateTime fromdate)
        {
            try
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                string Overtime = SettingLogin.GetValue("ServiceOvertime");
                int ServiceOvertime = 0;
                if (!string.IsNullOrEmpty(Overtime))
                    ServiceOvertime = int.Parse(Overtime);
                barCreditUser.EditValue = Userlogin.Credit;
                txtFromDate.DateTime = fromdate;
                FilterTimeUser = fromdate;
                DateTime _from = DateTime.Parse(fromdate.ToShortDateString());
                DateTime _todate = _from;
                _todate = _todate.AddHours(23).AddMinutes(59).AddSeconds(59);
                listticket = TicketServices.GetListTicketDate(_from, _todate, AccessCompanyUser, Userlogin.IsLoacConnection, StatusTickets);
                listticket.RemoveAll(c => c.MoveTime.Value.AddMinutes(ServiceOvertime) < DateTime.Now && c.SaleType == 1);
                dgv.DataSource = listticket;
            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }
            finally
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();

        }

        /// <summary>
        ///  چاپ بلیط
        /// </summary>
        /// <param name="_tic"></param>
        /// <param name="_companytitle"></param>
        /// <param name="_totalPrice"></param>
        /// <param name="_CityStart"></param>
        /// <param name="_typebus"></param>
        void GetTicktPrint(Ticket _tic, string _companytitle, int _totalPrice, string _CityStart, string _typebus)
        {

            try
            {

                Trip _trip = TripServices.FindByID(_tic.TripID, ConnectionCompany);
                DateTime saledt = _trip != null ? _trip.Date : _tic.SaleDate.Value;
                string nameday = saledt.GetNameOfDayDate();
                string destCity = "";
                if (!(_tic.CityID > 0))
                {
                    City city = CityService.FindByCode(_tic.CityCode, ConnectionCompany);
                    if (city != null)
                    {
                        destCity = city.Title;
                    }
                }
                TicketReport _ticprint = new TicketReport()
                {
                    ID = _tic.ID,

                    TripID = _tic.TripID,
                    Man = _tic.Man,
                    Fullname = _tic.Fullname,
                    No = _tic.No,
                    Chairs = _tic.Chairs.Replace("f", string.Empty).Replace("m", string.Empty),
                    CityID = _tic.CityID,
                    Tel = _tic.Tel,
                    Price = _tic.Price + _tic.FundPrice + _tic.OnlinePrice,
                    NumChairs = _tic.NumChairs,
                    CashierCode = _tic.CashierCode,
                    CashierName = _tic.CashierName,
                    CompanyCode = _tic.CompanyCode,

                    SaleDate = _trip != null ? nameday + " " + _trip.Date.JulianToPersianDate() + "  " + _trip.DepartureTime : "",
                    PrintDate = DateTime.Now.JulianToPersianDateTime(),
                    CompanyTitle = _companytitle,
                    TotalPrice = _totalPrice,
                    CityNameEnd = !string.IsNullOrEmpty(_tic.CityName) ? _tic.CityName : destCity,
                    CityNameStart = _CityStart,
                    TypeBus = _typebus
         ,
                    Logo = Logo
                };


                List<TicketReport> li = new List<TicketReport>();
                li.Add(_ticprint);

                RptTicket rpt = new RptTicket();

                if (PrintAgain)
                {
                    rpt.StatePrint.Value = " چاپ مجدد " + Userlogin.Code + " " + _ticprint.PrintDate;
                }

                rpt.DataSource = li;

                rpt.DescriptionTicket.Value = SettingLogin.GetValue("DescriptionReport").ToString();

                //rpt.DataMember = dtvw.TableName;
                //rpt.DataAdapter = _ticprint;
                rpt.PriceString.Value = JntNum2Text.Num2Text.ToFarsi(_ticprint.TotalPrice) + " ریال";
                rpt.RequestParameters = false;
                rpt.ShowPrintStatusDialog = false;
                rpt.PrintingSystem.ShowMarginsWarning = false;
                ReportPrintTool rp = new ReportPrintTool(rpt);
                rp.PrinterSettings.Copies = 1;
                rp.PrinterSettings.PrinterName = Userlogin.PrinterUser;
                //rp.ShowPreview();
                rp.Print();


                if (!PrintAgain)
                {
                    _tic.Printed = true;
                    TicketServices.UpdateticketNet(_tic, ConnectionCompany);// چاپ بلیط و فعال کردن چاپ اولیه
                }
                PrintAgain = false;


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                

                if (ex.Message.Contains("Settings to access printer"))
                {
                    XtraMessageBox.Show("پرینتر مورد نظر یافت نشد . لطفا تنظیمات خود را بررسی نمایید", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }

        }

        /// <summary>
        /// پرینترموجود است یا خیر
        /// </summary>
        /// <param name="printerName"></param>
        /// <returns></returns>
        public static bool PrinterExists(string printerName)
        {
            if (String.IsNullOrEmpty(printerName)) { throw new ArgumentNullException("printerName"); }
            return PrinterSettings.InstalledPrinters.Cast<string>().Any(name => printerName.ToUpper().Trim() == name.ToUpper().Trim());
        }


        /// <summary>
        /// تابع چاپ بلیط منتخب یا مورد نظر
        /// </summary>
        void PrintTicket(int TotalPrice, int idticket, int tripID)
        {
            try
            {

                if (!PrinterExists(Userlogin.PrinterUser))
                {
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("پرینتر مورد نظر یافت نشد . لطفا تنظیمات خود را بررسی نمایید", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }


                /// اطلاعات بلیط تعاونی
                Ticket _ticktNet = TicketServices.FindByIDNet(idticket, ConnectionCompany);

                if (_ticktNet.PayLock == true)
                    return;


                if (_ticktNet.Printed == null || !_ticktNet.Printed.Value)
                    PrintAgain = false;
                else
                {
                    PrintAgain = true;
                }


                ////        ارسال با ریپورت چاپ

                testNetEntities dbN = testNetEntities.Create(ConnectionCompany);
                dbN.Database.Connection.ConnectionString = ConnectionCompany;
                var query = (from Trips in dbN.Trips
                             join Services in dbN.Services on new { ID = Trips.ServiceID } equals new { ID = Services.ID }
                             join Paths in dbN.Paths on new { PathID = Services.PathID } equals new { PathID = Paths.ID }
                             join City1 in dbN.Cities on new { SrcCityID = Paths.SrcCityID } equals new { SrcCityID = City1.ID }
                             join City2 in dbN.Cities on new { DestCityID = Paths.DestCityID } equals new { DestCityID = City2.ID }
                             join CarTypes in dbN.CarTypes on new { CarTypeID = (int)Trips.CarTypeID } equals new { CarTypeID = CarTypes.ID } into CarTypes_join
                             from CarTypes in CarTypes_join.DefaultIfEmpty()
                             where Trips.ID == tripID
                             select new
                             {
                                 Trips.ID,
                                 PathTitle = (City1.Title + " -> " + City2.Title),
                                 CarTypeTitle = CarTypes.Title,
                             }).SingleOrDefault();

                string path = query.PathTitle.Replace("->", "-");
                string[] city = path.Split('-');
                string CityStart = city[0];
                string typeBus = query.CarTypeTitle;
                TotalPrice = ((_ticktNet.Price + _ticktNet.FundPrice + _ticktNet.OnlinePrice) * _ticktNet.NumChairs.Value) - (_ticktNet.Discount.Value * _ticktNet.NumChairs.Value);

                GetTicktPrint(_ticktNet, TitleCompany, TotalPrice, CityStart, typeBus);

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }

        /// <summary>
        /// استرداد یا برگشت بلیط
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnbackSale_ItemClick(object sender, ItemClickEventArgs e)
        {
            //  استرداد یا برگشت بلیط
            try
            {

                bool ISOnline = Convert.ToBoolean(SettingLogin.GetValue("IsOnline"));
                if (!ISOnline)
                {
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("امکان استرداد بلیط های اینترنتی وجود ندارد", "پیام", MessageBoxButtons.OK);
                    return;
                }
                var cityid = Convert.ToInt32(grd.GetFocusedRowCellValue(colCityID));
                var saletype = Convert.ToInt32(grd.GetFocusedRowCellValue(colSaleType));

                System.Windows.Forms.DialogResult DrResult;
                DevExpress.LookAndFeel.DefaultLookAndFeel StyleMessage = new DevExpress.LookAndFeel.DefaultLookAndFeel();
                StyleMessage.LookAndFeel.SkinName = "Office 2016 Colorful";
                UseridTicket us = new UseridTicket();
                int idTicket = Convert.ToInt32(grd.GetFocusedRowCellValue(colID));
                us.txtIdTicket.Enabled = ISOnline;
                if (!ISOnline)
                    us.txtIdTicket.EditValue = idTicket;


                string Noticket = grd.GetFocusedRowCellValue(colNo).ToString();


                int pricetick = Convert.ToInt32(grd.GetFocusedRowCellValue(colPrice));
                int FoundPrice = Convert.ToInt32(grd.GetFocusedRowCellValue(colFundPriceValue));
                int NumChair = Convert.ToInt32(grd.GetFocusedRowCellValue(colNumChairs));
                int Discount = Convert.ToInt32(grd.GetFocusedRowCellValue(colSumDiscount));
                int OnlinePrice = Convert.ToInt32(grd.GetFocusedRowCellValue(colOnlinePrice));

                int TotalTicket = (((pricetick + OnlinePrice) * NumChair) + FoundPrice) - Discount;
                us.TotalTicket = TotalTicket;

                us.Saletime = Convert.ToDateTime(grd.GetFocusedRowCellValue(colMoveTime));

                string[] NoNumbert = Noticket.Split('-');

                if (cityid == 0 && saletype == 0)
                {
                    us.IsOnline = true;
                }

                else
                {
                    us.txtIdTicket.EditValue = idTicket;

                }

                DrResult = XtraDialog.Show(StyleMessage.LookAndFeel, us, "", MessageBoxButtons.YesNo);
                if (DrResult == DialogResult.Yes && !string.IsNullOrEmpty(us.txtIdTicket.Text))
                {
                    if (cityid == 0 && saletype == 0)
                    {
                        string noinput = NoNumbert[0] + "-" + us.txtIdTicket.EditValue.ToString();
                        if (noinput != Noticket)
                        {
                            XtraMessageBox.Show("شماره بلیط اشتباه می باشد.لطفا در وارد نمودن شماره بلیط دقت نمایید", "پیام", MessageBoxButtons.OK);
                            return;
                        }
                    }
                    else
                    {
                        idTicket = Convert.ToInt32(us.txtIdTicket.EditValue);
                    }



                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);

                    int priceMode = 0, percent = 0, jamekol = 0;
                    /// اطلاعات بلیط تعاونی
                    Ticket _ticktNet = TicketServices.FindByIDNet(idTicket, ConnectionCompany);

                    if (_ticktNet == null)
                    {
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                        XtraMessageBox.Show("بلیط مورد نظر نا معتبر می باشد", "پیام", MessageBoxButtons.OK);
                        return;
                    }

                    if (_ticktNet.PayLock == true)
                    {
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                        XtraMessageBox.Show("امکان استرداد این بلیط وجود ندارد", "پیام", MessageBoxButtons.OK);
                        return;
                    }


                    if (_ticktNet.SaleType == 0 && _ticktNet.CityID == 0)
                    {

                        bool value = Convert.ToBoolean(SettingLogin.GetValue("IsOnline"));
                        if (!value)
                        {
                            ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                            XtraMessageBox.Show("امکان استرداد بلیط های اینترنتی وجود ندارد", "پیام", MessageBoxButtons.OK);
                            return;
                        }
                    }



                    if (_ticktNet != null)
                    {

                        string chairs = grd.GetFocusedRowCellValue(colChairsName).ToString();
                        if (us.txtPercent.EditValue != null)
                            percent = Convert.ToInt32(us.txtPercent.EditValue);

                        jamekol = _ticktNet.Price * _ticktNet.NumChairs.Value;

                        _ticktNet.PaidBack = 1;
                        _ticktNet.PaidBackPercent = Convert.ToInt32(percent);
                        _ticktNet.PaidBackDate = DateTime.Now;
                        _ticktNet.ReturnUserID = Userlogin.ID;
                        _ticktNet.ReturnUserName = Userlogin.FullName;
                        _ticktNet.ReturnUserCode = Userlogin.Code;

                        _ticktNet.CompanyCode = CodeCompany;

                        // برای بلیط ها که در صندوق نیستند مورد استثنا
                        //priceMode = ((_ticktNet.Price + _ticktNet.FundPrice.Value) * _ticktNet.NumChairs.Value);
                        //mablaghdarsad = (percent * priceMode) / 100;
                        //priceMode = priceMode - mablaghdarsad;
                        priceMode = Convert.ToInt32(us.txtPricePaidBack.EditValue);


                        if (_ticktNet.CityID > 0)
                        {
                            City city = CityService.FindByID(_ticktNet.CityID, ConnectionCompany);
                            if (city != null)
                            {
                                _ticktNet.CityName = city.Title;
                                _ticktNet.CityCode = city.Code;
                                _ticktNet.CityID = city.ID;
                            }

                        }

                        _ticktNet.PaidBackPrice = priceMode;

                        int Result = TicketServices.Update(_ticktNet, ConnectionCompany);
                        if (Result == 1)
                        {

                            int numChairs = Convert.ToInt32(grd.GetFocusedRowCellValue(colNumChairs));
                            //if (Discount > 0)
                            //    priceMode -= (Discount * numChairs);

                            User user = UserServices.FindByID(Userlogin.ID);
                            user.Credit += priceMode;
                            Userlogin.Credit = user.Credit;
                            UserServices.Update(user);

                            barCreditUser.EditValue = Userlogin.Credit;

                            PaidBackCreditUserreport(priceMode); /// کسر اعتبار و ثبت در جدول گزارشات کاربر

                            ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                            XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            grd.SetFocusedRowCellValue(colStatusPaidBack, "برگشتی صندوق");


                            LoadListTicket(FilterTimeUser);
                        }
                        else
                        {

                            ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                            XtraMessageBox.Show("عملیات با مشکل مواجه شد لطفا مجددا سعی نمایید", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                    }
                    else
                    {
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                        XtraMessageBox.Show("شماره بلیط نامعتبر می باشد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }



                }


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }

            finally
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }


        int RoundPrice(int Price)
        {
            int PriceSummery = Price;
            int Mode = Price % 10000;
            if (Mode != 0)
            {
                PriceSummery = (10000 - Mode) + Price;
            }

            return PriceSummery;
        }



        // ارسال به کارتخوان
        private void bgpose_DoWork(object sender, DoWorkEventArgs e)
        {

            /// توابع ارسال
            PayPosePrice();
        }

        private void bgpose_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);

        }



        //***************************************************************                کدهای پرداخت کارتخوان               ***********************************************************************************

        PcPosFactory _PcPosFactory;
        MediaType _mediaType = MediaType.Network;
        AccountType _accountType = AccountType.Single;
        ResponseLanguage _responseLanguage;

        AsyncType _asyncType = AsyncType.Sync;
        ReportAction _reportAction;

        ReportFilterType _reportFilterType;

        TransactionType _tracsactionType;

        PcPosDll.PcPosBusiness PcPos = new PcPosDll.PcPosBusiness();
        PcPosDll.PcPosDiscovery searchPos = new PcPosDll.PcPosDiscovery();



        public string ShomreErja { get; set; }
        public string ShomarePeygiri { get; set; }
        public string ParentIDKart { get; set; }
        public bool PaymentPostActive { get; set; }
        public string Mablagh { get; set; }
        String IpPos, typePos;


        /// <summary>
        /// لغو عملیات پرداخت
        /// </summary>
        public void CancelPay()
        {
            try
            {
                // Fwait.Close();


                if (typePos != null)
                {
                    if (typePos.Contains("سداد"))
                    {
                        if (PcPos == null)
                            PcPos = new PcPosBusiness();
                        // لغو عملیات پرداخت
                        PcPos.AbortPcPosOperation();

                        grd.Appearance.FocusedRow.BackColor = Color.Red;
                    }
                    if (typePos.Contains("سامان"))
                    {
                        if (_PcPosFactory == null)
                        {
                            _PcPosFactory = new PcPosFactory();
                        }
                        _PcPosFactory.Dispose();

                        grd.Appearance.FocusedRow.BackColor = Color.Red;

                    }


                    if (typePos.Contains("آسان پرداخت"))
                    {
                        if (PcposAsanPardakht == null)
                        {
                            PcposAsanPardakht = new PosInterface.PCPos();
                        }
                        PcposAsanPardakht.Stop();

                        grd.Appearance.FocusedRow.BackColor = Color.Red;


                    }
                    //         if (typePos.Contains("ملت")) {
                    //             mellat.StopProcessing();
                    //txtStatusPayment.EditValue="عملیات دریافت از کارتخوان لغو گردید";

                    //         }
                }

                PaymentPostActive = false;
                btnCancelPose.Enabled = false;

            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }


        }

        /// <summary>
        /// ارسال مبلغ به کارتخوان مورد نظر
        /// </summary>
        public void PayPosePrice()
        {
            try
            {

                //OperationSaveFact = true;


                if (typePos.Contains("سداد"))
                {
                    PaymentPostActive = true;
                    if (PcPos == null)
                        PcPos = new PcPosDll.PcPosBusiness();

                    {
                        PcPos.PcPosIp = IpPos;
                        PcPos.PcPosPort = 8888;
                        PcPos.PcPosMode = PcPosConnectionType.Lan;
                        PcPos.Pc2PosAmount = Mablagh;
                        PcPos.PcPosRetryTimeOut = new int[] {
                5000,
                5000,
                5000
            };
                        PcPos.PcPosResponseTimeout = new int[] {
                180000,
                5000,
                5000
            };
                        PcPos.OnPcPosResult += PcPosSaleResult;
                        PcPos.AsyncPcPosSaleTransaction();

                    }
                }

                if (typePos.Contains("سامان کیش"))
                {
                    PaymentPostActive = true;
                    _PcPosFactory = new PcPosFactory();
                    if (PurchaseInitialization())
                    {
                        return;
                    }

                    List<string> lstAmnts = new List<string>();
                    SSP1126.PcPos.Infrastructure.PosResult posResult = new SSP1126.PcPos.Infrastructure.PosResult();
                    string TID = null;
                    if (_accountType == AccountType.Single)
                    {
                        posResult = _PcPosFactory.PcStarterPurchase(Mablagh, string.Empty, null, "", TID);
                    }
                    if (_asyncType == AsyncType.Sync && posResult != null)
                    {
                        PurchaseResultReceived(posResult);
                    }
                    else
                    {
                        FailedPayPose();
                    }
                }



                if (typePos.Contains("آسان پرداخت"))
                {
                    PcposAsanPardakht.InitLAN(IpPos, 17000);
                    PcposAsanPardakht.DoASyncPayment(Mablagh, "", "", DateTime.Now, this);


                }

                //if (typePos.Contains("ملت"))
                //{
                //    string ErrorMessage;
                //    string NumberError;
                //    string NumberPeygiri;
                //    string NumberErja;
                //    string ReasonCode;

                //    mellat = new MellatPose();
                //    bool Response = mellat.PeymentFunc(Mablagh, IpPos, 1024, ShomarePeygiri, ShomreErja, NumberError, ReasonCode);

                //    switch (Response)
                //    {

                //        case true:
                //            Invoke(new MethodInvoker(() =>
                //            {
                //                System.Threading.Thread.Sleep(1000);
                // txtStatusPayment.EditValue= "پرداخت با موفقیت انجام شد";

                //                //ShomarePeygiri = NumberPeygiri
                //                //ShomreErja = NumberErja
                //                PaymentPostActive = false;
                //               // Fwait.Close();

                //                   /// ثبت فاکتور

                //            }));

                //            break;
                //        case false:
                //            PaymentPostActive = false;
                //            btnCancelPose.Enabled = false;
                //        
                //  txtStatusPayment.EditValue=   "عملیات پرداخت ناموفق بود" ;

                //           // Fwait.Close();
                //            /// عملیات پرداخت ناموفق
                //            break;


                //    }

                //}

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        //  نتیجه عملیات پرداخت دستگاه سداد
        private void PcPosSaleResult(object sender, PcPosDll.PosResult e)
        {
            //AdUPos.ShowGuides = DevExpress.Utils.DefaultBoolean.True
            try
            {

                Invoke(new MethodInvoker(() =>
                {
                    System.Threading.Thread.Sleep(1000);

                    if (e.ResponseCode == "00")
                    {

                        SuccessPayPose();

                        grd.Appearance.FocusedRow.BackColor = Color.GreenYellow;
                        ShomarePeygiri = e.ApprovalCode.ToString();
                        ShomreErja = e.TransactionNo;
                        PaymentPostActive = false;
                        // Fwait.Close();

                        /// عملیات ثبت فاکتور

                    }
                    else
                    {
                        FailedPayPose();
                        ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                        XtraMessageBox.Show(" عملیات پرداخت کارتخوان ناموفق");

                        // Fwait.Close();
                        /// عملیات پرداخت ناموفق
                    }

                    PcPos.OnPcPosResult -= PcPosSaleResult;
                    PcPos = null;
                }));


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }


        }



        //   دریافت نتیجه پرداخت سامان کیش
        private void PurchaseResultReceived(SSP1126.PcPos.Infrastructure.PosResult posResult)
        {
            try
            {

                if (posResult == null)
                {
                    return;
                }

                //Successful result
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        if (posResult.ResponseCode == "00")
                        {
                            SuccessPayPose();

                            grd.Appearance.FocusedRow.BackColor = Color.GreenYellow;
                            ShomarePeygiri = posResult.TraceNumber;
                            ShomreErja = posResult.RRN;
                            PaymentPostActive = false;
                            /// عملیات ثبت فاکتور
                        }
                        else
                        {

                            FailedPayPose();

                            ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                            XtraMessageBox.Show(" عملیات پرداخت کارتخوان ناموفق");
                            /// عملیات پرداخت ناموفق
                        }
                    }));
                }


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        public void OnFinish(string message)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// نتیجه عملیات پرداخت آسان پرداخت
        /// </summary>
        /// <param name="result"></param>
        public void OnTransactionDone(TransactionResult result)
        {
            {
                if (this.InvokeRequired)
                {

                    this.Invoke(new MethodInvoker(() =>
                    {
                        if (result.ErrorCode == 0)
                        {

                            SuccessPayPose();

                            grd.Appearance.FocusedRow.BackColor = Color.GreenYellow;
                            ShomarePeygiri = result.Stan;
                            ShomreErja = result.RRN;
                            PaymentPostActive = false;
                            // Fwait.Close();
                            ///  عملیات ثبت فاکتور
                        }
                        else
                        {
                            FailedPayPose();

                            ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                            XtraMessageBox.Show(" عملیات پرداخت کارتخوان ناموفق");
                            // Fwait.Close();

                            // عملیات پرداخت ناموفق
                        }
                    }));
                }

            }
        }


        //MellatPose mellat = new MellatPose();
        PosInterface.PCPos PcposAsanPardakht = new PosInterface.PCPos();
        private bool PurchaseInitialization()
        {

            _tracsactionType = TransactionType.Purchase;

            if (TransactionMediaInitialization())
            {
                return false;
            }

            _PcPosFactory.Initialization(_responseLanguage, 0, _asyncType);
            return false;
        }


        // لغو ارسال به کارتخوان
        private async void btnCancelPose_ItemClick(object sender, ItemClickEventArgs e)
        {

            if (CheckingPayPosLock)
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);

                //bgpose.CancelAsync();
                //bgpose.Dispose();

                FailedPayPose();

                await Task.Run(() =>
                {
                    CancelPay();
                });


            }



        }

        private void FrmReserve_FormClosing(object sender, FormClosingEventArgs e)
        {
            btnCancelPose_ItemClick(null, null);
            e.Cancel = true;
            this.Hide();
        }

        private bool TransactionMediaInitialization()
        {
            //If _mediaType = MediaType.Network Then
            if (string.IsNullOrEmpty(IpPos))
            {
                XtraMessageBox.Show("ای پی دستگاه اشتباه می باشد", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //grd.Appearance.FocusedRow.BackColor = Color.Yellow;

                return true;
            }
            _PcPosFactory.SetLan(IpPos);
            //End If
            _PcPosFactory.Initialization(_responseLanguage, 0, _asyncType);
            return false;
        }

        //***************************************************************************************************************************************************************************


        string stateticket = "";
        string StatePaidback = "";
        private void grd_CustomRowCellEdit(object sender, DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventArgs e)
        {
            try
            {


                stateticket = grd.GetRowCellValue(e.RowHandle, colStateticket).ToString();
                StatePaidback = grd.GetRowCellValue(e.RowHandle, colStatusPaidBack).ToString();
                bool Closed = Convert.ToBoolean(grd.GetRowCellValue(e.RowHandle, colClosed));

                if (e.Column == col1)
                {
                    if (stateticket.Contains("فروش") && StatePaidback.Contains("برگشت نشده"))
                    {
                        e.RepositoryItem = reBtnPrintticket;
                    }

                    if (stateticket.Contains("رزرو"))
                    {
                        e.RepositoryItem = reBtnPayCredit;
                    }


                }


                if (e.Column == col2)
                {
                    if (Closed)
                    {
                        return;
                    }

                    if (stateticket.Contains("فروش") && StatePaidback.Contains("برگشت نشده"))
                    {
                        e.RepositoryItem = ReBtnPayback;
                    }

                    if (stateticket.Contains("رزرو"))
                    {
                        e.RepositoryItem = reBtnPayPose;
                    }

                }

            }
            catch (Exception ex)
            {


            }

        }


        /// <summary>
        /// پرداخت اعتباری
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void reBtnPayCredit_Click(object sender, EventArgs e)
        {
            try
            {
                /// پرداخت اعتباری

                SetProp();

                int NumChairs = (int)grd.GetFocusedRowCellValue(colNumChairs);
                int _Price = (int)grd.GetFocusedRowCellValue(colPrice);
                TotalpricePose = NumChairs * _Price;
                int idticket = (int)grd.GetFocusedRowCellValue(colID);
                int tripID = (int)grd.GetFocusedRowCellValue(colTripID);

                if (TotalpricePose > Userlogin.Credit)
                {

                    XtraMessageBox.Show("کاربر گرامی اعتبار جهت پرداخت کافی نمی باشد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }




                /// اطلاعات بلیط تعاونی
                Ticket _ticktNet = TicketServices.FindByIDNetSaly(idticket, ConnectionCompany);
                if (_ticktNet == null)
                {
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("این بلیط فروخته شده است.لطفا در انتخاب خود دقت نمایید", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    LoadListTicket(DateTime.Now);
                    return;
                }

                _ticktNetPayPose = new Ticket();
                CopyClass.Copy<Ticket>(_ticktNet, _ticktNetPayPose);

                if (_ticktNet != null)
                {


                    #region مقدار دهی کلاس


                    _ticktNet.SaleType = 0;
                    _ticktNet.SaleDate = DateTime.Now;
                    _ticktNet.CashierCode = Userlogin.Code;
                    _ticktNet.CashierName = Userlogin.FullName;
                    _ticktNet.CompanyCode = CodeCompany;

                    City city = CityService.FindByID(_ticktNet.CityID, ConnectionCompany);
                    _ticktNet.CityName = city.Title;
                    _ticktNet.CityCode = city.Code;
                    CalculateFundPrice(_Price, NumChairs);
                    _ticktNet.TypeTicket = "پرداخت اعتباری";
                    _ticktNet.PayDate = DateTime.Now;
                    _ticktNet.FundID = 100;
                    _ticktNet.FundPrice = FundPriceCash;
                    _ticktNet.Printed = false;
                    _ticktNet.PayLock = true;
                    _ticktNet.Price -= FundPriceCash;

                    //if (PrinterExists(Userlogin.PrinterUser))
                    //    {
                    //        _ticktNet.Printed = true;
                    //    }

                    #endregion

                    int Result = TicketServices.UpdateticketNet(_ticktNet, ConnectionCompany); // پرداخت اعتباری
                    if (Result == 1)
                    {

                        if (XtraMessageBox.Show("آیا مایل به پرداخت اعتباری  می باشید؟", "پیام", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {

                            ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                            if (SuccessPayPose() == 1)
                            {
                                User user = UserServices.FindByID(Userlogin.ID);
                                user.Credit -= TotalpricePose;
                                Userlogin.Credit = user.Credit;
                                UserServices.Update(user);
                                barCreditUser.EditValue = Userlogin.Credit;
                                AddCreditUserreport(TotalpricePose);
                            }
                        }
                        else
                        {
                            //  حذف بلیط صندوقدار و برگشت بلیط رزروی به حالت اولیه
                            FailedPayPose();
                        }
                    }
                }





            }
            catch (Exception ex)
            {
                FailedPayPose();
               GetError.WriteError(ex,this);
                
            }
            finally
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }




        }


        /// <summary>
        /// ثبت در جدول گزارشات اعتبار کاربر
        /// </summary>
        void AddCreditUserreport(int _Recived)
        {
            UserCreditReport _uc = new UserCreditReport();
            _uc.ActionDate = DateTime.Now;
            _uc.ActionType = 1;
            _uc.UserCode = Userlogin.Code;
            _uc.Recived = 0;
            _uc.Payment = _Recived;
            _uc.Status = 1;
            UserCreditReportServices.Insert(_uc);
        }


        /// <summary>
        /// کسر اعتبار و ثبت در جدول گزارشات اعتبار کاربر
        /// </summary>
        void PaidBackCreditUserreport(int _Payment)
        {
            UserCreditReport _uc = new UserCreditReport();
            _uc.ActionDate = DateTime.Now;
            _uc.ActionType = 2;
            _uc.UserCode = Userlogin.Code;
            _uc.Recived = _Payment;
            _uc.Payment = 0;
            _uc.Status = 1;
            UserCreditReportServices.Insert(_uc);
        }


        Ticket _ticktNetPayPose;
        bool CheckingPayPosLock = false;
        int TotalpricePose = 0;

        /// <summary>
        /// عملیات پرداخت کارتخوان و فروش بلیط کارتخوان
        /// </summary>
        void PayPose()
        {
            try
            {
                /// پرداخت کارتخوان

                int NumChairs = (int)grd.GetFocusedRowCellValue(colNumChairs);
                int _Price = (int)grd.GetFocusedRowCellValue(colPrice);
                TotalpricePose = NumChairs * _Price;
                int idticket = (int)grd.GetFocusedRowCellValue(colID);

                /// اطلاعات بلیط تعاونی
                Ticket _ticktNet = TicketServices.FindByIDNetSaly(idticket, ConnectionCompany);

                if (_ticktNet == null)
                {
                    CheckingPayPosLock = true;
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("این بلیط فروخته شده است.لطفا در انتخاب خود دقت نمایید", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    LoadListTicket(DateTime.Now);
                    return;
                }


                _ticktNetPayPose = new Ticket();
                CopyClass.Copy<Ticket>(_ticktNet, _ticktNetPayPose);

                if (_ticktNet != null)
                {
                    _ticktNet.SaleType = 0;
                    _ticktNet.SaleDate = DateTime.Now;
                    _ticktNet.CompanyCode = CodeCompany;
                    _ticktNet.CashierCode = Userlogin.Code;
                    _ticktNet.CashierName = Userlogin.FullName;


                    City city = CityService.FindByID(_ticktNet.CityID, ConnectionCompany);
                    _ticktNet.CityName = city.Title;
                    _ticktNet.CityCode = city.Code;
                    CalculateFundPrice(_Price, NumChairs);
                    _ticktNet.TypeTicket = "پرداخت کارتخوان";
                    _ticktNet.PayDate = DateTime.Now;
                    _ticktNet.FundID = 100;
                    _ticktNet.FundPrice = FundPriceCash;
                    _ticktNet.Printed = false;
                    _ticktNet.PayLock = true;
                    _ticktNet.Price -= FundPriceCash;
                    //if (PrinterExists(Userlogin.PrinterUser))
                    //{
                    //    _ticktNet.Printed = true;
                    //}

                    int Result = TicketServices.UpdateticketNet(_ticktNet, ConnectionCompany); /// پرداخت کارتخوان

                }
                else
                {
                    CheckingPayPosLock = true;
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("اطلاعات بلیط یافت نشد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }





            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("The underlying provider failed on Open."))
                {
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("ارتباط با صرور قطع می باشد.جهت بازگشت این بلیط به حالت اولیه بایستی به سرور متصل بود.\n پس از تایید این پیغام برای حذف این بلیط تلاش می شود", "اخطار", MessageBoxButtons.OK);
                    FailedPayPose();
                }


                CheckingPayPosLock = true;
               GetError.WriteError(ex,this);
                
            }

        }

        /// <summary>
        /// پرداخت موفق کارتخوان و چاپ بلیط
        /// </summary>
        int SuccessPayPose()
        {
            int result = 0;

            try
            {

                result = TicketServices.SuccessPayLockPos(_ticktNetPayPose.ID, ConnectionCompany);
                PrintTicket(TotalpricePose, _ticktNetPayPose.ID, _ticktNetPayPose.TripID);
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                grd.SetFocusedRowCellValue(colStateticket, "فروخته شده");
                LoadListTicket(FilterTimeUser);
                CheckingPayPosLock = false;
                return result;
            }
            catch (Exception ex)
            {
                FailedPayPose();
               GetError.WriteError(ex,this);
                return 0;
            }


        }

        /// <summary>
        /// اشکال در عملیات پرداخت کارتخوان
        /// </summary>
        void FailedPayPose()
        {
            if (_ticktNetPayPose != null)
            {
                //  بلیط رزور شده را ویرایش کن و به حالت قبل برگردان

                int result = TicketServices.UpdateTicketCompany(_ticktNetPayPose, ConnectionCompany);

            }


            ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            CheckingPayPosLock = false;
        }

        bool CheckingPingPose()
        {
            Ping p1 = new Ping();
            PingReply PR = p1.Send(Userlogin.IpPose);
            if (PR.Status == IPStatus.Success)
                return true;
            return false;
        }

        /// <summary>
        /// ارسال به دستگاه کارتخوان
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void reBtnPayPose_Click(object sender, EventArgs e)
        {

            try
            {

                if (!CheckingPingPose())
                {
                    XtraMessageBox.Show("اتصال به کارتخوان مقدور نمی باشد . لطفا آدرس دستگاه یا وضعیت اتصال را بررسی نمایید", "", MessageBoxButtons.OK);
                    return;
                }


                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                SetProp();

                CheckingPayPosLock = false;
                PayPose();


                int NumChairs = (int)grd.GetFocusedRowCellValue(colNumChairs);
                int _Price = (int)grd.GetFocusedRowCellValue(colPrice);
                int TotalPrice = NumChairs * _Price;

                Mablagh = TotalPrice.ToString();
                IpPos = Userlogin.IpPose;
                typePos = Userlogin.PoseName;
                btnCancelPose.Enabled = true;


                if (CheckingPayPosLock)
                {
                    CheckingPayPosLock = false;
                    return;
                }

                CheckingPayPosLock = true;


                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                await Task.Run(() =>
                {
                    PayPosePrice();
                });


                //if (!bgpose.IsBusy)
                //{
                //         ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                //        bgpose.RunWorkerAsync();

                //}
                //else
                //{
                //     ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                //    XtraMessageBox.Show("سیستم در حالت ارتباط با دستگاه کارتخوان می باشد .لطفا چند لحظه  شکیبا باشید", "پیام", MessageBoxButtons.OK);
                //     ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                //}




            }
            catch (Exception ex)
            {
                FailedPayPose();
               GetError.WriteError(ex,this);
                
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }


        }

        /// <summary>
        /// مشخص شدن وضعت چاپ بلیط.چاپ مجدد یا معمولی
        /// </summary>
        bool PrintAgain;
        private void reBtnPrintticket_Click(object sender, EventArgs e)
        {

            try
            {

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                SetProp();

                int idticket = (int)grd.GetFocusedRowCellValue(colID);
                int NumChairs = (int)grd.GetFocusedRowCellValue(colNumChairs);
                int _Price = (int)grd.GetFocusedRowCellValue(colPrice);
                int TotalPrice = NumChairs * _Price;
                int tripID = (int)grd.GetFocusedRowCellValue(colTripID);

                PrintTicket(TotalPrice, idticket, tripID);


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

            finally
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }



        }

        private void ChCompany_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            try
            {

                string str = GetFilterStringBindedColumns("CompanyCode", ChCompany);
                grd.Columns["CompanyCode"].FilterInfo = new ColumnFilterInfo(str);

            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }
        }
        private string GetFilterStringBindedColumns(string column, CheckedListBoxControl CheckListBox)
        {

            CheckedListBoxControl clbc = (CheckedListBoxControl)CheckListBox;
            string str = string.Empty;

            for (int i = 0; i <= clbc.CheckedItems.Count - 1; i++)
            {
                if (str != string.Empty)
                {
                    str = str + "OR";
                }
                str = str + "[" + column + "] = '" + clbc.CheckedItems[i].ToString() + "' ";
            }

            return str;

        }

        /// <summary>
        ///  برگشت مبلغ یا استرداد
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ReBtnPayback_Click(object sender, EventArgs e)
        {
            SetProp();
            BtnbackSale_ItemClick(null, null);

        }

        private void chAll_CheckedChanged(object sender, EventArgs e)
        {
            if (chAll.Checked)
            {
                StatusTickets = 5;
                grd.ClearColumnsFilter();
                chRezerv.EditValue = false;
                chSale.EditValue = false;
            }

        }

        private void chRezerv_CheckedChanged(object sender, EventArgs e)
        {
            if (chRezerv.Checked)
            {
                grd.ActiveFilterString = "Stateticket like '%رزرو%'";
                StatusTickets = 1;
                chSale.EditValue = false;
                chAll.EditValue = false;

            }
            else
            {
                chAll.EditValue = true;
            }
        }

        private void chSale_CheckedChanged(object sender, EventArgs e)
        {
            if (chSale.Checked)
            {
                grd.ActiveFilterString = "Stateticket like '%فروش%'";
                StatusTickets = 0;
                chRezerv.EditValue = false;
                chAll.EditValue = false;
            }
            else
            {
                chAll.EditValue = true;
            }

        }

        private void btnNextDate_Click(object sender, EventArgs e)
        {
            txtFromDate.DateTime = txtFromDate.DateTime.AddDays(-1);
            lblNameDay.Text = txtFromDate.DateTime.GetNameOfDayDate();
        }

        private void btnBackDate_Click(object sender, EventArgs e)
        {
            txtFromDate.DateTime = txtFromDate.DateTime.AddDays(1);
            lblNameDay.Text = txtFromDate.DateTime.GetNameOfDayDate();
        }

        private void btnNowDate_Click(object sender, EventArgs e)
        {
            txtFromDate.DateTime = DateTime.Now;
            LoadListTicket(txtFromDate.DateTime);
        }

        private void btnshowReport_Click(object sender, EventArgs e)
        {
            LoadListTicket(txtFromDate.DateTime);
        }

        private void txtFromDate_Click(object sender, EventArgs e)
        {
            txtFromDate.ShowPopup();
        }

        private void txtFromDate_EditValueChanged(object sender, EventArgs e)
        {
            if (txtFromDate.DateTime.ToShortDateString() != DateTime.Now.ToShortDateString())
                txtFromDate.BackColor = Color.Yellow;
            else
                txtFromDate.BackColor = Color.White;
            lblNameDay.Text = txtFromDate.DateTime.GetNameOfDayDate();
        }

        private void grd_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            GridView View = (GridView)sender;
            if ((e.RowHandle >= 0))
            {
                string category = View.GetRowCellDisplayText(e.RowHandle, View.Columns["Stateticket"]);
                string StateP = View.GetRowCellDisplayText(e.RowHandle, View.Columns["StatusPaidBack"]);

                if (StateP.Contains("برگشتی صندوق"))
                {
                    e.Appearance.BackColor = Color.Tomato;
                    e.HighPriority = true;
                }
                else
                {
                    if (category.Contains("رزرو"))
                    {
                        e.Appearance.BackColor = Color.Khaki;
                        e.HighPriority = true;
                    }
                    if (category.Contains("فروش"))
                    {
                        e.Appearance.BackColor = Color.Turquoise;
                        e.HighPriority = true;
                    }

                }

            }


            if (e.RowHandle == hotRowHandle)
            {
                e.Appearance.BackColor = Color.DodgerBlue;
                e.Appearance.ForeColor = Color.White;
                return;
            }



        }

        private void FrmTicketList_Load(object sender, EventArgs e)
        {
            chRezerv.Checked = true;
        }

        private void toolTip_GetActiveObjectInfo(object sender, DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (e.SelectedControl != dgv)
                return;
            GridHitInfo hitInfo = grd.CalcHitInfo(e.ControlMousePosition);
            if (hitInfo.InRow == false)
                return;

            SuperToolTipSetupArgs toolTipArgs = new SuperToolTipSetupArgs();
            var UserpaidBack = grd.GetRowCellValue(hitInfo.RowHandle, colReturnUserName);
            if (UserpaidBack == null)
                return;

            DateTime PaidDate = DateTime.Parse(grd.GetRowCellValue(hitInfo.RowHandle, colPaidBackDate).ToString());
            string datePaidBakc = PaidDate.GetNameOfDayDate();
            toolTipArgs.Title.Text = UserpaidBack.ToString();
            toolTipArgs.Contents.Text = string.Format("تاریخ برگشت : {0}    در ساعت :  {1} ", datePaidBakc, PaidDate.ToString("HH:mm"));
            e.Info = new ToolTipControlInfo();
            e.Info.Object = hitInfo.HitTest.ToString() + hitInfo.RowHandle.ToString(); //NewLine  
            e.Info.ToolTipType = ToolTipType.SuperTip;
            e.Info.SuperTip = new SuperToolTip();
            e.Info.SuperTip.Setup(toolTipArgs);
        }

        private void grd_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            if (e.Column.FieldName != "Time") return;
            int row = e.ListSourceRowIndex;
            DateTime date = DateTime.Parse(grd.GetListSourceRowCellValue(row, colSaleDate).ToString());
            e.Value = date.JulianToPersianDateTime();
        }


        int hotRowHandle = -1;
        private void grd_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = view.CalcHitInfo(e.Location);
            if (hi.InRow)
            {
                int prevHandle = hotRowHandle;
                hotRowHandle = hi.RowHandle;
                view.RefreshRow(hotRowHandle);
                view.RefreshRow(prevHandle);
            }
            else
            {
                if (hotRowHandle != -1)
                {
                    int prevHandle = hotRowHandle;
                    hotRowHandle = -1;
                    view.RefreshRow(prevHandle);
                }
            }
        }

        private void btnLoadRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadListTicket(txtFromDate.DateTime);
        }

        void SetProp()
        {
            int _TripID = Convert.ToInt32(grd.GetFocusedRowCellValue(colTripID));
            CodeCompany = grd.GetFocusedRowCellValue(colCodeCompany).ToString();
            var company = AccessCompanyUser.Where(c => c.Code == CodeCompany).SingleOrDefault();
            if (company != null)
            {
                TitleCompany = company.Title;


                if (Userlogin.IsLoacConnection)
                    ConnectionCompany = company.LocalConnectionString;
                else
                    ConnectionCompany = company.ConnectionString;

                Logo = company.Logo;
            }
            Trip _trip = TripServices.FindByID(_TripID, ConnectionCompany);
            IDService = _trip.ServiceID;

            TripID = Convert.ToInt32(grd.GetFocusedRowCellValue(colTripID));





        }

    }
}