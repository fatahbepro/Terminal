﻿namespace Test
{
    partial class FrmUserCriditReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmUserCriditReport));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnReport = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.navigationPane1 = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.LayoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.SearchControl1 = new DevExpress.XtraEditors.SearchControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.reportCreditUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colRecived = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDateSH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAction = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnShowAllUser = new DevExpress.XtraEditors.SimpleButton();
            this.txtTodate = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.txtFromDate = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.cmbUsers = new DevExpress.XtraEditors.LookUpEdit();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layUsers = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Test.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).BeginInit();
            this.navigationPane1.SuspendLayout();
            this.navigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).BeginInit();
            this.LayoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportCreditUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUsers.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnReport,
            this.btnRefresh,
            this.btnClose});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 4;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1384, 146);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // btnReport
            // 
            this.btnReport.Caption = "گزارش";
            this.btnReport.Glyph = ((System.Drawing.Image)(resources.GetObject("btnReport.Glyph")));
            this.btnReport.Id = 1;
            this.btnReport.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6);
            this.btnReport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnReport.LargeGlyph")));
            this.btnReport.Name = "btnReport";
            this.btnReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReport_ItemClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "بروز رسانی";
            this.btnRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Glyph")));
            this.btnRefresh.Id = 2;
            this.btnRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btnRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.LargeGlyph")));
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // btnClose
            // 
            this.btnClose.Caption = "بستن";
            this.btnClose.Glyph = ((System.Drawing.Image)(resources.GetObject("btnClose.Glyph")));
            this.btnClose.Id = 3;
            this.btnClose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnClose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnClose.LargeGlyph")));
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "عملیات";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnReport);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnRefresh);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "گزارشات";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnClose);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 696);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1384, 21);
            // 
            // navigationPane1
            // 
            this.navigationPane1.Controls.Add(this.navigationPage1);
            this.navigationPane1.Dock = System.Windows.Forms.DockStyle.Right;
            this.navigationPane1.Location = new System.Drawing.Point(1107, 146);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.PageProperties.AppearanceCaption.Options.UseTextOptions = true;
            this.navigationPane1.PageProperties.AppearanceCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.navigationPane1.PageProperties.ShowExpandButton = false;
            this.navigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.Image;
            this.navigationPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.navigationPage1});
            this.navigationPane1.RegularSize = new System.Drawing.Size(277, 550);
            this.navigationPane1.SelectedPage = this.navigationPage1;
            this.navigationPane1.Size = new System.Drawing.Size(277, 550);
            this.navigationPane1.TabIndex = 26;
            this.navigationPane1.Text = "navigationPane1";
            // 
            // navigationPage1
            // 
            this.navigationPage1.Caption = "گزارشات";
            this.navigationPage1.Controls.Add(this.LayoutControl2);
            this.navigationPage1.Image = ((System.Drawing.Image)(resources.GetObject("navigationPage1.Image")));
            this.navigationPage1.ImageUri.Uri = "Zoom";
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(207, 483);
            // 
            // LayoutControl2
            // 
            this.LayoutControl2.Controls.Add(this.SearchControl1);
            this.LayoutControl2.Controls.Add(this.btnShowAllUser);
            this.LayoutControl2.Controls.Add(this.txtTodate);
            this.LayoutControl2.Controls.Add(this.txtFromDate);
            this.LayoutControl2.Controls.Add(this.cmbUsers);
            this.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl2.Name = "LayoutControl2";
            this.LayoutControl2.OptionsView.RightToLeftMirroringApplied = true;
            this.LayoutControl2.Root = this.LayoutControlGroup2;
            this.LayoutControl2.Size = new System.Drawing.Size(207, 483);
            this.LayoutControl2.TabIndex = 0;
            this.LayoutControl2.Text = "LayoutControl2";
            // 
            // SearchControl1
            // 
            this.SearchControl1.Client = this.gridControl1;
            this.SearchControl1.Location = new System.Drawing.Point(6, 6);
            this.SearchControl1.Name = "SearchControl1";
            this.SearchControl1.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.SearchControl1.Properties.Appearance.Options.UseFont = true;
            this.SearchControl1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.SearchControl1.Properties.Client = this.gridControl1;
            this.SearchControl1.Size = new System.Drawing.Size(195, 26);
            this.SearchControl1.StyleController = this.LayoutControl2;
            this.SearchControl1.TabIndex = 16;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.reportCreditUserBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 146);
            this.gridControl1.MainView = this.grd;
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1107, 550);
            this.gridControl1.TabIndex = 27;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grd});
            // 
            // reportCreditUserBindingSource
            // 
            this.reportCreditUserBindingSource.DataSource = typeof(Model.ReportCreditUser);
            // 
            // grd
            // 
            this.grd.Appearance.FocusedCell.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedCell.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grd.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grd.Appearance.FocusedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grd.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grd.Appearance.SelectedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grd.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grd.ColumnPanelRowHeight = 30;
            this.grd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colRecived,
            this.colPayment,
            this.colActionType,
            this.colActionDateSH,
            this.colAction,
            this.colMode});
            this.grd.GridControl = this.gridControl1;
            this.grd.IndicatorWidth = 30;
            this.grd.Name = "grd";
            this.grd.OptionsBehavior.Editable = false;
            this.grd.OptionsCustomization.AllowFilter = false;
            this.grd.OptionsCustomization.AllowSort = false;
            this.grd.OptionsView.ShowGroupPanel = false;
            this.grd.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grd_CustomDrawRowIndicator);
            // 
            // colID
            // 
            this.colID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colID.AppearanceCell.Options.UseFont = true;
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colID.AppearanceHeader.Options.UseFont = true;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.Caption = "کد";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.Visible = true;
            this.colID.VisibleIndex = 2;
            this.colID.Width = 100;
            // 
            // colRecived
            // 
            this.colRecived.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colRecived.AppearanceCell.Options.UseFont = true;
            this.colRecived.AppearanceCell.Options.UseTextOptions = true;
            this.colRecived.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRecived.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colRecived.AppearanceHeader.Options.UseFont = true;
            this.colRecived.AppearanceHeader.Options.UseTextOptions = true;
            this.colRecived.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colRecived.Caption = "دریافتی";
            this.colRecived.DisplayFormat.FormatString = "c0";
            this.colRecived.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colRecived.FieldName = "Recived";
            this.colRecived.Name = "colRecived";
            this.colRecived.Visible = true;
            this.colRecived.VisibleIndex = 5;
            this.colRecived.Width = 149;
            // 
            // colPayment
            // 
            this.colPayment.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPayment.AppearanceCell.Options.UseFont = true;
            this.colPayment.AppearanceCell.Options.UseTextOptions = true;
            this.colPayment.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPayment.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPayment.AppearanceHeader.Options.UseFont = true;
            this.colPayment.AppearanceHeader.Options.UseTextOptions = true;
            this.colPayment.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPayment.Caption = "پرداختی";
            this.colPayment.DisplayFormat.FormatString = "c0";
            this.colPayment.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPayment.FieldName = "Payment";
            this.colPayment.Name = "colPayment";
            this.colPayment.Visible = true;
            this.colPayment.VisibleIndex = 4;
            this.colPayment.Width = 149;
            // 
            // colActionType
            // 
            this.colActionType.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colActionType.AppearanceCell.Options.UseFont = true;
            this.colActionType.AppearanceCell.Options.UseTextOptions = true;
            this.colActionType.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colActionType.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colActionType.AppearanceHeader.Options.UseFont = true;
            this.colActionType.AppearanceHeader.Options.UseTextOptions = true;
            this.colActionType.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colActionType.Caption = "عملیات";
            this.colActionType.FieldName = "ActionType";
            this.colActionType.Name = "colActionType";
            this.colActionType.Visible = true;
            this.colActionType.VisibleIndex = 3;
            this.colActionType.Width = 149;
            // 
            // colActionDateSH
            // 
            this.colActionDateSH.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colActionDateSH.AppearanceCell.Options.UseFont = true;
            this.colActionDateSH.AppearanceCell.Options.UseTextOptions = true;
            this.colActionDateSH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colActionDateSH.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colActionDateSH.AppearanceHeader.Options.UseFont = true;
            this.colActionDateSH.AppearanceHeader.Options.UseTextOptions = true;
            this.colActionDateSH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colActionDateSH.Caption = "تاریخ";
            this.colActionDateSH.FieldName = "ActionDateSH";
            this.colActionDateSH.Name = "colActionDateSH";
            this.colActionDateSH.Visible = true;
            this.colActionDateSH.VisibleIndex = 0;
            this.colActionDateSH.Width = 157;
            // 
            // colAction
            // 
            this.colAction.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colAction.AppearanceCell.Options.UseFont = true;
            this.colAction.AppearanceCell.Options.UseTextOptions = true;
            this.colAction.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAction.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colAction.AppearanceHeader.Options.UseFont = true;
            this.colAction.AppearanceHeader.Options.UseTextOptions = true;
            this.colAction.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAction.Caption = "ساعت";
            this.colAction.DisplayFormat.FormatString = "HH:mm";
            this.colAction.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colAction.FieldName = "ActionDate";
            this.colAction.Name = "colAction";
            this.colAction.Visible = true;
            this.colAction.VisibleIndex = 1;
            this.colAction.Width = 107;
            // 
            // colMode
            // 
            this.colMode.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colMode.AppearanceCell.Options.UseFont = true;
            this.colMode.AppearanceCell.Options.UseTextOptions = true;
            this.colMode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMode.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colMode.AppearanceHeader.Options.UseFont = true;
            this.colMode.AppearanceHeader.Options.UseTextOptions = true;
            this.colMode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMode.Caption = "مانده";
            this.colMode.DisplayFormat.FormatString = "c0";
            this.colMode.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colMode.FieldName = "Mode";
            this.colMode.Name = "colMode";
            this.colMode.Visible = true;
            this.colMode.VisibleIndex = 6;
            this.colMode.Width = 105;
            // 
            // btnShowAllUser
            // 
            this.btnShowAllUser.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnShowAllUser.Appearance.Options.UseFont = true;
            this.btnShowAllUser.Image = ((System.Drawing.Image)(resources.GetObject("btnShowAllUser.Image")));
            this.btnShowAllUser.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnShowAllUser.Location = new System.Drawing.Point(18, 262);
            this.btnShowAllUser.Name = "btnShowAllUser";
            this.btnShowAllUser.Size = new System.Drawing.Size(171, 29);
            this.btnShowAllUser.StyleController = this.LayoutControl2;
            this.btnShowAllUser.TabIndex = 32;
            this.btnShowAllUser.Text = "نمایش همه کاربران";
            // 
            // txtTodate
            // 
            this.txtTodate.EditValue = null;
            this.txtTodate.Location = new System.Drawing.Point(18, 153);
            this.txtTodate.MenuManager = this.ribbon;
            this.txtTodate.Name = "txtTodate";
            this.txtTodate.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.txtTodate.Properties.Appearance.Options.UseFont = true;
            this.txtTodate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTodate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTodate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTodate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTodate.Properties.NullValuePrompt = "تا تاریخ";
            this.txtTodate.Size = new System.Drawing.Size(171, 28);
            this.txtTodate.StyleController = this.LayoutControl2;
            this.txtTodate.TabIndex = 4;
            // 
            // txtFromDate
            // 
            this.txtFromDate.EditValue = null;
            this.txtFromDate.Location = new System.Drawing.Point(18, 94);
            this.txtFromDate.MenuManager = this.ribbon;
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.txtFromDate.Properties.Appearance.Options.UseFont = true;
            this.txtFromDate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFromDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Properties.NullValuePrompt = "از تاریخ";
            this.txtFromDate.Size = new System.Drawing.Size(171, 28);
            this.txtFromDate.StyleController = this.LayoutControl2;
            this.txtFromDate.TabIndex = 3;
            // 
            // cmbUsers
            // 
            this.cmbUsers.Location = new System.Drawing.Point(18, 230);
            this.cmbUsers.MenuManager = this.ribbon;
            this.cmbUsers.Name = "cmbUsers";
            this.cmbUsers.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.cmbUsers.Properties.Appearance.Options.UseFont = true;
            this.cmbUsers.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbUsers.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 36, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "کد", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FullName", "نام کاربر", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Credit", "اعتبار حساب", 47, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Center)});
            this.cmbUsers.Properties.DataSource = this.userBindingSource;
            this.cmbUsers.Properties.DisplayMember = "FullName";
            this.cmbUsers.Properties.NullText = "";
            this.cmbUsers.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.cmbUsers.Properties.PopupSizeable = false;
            this.cmbUsers.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cmbUsers.Properties.ValueMember = "Code";
            this.cmbUsers.Size = new System.Drawing.Size(171, 28);
            this.cmbUsers.StyleController = this.LayoutControl2;
            this.cmbUsers.TabIndex = 29;
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(Model.User);
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup2.GroupBordersVisible = false;
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup3,
            this.LayoutControlItem3,
            this.layUsers});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup2.Name = "Root";
            this.LayoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.LayoutControlGroup2.Size = new System.Drawing.Size(207, 483);
            this.LayoutControlGroup2.TextVisible = false;
            // 
            // LayoutControlGroup3
            // 
            this.LayoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup3.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.LayoutControlGroup3.AppearanceItemCaption.Options.UseFont = true;
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem1});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 30);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Size = new System.Drawing.Size(199, 161);
            this.LayoutControlGroup3.Text = "گزارش تاریخ";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txtFromDate;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(175, 59);
            this.layoutControlItem5.Text = "از تاریخ";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(41, 24);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtTodate;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 59);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(175, 59);
            this.layoutControlItem1.Text = "تا تاریخ";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(41, 24);
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.SearchControl1;
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(199, 30);
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextVisible = false;
            this.LayoutControlItem3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layUsers
            // 
            this.layUsers.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F, System.Drawing.FontStyle.Bold);
            this.layUsers.AppearanceGroup.Options.UseFont = true;
            this.layUsers.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layUsers.AppearanceItemCaption.Options.UseFont = true;
            this.layUsers.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem8});
            this.layUsers.Location = new System.Drawing.Point(0, 191);
            this.layUsers.Name = "layUsers";
            this.layUsers.Size = new System.Drawing.Size(199, 284);
            this.layUsers.Text = "کاربران";
            this.layUsers.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.cmbUsers;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(175, 32);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnShowAllUser;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(175, 207);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // FrmUserCriditReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 717);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.navigationPane1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Name = "FrmUserCriditReport";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "گزارش اعتبار کاربر";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmUserCriditReport_FormClosing);
            this.Load += new System.EventHandler(this.FrmUserCriditReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).EndInit();
            this.navigationPane1.ResumeLayout(false);
            this.navigationPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).EndInit();
            this.LayoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SearchControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportCreditUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUsers.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem btnReport;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Navigation.NavigationPane navigationPane1;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        internal DevExpress.XtraLayout.LayoutControl LayoutControl2;
        internal DevExpress.XtraEditors.SearchControl SearchControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView grd;
        private DevExpress.XtraEditors.SimpleButton btnShowAllUser;
        private FarsiLibrary.Win.DevExpress.XtraFADateEdit txtTodate;
        private FarsiLibrary.Win.DevExpress.XtraFADateEdit txtFromDate;
        private DevExpress.XtraEditors.LookUpEdit cmbUsers;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlGroup layUsers;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private System.Windows.Forms.BindingSource userBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colRecived;
        private DevExpress.XtraGrid.Columns.GridColumn colPayment;
        private DevExpress.XtraGrid.Columns.GridColumn colActionType;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDateSH;
        private DevExpress.XtraGrid.Columns.GridColumn colAction;
        private DevExpress.XtraGrid.Columns.GridColumn colMode;
        private System.Windows.Forms.BindingSource reportCreditUserBindingSource;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
    }
}