﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Model;
using BL;
using DevExpress.XtraEditors;

namespace Test
{
    public partial class FrmUserCriditReport : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public FrmUserCriditReport()
        {
            InitializeComponent();

            if (Userlogin.isAdmin)
                LoadData();
        }


        /// <summary>
        /// نمایش  اطلاعات کاربر  
        /// </summary>
        void LoadData()
        {
            try
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);

                layUsers.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
                btnRefresh.Visibility = BarItemVisibility.Always;

                List<User> listuser = new List<User>();

                if (Userlogin.isAdmin)
                    listuser = UserServices.GetAll();

                cmbUsers.Properties.DataSource = listuser;

                txtFromDate.DateTime = DateTime.Now;
                txtTodate.DateTime = DateTime.Now;

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
            }
            finally
            {

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }

        void Report(DateTime from, DateTime todate)
        {
            try
            {

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);

                List<ReportCreditUser> li = new List<ReportCreditUser>();
                List<ReportCreditUser> report = new List<ReportCreditUser>();

                DateTime _from = DateTime.Parse(from.ToShortDateString());
                DateTime _todate = todate.AddHours(23).AddMinutes(59).AddSeconds(59);


                if (Userlogin.isAdmin)
                {
                    if (cmbUsers.EditValue == null)
                    {
                        XtraMessageBox.Show("لطفا کاربری را انتخاب نمایید");
                        return;
                    }

                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                    li = UserCreditReportServices.ReportUser(_from, _todate, cmbUsers.EditValue.ToString());
                }
                else
                {
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                    li = UserCreditReportServices.ReportUser(_from, _todate, Userlogin.Code);

                }

                ReportCreditUser firstRowMode = li[0];
                report.Add(firstRowMode);

                for (int i = 1; i < li.Count; i++)
                {
                    ReportCreditUser beforeRowMode = li[i - 1];
                    ReportCreditUser currentrow = li[i];
                    long calc1 = Convert.ToInt64(beforeRowMode.Mode - currentrow.Payment);
                    currentrow.Mode = calc1 + currentrow.Recived;
                    report.Add(currentrow);
                }

                gridControl1.DataSource = report;

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
            }
            finally
            {

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }
        private void grd_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {
            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            Report(txtFromDate.DateTime, txtTodate.DateTime);
        }

        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void FrmUserCriditReport_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Hide();
            }
        }

        private void FrmUserCriditReport_Load(object sender, EventArgs e)
        {
            txtFromDate.DateTime = DateTime.Now;
            txtTodate.DateTime = DateTime.Now;
        }
    }
}