﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;

using DevExpress.XtraWaitForm;
using DevExpress.XtraEditors;
using BL;
using System.Threading.Tasks;
using DevExpress.Skins;
using DevExpress.LookAndFeel;

namespace Test
{
    public partial class frmticketHome : DevExpress.XtraBars.Ribbon.RibbonForm
    {


        public frmticketHome()
        {
            InitializeComponent();


            SkinElement element = SkinManager.GetSkinElement(SkinProductId.Ribbon, DevExpress.LookAndFeel.UserLookAndFeel.Default, "Office 2016 Colorful");
            LookAndFeelHelper.ForceDefaultLookAndFeelChanged();

            rbnDefination.Visible = Userlogin.isAdmin;
            if (Userlogin.isAdmin)
            {
                btnSetting.Visibility = BarItemVisibility.Always;
            }
            else
            {
                btnReportTick.Visibility = BarItemVisibility.Never;
                btnSetting.Visibility = BarItemVisibility.Never;
                btnPermission.Visibility = BarItemVisibility.Never;
                btnReportSales.Visibility = BarItemVisibility.Never;
            }

            lblVersion.Caption = "نسخه برنامه :  " + Application.ProductVersion.ToString();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    btnExit_ItemClick(null, null);
                    break;
                case Keys.F2:
                    btnTrip_ItemClick(null, null);
                    break;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }


        private void btnCompany_ItemClick(object sender, ItemClickEventArgs e)
        {

            Form frmticket = Application.OpenForms["frmticketCompany"];
            if (frmticket == null)
            {
                frmticketCompany f = new frmticketCompany();
                f.Show();
            }
            else
            {
                frmticketCompany fm = (frmticketCompany)frmticket;
                fm.Show();
                fm.BringToFront();
            }



        }


        private void btnSetting_ItemClick(object sender, ItemClickEventArgs e)
        {

            Form frmticket = Application.OpenForms["frmticketSetting"];
            if (frmticket == null)
            {
                frmticketSetting f = new frmticketSetting();
                f.Show();
            }
            else
            {
                frmticketSetting fm = (frmticketSetting)frmticket;
                fm.Show();
                fm.BringToFront();
            }
        }

        private void btnUser_ItemClick(object sender, ItemClickEventArgs e)
        {

            Form frmticket = Application.OpenForms["frmticketUser"];
            if (frmticket == null)
            {
                frmticketUser f = new frmticketUser();
                f.Show();
            }
            else
            {
                frmticketUser fm = (frmticketUser)frmticket;
                fm.Show();
                fm.BringToFront();
            }



        }

        private void btnTrip_ItemClick(object sender, ItemClickEventArgs e)
        {

            Form frmticket = Application.OpenForms["frmticketTrip"];
            if (frmticket == null)
            {
                FrmTrips f = new FrmTrips();
                f.Show();
            }
            else
            {
                FrmTrips fm = (FrmTrips)frmticket;
                fm.Show();
                fm.BringToFront();
            }

        }

        private void frmticketHome_Load(object sender, EventArgs e)
        {


        }

        private void frmticketHome_Shown(object sender, EventArgs e)
        {
            Utilitise.ShowWating(this, Utilitise.StateWating.Close);
        }

        private void btnReportSandogh_ItemClick(object sender, ItemClickEventArgs e)
        {

            Form frmticket = Application.OpenForms["frmticketListTicketUser"];
            if (frmticket == null)
            {
                frmticketListTicketUser f = new frmticketListTicketUser();
                f.Show();
            }
            else
            {
                frmticketListTicketUser fm = (frmticketListTicketUser)frmticket;
                fm.Show();
                fm.BringToFront();
            }
        }

        private void btnExit_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (XtraMessageBox.Show("آیا مایل به خروج از برنامه هستید؟", "خطا", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.ExitThread();
            }
        }

        private void frmticketHome_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                Application.ExitThread();
            }
        }


        // کنترل کردن زمان اتمام کاری کاربر
        private async void timer1_Tick(object sender, EventArgs e)
        {
            try
            {


                TimeSpan tt = TimeSpan.Parse(DateTime.Now.ToString("HH:mm"));
                Boolean islogin = false;
                await Task.Run(() =>
                            {
                                islogin = UserServices.LoginTime(tt, Userlogin.ID);
                            }
                    );

                if (!islogin)
                {
                    XtraMessageBox.Show("کاربر گرامی با توجه به زمان تعیین شده امکان استفاده از برنامه در دسترس شما نمی باشد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    Application.ExitThread();
                }

            }
            catch (Exception)
            {


            }

        }

        private void BtnCreditUser_ItemClick(object sender, ItemClickEventArgs e)
        {
            FrmAddCredit fr = new Test.FrmAddCredit();
            fr.ShowDialog();

        }

        private void btnPermission_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = Application.OpenForms["FrmPermission"];
            if (frm == null)
            {
                FrmPermission f = new FrmPermission();
                f.Show();
            }
            else
            {
                FrmPermission fm = (FrmPermission)frm;
                fm.Show();
                fm.BringToFront();
            }

        }

        private void btnReportTick_ItemClick(object sender, ItemClickEventArgs e)
        {

            Form frmticket = Application.OpenForms["FrmReportTicket"];
            if (frmticket == null)
            {
                FrmReportTicket f = new FrmReportTicket();
                f.Show();
            }
            else
            {
                FrmReportTicket fm = (FrmReportTicket)frmticket;
                fm.Show();
                fm.BringToFront();
            }


        }

        private void btnCreditReport_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = Application.OpenForms["FrmUserCriditReport"];
            if (frm == null)
            {
                FrmUserCriditReport f = new FrmUserCriditReport();
                f.Show();
            }
            else
            {
                FrmUserCriditReport fm = (FrmUserCriditReport)frm;
                fm.Show();
                fm.BringToFront();
            }
        }

        private void btnReportSales_ItemClick(object sender, ItemClickEventArgs e)
        {
            Form frm = Application.OpenForms["FrmReportTicketSales"];
            if (frm == null)
            {
                FrmReportTicketSales f = new FrmReportTicketSales();
                f.Show();
            }
            else
            {
                FrmReportTicketSales fm = (FrmReportTicketSales)frm;
                fm.Show();
                fm.BringToFront();
            }
        }
    }
}