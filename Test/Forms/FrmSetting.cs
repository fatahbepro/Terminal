﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;


using Model;
using BL;
using DevExpress.XtraEditors;

namespace Test
{
    public partial class frmticketSetting : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public frmticketSetting()
        {
            InitializeComponent();
            GetList();
            Refresh_frmticket();

        }



        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    btnClose_ItemClick(null, null);
                    break;
                case Keys.F2:
                    btnSave_ItemClick(null, null);
                    break;
                case Keys.F3:
                    BtnEmpty_ItemClick(null, null);
                    break;
                case Keys.F4:
                    btnClose_ItemClick(null, null);
                    break;
                case Keys.F5:
                    btnRefresh_ItemClick(null, null);
                    break;
                case Keys.Delete:
                    btnDelete_ItemClick(null, null);
                    break;

            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


        /// <summary>
        /// نمایش کلیه تنظیمات
        /// </summary>
        void GetList()
        {

            try{
            gridControl1.DataSource = SettingServices.GetAll();

               }
            catch (Exception ex)
            {
                
              GetError.WriteError(ex,this);
                
            }
        }

        // خالی کردن فرم

        void Refresh_frmticket()
        {
            try {
            
                ValueTextEdit.Properties.NullValuePrompt = string.Empty;
                ValueTextEdit.Properties.NullValuePromptShowForEmptyValue = false;

                settingBindingSource.DataSource = new Setting();
            Editvalue = false;
            NameTextEdit.EditValue = string.Empty;
            TitleTextEdit.EditValue = string.Empty;
            ValueTextEdit.EditValue = string.Empty;
            NameTextEdit.Enabled = true;
            TitleTextEdit.Enabled = true;

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void NameTextEdit_Validated(object sender, EventArgs e)
        {
            dxValidationProvider1.Validate(); 
        }


        bool Editvalue;


        // ذخیره تنظیمات
        private void btnSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {

       
            if (!dxValidationProvider1.Validate())
                return;

            int Resut = 0;
            if (Editvalue)
            {
                settingBindingSource.EndEdit();
                var Setting = (Setting)settingBindingSource.Current;
              
                Resut = SettingServices.Update(Setting);

            }
            else
            {
                settingBindingSource.EndEdit();
                var Setting = (Setting)settingBindingSource.Current;
                Resut = SettingServices.Insert(settingBindingSource.Current as Setting);
            }


            if (Resut == 1)
            {
                    XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SettingLogin.ListSettig = SettingServices.GetAll();
                    GetList();
                Refresh_frmticket();
            }
            else
            {
               XtraMessageBox.Show("اشکال در اجرای عملیات", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }

        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            GetList();
        }

        private void BtnEmpty_ItemClick(object sender, ItemClickEventArgs e)
        {
            Refresh_frmticket();
        }


        /// <summary>
        /// حذف تنظیمات مورد نظر
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDelete_ItemClick(object sender, ItemClickEventArgs e)
        {

            try{

            if (XtraMessageBox.Show("آیا مایل به حذف تنظیمات می باشید؟", "پیام", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                String name = grd.GetFocusedRowCellValue(colName).ToString();
                int Resut = SettingServices.Remove(name);

                if (Resut == 1)
                {
                    XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    grd.DeleteSelectedRows();
                }
                else
                {
                   XtraMessageBox.Show("اشکال در اجرای عملیات", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }


               }
            catch (Exception ex)
            {
                
              GetError.WriteError(ex,this);
                
            }
       
        }

        private void grd_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            if(e.Column == colEdit)
            {
                grd_DoubleClick(null, null);
            }
        }

        private void NameTextEdit_Validating(object sender, CancelEventArgs e)
        {
            dxValidationProvider1.Validate(); 
        }

        private void frmticketSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }


       // نمایش تنظیمات در کادر مورد نظر
        private void grd_DoubleClick(object sender, EventArgs e)
        {
            try{

            String name = grd.GetFocusedRowCellValue(colName).ToString();
                var setting = SettingServices.FindByName(name);

                settingBindingSource.DataSource = setting;
            Editvalue = true;
            NameTextEdit.Enabled = false;
            TitleTextEdit.Enabled = false;

                if(setting.Name== "DefaultPaidbackPercent")
                {
                    ValueTextEdit.Properties.NullValuePromptShowForEmptyValue = true;
                    ValueTextEdit.Properties.NullValuePrompt = string.Format("{2}:{1}:{0}", "زمان مانده تا حرکت به دقیقه", "درصد کسر اول", "درصد کسر دوم");
            
                }
            }
            catch (Exception ex)
            {
                
              GetError.WriteError(ex,this);
                
            }
        }
    }
}