﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraGrid;

using Model;
using BL;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using System.Threading.Tasks;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraGrid.Views.Base;

namespace Test
{
    public partial class frmticketListTicketUser : DevExpress.XtraBars.Ribbon.RibbonForm
    {


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    btnClose_ItemClick(null, null);
                    break;
                case Keys.F4:
                    btnClose_ItemClick(null, null);
                    break;
                case Keys.F5:
                    btnRefresh_ItemClick(null, null);
                    break;
                case Keys.F6:
                    btnReport_ItemClick(null, null);
                    break;


            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        public frmticketListTicketUser()
        {
            InitializeComponent();

            if (Userlogin.isAdmin)
            {
                btnCredit_SaleBack.Visibility = BarItemVisibility.Never;
                layUsers.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Always;
            }
            colFundPriceValue.Visible = Userlogin.isAdmin;
            LoadData();

        }

        /// <summary>
        /// لیست کلیه بلیط های خوانده شده
        /// </summary>
        List<TicketLocal> listTicket = new List<TicketLocal>();

        List<Company> AccessCompanyUser;

        string StateTime = "Both";

        /// <summary>
        /// نمایش  اطلاعات کاربر یا شرکتها
        /// </summary>
         void LoadData()
        {
            try
            {


                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);

                List<Company> listcompanys = new List<Company>();
                List<User> listuser = new List<User>();

                listcompanys = CompanysServices.GetAll();
                if (Userlogin.isAdmin)
                    listuser = UserServices.GetAll();
                reConpany.DataSource = listcompanys;
                ChCompany.DataSource = listcompanys;
                cmbUsers.Properties.DataSource = listuser;
                txtFromDate.DateTime = DateTime.Now;
                txtTodate.DateTime = DateTime.Now;
                txtTimeFrom.EditValue = DateTime.Parse("12:00");
                txttimeTo.EditValue = DateTime.Parse("12:00");
                if (Userlogin.AccessCompanies == null)
                {
                    XtraMessageBox.Show("کاربر گرامی سطح دسترسی شما تعریف نشده است", "پیام", MessageBoxButtons.OK);
                    return;
                }
                AccessCompanyUser = CompanysServices.GetCompaniesByListCode(Userlogin.AccessCompanies);  //  لیست شرکت های سطح دسترسی
            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
            }
            finally
            {

                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }


        /// <summary>
        /// گزارش
        /// </summary>
        /// <param name="_fromdate">تاریخ شروع</param>
        /// <param name="_todate">تاریخ اتمام</param>
        async void Report(DateTime _fromdate, DateTime _todate)
        {
            try
            {

                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);

                _todate = Convert.ToDateTime(_todate.ToShortDateString());
                _todate = _todate.AddHours(23).AddMinutes(59).AddSeconds(59);

                List<TicketLocal> lirollback = new List<TicketLocal>();
                List<TicketLocal> lticket = new List<TicketLocal>();

                await Task.Run(() =>
                {
                    if (Userlogin.isAdmin)
                    {
                        string userCode = "";
                        if (cmbUsers.EditValue == null)
                            userCode = Userlogin.Code;

                        listTicket = TicketServices.GetTicketByDate(_fromdate, _todate, Userlogin.isAdmin, userCode, AccessCompanyUser,Userlogin.IsLoacConnection).OrderBy(c => c.SaleDate).ToList();
                        lirollback = TicketServices.GetTicketRollBakByDate(_fromdate, _todate,Userlogin.isAdmin, Userlogin.Code, AccessCompanyUser,Userlogin.IsLoacConnection).OrderBy(c => c.PaidBackDate).ToList();
                        listTicket.AddRange(lirollback.Where(a => !listTicket.Select(c => c.No).Contains(a.No)).ToList());

                    }
                    else
                    {

                        listTicket = TicketServices.GetTicketByDate(_fromdate, _todate, Userlogin.isAdmin, Userlogin.Code, AccessCompanyUser,Userlogin.IsLoacConnection).OrderBy(c => c.SaleDate).ToList();
                        lirollback = TicketServices.GetTicketRollBakByDate(_fromdate, _todate, Userlogin.isAdmin, Userlogin.Code, AccessCompanyUser,Userlogin.IsLoacConnection).OrderBy(c => c.PaidBackDate).ToList();
                        listTicket.AddRange(lirollback.Where(a => !listTicket.Select(c => c.No).Contains(a.No)).ToList());
                    }

                });


                gridControl1.DataSource = listTicket;

                GetSummery();
            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
            finally
            {
                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }

        //List<T> Clone<T>(IEnumerable<T> oldList)
        //{
        //return newList = new List<T>(oldList);
        //}


        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();

        }

        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }




        private string GetFilterStringBindedColumns(string column, CheckedListBoxControl CheckListBox)
        {



            CheckedListBoxControl clbc = (CheckedListBoxControl)CheckListBox;
            string str = string.Empty;

            for (int i = 0; i <= clbc.CheckedItems.Count - 1; i++)
            {
                if (str != string.Empty)
                {
                    str = str + "OR";
                }
                str = str + "[" + column + "] = '" + clbc.CheckedItems[i].ToString() + "' ";
            }

            return str;


        }


        // نماش سرویس های شرکت
        private void ChCompany_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            try
            {
                string str = GetFilterStringBindedColumns("CompanyCode", ChCompany);
                if (string.IsNullOrEmpty(str))
                {
                    grd.ActiveFilter.Clear();
                }
                else
                {
                    grd.Columns["CompanyCode"].FilterInfo = new DevExpress.XtraGrid.Columns.ColumnFilterInfo(str);
                }

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        private void btnReport_ItemClick(object sender, ItemClickEventArgs e)
        {

            Report(Convert.ToDateTime(txtFromDate.DateTime.ToShortDateString()), Convert.ToDateTime(txtTodate.DateTime.ToShortDateString()));
        }

        private void grd_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {

            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void frmticketListTicketUser_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void cmbUsers_EditValueChanged(object sender, EventArgs e)
        {
            string str = "[CashierCode] = '" + cmbUsers.EditValue + "' OR [ReturnUserCode] = '" + cmbUsers.EditValue + "' ";
            grd.ActiveFilterString = str;

        }


        // گزارش بر اساس زمان
        private void timeEdit2_EditValueChanged(object sender, EventArgs e)
        {
            try
            {

                DateTime fromDateTime = Convert.ToDateTime(txtFromDate.DateTime.ToShortDateString()).AddHours(Convert.ToDateTime(txtTimeFrom.EditValue).Hour).AddMinutes(Convert.ToDateTime(txtTimeFrom.EditValue).Minute);
                DateTime toDateTime = Convert.ToDateTime(txtTodate.DateTime.ToShortDateString()).AddHours(Convert.ToDateTime(txttimeTo.EditValue).Hour).AddMinutes(Convert.ToDateTime(txttimeTo.EditValue).Minute);
                //UserListCredit = new Dictionary<int, string>();

                grd.ClearColumnsFilter();

                switch (StateTime)
                {
                    case "Sale":
                        var filterSalelist = listTicket.Where(c => c.SaleDate != null && c.SaleDate.Value>=fromDateTime && c.SaleDate.Value<=toDateTime).ToList();
                        gridControl1.DataSource = filterSalelist;
                        grd.ClearColumnsFilter();
                        grd.ActiveFilterString = "TypeTicket = 'پرداخت کارتخوان'";
                        break;

                    case "SaleCredit":
                        var filterSaleCreditlist = listTicket.Where(c => c.SaleDate != null && c.SaleDate.Value >= fromDateTime && c.SaleDate.Value <= toDateTime).ToList();
                        gridControl1.DataSource = filterSaleCreditlist;
                        grd.ClearColumnsFilter();
                        grd.ActiveFilterString = "TypeTicket = 'پرداخت اعتباری' and StatusPaidBack='برگشت نشده'";
                        break;
                    case "PaidBack":
                        var filterlist = listTicket.Where(c => c.PaidBackDate != null && c.PaidBackDate.Value>=fromDateTime && c.PaidBackDate.Value<=toDateTime).ToList();
                        gridControl1.DataSource = filterlist;
                        break;

                    case "Both":
                        List<TicketLocal> _liFilterPaidBack = listTicket.Where(c =>
                        c.PaidBackDate != null

                        && c.PaidBackDate.Value >= fromDateTime && c.PaidBackDate.Value <= toDateTime

                        ).ToList();


                        var _liFilterSale = listTicket.Where(c =>
                        c.SaleDate != null
                        && c.PaidBackDate == null
                        && c.SaleDate.Value >= fromDateTime && c.SaleDate.Value <= toDateTime
                        ).ToList();

                        List<TicketLocal> list = new List<TicketLocal>();
                        list.AddRange(_liFilterPaidBack);
                        list.AddRange(_liFilterSale);
                        gridControl1.DataSource = list;

                        break;

                    case "Credit":
                        getTime();
                        break;

                }

                grd.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colSaleDateSH, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colTimeSale, DevExpress.Data.ColumnSortOrder.Ascending)

           , new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colPaidBackDateSH, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colPaidBackDateTime, DevExpress.Data.ColumnSortOrder.Ascending)

            });


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
            }
        }

        private void txtFromDate_Click(object sender, EventArgs e)
        {
            txtFromDate.ShowPopup();
        }

        private void txtTodate_Click(object sender, EventArgs e)
        {
            txtTodate.ShowPopup();
        }

        private void cmbUsers_Click(object sender, EventArgs e)
        {
            cmbUsers.ShowPopup();
        }


        // چاپ گزارش
        private void btnPrint_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (!gridControl1.IsPrintingAvailable)
            {
                XtraMessageBox.Show("امکان چاپ وجود ندارد", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            moveColumnGride();


            gridControl1.ShowPrintPreview();
            ResetColumnGride();


        }


        Dictionary<int, string> locColumn = new Dictionary<int, string>();
        void moveColumnGride()
        {

            locColumn = new Dictionary<int, string>();
            foreach (DevExpress.XtraGrid.Columns.GridColumn col in ((ColumnView)gridControl1.Views[0]).Columns)
            {
                if (col.VisibleIndex >= 0 && col.Visible == true)
                {
                    locColumn.Add(col.VisibleIndex, col.Name);
                }

            }

            //  locColumn.OrderBy(c => c.Key);
            //var _locationColumn = (from pair in locColumn
            //              orderby pair.Key descending
            //              select pair).ToList();

            int max = locColumn.Max(c => c.Key);
            int countm = max;
            DevExpress.XtraGrid.Columns.GridColumnCollection columns = grd.Columns;

            for (int i = 0; i <= countm; i++)
            {
                string _columnName = locColumn[i];


                foreach (GridColumn column in columns)
                {
                    if (String.Equals(column.Name, _columnName))
                    {
                        column.VisibleIndex = max;
                        max--;

                    }
                }

            }


        }
        void ResetColumnGride()
        {


            //locColumn = new Dictionary<int, string>();
            //foreach (DevExpress.XtraGrid.Columns.GridColumn col in ((ColumnView)gridControl1.Views[0]).Columns)
            //{
            //    if (col.VisibleIndex >= 0 && col.Visible == true)
            //    {
            //        locColumn.Add(col.VisibleIndex, col.Name);
            //    }

            //}

            //  locColumn.OrderBy(c => c.Key);
            //var _locationColumn = (from pair in locColumn
            //              orderby pair.Key descending
            //              select pair).ToList();

            int max = locColumn.Max(c => c.Key);
            int counter = max;
            DevExpress.XtraGrid.Columns.GridColumnCollection columns = grd.Columns;

            int min = locColumn.Min(c => c.Key);
            for (int i = 0; i <= counter; i++)
            {
                string _columnName = locColumn[i];


                foreach (GridColumn column in columns)
                {
                    if (String.Equals(column.Name, _columnName))
                    {
                        column.VisibleIndex = i;
                        //max--;

                    }
                }

            }

        }

        private void ribbon_Click(object sender, EventArgs e)
        {

        }

        private void BtnTicketInternet_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {

                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                DateTime _from = DateTime.Parse(txtFromDate.DateTime.ToShortDateString());
                DateTime _todate = Convert.ToDateTime(txtTodate.DateTime.ToShortDateString());
                _todate = _todate.AddHours(23).AddMinutes(59).AddSeconds(59);
                if (Userlogin.isAdmin)
                {
                    string userCode = "";
                    if (cmbUsers.EditValue == null)
                        userCode = Userlogin.Code;
                    listTicket = TicketServices.GetTicketNetByDate(_from, _todate, AccessCompanyUser,Userlogin.IsLoacConnection);
                }
                else
                {
                    listTicket = TicketServices.GetTicketNetByDate(_from, _todate, AccessCompanyUser,Userlogin.IsLoacConnection);
                }


                gridControl1.DataSource = listTicket;
                GetSummery();



            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
            finally
            {
                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }


        }

        private void frmticketListTicketUser_Load(object sender, EventArgs e)
        {
            Report(Convert.ToDateTime(DateTime.Now.ToShortDateString()), Convert.ToDateTime(DateTime.Now.ToShortDateString()));

        }

        private void btnPayPose_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl1.DataSource = listTicket;

            grd.ClearColumnsFilter();
            grd.ActiveFilterString = "TypeTicket= 'پرداخت کارتخوان'";
            GetSummery();
            StateTime = "Sale";

            grd.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colSaleDateSH, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colTimeSale, DevExpress.Data.ColumnSortOrder.Ascending)});

        }

        private void btnPayCredit_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl1.DataSource = listTicket;
            grd.ClearColumnsFilter();
            grd.ActiveFilterString = "TypeTicket = 'پرداخت اعتباری' and StatusPaidBack='برگشت نشده'";
            GetSummery();
            StateTime = "SaleCredit";
            grd.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colSaleDateSH, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colTimeSale, DevExpress.Data.ColumnSortOrder.Ascending)});


        }

        private void btnshowAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl1.DataSource = listTicket;
            grd.ClearColumnsFilter();
            GetSummery();
            StateTime = "Both";

            grd.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colSaleDateSH, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colTimeSale, DevExpress.Data.ColumnSortOrder.Ascending)

           , new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colPaidBackDateSH, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colPaidBackDateTime, DevExpress.Data.ColumnSortOrder.Ascending)

            });

        }

        private void btnStatePaidBack_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridControl1.DataSource = listTicket;
            grd.ClearColumnsFilter();
            grd.ActiveFilterString = "StatusPaidBack like '%برگشتی%'";
            GetSummery();
            colReturnUserName.Visible = true;
            StateTime = "PaidBack";



            grd.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colPaidBackDateSH, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colPaidBackDateTime, DevExpress.Data.ColumnSortOrder.Ascending)});


        }

        private void btnShowAllUser_Click(object sender, EventArgs e)
        {
            grd.ClearColumnsFilter();
        }

        private void navigationPane1_Click(object sender, EventArgs e)
        {

        }

        void GetSummery()
        {
            txtSum.EditValue = colTotalTicket.SummaryItem.SummaryValue;
            txtFundPrice.EditValue = colFundPriceValue.SummaryItem.SummaryValue;
            txtPriceRollBack.EditValue = colPricePaidBack.SummaryItem.SummaryValue;
            colUserCredit.Visible = false;
            colReturnUserName.Visible = false;

        }

        private void gridControl1_Click(object sender, EventArgs e)
        {

        }


        private void btnCredit_SaleBack_ItemClick(object sender, ItemClickEventArgs e)
        {

            List<TicketLocal> listAllCreditPaidBack = new List<TicketLocal>();
            listAllCreditPaidBack.AddRange(listTicket.OrderBy(c => c.SaleDate));
            // خواندن بلیط هایی که کاربر فعلی فروخته و همان را برگشت زده است
            List<TicketLocal> lipaidback = listTicket.Where(c => c.ReturnUserCode != null && c.ReturnUserCode == c.CashierCode && c.CashierCode == Userlogin.Code).ToList();
            lipaidback.OrderBy(c => c.PaidBackDate).ToList();

            foreach (TicketLocal item in lipaidback)
            {
                if (Convert.ToDateTime(item.SaleDate.Value.ToShortDateString()) == Convert.ToDateTime(item.PaidBackDate.Value.ToShortDateString()))
                {
                    TicketLocal newitem = new TicketLocal();
                    listAllCreditPaidBack.Remove(item);
                    CopyClass.Copy<TicketLocal>(item, newitem);
                    newitem.PaidBack = 0;
                    newitem.PaidBackDate = null;
                    newitem.PaidBackPercent = 0;
                    newitem.PayDate = null;
                    newitem.PricePaidBack = 0;
                    newitem.ReturnUserCode = null;
                    newitem.ReturnUserID = null;
                    newitem.ReturnUserName = null;
                    listAllCreditPaidBack.Add(newitem);
                    listAllCreditPaidBack.Add(item);

                }

            }

            UserListCredit = new Dictionary<int, string>();
            sumCreditUser = 0;
            listAllCreditPaidBack.RemoveAll(c => c.TypeTicket.Contains("پرداخت کارتخوان"));
            gridControl1.DataSource = listAllCreditPaidBack.ToList();


            //grd.ClearColumnsFilter();
            //grd.ActiveFilterString = "TypeTicket <> 'پرداخت کارتخوان'";
            GetSummery();
            colReturnUserName.Visible = true;
            colUserCredit.Visible = true;

            StateTime = "Credit";


        }


        void getTime()
        {
            List<TicketLocal> _litick = new List<TicketLocal>();
            List<TicketLocal> _litickDuplicate = new List<TicketLocal>();
            List<TicketLocal> _RemoveItem = new List<TicketLocal>();
            DateTime fromDateTime = Convert.ToDateTime(txtFromDate.DateTime.ToShortDateString()).AddHours(Convert.ToDateTime(txtTimeFrom.EditValue).Hour).AddMinutes(Convert.ToDateTime(txtTimeFrom.EditValue).Minute);
            DateTime toDateTime = Convert.ToDateTime(txtTodate.DateTime.ToShortDateString()).AddHours(Convert.ToDateTime(txttimeTo.EditValue).Hour).AddMinutes(Convert.ToDateTime(txttimeTo.EditValue).Minute);
            _litick.AddRange(listTicket.Where(c => c.TypeTicket != "پرداخت کارتخوان" && c.SaleDate.Value>=fromDateTime && c.SaleDate.Value<=toDateTime).OrderBy(c => c.SaleDate));

            // خواندن بلیط هایی که کاربر فعلی فروخته و همان را برگشت زده است
            List<TicketLocal> lipaidback = listTicket.Where(c =>
                        c.PaidBackDate != null
                        && c.ReturnUserCode == Userlogin.Code && c.PaidBackDate.Value>=fromDateTime && c.PaidBackDate.Value<=toDateTime).ToList();
            lipaidback.OrderBy(c => c.PaidBackDate).ToList();
            List<TicketLocal> tempList=new List<TicketLocal>();
            tempList.AddRange(_litick);
            foreach (TicketLocal item in tempList)
            {
                if (item.PaidBackDate != null)
                {

                    TicketLocal newitem = new TicketLocal();
                    CopyClass.Copy<TicketLocal>(item, newitem);
                    newitem.PaidBack = 0;
                    newitem.PaidBackDate = null;
                    newitem.PaidBackPercent = 0;
                    newitem.PayDate = null;
                    newitem.PricePaidBack = 0;
                    newitem.ReturnUserCode = null;
                    newitem.ReturnUserID = null;
                    newitem.ReturnUserName = null;
                    _litick.Remove(item);
                    _litick.Add(newitem);

                }

            }
            _litick.AddRange(lipaidback);
            //foreach (TicketLocal item in lipaidback)
            //{
            //    if (Convert.ToDateTime(item.SaleDate.Value.ToShortDateString()) == Convert.ToDateTime(item.PaidBackDate.Value.ToShortDateString()))
            //    {
            //        TicketLocal newitem = new TicketLocal();

            //        if (_litick.Contains(item))
            //        {
            //            _RemoveItem.Add(item);

            //            _litick.Remove(item); CopyClass.Copy<TicketLocal>(item, newitem);
            //            newitem.PaidBack = 0;
            //            newitem.PaidBackDate = null;
            //            newitem.PaidBackPercent = 0;
            //            newitem.PayDate = null;
            //            newitem.PricePaidBack = 0;
            //            newitem.StatusPaidBack = "برگشت نشده";
            //            newitem.ReturnUserCode = null;
            //            newitem.ReturnUserID = null;
            //            newitem.ReturnUserName = null;
            //            _litick.Add(newitem);
            //            _litick.Add(item);
            //        }


            //    }


            //}
            //_litick.AddRange(lipaidback);
            //foreach (TicketLocal item in _litick)
            //{
            //    if (item.PaidBackDate != null)
            //    {

            //        List<TicketLocal> counters = _litick.Where(c => c.ID == item.ID).ToList();

            //        if (counters.Count == 1)
            //        {
            //            if (Convert.ToDateTime(item.SaleDate.Value.ToShortDateString()) == Convert.ToDateTime(item.PaidBackDate.Value.ToShortDateString()))
            //            {

            //                TicketLocal newitem = new TicketLocal();
            //                _RemoveItem.Add(item);
            //                CopyClass.Copy<TicketLocal>(item, newitem);
            //                newitem.PaidBack = 0;
            //                newitem.PaidBackDate = null;
            //                newitem.PaidBackPercent = 0;
            //                newitem.PayDate = null;
            //                newitem.PricePaidBack = 0;
            //                newitem.StatusPaidBack = "برگشت نشده";
            //                newitem.ReturnUserCode = null;
            //                newitem.ReturnUserID = null;
            //                newitem.ReturnUserName = null;
            //                _litickDuplicate.Add(newitem);

            //            }

            //        }



            //    }

            //}

            //foreach (var item in _RemoveItem)
            //{
            //    _litick.Remove(item);
            //}

            //_litick.AddRange(_litickDuplicate);

            UserListCredit = new Dictionary<int, string>();
            sumCreditUser = 0;
            _litick = _litick.OrderBy(c => c.SaleDate).ToList();
            gridControl1.DataSource = _litick;


            grd.ClearColumnsFilter();
            //grd.ActiveFilterString = "TypeTicket <> 'پرداخت کارتخوان'";
            GetSummery();
            colReturnUserName.Visible = true;
            colUserCredit.Visible = true;
        }
        int sumCreditUser = 0;


        Dictionary<int, string> UserListCredit = new Dictionary<int, string>();
        private void grd_CustomUnboundColumnData(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDataEventArgs e)
        {
            try
            {

                if (UserListCredit.ContainsKey(e.ListSourceRowIndex))
                {
                    e.Value = UserListCredit[e.ListSourceRowIndex];
                    return;
                }


                if (e.Column.FieldName != "UerCredit") return;
                int row = e.ListSourceRowIndex;

                #region محاسبه حساب کاربر

                if (row == 0)
                    sumCreditUser = 0;


                int TotalTicket = 0 - Convert.ToInt32(grd.GetListSourceRowCellValue(row, colTotalTicket).ToString());
                int PricePaidBack = Convert.ToInt32(grd.GetListSourceRowCellValue(row, colPricePaidBack).ToString());
                var ReturnUserCode = grd.GetListSourceRowCellValue(row, colReturnUserCode);
                var FullName = grd.GetListSourceRowCellValue(row, colFullname);

                if (PricePaidBack > 0)
                {
                    if (ReturnUserCode == null)
                    {

                        sumCreditUser += PricePaidBack;
                    }
                    else
                    {
                        if (ReturnUserCode.ToString() == Userlogin.Code)
                            sumCreditUser += PricePaidBack;
                        else
                            sumCreditUser += TotalTicket;
                    }
                }
                else
                {
                    sumCreditUser += TotalTicket;
                }

                if (sumCreditUser < 0)
                {
                    int sum = sumCreditUser;
                    e.Value = System.Math.Abs(sum).ToString() + " -";
                    UserListCredit[e.ListSourceRowIndex] = e.Value.ToString();
                }
                else
                {
                    e.Value = sumCreditUser;
                    UserListCredit[e.ListSourceRowIndex] = e.Value.ToString();
                }

                //if (grd.RowCount == (row + 1))
                //    sumCreditUser = 0;


                #endregion



            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }



        }

        private void grd_RowClick(object sender, RowClickEventArgs e)
        {

        }

        private void btnShowAllTime_Click(object sender, EventArgs e)
        {
            grd.ClearColumnsFilter();
        }

        private void btnRemoveFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
            Report(Convert.ToDateTime(DateTime.Now.ToShortDateString()), Convert.ToDateTime(DateTime.Now.ToShortDateString()));

        }
    }
}