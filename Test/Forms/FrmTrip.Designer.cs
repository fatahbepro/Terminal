﻿namespace Test
{
    partial class FrmTrips
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTrips));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem2 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barCreditUser = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.getTripsReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCarTypeTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDateSH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDepartureTime2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumTickets = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumReserves = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDay = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPathTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reBtnSale = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colTicket = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reTicket = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPathID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTripID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLayoutID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTripLocked = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colServiceID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCitiesBetween = new DevExpress.XtraGrid.Columns.GridColumn();
            this.txtFromDate = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.BtnTicketTripSelected = new DevExpress.XtraEditors.SimpleButton();
            this.lblNameDay = new DevExpress.XtraEditors.LabelControl();
            this.btnshowReport = new DevExpress.XtraEditors.SimpleButton();
            this.btnNextDate = new DevExpress.XtraEditors.SimpleButton();
            this.btnNowDate = new DevExpress.XtraEditors.SimpleButton();
            this.btnBackDate = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnClearSearchTrip = new DevExpress.XtraEditors.SimpleButton();
            this.ChCompany = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.companyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ChPath = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.pathTripBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cmbCities = new DevExpress.XtraEditors.GridLookUpEdit();
            this.spGetCitiesTripResultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.cityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.navigationPane1 = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.galleryDropDown1 = new DevExpress.XtraBars.Ribbon.GalleryDropDown(this.components);
            this.popupMenu1 = new DevExpress.XtraBars.PopupMenu(this.components);
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Test.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getTripsReportBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnSale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reTicket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).BeginInit();
            this.LayoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ChCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pathTripBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCities.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spGetCitiesTripResultBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).BeginInit();
            this.navigationPane1.SuspendLayout();
            this.navigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.AllowDrawArrow = false;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.ExpandCollapseItem.ShowItemShortcut = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnRefresh,
            this.barButtonItem1,
            this.btnReport,
            this.barButtonItem2,
            this.barCreditUser});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 7;
            this.ribbon.Name = "ribbon";
            this.ribbon.OptionsTouch.TouchUI = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1372, 146);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "بروزرسانی تعاونی ها";
            this.btnRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Glyph")));
            this.btnRefresh.Id = 1;
            this.btnRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btnRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.LargeGlyph")));
            this.btnRefresh.Name = "btnRefresh";
            toolTipTitleItem1.Text = "بروزرسانی";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "نمایش کلیه تعاونی هایی که کاربر سطح دسترسی به آنها را دارد";
            toolTipTitleItem2.LeftIndent = 6;
            toolTipTitleItem2.Text = "F5";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipSeparatorItem1);
            superToolTip1.Items.Add(toolTipTitleItem2);
            this.btnRefresh.SuperTip = superToolTip1;
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "بستن";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // btnReport
            // 
            this.btnReport.Caption = "گزارش";
            this.btnReport.Id = 4;
            this.btnReport.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6);
            this.btnReport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnReport.LargeGlyph")));
            this.btnReport.Name = "btnReport";
            toolTipTitleItem3.Text = "گزارش سرویس";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "نمایش کلیه سرویس های ثبت شده در تاریخ مورد نظر";
            toolTipTitleItem4.LeftIndent = 6;
            toolTipTitleItem4.Text = "F6";
            superToolTip2.Items.Add(toolTipTitleItem3);
            superToolTip2.Items.Add(toolTipItem2);
            superToolTip2.Items.Add(toolTipSeparatorItem2);
            superToolTip2.Items.Add(toolTipTitleItem4);
            this.btnReport.SuperTip = superToolTip2;
            this.btnReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 5;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barCreditUser
            // 
            this.barCreditUser.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barCreditUser.Caption = "اعتبار حساب کاربر";
            this.barCreditUser.Edit = this.repositoryItemTextEdit1;
            this.barCreditUser.EditWidth = 200;
            this.barCreditUser.Id = 6;
            this.barCreditUser.Name = "barCreditUser";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.repositoryItemTextEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "c0";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.EditFormat.FormatString = "c0";
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "c0";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.ReadOnly = true;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup4});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "عملیات";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnReport);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "گزارش";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnRefresh);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "بروز رسانی";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barCreditUser);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 757);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1372, 21);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.getTripsReportBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(12, 54);
            this.gridControl1.MainView = this.grd;
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.reBtnSale,
            this.reTicket});
            this.gridControl1.Size = new System.Drawing.Size(1070, 545);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grd});
            // 
            // getTripsReportBindingSource
            // 
            this.getTripsReportBindingSource.DataSource = typeof(Model.GetTripsReport);
            // 
            // grd
            // 
            this.grd.ActiveFilterEnabled = false;
            this.grd.Appearance.FocusedCell.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedCell.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grd.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grd.Appearance.FocusedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grd.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grd.Appearance.FooterPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.FooterPanel.Options.UseFont = true;
            this.grd.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grd.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.SelectedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grd.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grd.ColumnPanelRowHeight = 30;
            this.grd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCompanyName,
            this.colCarTypeTitle,
            this.colDateSH,
            this.colDepartureTime2,
            this.colNumTickets,
            this.colNumReserves,
            this.colDay,
            this.colPathTitle,
            this.colSale,
            this.colTicket,
            this.colPathID,
            this.colCompanyCode,
            this.colTotalPrice,
            this.colTripID,
            this.colLayoutID,
            this.colTripLocked,
            this.colServiceID,
            this.colCitiesBetween});
            this.grd.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFullFocus;
            this.grd.GridControl = this.gridControl1;
            this.grd.IndicatorWidth = 30;
            this.grd.Name = "grd";
            this.grd.OptionsBehavior.Editable = false;
            this.grd.OptionsCustomization.AllowFilter = false;
            this.grd.OptionsFind.FindNullPrompt = "جست و جو . . . ";
            this.grd.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grd.OptionsView.ShowFooter = true;
            this.grd.OptionsView.ShowGroupPanel = false;
            this.grd.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colDepartureTime2, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.grd.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grd_RowCellClick);
            this.grd.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grd_CustomDrawRowIndicator);
            this.grd.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grd_RowStyle);
            this.grd.MouseMove += new System.Windows.Forms.MouseEventHandler(this.grd_MouseMove);
            // 
            // colCompanyName
            // 
            this.colCompanyName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCompanyName.AppearanceCell.Options.UseFont = true;
            this.colCompanyName.AppearanceCell.Options.UseTextOptions = true;
            this.colCompanyName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCompanyName.AppearanceHeader.Options.UseFont = true;
            this.colCompanyName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCompanyName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyName.Caption = "تعاونی";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "CompanyName", "{0}")});
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 0;
            this.colCompanyName.Width = 128;
            // 
            // colCarTypeTitle
            // 
            this.colCarTypeTitle.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCarTypeTitle.AppearanceCell.Options.UseFont = true;
            this.colCarTypeTitle.AppearanceCell.Options.UseTextOptions = true;
            this.colCarTypeTitle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCarTypeTitle.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCarTypeTitle.AppearanceHeader.Options.UseFont = true;
            this.colCarTypeTitle.AppearanceHeader.Options.UseTextOptions = true;
            this.colCarTypeTitle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCarTypeTitle.Caption = "نوع سرویس";
            this.colCarTypeTitle.FieldName = "CarTypeTitle";
            this.colCarTypeTitle.Name = "colCarTypeTitle";
            this.colCarTypeTitle.Visible = true;
            this.colCarTypeTitle.VisibleIndex = 3;
            this.colCarTypeTitle.Width = 104;
            // 
            // colDateSH
            // 
            this.colDateSH.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colDateSH.AppearanceCell.Options.UseFont = true;
            this.colDateSH.AppearanceCell.Options.UseTextOptions = true;
            this.colDateSH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateSH.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colDateSH.AppearanceHeader.Options.UseFont = true;
            this.colDateSH.AppearanceHeader.Options.UseTextOptions = true;
            this.colDateSH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDateSH.Caption = "ت حرکت";
            this.colDateSH.FieldName = "DateSH";
            this.colDateSH.Name = "colDateSH";
            this.colDateSH.Visible = true;
            this.colDateSH.VisibleIndex = 5;
            this.colDateSH.Width = 100;
            // 
            // colDepartureTime2
            // 
            this.colDepartureTime2.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colDepartureTime2.AppearanceCell.Options.UseFont = true;
            this.colDepartureTime2.AppearanceCell.Options.UseTextOptions = true;
            this.colDepartureTime2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDepartureTime2.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colDepartureTime2.AppearanceHeader.Options.UseFont = true;
            this.colDepartureTime2.AppearanceHeader.Options.UseTextOptions = true;
            this.colDepartureTime2.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDepartureTime2.Caption = "س حرکت";
            this.colDepartureTime2.FieldName = "DepartureTime2";
            this.colDepartureTime2.Name = "colDepartureTime2";
            this.colDepartureTime2.Visible = true;
            this.colDepartureTime2.VisibleIndex = 6;
            this.colDepartureTime2.Width = 100;
            // 
            // colNumTickets
            // 
            this.colNumTickets.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colNumTickets.AppearanceCell.Options.UseFont = true;
            this.colNumTickets.AppearanceCell.Options.UseTextOptions = true;
            this.colNumTickets.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumTickets.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colNumTickets.AppearanceHeader.Options.UseFont = true;
            this.colNumTickets.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumTickets.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumTickets.Caption = "ت بلیط";
            this.colNumTickets.FieldName = "NumTickets";
            this.colNumTickets.Name = "colNumTickets";
            this.colNumTickets.Visible = true;
            this.colNumTickets.VisibleIndex = 7;
            this.colNumTickets.Width = 41;
            // 
            // colNumReserves
            // 
            this.colNumReserves.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colNumReserves.AppearanceCell.Options.UseFont = true;
            this.colNumReserves.AppearanceCell.Options.UseTextOptions = true;
            this.colNumReserves.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumReserves.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colNumReserves.AppearanceHeader.Options.UseFont = true;
            this.colNumReserves.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumReserves.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumReserves.Caption = "ت رزور";
            this.colNumReserves.FieldName = "NumReserves";
            this.colNumReserves.Name = "colNumReserves";
            this.colNumReserves.Visible = true;
            this.colNumReserves.VisibleIndex = 8;
            this.colNumReserves.Width = 52;
            // 
            // colDay
            // 
            this.colDay.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colDay.AppearanceCell.Options.UseFont = true;
            this.colDay.AppearanceCell.Options.UseTextOptions = true;
            this.colDay.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDay.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colDay.AppearanceHeader.Options.UseFont = true;
            this.colDay.AppearanceHeader.Options.UseTextOptions = true;
            this.colDay.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDay.Caption = "روز";
            this.colDay.FieldName = "Day";
            this.colDay.Name = "colDay";
            this.colDay.Visible = true;
            this.colDay.VisibleIndex = 4;
            this.colDay.Width = 90;
            // 
            // colPathTitle
            // 
            this.colPathTitle.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPathTitle.AppearanceCell.Options.UseFont = true;
            this.colPathTitle.AppearanceCell.Options.UseTextOptions = true;
            this.colPathTitle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPathTitle.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPathTitle.AppearanceHeader.Options.UseFont = true;
            this.colPathTitle.AppearanceHeader.Options.UseTextOptions = true;
            this.colPathTitle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPathTitle.Caption = "مسیر";
            this.colPathTitle.FieldName = "PathTitle";
            this.colPathTitle.Name = "colPathTitle";
            this.colPathTitle.Visible = true;
            this.colPathTitle.VisibleIndex = 1;
            this.colPathTitle.Width = 163;
            // 
            // colSale
            // 
            this.colSale.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.colSale.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSale.AppearanceCell.ForeColor = System.Drawing.Color.Black;
            this.colSale.AppearanceCell.Options.UseBackColor = true;
            this.colSale.AppearanceCell.Options.UseFont = true;
            this.colSale.AppearanceCell.Options.UseForeColor = true;
            this.colSale.AppearanceCell.Options.UseTextOptions = true;
            this.colSale.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSale.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSale.AppearanceHeader.Options.UseFont = true;
            this.colSale.AppearanceHeader.Options.UseTextOptions = true;
            this.colSale.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSale.ColumnEdit = this.reBtnSale;
            this.colSale.MaxWidth = 90;
            this.colSale.MinWidth = 40;
            this.colSale.Name = "colSale";
            this.colSale.Visible = true;
            this.colSale.VisibleIndex = 9;
            this.colSale.Width = 81;
            // 
            // reBtnSale
            // 
            this.reBtnSale.AutoHeight = false;
            this.reBtnSale.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "فروش", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.TopCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F9), serializableAppearanceObject1, "", null, null, true)});
            this.reBtnSale.Name = "reBtnSale";
            this.reBtnSale.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // colTicket
            // 
            this.colTicket.AppearanceCell.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.colTicket.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTicket.AppearanceCell.Options.UseBackColor = true;
            this.colTicket.AppearanceCell.Options.UseFont = true;
            this.colTicket.AppearanceCell.Options.UseTextOptions = true;
            this.colTicket.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTicket.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTicket.AppearanceHeader.Options.UseFont = true;
            this.colTicket.AppearanceHeader.Options.UseTextOptions = true;
            this.colTicket.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTicket.ColumnEdit = this.reTicket;
            this.colTicket.MaxWidth = 90;
            this.colTicket.MinWidth = 40;
            this.colTicket.Name = "colTicket";
            this.colTicket.Visible = true;
            this.colTicket.VisibleIndex = 10;
            this.colTicket.Width = 88;
            // 
            // reTicket
            // 
            this.reTicket.AccessibleDescription = "";
            this.reTicket.AutoHeight = false;
            this.reTicket.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "بلیط ها", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F10), serializableAppearanceObject2, "", null, null, true)});
            this.reTicket.Name = "reTicket";
            this.reTicket.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            // 
            // colPathID
            // 
            this.colPathID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPathID.AppearanceCell.Options.UseFont = true;
            this.colPathID.AppearanceCell.Options.UseTextOptions = true;
            this.colPathID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPathID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPathID.AppearanceHeader.Options.UseFont = true;
            this.colPathID.AppearanceHeader.Options.UseTextOptions = true;
            this.colPathID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPathID.Caption = "کد مسیر";
            this.colPathID.FieldName = "PathID";
            this.colPathID.Name = "colPathID";
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCompanyCode.AppearanceCell.Options.UseFont = true;
            this.colCompanyCode.AppearanceCell.Options.UseTextOptions = true;
            this.colCompanyCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyCode.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCompanyCode.AppearanceHeader.Options.UseFont = true;
            this.colCompanyCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCompanyCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyCode.Caption = "کد تعاونی";
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.Width = 94;
            // 
            // colTotalPrice
            // 
            this.colTotalPrice.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTotalPrice.AppearanceCell.Options.UseFont = true;
            this.colTotalPrice.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalPrice.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTotalPrice.AppearanceHeader.Options.UseFont = true;
            this.colTotalPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalPrice.Caption = "مبلغ";
            this.colTotalPrice.FieldName = "TotalPrice";
            this.colTotalPrice.Name = "colTotalPrice";
            // 
            // colTripID
            // 
            this.colTripID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTripID.AppearanceCell.Options.UseFont = true;
            this.colTripID.AppearanceCell.Options.UseTextOptions = true;
            this.colTripID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTripID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTripID.AppearanceHeader.Options.UseFont = true;
            this.colTripID.AppearanceHeader.Options.UseTextOptions = true;
            this.colTripID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTripID.Caption = "کد";
            this.colTripID.FieldName = "TripID";
            this.colTripID.Name = "colTripID";
            this.colTripID.Width = 52;
            // 
            // colLayoutID
            // 
            this.colLayoutID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colLayoutID.AppearanceCell.Options.UseFont = true;
            this.colLayoutID.AppearanceCell.Options.UseTextOptions = true;
            this.colLayoutID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLayoutID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colLayoutID.AppearanceHeader.Options.UseFont = true;
            this.colLayoutID.AppearanceHeader.Options.UseTextOptions = true;
            this.colLayoutID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLayoutID.FieldName = "LayoutID";
            this.colLayoutID.Name = "colLayoutID";
            // 
            // colTripLocked
            // 
            this.colTripLocked.Caption = "قفل سرویس";
            this.colTripLocked.FieldName = "Locked";
            this.colTripLocked.Name = "colTripLocked";
            // 
            // colServiceID
            // 
            this.colServiceID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colServiceID.AppearanceCell.Options.UseFont = true;
            this.colServiceID.AppearanceCell.Options.UseTextOptions = true;
            this.colServiceID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colServiceID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colServiceID.AppearanceHeader.Options.UseFont = true;
            this.colServiceID.AppearanceHeader.Options.UseTextOptions = true;
            this.colServiceID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colServiceID.Caption = "کد سرویس";
            this.colServiceID.FieldName = "ServiceID";
            this.colServiceID.Name = "colServiceID";
            this.colServiceID.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "ServiceID", "{0}")});
            this.colServiceID.Width = 87;
            // 
            // colCitiesBetween
            // 
            this.colCitiesBetween.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCitiesBetween.AppearanceCell.Options.UseFont = true;
            this.colCitiesBetween.AppearanceCell.Options.UseTextOptions = true;
            this.colCitiesBetween.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCitiesBetween.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCitiesBetween.AppearanceHeader.Options.UseFont = true;
            this.colCitiesBetween.AppearanceHeader.Options.UseTextOptions = true;
            this.colCitiesBetween.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCitiesBetween.Caption = "شهرهای بین راهی";
            this.colCitiesBetween.FieldName = "CitiesBetween";
            this.colCitiesBetween.Name = "colCitiesBetween";
            this.colCitiesBetween.Visible = true;
            this.colCitiesBetween.VisibleIndex = 2;
            this.colCitiesBetween.Width = 110;
            // 
            // txtFromDate
            // 
            this.txtFromDate.EditValue = null;
            this.txtFromDate.Location = new System.Drawing.Point(396, 12);
            this.txtFromDate.MenuManager = this.ribbon;
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 12F);
            this.txtFromDate.Properties.Appearance.Options.UseFont = true;
            this.txtFromDate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFromDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Size = new System.Drawing.Size(206, 34);
            this.txtFromDate.StyleController = this.dataLayoutControl1;
            this.txtFromDate.TabIndex = 3;
            this.txtFromDate.EditValueChanged += new System.EventHandler(this.txtFromDate_EditValueChanged);
            this.txtFromDate.Click += new System.EventHandler(this.txtFromDate_Click);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.BtnTicketTripSelected);
            this.dataLayoutControl1.Controls.Add(this.lblNameDay);
            this.dataLayoutControl1.Controls.Add(this.btnshowReport);
            this.dataLayoutControl1.Controls.Add(this.btnNextDate);
            this.dataLayoutControl1.Controls.Add(this.btnNowDate);
            this.dataLayoutControl1.Controls.Add(this.btnBackDate);
            this.dataLayoutControl1.Controls.Add(this.txtFromDate);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 146);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(674, 491, 250, 350);
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1094, 611);
            this.dataLayoutControl1.TabIndex = 27;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // BtnTicketTripSelected
            // 
            this.BtnTicketTripSelected.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 11F);
            this.BtnTicketTripSelected.Appearance.Options.UseFont = true;
            this.BtnTicketTripSelected.Location = new System.Drawing.Point(892, 12);
            this.BtnTicketTripSelected.Name = "BtnTicketTripSelected";
            this.BtnTicketTripSelected.Size = new System.Drawing.Size(190, 38);
            this.BtnTicketTripSelected.StyleController = this.dataLayoutControl1;
            this.BtnTicketTripSelected.TabIndex = 22;
            this.BtnTicketTripSelected.Text = "بلیط های رزوری و فروخته شده";
            this.BtnTicketTripSelected.Click += new System.EventHandler(this.BtnTicketTripSelected_Click);
            // 
            // lblNameDay
            // 
            this.lblNameDay.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.lblNameDay.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblNameDay.Location = new System.Drawing.Point(686, 12);
            this.lblNameDay.Name = "lblNameDay";
            this.lblNameDay.Size = new System.Drawing.Size(132, 24);
            this.lblNameDay.StyleController = this.dataLayoutControl1;
            this.lblNameDay.TabIndex = 21;
            this.lblNameDay.Text = "نام روز";
            // 
            // btnshowReport
            // 
            this.btnshowReport.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnshowReport.Appearance.Options.UseFont = true;
            this.btnshowReport.Image = ((System.Drawing.Image)(resources.GetObject("btnshowReport.Image")));
            this.btnshowReport.Location = new System.Drawing.Point(122, 12);
            this.btnshowReport.Name = "btnshowReport";
            this.btnshowReport.Size = new System.Drawing.Size(138, 38);
            this.btnshowReport.StyleController = this.dataLayoutControl1;
            this.btnshowReport.TabIndex = 20;
            this.btnshowReport.Text = "جست و جو F6";
            this.btnshowReport.Click += new System.EventHandler(this.btnshowReport_Click);
            // 
            // btnNextDate
            // 
            this.btnNextDate.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnNextDate.Appearance.Options.UseFont = true;
            this.btnNextDate.Image = ((System.Drawing.Image)(resources.GetObject("btnNextDate.Image")));
            this.btnNextDate.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnNextDate.Location = new System.Drawing.Point(642, 12);
            this.btnNextDate.Name = "btnNextDate";
            this.btnNextDate.Size = new System.Drawing.Size(40, 38);
            this.btnNextDate.StyleController = this.dataLayoutControl1;
            this.btnNextDate.TabIndex = 17;
            this.btnNextDate.Click += new System.EventHandler(this.btnNextDate_Click);
            // 
            // btnNowDate
            // 
            this.btnNowDate.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnNowDate.Appearance.Options.UseFont = true;
            this.btnNowDate.Location = new System.Drawing.Point(264, 12);
            this.btnNowDate.Name = "btnNowDate";
            this.btnNowDate.Size = new System.Drawing.Size(84, 38);
            this.btnNowDate.StyleController = this.dataLayoutControl1;
            this.btnNowDate.TabIndex = 18;
            this.btnNowDate.Text = "امروز";
            this.btnNowDate.Click += new System.EventHandler(this.btnNowDate_Click);
            // 
            // btnBackDate
            // 
            this.btnBackDate.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnBackDate.Appearance.Options.UseFont = true;
            this.btnBackDate.Image = ((System.Drawing.Image)(resources.GetObject("btnBackDate.Image")));
            this.btnBackDate.Location = new System.Drawing.Point(352, 12);
            this.btnBackDate.Name = "btnBackDate";
            this.btnBackDate.Size = new System.Drawing.Size(40, 38);
            this.btnBackDate.StyleController = this.dataLayoutControl1;
            this.btnBackDate.TabIndex = 19;
            this.btnBackDate.Click += new System.EventHandler(this.btnBackDate_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem5,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlItem7,
            this.layoutControlItem6,
            this.layoutControlItem3,
            this.layoutControlItem9,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1094, 611);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.gridControl1;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 42);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(1074, 549);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txtFromDate;
            this.layoutControlItem5.Location = new System.Drawing.Point(384, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(246, 42);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(246, 42);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(246, 42);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = " تاریخ ";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(36, 24);
            this.layoutControlItem5.TextToControlDistance = 0;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(810, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(70, 42);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(110, 42);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnNextDate;
            this.layoutControlItem1.Location = new System.Drawing.Point(630, 0);
            this.layoutControlItem1.MaxSize = new System.Drawing.Size(44, 42);
            this.layoutControlItem1.MinSize = new System.Drawing.Size(44, 42);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(44, 42);
            this.layoutControlItem1.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnBackDate;
            this.layoutControlItem7.Location = new System.Drawing.Point(340, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(44, 42);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(44, 42);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(44, 42);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnNowDate;
            this.layoutControlItem6.Location = new System.Drawing.Point(252, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(88, 42);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(88, 42);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(88, 42);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.lblNameDay;
            this.layoutControlItem3.Location = new System.Drawing.Point(674, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(136, 28);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(136, 28);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(136, 42);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnshowReport;
            this.layoutControlItem9.Location = new System.Drawing.Point(110, 0);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(132, 42);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(142, 42);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.BtnTicketTripSelected;
            this.layoutControlItem10.Location = new System.Drawing.Point(880, 0);
            this.layoutControlItem10.MaxSize = new System.Drawing.Size(194, 42);
            this.layoutControlItem10.MinSize = new System.Drawing.Size(194, 42);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(194, 42);
            this.layoutControlItem10.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // LayoutControl2
            // 
            this.LayoutControl2.Controls.Add(this.btnClearSearchTrip);
            this.LayoutControl2.Controls.Add(this.ChCompany);
            this.LayoutControl2.Controls.Add(this.ChPath);
            this.LayoutControl2.Controls.Add(this.cmbCities);
            this.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl2.Margin = new System.Windows.Forms.Padding(0);
            this.LayoutControl2.Name = "LayoutControl2";
            this.LayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1001, 355, 250, 350);
            this.LayoutControl2.OptionsView.RightToLeftMirroringApplied = true;
            this.LayoutControl2.Root = this.LayoutControlGroup2;
            this.LayoutControl2.Size = new System.Drawing.Size(208, 551);
            this.LayoutControl2.TabIndex = 0;
            this.LayoutControl2.Text = "LayoutControl2";
            // 
            // btnClearSearchTrip
            // 
            this.btnClearSearchTrip.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnClearSearchTrip.Appearance.Options.UseFont = true;
            this.btnClearSearchTrip.Image = ((System.Drawing.Image)(resources.GetObject("btnClearSearchTrip.Image")));
            this.btnClearSearchTrip.Location = new System.Drawing.Point(6, 279);
            this.btnClearSearchTrip.Name = "btnClearSearchTrip";
            this.btnClearSearchTrip.Size = new System.Drawing.Size(196, 29);
            this.btnClearSearchTrip.StyleController = this.LayoutControl2;
            this.btnClearSearchTrip.TabIndex = 7;
            this.btnClearSearchTrip.Text = "نمایش همه سفرها";
            this.btnClearSearchTrip.Click += new System.EventHandler(this.btnClearSearchTrip_Click);
            // 
            // ChCompany
            // 
            this.ChCompany.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.ChCompany.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.ChCompany.Appearance.Options.UseBackColor = true;
            this.ChCompany.Appearance.Options.UseFont = true;
            this.ChCompany.CheckOnClick = true;
            this.ChCompany.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChCompany.DataSource = this.companyBindingSource;
            this.ChCompany.DisplayMember = "Title";
            this.ChCompany.Location = new System.Drawing.Point(9, 28);
            this.ChCompany.MultiColumn = true;
            this.ChCompany.Name = "ChCompany";
            this.ChCompany.Size = new System.Drawing.Size(190, 212);
            this.ChCompany.StyleController = this.LayoutControl2;
            this.ChCompany.TabIndex = 5;
            this.ChCompany.ValueMember = "Code";
            this.ChCompany.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.ChCompany_ItemCheck);
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataSource = typeof(Model.Company);
            // 
            // ChPath
            // 
            this.ChPath.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.ChPath.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.ChPath.Appearance.Options.UseBackColor = true;
            this.ChPath.Appearance.Options.UseFont = true;
            this.ChPath.CheckOnClick = true;
            this.ChPath.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChPath.DataSource = this.pathTripBindingSource;
            this.ChPath.DisplayMember = "PathTitle";
            this.ChPath.Location = new System.Drawing.Point(9, 334);
            this.ChPath.MultiColumn = true;
            this.ChPath.Name = "ChPath";
            this.ChPath.Size = new System.Drawing.Size(190, 208);
            this.ChPath.StyleController = this.LayoutControl2;
            this.ChPath.TabIndex = 5;
            this.ChPath.ValueMember = "PathTitle";
            this.ChPath.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.ChPath_ItemCheck);
            // 
            // pathTripBindingSource
            // 
            this.pathTripBindingSource.DataSource = typeof(Model.PathTrip);
            // 
            // cmbCities
            // 
            this.cmbCities.Location = new System.Drawing.Point(6, 247);
            this.cmbCities.MenuManager = this.ribbon;
            this.cmbCities.Name = "cmbCities";
            this.cmbCities.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.cmbCities.Properties.Appearance.Options.UseFont = true;
            this.cmbCities.Properties.Appearance.Options.UseTextOptions = true;
            this.cmbCities.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.cmbCities.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbCities.Properties.DataSource = this.spGetCitiesTripResultBindingSource;
            this.cmbCities.Properties.DisplayMember = "Title";
            this.cmbCities.Properties.NullText = "";
            this.cmbCities.Properties.NullValuePrompt = "لیست شهرها";
            this.cmbCities.Properties.PopupFilterMode = DevExpress.XtraEditors.PopupFilterMode.Contains;
            this.cmbCities.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cmbCities.Properties.ValueMember = "Code";
            this.cmbCities.Properties.View = this.gridLookUpEdit1View;
            this.cmbCities.Size = new System.Drawing.Size(159, 28);
            this.cmbCities.StyleController = this.LayoutControl2;
            this.cmbCities.TabIndex = 6;
            this.cmbCities.EditValueChanged += new System.EventHandler(this.cmbCities_EditValueChanged);
            // 
            // spGetCitiesTripResultBindingSource
            // 
            this.spGetCitiesTripResultBindingSource.DataSource = typeof(Model.Sp_GetCitiesTrip_Result);
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colTitle});
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsBehavior.Editable = false;
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colID
            // 
            this.colID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colID.AppearanceCell.Options.UseFont = true;
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colID.AppearanceHeader.Options.UseFont = true;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            // 
            // colTitle
            // 
            this.colTitle.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTitle.AppearanceCell.Options.UseFont = true;
            this.colTitle.AppearanceCell.Options.UseTextOptions = true;
            this.colTitle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTitle.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTitle.AppearanceHeader.Options.UseFont = true;
            this.colTitle.AppearanceHeader.Options.UseTextOptions = true;
            this.colTitle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTitle.Caption = "نام شهر";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 0;
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup2.GroupBordersVisible = false;
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup4,
            this.LayoutControlGroup6,
            this.layoutControlItem11,
            this.layoutControlItem12});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup2.Name = "Root";
            this.LayoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.LayoutControlGroup2.Size = new System.Drawing.Size(208, 551);
            this.LayoutControlGroup2.TextVisible = false;
            // 
            // LayoutControlGroup4
            // 
            this.LayoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem2});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup4.Size = new System.Drawing.Size(200, 241);
            this.LayoutControlGroup4.Text = "شرکت ها";
            // 
            // LayoutControlItem2
            // 
            this.LayoutControlItem2.Control = this.ChCompany;
            this.LayoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem2.Name = "LayoutControlItem2";
            this.LayoutControlItem2.Size = new System.Drawing.Size(194, 216);
            this.LayoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem2.TextVisible = false;
            // 
            // LayoutControlGroup6
            // 
            this.LayoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup6.CustomizationFormText = "طریقه آشنایی";
            this.LayoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlItem4});
            this.LayoutControlGroup6.Location = new System.Drawing.Point(0, 306);
            this.LayoutControlGroup6.Name = "LayoutControlGroup6";
            this.LayoutControlGroup6.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup6.Size = new System.Drawing.Size(200, 237);
            this.LayoutControlGroup6.Text = "مسیرها";
            // 
            // LayoutControlItem4
            // 
            this.LayoutControlItem4.Control = this.ChPath;
            this.LayoutControlItem4.CustomizationFormText = "LayoutControlItem2";
            this.LayoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem4.Name = "LayoutControlItem4";
            this.LayoutControlItem4.Size = new System.Drawing.Size(194, 212);
            this.LayoutControlItem4.Text = "LayoutControlItem2";
            this.LayoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.Control = this.cmbCities;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 241);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(200, 32);
            this.layoutControlItem11.Text = "شهرها";
            this.layoutControlItem11.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(34, 24);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.btnClearSearchTrip;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 273);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(200, 33);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // cityBindingSource
            // 
            this.cityBindingSource.DataSource = typeof(Model.City);
            // 
            // navigationPane1
            // 
            this.navigationPane1.Controls.Add(this.navigationPage1);
            this.navigationPane1.Dock = System.Windows.Forms.DockStyle.Right;
            this.navigationPane1.Location = new System.Drawing.Point(1094, 146);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.PageProperties.AppearanceCaption.Options.UseTextOptions = true;
            this.navigationPane1.PageProperties.AppearanceCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.navigationPane1.PageProperties.ShowExpandButton = false;
            this.navigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.Image;
            this.navigationPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.navigationPage1});
            this.navigationPane1.RegularSize = new System.Drawing.Size(278, 608);
            this.navigationPane1.SelectedPage = this.navigationPage1;
            this.navigationPane1.Size = new System.Drawing.Size(278, 611);
            this.navigationPane1.TabIndex = 24;
            this.navigationPane1.Text = "navigationPane1";
            // 
            // navigationPage1
            // 
            this.navigationPage1.Caption = "جستجو پیشرفته";
            this.navigationPage1.Controls.Add(this.LayoutControl2);
            this.navigationPage1.Image = ((System.Drawing.Image)(resources.GetObject("navigationPage1.Image")));
            this.navigationPage1.ImageUri.Uri = "Zoom";
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(208, 551);
            // 
            // galleryDropDown1
            // 
            this.galleryDropDown1.Name = "galleryDropDown1";
            this.galleryDropDown1.Ribbon = this.ribbon;
            // 
            // popupMenu1
            // 
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.Ribbon = this.ribbon;
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // FrmTrips
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1372, 778);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.navigationPane1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Name = "FrmTrips";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "سفر ها";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmticketTrip_FormClosing);
            this.Load += new System.EventHandler(this.frmticketTrip_Load);
            this.Shown += new System.EventHandler(this.frmticketTrip_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getTripsReportBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnSale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reTicket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).EndInit();
            this.LayoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ChCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pathTripBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCities.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spGetCitiesTripResultBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).EndInit();
            this.navigationPane1.ResumeLayout(false);
            this.navigationPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.galleryDropDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView grd;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCarTypeTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colDateSH;
        private DevExpress.XtraGrid.Columns.GridColumn colDepartureTime2;
        private DevExpress.XtraGrid.Columns.GridColumn colNumTickets;
        private DevExpress.XtraGrid.Columns.GridColumn colNumReserves;
        private DevExpress.XtraGrid.Columns.GridColumn colDay;
        private DevExpress.XtraGrid.Columns.GridColumn colTripID;
        private DevExpress.XtraGrid.Columns.GridColumn colServiceID;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colPathTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colSale;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reBtnSale;
        private DevExpress.XtraGrid.Columns.GridColumn colTicket;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reTicket;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;

        private FarsiLibrary.Win.DevExpress.XtraFADateEdit txtFromDate;
        private DevExpress.XtraBars.Navigation.NavigationPane navigationPane1;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        internal DevExpress.XtraLayout.LayoutControl LayoutControl2;
        internal DevExpress.XtraEditors.CheckedListBoxControl ChCompany;
        internal DevExpress.XtraEditors.CheckedListBoxControl ChPath;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem2;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup6;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem4;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private System.Windows.Forms.BindingSource pathTripBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPathID;
        private DevExpress.XtraGrid.Columns.GridColumn colLayoutID;
        private DevExpress.XtraBars.BarButtonItem btnReport;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraGrid.Columns.GridColumn colTripLocked;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraEditors.SimpleButton btnBackDate;
        private DevExpress.XtraEditors.SimpleButton btnNowDate;
        private DevExpress.XtraEditors.SimpleButton btnNextDate;
        private DevExpress.XtraBars.BarEditItem barCreditUser;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton btnshowReport;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.LabelControl lblNameDay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraBars.Ribbon.GalleryDropDown galleryDropDown1;
        private DevExpress.XtraBars.PopupMenu popupMenu1;
        public DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraEditors.SimpleButton BtnTicketTripSelected;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private System.Windows.Forms.BindingSource cityBindingSource;
        private DevExpress.XtraEditors.GridLookUpEdit cmbCities;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraEditors.SimpleButton btnClearSearchTrip;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraGrid.Columns.GridColumn colCitiesBetween;
        private System.Windows.Forms.BindingSource spGetCitiesTripResultBindingSource;
        private System.Windows.Forms.BindingSource getTripsReportBindingSource;
    }
}