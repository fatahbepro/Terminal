﻿namespace Test
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
            this.lblError = new System.Windows.Forms.Label();
            this.Btn_join = new System.Windows.Forms.Button();
            this.Txt_username = new DevExpress.XtraEditors.TextEdit();
            this.Txt_password = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rdNetwork = new System.Windows.Forms.RadioButton();
            this.rdLocal = new System.Windows.Forms.RadioButton();
            this.btnClose = new System.Windows.Forms.Button();
            this.img = new DevExpress.XtraEditors.PictureEdit();
            ((System.ComponentModel.ISupportInitialize)(this.Txt_username.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Txt_password.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.img.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblError
            // 
            this.lblError.BackColor = System.Drawing.Color.Transparent;
            this.lblError.Font = new System.Drawing.Font("Tahoma", 9F);
            this.lblError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblError.Location = new System.Drawing.Point(160, 11);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(262, 26);
            this.lblError.TabIndex = 3;
            this.lblError.Text = "نام کاربری یا کلمه عبور صحیح نیست";
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblError.Visible = false;
            // 
            // Btn_join
            // 
            this.Btn_join.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.Btn_join.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Btn_join.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 11F);
            this.Btn_join.Location = new System.Drawing.Point(267, 142);
            this.Btn_join.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_join.Name = "Btn_join";
            this.Btn_join.Size = new System.Drawing.Size(152, 41);
            this.Btn_join.TabIndex = 2;
            this.Btn_join.Text = "ورود به سیستم";
            this.Btn_join.UseVisualStyleBackColor = false;
            this.Btn_join.Click += new System.EventHandler(this.button1_Click);
            // 
            // Txt_username
            // 
            this.Txt_username.EditValue = "";
            this.Txt_username.EnterMoveNextControl = true;
            this.Txt_username.Location = new System.Drawing.Point(157, 39);
            this.Txt_username.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txt_username.Name = "Txt_username";
            this.Txt_username.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 11F);
            this.Txt_username.Properties.Appearance.Options.UseFont = true;
            this.Txt_username.Properties.Appearance.Options.UseTextOptions = true;
            this.Txt_username.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Txt_username.Properties.NullValuePrompt = "نام کاربری";
            this.Txt_username.Size = new System.Drawing.Size(262, 32);
            this.Txt_username.TabIndex = 0;
            // 
            // Txt_password
            // 
            this.Txt_password.EditValue = "";
            this.Txt_password.EnterMoveNextControl = true;
            this.Txt_password.Location = new System.Drawing.Point(157, 77);
            this.Txt_password.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Txt_password.Name = "Txt_password";
            this.Txt_password.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 11F);
            this.Txt_password.Properties.Appearance.Options.UseFont = true;
            this.Txt_password.Properties.Appearance.Options.UseTextOptions = true;
            this.Txt_password.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Txt_password.Properties.NullValuePrompt = "کلمه عبور";
            this.Txt_password.Properties.PasswordChar = '*';
            this.Txt_password.Size = new System.Drawing.Size(262, 32);
            this.Txt_password.TabIndex = 1;
            this.Txt_password.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Txt_password_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 11F);
            this.label1.Location = new System.Drawing.Point(425, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 25);
            this.label1.TabIndex = 11;
            this.label1.Text = "نام کاربری :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 11F);
            this.label2.Location = new System.Drawing.Point(424, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 25);
            this.label2.TabIndex = 11;
            this.label2.Text = "رمز عبور :";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rdNetwork
            // 
            this.rdNetwork.AutoSize = true;
            this.rdNetwork.BackColor = System.Drawing.Color.Transparent;
            this.rdNetwork.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.rdNetwork.Location = new System.Drawing.Point(188, 113);
            this.rdNetwork.Name = "rdNetwork";
            this.rdNetwork.Size = new System.Drawing.Size(91, 24);
            this.rdNetwork.TabIndex = 14;
            this.rdNetwork.Text = "ورود اینترنتی";
            this.rdNetwork.UseVisualStyleBackColor = false;
            // 
            // rdLocal
            // 
            this.rdLocal.AutoSize = true;
            this.rdLocal.BackColor = System.Drawing.Color.Transparent;
            this.rdLocal.Checked = true;
            this.rdLocal.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.rdLocal.Location = new System.Drawing.Point(314, 115);
            this.rdLocal.Name = "rdLocal";
            this.rdLocal.Size = new System.Drawing.Size(75, 24);
            this.rdLocal.TabIndex = 15;
            this.rdLocal.TabStop = true;
            this.rdLocal.Text = "ورود لوکال";
            this.rdLocal.UseVisualStyleBackColor = false;
            // 
            // btnClose
            // 
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 11F);
            this.btnClose.Location = new System.Drawing.Point(160, 142);
            this.btnClose.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(101, 41);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "لغو";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // img
            // 
            this.img.Cursor = System.Windows.Forms.Cursors.Default;
            this.img.EditValue = global::Test.Properties.Resources._50928;
            this.img.Location = new System.Drawing.Point(0, 25);
            this.img.Name = "img";
            this.img.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.img.Properties.Appearance.Options.UseBackColor = true;
            this.img.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.img.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.img.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.img.Properties.ZoomAccelerationFactor = 1D;
            this.img.Size = new System.Drawing.Size(155, 155);
            this.img.TabIndex = 16;
            // 
            // frmticketLogin
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(535, 194);
            this.ControlBox = false;
            this.Controls.Add(this.img);
            this.Controls.Add(this.rdLocal);
            this.Controls.Add(this.rdNetwork);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.Btn_join);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.Txt_username);
            this.Controls.Add(this.Txt_password);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmticketLogin";
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ورود به برنامه";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmticketLogin_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.Txt_username.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Txt_password.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.img.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Button Btn_join;
        private DevExpress.XtraEditors.TextEdit Txt_username;
        private DevExpress.XtraEditors.TextEdit Txt_password;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rdNetwork;
        private System.Windows.Forms.RadioButton rdLocal;
        private System.Windows.Forms.Button btnClose;
        private DevExpress.XtraEditors.PictureEdit img;
    }
}