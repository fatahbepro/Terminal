﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using BL;
using Model;

using PcPosDll;
using PosInterface;
using SSP1126.PcPos.Infrastructure;
using SSP1126.PcPos.BaseClasses;
using System.Net.NetworkInformation;

namespace Test
{
    public partial class FrmAddCredit : DevExpress.XtraEditors.XtraForm, PosInterface.ITransactionDoneHandler
    {

        public FrmAddCredit()
        {
            InitializeComponent();
            txtCreditNow.EditValue = Userlogin.Credit;
        }

        public string Mablagh { get; set; }
        String IpPos, typePos;

        private void FrmAddCredit_Load(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
          
            this.Close();
        }

        bool CheckingPingPose()
        {
            Ping p1 = new Ping();
            PingReply PR = p1.Send(Userlogin.IpPose);
            if (PR.Status == IPStatus.Success)
                return true;
            return false;
        }

        void AddCredit()
        {
            try { 

            User us = UserServices.FindByID(Userlogin.ID);
            us.Credit += Convert.ToInt32(txtPayCredit.EditValue);
            int Result=UserServices.Update(us);

            AddCreditUserreport(Convert.ToInt32(txtPayCredit.EditValue));


            Userlogin.Credit = us.Credit;
            txtCreditNow.EditValue = Userlogin.Credit;
            txtPayCredit.EditValue = 0;
        }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
             
            }
            finally
            {
                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

}

        /// <summary>
        /// ثبت در جدول گزارشات اعتبار کاربر
        /// </summary>
        void AddCreditUserreport(int _Recived)
        {
            UserCreditReport _uc = new UserCreditReport();
            _uc.ActionDate = DateTime.Now;
            _uc.ActionType =0;
            _uc.UserCode = Userlogin.Code;
            _uc.Recived = _Recived;
            _uc.Payment = 0;
            _uc.Status = 1;
            UserCreditReportServices.Insert(_uc);
        }


        private void btnPose_Click(object sender, EventArgs e)
        {

            try
            {

                if (!CheckingPingPose())
                {
                    XtraMessageBox.Show("اتصال به کارتخوان مقدور نمی باشد . لطفا آدرس دستگاه یا وضعیت اتصال را بررسی نمایید", "", MessageBoxButtons.OK);
                    return;
                }


                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                Mablagh = txtPayCredit.EditValue.ToString();
                IpPos = Userlogin.IpPose;
                typePos = Userlogin.PoseName;
                btnCancelPose.Enabled = true;

                if (!bgpose.IsBusy)
                {
                     ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                    bgpose.RunWorkerAsync();

                }
                else
                {
                     ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    XtraMessageBox.Show("سیستم در حالت ارتباط با دستگاه کارتخوان می باشد .لطفا چند لحظه  شکیبا باشید", "پیام", MessageBoxButtons.OK);
                     ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                }

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }

        private void bgpose_DoWork(object sender, DoWorkEventArgs e)
        {
            /// توابع ارسال
            PayPosePrice();
        }


        PcPosFactory _PcPosFactory;
        MediaType _mediaType = MediaType.Network;
        AccountType _accountType = AccountType.Single;
        ResponseLanguage _responseLanguage;

        AsyncType _asyncType = AsyncType.Sync;
        ReportAction _reportAction;
        ReportFilterType _reportFilterType;
        TransactionType _tracsactionType;

        PcPosDll.PcPosBusiness PcPos = new PcPosDll.PcPosBusiness();
        PcPosDll.PcPosDiscovery searchPos = new PcPosDll.PcPosDiscovery();
        //MellatPose mellat = new MellatPose();
        PosInterface.PCPos PcposAsanPardakht = new PosInterface.PCPos();



        /// <summary>
        /// لغو عملیات پرداخت
        /// </summary>
        public void CancelPay()
        {
            try
            {
                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);


                if (typePos != null)
                {
                    if (typePos.Contains("سداد"))
                    {
                        if (PcPos == null)
                            PcPos = new PcPosBusiness();
                        // لغو عملیات پرداخت
                        PcPos.AbortPcPosOperation();

                    }
                    if (typePos.Contains("سامان"))
                    {
                        if (_PcPosFactory == null)
                        {
                            _PcPosFactory = new PcPosFactory();
                        }
                        _PcPosFactory.Dispose();



                    }


                    if (typePos.Contains("آسان پرداخت"))
                    {
                        if (PcposAsanPardakht == null)
                        {
                            PcposAsanPardakht = new PosInterface.PCPos();
                        }
                        PcposAsanPardakht.Stop();



                    }
                    //         if (typePos.Contains("ملت")) {
                    //             mellat.StopProcessing();
                    //txtStatusPayment.EditValue="عملیات دریافت از کارتخوان لغو گردید";

                    //         }
                }

                btnCancelPose.Enabled = false;

            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }


        }

        /// <summary>
        /// ارسال مبلغ به کارتخوان مورد نظر
        /// </summary>
        public void PayPosePrice()
        {
            try
            {

                //OperationSaveFact = true;


                if (typePos.Contains("سداد"))
                {

                    if (PcPos == null)
                        PcPos = new PcPosDll.PcPosBusiness();

                    {
                        PcPos.PcPosIp = IpPos;
                        PcPos.PcPosPort = 8888;
                        PcPos.PcPosMode = PcPosConnectionType.Lan;
                        PcPos.Pc2PosAmount = Mablagh;
                        PcPos.PcPosRetryTimeOut = new int[] {
                5000,
                5000,
                5000
            };
                        PcPos.PcPosResponseTimeout = new int[] {
                180000,
                5000,
                5000
            };
                        PcPos.OnPcPosResult += PcPosSaleResult;
                        PcPos.AsyncPcPosSaleTransaction();

                    }
                }

                if (typePos.Contains("سامان کیش"))
                {
                    _PcPosFactory = new PcPosFactory();
                    if (PurchaseInitialization())
                    {
                        return;
                    }

                    List<string> lstAmnts = new List<string>();
                    SSP1126.PcPos.Infrastructure.PosResult posResult = new SSP1126.PcPos.Infrastructure.PosResult();
                    string TID = null;
                    if (_accountType == AccountType.Single)
                    {
                        posResult = _PcPosFactory.PcStarterPurchase(Mablagh, string.Empty, null, "", TID);
                    }
                    if (_asyncType == AsyncType.Sync && posResult != null)
                    {
                        PurchaseResultReceived(posResult);
                    }
                }



                if (typePos.Contains("آسان پرداخت"))
                {
                    PcposAsanPardakht.InitLAN(IpPos, 17000);
                    PcposAsanPardakht.DoASyncPayment(Mablagh, "", "", DateTime.Now, this);


                }

                //if (typePos.Contains("ملت"))
                //{
                //    string ErrorMessage;
                //    string NumberError;
                //    string NumberPeygiri;
                //    string NumberErja;
                //    string ReasonCode;

                //    mellat = new MellatPose();
                //    bool Response = mellat.PeymentFunc(Mablagh, IpPos, 1024, ShomarePeygiri, ShomreErja, NumberError, ReasonCode);

                //    switch (Response)
                //    {

                //        case true:
                //            Invoke(new MethodInvoker(() =>
                //            {
                //                System.Threading.Thread.Sleep(1000);
                // txtStatusPayment.EditValue= "پرداخت با موفقیت انجام شد";

                //                //ShomarePeygiri = NumberPeygiri
                //                //ShomreErja = NumberErja
                //                PaymentPostActive = false;
                //               // Fwait.Close();

                //                   /// ثبت فاکتور

                //            }));

                //            break;
                //        case false:
                //            PaymentPostActive = false;
                //            btnCancelPose.Enabled = false;
                //        
                //  txtStatusPayment.EditValue=   "عملیات پرداخت ناموفق بود" ;

                //           // Fwait.Close();
                //            /// عملیات پرداخت ناموفق
                //            break;


                //    }

                //}

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        //  نتیجه عملیات پرداخت دستگاه سداد
        private void PcPosSaleResult(object sender, PcPosDll.PosResult e)
        {
            //AdUPos.ShowGuides = DevExpress.Utils.DefaultBoolean.True
            try
            {

                Invoke(new MethodInvoker(() =>
                {
                    System.Threading.Thread.Sleep(1000);

                    if (e.ResponseCode == "00")
                    {

                        AddCredit();
                        //ShomarePeygiri = e.ApprovalCode.ToString();
                        //ShomreErja = e.TransactionNo;

                        /// عملیات ثبت فاکتور

                    }
                    else
                    {
                        //grd.Appearance.FocusedRow.BackColor = Color.Yellow;
                         ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                        XtraMessageBox.Show(" عملیات پرداخت کارتخوان ناموفق");

                        // Fwait.Close();
                        /// عملیات پرداخت ناموفق
                    }

                    PcPos.OnPcPosResult -= PcPosSaleResult;
                    PcPos = null;
                }));


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }


        }

        //   دریافت نتیجه پرداخت سامان کیش
        private void PurchaseResultReceived(SSP1126.PcPos.Infrastructure.PosResult posResult)
        {
            try
            {

                if (posResult == null)
                {
                    return;
                }

                //Successful result
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(() =>
                    {
                        if (posResult.ResponseCode == "00")
                        {
                            AddCredit();// عملیات پرداخت
                  
                            /// عملیات ثبت فاکتور
                        }
                        else
                        {

                            //grd.Appearance.FocusedRow.BackColor = Color.Yellow;
                             ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                            XtraMessageBox.Show(" عملیات پرداخت کارتخوان ناموفق");
                            /// عملیات پرداخت ناموفق
                        }
                    }));
                }


            }
            catch (Exception ex)
            {

                GetError.WriteError(ex,this);
                
            }
        }

        public void OnFinish(string message)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// نتیجه عملیات پرداخت آسان پرداخت
        /// </summary>
        /// <param name="result"></param>
        public void OnTransactionDone(TransactionResult result)
        {
            {
                if (this.InvokeRequired)
                {

                    this.Invoke(new MethodInvoker(() =>
                    {
                        if (result.ErrorCode == 0)
                        {

                            AddCredit();
                            //ShomarePeygiri = result.Stan;
                            //ShomreErja = result.RRN;

                            // Fwait.Close();
                            ///  عملیات ثبت فاکتور
                        }
                        else
                        {
                            //grd.Appearance.FocusedRow.BackColor = Color.Yellow;
                             ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                            XtraMessageBox.Show(" عملیات پرداخت کارتخوان ناموفق");
                            // Fwait.Close();

                            // عملیات پرداخت ناموفق
                        }
                    }));
                }

            }
        }



        private bool PurchaseInitialization()
        {

            _tracsactionType = TransactionType.Purchase;

            if (TransactionMediaInitialization())
            {
                return false;
            }

            _PcPosFactory.Initialization(_responseLanguage, 0, _asyncType);
            return false;
        }

        private void btnCancelPose_Click(object sender, EventArgs e)
        {
            CancelPay();
        }

        private void FrmAddCredit_FormClosing(object sender, FormClosingEventArgs e)
        {
            btnCancelPose_Click(null, null);
        }

        private bool TransactionMediaInitialization()
        {
            //If _mediaType = MediaType.Network Then
            if (string.IsNullOrEmpty(IpPos))
            {
                XtraMessageBox.Show("ای پی دستگاه اشتباه می باشد", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //grd.Appearance.FocusedRow.BackColor = Color.Yellow;

                return true;
            }
            _PcPosFactory.SetLan(IpPos);
            //End If
            _PcPosFactory.Initialization(_responseLanguage, 0, _asyncType);
            return false;
        }

    }
}