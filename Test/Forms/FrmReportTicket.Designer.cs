﻿namespace Test
{
    partial class FrmReportTicket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReportTicket));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnGetTrip = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrintReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnSaleTicket = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.getTripsResultBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPathTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reCompany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.companyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colSumPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSumFundPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSumOnlinePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSummery = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colsumNumberchair = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCountTrip = new DevExpress.XtraGrid.Columns.GridColumn();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Test.WaitForm1), true, true);
            this.navigationPane1 = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.LayoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.TimeEndtimeEdit = new DevExpress.XtraEditors.TimeEdit();
            this.TimeStarttimeEdit = new DevExpress.XtraEditors.TimeEdit();
            this.btnClearfilter = new DevExpress.XtraEditors.SimpleButton();
            this.ChCompany = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.ChPath = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.pathTripBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.SearchControl1 = new DevExpress.XtraEditors.SearchControl();
            this.txtTodate = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.txtFromDate = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink1 = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.colSumTollPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.getTripsResultBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).BeginInit();
            this.navigationPane1.SuspendLayout();
            this.navigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).BeginInit();
            this.LayoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TimeEndtimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeStarttimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChPath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pathTripBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnGetTrip,
            this.btnClose,
            this.btnRefresh,
            this.btnPrintReport,
            this.barButtonItem1,
            this.btnSaleTicket});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 7;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1324, 147);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // btnGetTrip
            // 
            this.btnGetTrip.Caption = "گزارش  فروش";
            this.btnGetTrip.Glyph = ((System.Drawing.Image)(resources.GetObject("btnGetTrip.Glyph")));
            this.btnGetTrip.Id = 1;
            this.btnGetTrip.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnGetTrip.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnGetTrip.LargeGlyph")));
            this.btnGetTrip.Name = "btnGetTrip";
            this.btnGetTrip.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnGetTrip_ItemClick);
            // 
            // btnClose
            // 
            this.btnClose.Caption = "بستن";
            this.btnClose.Glyph = ((System.Drawing.Image)(resources.GetObject("btnClose.Glyph")));
            this.btnClose.Id = 2;
            this.btnClose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnClose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnClose.LargeGlyph")));
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "بروزرسانی تعاونی ها";
            this.btnRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Glyph")));
            this.btnRefresh.Id = 3;
            this.btnRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btnRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.LargeGlyph")));
            this.btnRefresh.Name = "btnRefresh";
            toolTipTitleItem1.Text = "تعاونی ها";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "بروز رسانی لیست تعاونی های مورد نظر";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnRefresh.SuperTip = superToolTip1;
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // btnPrintReport
            // 
            this.btnPrintReport.Caption = "چاپ لیست";
            this.btnPrintReport.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPrintReport.Glyph")));
            this.btnPrintReport.Id = 4;
            this.btnPrintReport.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F10);
            this.btnPrintReport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnPrintReport.LargeGlyph")));
            this.btnPrintReport.Name = "btnPrintReport";
            this.btnPrintReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrintReport_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 5;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // btnSaleTicket
            // 
            this.btnSaleTicket.Caption = "گزارش پیش فروش";
            this.btnSaleTicket.Glyph = ((System.Drawing.Image)(resources.GetObject("btnSaleTicket.Glyph")));
            this.btnSaleTicket.Id = 6;
            this.btnSaleTicket.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnSaleTicket.LargeGlyph")));
            this.btnSaleTicket.Name = "btnSaleTicket";
            toolTipTitleItem2.Text = "گزارش";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "نمایش گزارش بلیط های پیش فروش تعاونی ها به صورت دسته بندی و مجموع";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            this.btnSaleTicket.SuperTip = superToolTip2;
            this.btnSaleTicket.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSaleTicket_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "گزارشات";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnGetTrip);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnSaleTicket);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnRefresh);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "گزارش فروش";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnPrintReport);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "چاپ";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnClose);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 741);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1042, 23);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.getTripsResultBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 147);
            this.gridControl1.MainView = this.grd;
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.reCompany});
            this.gridControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.gridControl1.Size = new System.Drawing.Size(1042, 594);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grd});
            // 
            // getTripsResultBindingSource
            // 
            this.getTripsResultBindingSource.DataSource = typeof(Model.GetTripsResultReport);
            // 
            // grd
            // 
            this.grd.Appearance.FilterPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.FilterPanel.Options.UseFont = true;
            this.grd.Appearance.FocusedCell.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedCell.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grd.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grd.Appearance.FocusedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grd.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grd.Appearance.FooterPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.FooterPanel.Options.UseFont = true;
            this.grd.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grd.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.GroupFooter.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.GroupFooter.Options.UseFont = true;
            this.grd.Appearance.GroupFooter.Options.UseTextOptions = true;
            this.grd.Appearance.GroupFooter.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.GroupPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.GroupPanel.Options.UseFont = true;
            this.grd.Appearance.GroupRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.GroupRow.Options.UseFont = true;
            this.grd.Appearance.Row.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.Row.Options.UseFont = true;
            this.grd.Appearance.SelectedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grd.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grd.Appearance.TopNewRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.TopNewRow.Options.UseFont = true;
            this.grd.AppearancePrint.EvenRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.EvenRow.Options.UseFont = true;
            this.grd.AppearancePrint.FilterPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.FilterPanel.Options.UseFont = true;
            this.grd.AppearancePrint.FooterPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.FooterPanel.Options.UseFont = true;
            this.grd.AppearancePrint.GroupFooter.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.GroupFooter.Options.UseFont = true;
            this.grd.AppearancePrint.GroupRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.GroupRow.Options.UseFont = true;
            this.grd.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.grd.AppearancePrint.Lines.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.Lines.Options.UseFont = true;
            this.grd.AppearancePrint.OddRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.OddRow.Options.UseFont = true;
            this.grd.AppearancePrint.Preview.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.Preview.Options.UseFont = true;
            this.grd.AppearancePrint.Row.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.Row.Options.UseFont = true;
            this.grd.ColumnPanelRowHeight = 25;
            this.grd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPathTitle,
            this.colCompanyCode,
            this.colSumPrice,
            this.colSumFundPrice,
            this.colSumOnlinePrice,
            this.colSummery,
            this.colsumNumberchair,
            this.colCountTrip,
            this.colSumTollPrice});
            this.grd.GridControl = this.gridControl1;
            this.grd.Name = "grd";
            this.grd.OptionsBehavior.Editable = false;
            this.grd.OptionsCustomization.AllowFilter = false;
            this.grd.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grd.OptionsPrint.PrintDetails = true;
            this.grd.OptionsPrint.PrintPreview = true;
            this.grd.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grd.OptionsView.ShowFooter = true;
            this.grd.OptionsView.ShowGroupPanel = false;
            // 
            // colPathTitle
            // 
            this.colPathTitle.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPathTitle.AppearanceCell.Options.UseFont = true;
            this.colPathTitle.AppearanceCell.Options.UseTextOptions = true;
            this.colPathTitle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPathTitle.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPathTitle.AppearanceHeader.Options.UseFont = true;
            this.colPathTitle.AppearanceHeader.Options.UseTextOptions = true;
            this.colPathTitle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPathTitle.Caption = "مسیر";
            this.colPathTitle.FieldName = "PathTitle";
            this.colPathTitle.Name = "colPathTitle";
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCompanyCode.AppearanceCell.Options.UseFont = true;
            this.colCompanyCode.AppearanceCell.Options.UseTextOptions = true;
            this.colCompanyCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyCode.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCompanyCode.AppearanceHeader.Options.UseFont = true;
            this.colCompanyCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCompanyCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyCode.Caption = "تعاونی";
            this.colCompanyCode.ColumnEdit = this.reCompany;
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.Visible = true;
            this.colCompanyCode.VisibleIndex = 0;
            this.colCompanyCode.Width = 146;
            // 
            // reCompany
            // 
            this.reCompany.AutoHeight = false;
            this.reCompany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.reCompany.DataSource = this.companyBindingSource;
            this.reCompany.DisplayMember = "Title";
            this.reCompany.Name = "reCompany";
            this.reCompany.NullText = "نامشخص";
            this.reCompany.ValueMember = "Code";
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataSource = typeof(Model.Company);
            // 
            // colSumPrice
            // 
            this.colSumPrice.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSumPrice.AppearanceCell.Options.UseFont = true;
            this.colSumPrice.AppearanceCell.Options.UseTextOptions = true;
            this.colSumPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumPrice.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSumPrice.AppearanceHeader.Options.UseFont = true;
            this.colSumPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colSumPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumPrice.Caption = "سهم تعاونی";
            this.colSumPrice.DisplayFormat.FormatString = "c0";
            this.colSumPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSumPrice.FieldName = "SumPrice";
            this.colSumPrice.Name = "colSumPrice";
            this.colSumPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SumPrice", "ج {0:c0}")});
            this.colSumPrice.Visible = true;
            this.colSumPrice.VisibleIndex = 3;
            this.colSumPrice.Width = 156;
            // 
            // colSumFundPrice
            // 
            this.colSumFundPrice.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSumFundPrice.AppearanceCell.Options.UseFont = true;
            this.colSumFundPrice.AppearanceCell.Options.UseTextOptions = true;
            this.colSumFundPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumFundPrice.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSumFundPrice.AppearanceHeader.Options.UseFont = true;
            this.colSumFundPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colSumFundPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumFundPrice.Caption = "سهم صندوق";
            this.colSumFundPrice.DisplayFormat.FormatString = "c0";
            this.colSumFundPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSumFundPrice.FieldName = "SumFundPrice";
            this.colSumFundPrice.Name = "colSumFundPrice";
            this.colSumFundPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SumFundPrice", "ج {0:c0}")});
            this.colSumFundPrice.Visible = true;
            this.colSumFundPrice.VisibleIndex = 4;
            this.colSumFundPrice.Width = 156;
            // 
            // colSumOnlinePrice
            // 
            this.colSumOnlinePrice.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSumOnlinePrice.AppearanceCell.Options.UseFont = true;
            this.colSumOnlinePrice.AppearanceCell.Options.UseTextOptions = true;
            this.colSumOnlinePrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumOnlinePrice.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSumOnlinePrice.AppearanceHeader.Options.UseFont = true;
            this.colSumOnlinePrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colSumOnlinePrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumOnlinePrice.Caption = "سهم اینترنتی";
            this.colSumOnlinePrice.DisplayFormat.FormatString = "c0";
            this.colSumOnlinePrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSumOnlinePrice.FieldName = "SumOnlinePrice";
            this.colSumOnlinePrice.Name = "colSumOnlinePrice";
            this.colSumOnlinePrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "SumOnlinePrice", "ج {0:c0}")});
            this.colSumOnlinePrice.Visible = true;
            this.colSumOnlinePrice.VisibleIndex = 5;
            this.colSumOnlinePrice.Width = 156;
            // 
            // colSummery
            // 
            this.colSummery.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSummery.AppearanceCell.Options.UseFont = true;
            this.colSummery.AppearanceCell.Options.UseTextOptions = true;
            this.colSummery.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSummery.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSummery.AppearanceHeader.Options.UseFont = true;
            this.colSummery.AppearanceHeader.Options.UseTextOptions = true;
            this.colSummery.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSummery.Caption = "جمع کل";
            this.colSummery.DisplayFormat.FormatString = "c0";
            this.colSummery.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSummery.FieldName = "Summery";
            this.colSummery.Name = "colSummery";
            this.colSummery.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Summery", "ج {0:c0}")});
            this.colSummery.Visible = true;
            this.colSummery.VisibleIndex = 7;
            this.colSummery.Width = 113;
            // 
            // colsumNumberchair
            // 
            this.colsumNumberchair.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colsumNumberchair.AppearanceCell.Options.UseFont = true;
            this.colsumNumberchair.AppearanceCell.Options.UseTextOptions = true;
            this.colsumNumberchair.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsumNumberchair.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colsumNumberchair.AppearanceHeader.Options.UseFont = true;
            this.colsumNumberchair.AppearanceHeader.Options.UseTextOptions = true;
            this.colsumNumberchair.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colsumNumberchair.Caption = "تعداد مسافر";
            this.colsumNumberchair.FieldName = "sumNumberchair";
            this.colsumNumberchair.Name = "colsumNumberchair";
            this.colsumNumberchair.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "sumNumberchair", " {0:0.##}")});
            this.colsumNumberchair.Visible = true;
            this.colsumNumberchair.VisibleIndex = 1;
            this.colsumNumberchair.Width = 108;
            // 
            // colCountTrip
            // 
            this.colCountTrip.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCountTrip.AppearanceCell.Options.UseFont = true;
            this.colCountTrip.AppearanceCell.Options.UseTextOptions = true;
            this.colCountTrip.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCountTrip.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCountTrip.AppearanceHeader.Options.UseFont = true;
            this.colCountTrip.AppearanceHeader.Options.UseTextOptions = true;
            this.colCountTrip.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCountTrip.Caption = "تعداد سرویس";
            this.colCountTrip.FieldName = "CountTrip";
            this.colCountTrip.Name = "colCountTrip";
            this.colCountTrip.Visible = true;
            this.colCountTrip.VisibleIndex = 2;
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // navigationPane1
            // 
            this.navigationPane1.Controls.Add(this.navigationPage1);
            this.navigationPane1.Dock = System.Windows.Forms.DockStyle.Right;
            this.navigationPane1.Location = new System.Drawing.Point(1042, 147);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.PageProperties.AppearanceCaption.Options.UseTextOptions = true;
            this.navigationPane1.PageProperties.AppearanceCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.navigationPane1.PageProperties.ShowExpandButton = false;
            this.navigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.Image;
            this.navigationPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.navigationPage1});
            this.navigationPane1.RegularSize = new System.Drawing.Size(282, 617);
            this.navigationPane1.SelectedPage = this.navigationPage1;
            this.navigationPane1.Size = new System.Drawing.Size(282, 617);
            this.navigationPane1.TabIndex = 26;
            this.navigationPane1.Text = "navigationPane1";
            // 
            // navigationPage1
            // 
            this.navigationPage1.Caption = "گزارش فروش";
            this.navigationPage1.Controls.Add(this.LayoutControl2);
            this.navigationPage1.Image = ((System.Drawing.Image)(resources.GetObject("navigationPage1.Image")));
            this.navigationPage1.ImageUri.Uri = "Zoom";
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(220, 556);
            // 
            // LayoutControl2
            // 
            this.LayoutControl2.Controls.Add(this.TimeEndtimeEdit);
            this.LayoutControl2.Controls.Add(this.TimeStarttimeEdit);
            this.LayoutControl2.Controls.Add(this.btnClearfilter);
            this.LayoutControl2.Controls.Add(this.ChCompany);
            this.LayoutControl2.Controls.Add(this.ChPath);
            this.LayoutControl2.Controls.Add(this.SearchControl1);
            this.LayoutControl2.Controls.Add(this.txtTodate);
            this.LayoutControl2.Controls.Add(this.txtFromDate);
            this.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl2.Name = "LayoutControl2";
            this.LayoutControl2.OptionsView.RightToLeftMirroringApplied = true;
            this.LayoutControl2.Root = this.LayoutControlGroup2;
            this.LayoutControl2.Size = new System.Drawing.Size(220, 556);
            this.LayoutControl2.TabIndex = 0;
            this.LayoutControl2.Text = "LayoutControl2";
            // 
            // TimeEndtimeEdit
            // 
            this.TimeEndtimeEdit.EditValue = new System.DateTime(2019, 11, 29, 0, 0, 0, 0);
            this.TimeEndtimeEdit.Location = new System.Drawing.Point(9, 200);
            this.TimeEndtimeEdit.MenuManager = this.ribbon;
            this.TimeEndtimeEdit.Name = "TimeEndtimeEdit";
            this.TimeEndtimeEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.TimeEndtimeEdit.Properties.Appearance.Options.UseFont = true;
            this.TimeEndtimeEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TimeEndtimeEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeEndtimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TimeEndtimeEdit.Properties.DisplayFormat.FormatString = "T";
            this.TimeEndtimeEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEndtimeEdit.Properties.EditFormat.FormatString = "T";
            this.TimeEndtimeEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.TimeEndtimeEdit.Size = new System.Drawing.Size(156, 24);
            this.TimeEndtimeEdit.StyleController = this.LayoutControl2;
            this.TimeEndtimeEdit.TabIndex = 31;
            // 
            // TimeStarttimeEdit
            // 
            this.TimeStarttimeEdit.EditValue = new System.DateTime(2019, 5, 14, 0, 0, 0, 0);
            this.TimeStarttimeEdit.Location = new System.Drawing.Point(9, 172);
            this.TimeStarttimeEdit.MenuManager = this.ribbon;
            this.TimeStarttimeEdit.Name = "TimeStarttimeEdit";
            this.TimeStarttimeEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.TimeStarttimeEdit.Properties.Appearance.Options.UseFont = true;
            this.TimeStarttimeEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.TimeStarttimeEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.TimeStarttimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TimeStarttimeEdit.Size = new System.Drawing.Size(156, 24);
            this.TimeStarttimeEdit.StyleController = this.LayoutControl2;
            this.TimeStarttimeEdit.TabIndex = 30;
            // 
            // btnClearfilter
            // 
            this.btnClearfilter.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnClearfilter.Appearance.Options.UseFont = true;
            this.btnClearfilter.Location = new System.Drawing.Point(6, 117);
            this.btnClearfilter.Name = "btnClearfilter";
            this.btnClearfilter.Size = new System.Drawing.Size(208, 29);
            this.btnClearfilter.StyleController = this.LayoutControl2;
            this.btnClearfilter.TabIndex = 29;
            this.btnClearfilter.Text = "نمایش همه";
            this.btnClearfilter.Click += new System.EventHandler(this.btnClearfilter_Click);
            // 
            // ChCompany
            // 
            this.ChCompany.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.ChCompany.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.ChCompany.Appearance.Options.UseBackColor = true;
            this.ChCompany.Appearance.Options.UseFont = true;
            this.ChCompany.CheckOnClick = true;
            this.ChCompany.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChCompany.DataSource = this.companyBindingSource;
            this.ChCompany.DisplayMember = "Title";
            this.ChCompany.Location = new System.Drawing.Point(9, 253);
            this.ChCompany.MultiColumn = true;
            this.ChCompany.Name = "ChCompany";
            this.ChCompany.Size = new System.Drawing.Size(202, 133);
            this.ChCompany.StyleController = this.LayoutControl2;
            this.ChCompany.TabIndex = 28;
            this.ChCompany.ValueMember = "Code";
            this.ChCompany.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.ChCompany_ItemCheck);
            // 
            // ChPath
            // 
            this.ChPath.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.ChPath.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.ChPath.Appearance.Options.UseBackColor = true;
            this.ChPath.Appearance.Options.UseFont = true;
            this.ChPath.CheckOnClick = true;
            this.ChPath.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChPath.DataSource = this.pathTripBindingSource;
            this.ChPath.DisplayMember = "PathTitle";
            this.ChPath.Location = new System.Drawing.Point(9, 415);
            this.ChPath.MultiColumn = true;
            this.ChPath.Name = "ChPath";
            this.ChPath.Size = new System.Drawing.Size(202, 132);
            this.ChPath.StyleController = this.LayoutControl2;
            this.ChPath.TabIndex = 27;
            this.ChPath.ValueMember = "PathTitle";
            this.ChPath.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.ChPath_ItemCheck);
            // 
            // pathTripBindingSource
            // 
            this.pathTripBindingSource.DataSource = typeof(Model.PathTrip);
            // 
            // SearchControl1
            // 
            this.SearchControl1.Client = this.gridControl1;
            this.SearchControl1.Location = new System.Drawing.Point(6, 6);
            this.SearchControl1.Name = "SearchControl1";
            this.SearchControl1.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.SearchControl1.Properties.Appearance.Options.UseFont = true;
            this.SearchControl1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.SearchControl1.Properties.Client = this.gridControl1;
            this.SearchControl1.Size = new System.Drawing.Size(208, 26);
            this.SearchControl1.StyleController = this.LayoutControl2;
            this.SearchControl1.TabIndex = 16;
            // 
            // txtTodate
            // 
            this.txtTodate.EditValue = null;
            this.txtTodate.Location = new System.Drawing.Point(9, 86);
            this.txtTodate.MenuManager = this.ribbon;
            this.txtTodate.Name = "txtTodate";
            this.txtTodate.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txtTodate.Properties.Appearance.Options.UseFont = true;
            this.txtTodate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTodate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTodate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTodate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTodate.Properties.NullValuePrompt = "تا تاریخ";
            this.txtTodate.Size = new System.Drawing.Size(156, 24);
            this.txtTodate.StyleController = this.LayoutControl2;
            this.txtTodate.TabIndex = 4;
            // 
            // txtFromDate
            // 
            this.txtFromDate.EditValue = null;
            this.txtFromDate.Location = new System.Drawing.Point(9, 58);
            this.txtFromDate.MenuManager = this.ribbon;
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txtFromDate.Properties.Appearance.Options.UseFont = true;
            this.txtFromDate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFromDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Properties.NullValuePrompt = "از تاریخ";
            this.txtFromDate.Size = new System.Drawing.Size(156, 24);
            this.txtFromDate.StyleController = this.LayoutControl2;
            this.txtFromDate.TabIndex = 3;
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup2.GroupBordersVisible = false;
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup3,
            this.LayoutControlGroup4,
            this.LayoutControlItem3,
            this.layoutControlGroup1,
            this.layoutControlItem4,
            this.layoutControlGroup5});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup2.Name = "Root";
            this.LayoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.LayoutControlGroup2.Size = new System.Drawing.Size(220, 556);
            this.LayoutControlGroup2.TextVisible = false;
            // 
            // LayoutControlGroup3
            // 
            this.LayoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup3.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.LayoutControlGroup3.AppearanceItemCaption.Options.UseFont = true;
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem1});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 30);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup3.Size = new System.Drawing.Size(212, 81);
            this.LayoutControlGroup3.Text = "گزارش تاریخ";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txtFromDate;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(206, 28);
            this.layoutControlItem5.Text = "از تاریخ";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(43, 24);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtTodate;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(206, 28);
            this.layoutControlItem1.Text = "تا تاریخ";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(43, 24);
            // 
            // LayoutControlGroup4
            // 
            this.LayoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup4.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.LayoutControlGroup4.AppearanceItemCaption.Options.UseFont = true;
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 387);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup4.Size = new System.Drawing.Size(212, 161);
            this.LayoutControlGroup4.Text = "مسیر ها";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ChPath;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(206, 136);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.SearchControl1;
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(212, 30);
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextVisible = false;
            this.LayoutControlItem3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.ExpandButtonVisible = true;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 225);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(212, 162);
            this.layoutControlGroup1.Text = "تعاونی ها";
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.ChCompany;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(206, 137);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnClearfilter;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 111);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(212, 33);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup5.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup5.ExpandButtonVisible = true;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem8,
            this.layoutControlItem7});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 144);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup5.Size = new System.Drawing.Size(212, 81);
            this.layoutControlGroup5.Text = "ساعت فروش";
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.TimeEndtimeEdit;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(206, 28);
            this.layoutControlItem8.Text = " تا ساعت";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(43, 13);
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.TimeStarttimeEdit;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(206, 28);
            this.layoutControlItem7.Text = " ازساعت";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(43, 13);
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.printableComponentLink1});
            // 
            // printableComponentLink1
            // 
            this.printableComponentLink1.Component = this.gridControl1;
            this.printableComponentLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // colSumTollPrice
            // 
            this.colSumTollPrice.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSumTollPrice.AppearanceCell.Options.UseFont = true;
            this.colSumTollPrice.AppearanceCell.Options.UseTextOptions = true;
            this.colSumTollPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumTollPrice.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSumTollPrice.AppearanceHeader.Options.UseFont = true;
            this.colSumTollPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colSumTollPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumTollPrice.Caption = "عوارض";
            this.colSumTollPrice.DisplayFormat.FormatString = "c0";
            this.colSumTollPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSumTollPrice.FieldName = "SumTollPrice";
            this.colSumTollPrice.Name = "colSumTollPrice";
            this.colSumTollPrice.Visible = true;
            this.colSumTollPrice.VisibleIndex = 6;
            this.colSumTollPrice.Width = 111;
            // 
            // FrmReportTicket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1324, 764);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.navigationPane1);
            this.Controls.Add(this.ribbon);
            this.Name = "FrmReportTicket";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "آمار بلیط ها";
            this.Load += new System.EventHandler(this.FrmReportTicket_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.getTripsResultBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).EndInit();
            this.navigationPane1.ResumeLayout(false);
            this.navigationPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).EndInit();
            this.LayoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TimeEndtimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TimeStarttimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChPath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pathTripBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView grd;
        private DevExpress.XtraBars.BarButtonItem btnGetTrip;
        private System.Windows.Forms.BindingSource getTripsResultBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colPathTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colSumPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colSumFundPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colSumOnlinePrice;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraBars.Navigation.NavigationPane navigationPane1;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        internal DevExpress.XtraLayout.LayoutControl LayoutControl2;
        internal DevExpress.XtraEditors.SearchControl SearchControl1;
        private FarsiLibrary.Win.DevExpress.XtraFADateEdit txtTodate;
        private FarsiLibrary.Win.DevExpress.XtraFADateEdit txtFromDate;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        internal DevExpress.XtraEditors.CheckedListBoxControl ChPath;
        private System.Windows.Forms.BindingSource pathTripBindingSource;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit reCompany;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colSummery;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        internal DevExpress.XtraEditors.CheckedListBoxControl ChCompany;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraEditors.SimpleButton btnClearfilter;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn colsumNumberchair;
        private DevExpress.XtraBars.BarButtonItem btnPrintReport;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink1;
        private DevExpress.XtraBars.BarButtonItem btnSaleTicket;
        private DevExpress.XtraEditors.TimeEdit TimeEndtimeEdit;
        private DevExpress.XtraEditors.TimeEdit TimeStarttimeEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraGrid.Columns.GridColumn colCountTrip;
        private DevExpress.XtraGrid.Columns.GridColumn colSumTollPrice;
    }
}