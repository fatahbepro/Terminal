﻿namespace Test
{
    partial class frmticketCompany
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmticketCompany));
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule4 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.BtnSave = new DevExpress.XtraBars.BarButtonItem();
            this.BtnNew = new DevExpress.XtraBars.BarButtonItem();
            this.BtnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.BtnDelete = new DevExpress.XtraBars.BarButtonItem();
            this.Btnclose = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.BtnBrowsImg = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.companyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colConnectionString = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEnabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colLogo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLocalConnectionString = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TitleTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ConnectionStringTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EnabledCheckEdit = new DevExpress.XtraEditors.ToggleSwitch();
            this.LogoPictureEdit = new DevExpress.XtraEditors.PictureEdit();
            this.LocalConnectionStringtextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage2 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar1 = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForTitle = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForConnectionString = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEnabled = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForLogo = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForConnectionString1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxValidationProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TitleTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConnectionStringTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnabledCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocalConnectionStringtextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTitle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConnectionString)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConnectionString1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ColorScheme = DevExpress.XtraBars.Ribbon.RibbonControlColorScheme.Green;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.BtnSave,
            this.BtnNew,
            this.BtnRefresh,
            this.BtnDelete,
            this.Btnclose});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 6;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1293, 147);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // BtnSave
            // 
            this.BtnSave.Caption = "ذخیره";
            this.BtnSave.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnSave.Glyph")));
            this.BtnSave.Id = 1;
            this.BtnSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.BtnSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnSave.LargeGlyph")));
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnSave_ItemClick);
            // 
            // BtnNew
            // 
            this.BtnNew.Caption = "خالی کردن";
            this.BtnNew.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnNew.Glyph")));
            this.BtnNew.Id = 2;
            this.BtnNew.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.BtnNew.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnNew.LargeGlyph")));
            this.BtnNew.Name = "BtnNew";
            this.BtnNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnNew_ItemClick);
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Caption = "بروزرسانی";
            this.BtnRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnRefresh.Glyph")));
            this.BtnRefresh.Id = 3;
            this.BtnRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.BtnRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnRefresh.LargeGlyph")));
            this.BtnRefresh.Name = "BtnRefresh";
            this.BtnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnRefresh_ItemClick);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Caption = "حذف";
            this.BtnDelete.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnDelete.Glyph")));
            this.BtnDelete.Id = 4;
            this.BtnDelete.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Delete);
            this.BtnDelete.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnDelete.LargeGlyph")));
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnDelete_ItemClick);
            // 
            // Btnclose
            // 
            this.Btnclose.Caption = "بستن";
            this.Btnclose.Glyph = ((System.Drawing.Image)(resources.GetObject("Btnclose.Glyph")));
            this.Btnclose.Id = 5;
            this.Btnclose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.Btnclose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("Btnclose.LargeGlyph")));
            this.Btnclose.Name = "Btnclose";
            this.Btnclose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.Btnclose_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ذخیره اطلاعات";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.BtnSave);
            this.ribbonPageGroup1.ItemLinks.Add(this.BtnNew);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "ذخیره";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.BtnRefresh);
            this.ribbonPageGroup2.ItemLinks.Add(this.BtnDelete);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "بروزرسانی داده ها";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.Btnclose);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 694);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1293, 23);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.BtnBrowsImg);
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.TitleTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ConnectionStringTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EnabledCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.LogoPictureEdit);
            this.dataLayoutControl1.Controls.Add(this.LocalConnectionStringtextEdit);
            this.dataLayoutControl1.DataSource = this.companyBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 147);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1173, 322, 250, 350);
            this.dataLayoutControl1.OptionsFocus.MoveFocusRightToLeft = true;
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1293, 547);
            this.dataLayoutControl1.TabIndex = 2;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // BtnBrowsImg
            // 
            this.BtnBrowsImg.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.BtnBrowsImg.Appearance.Options.UseFont = true;
            this.BtnBrowsImg.Image = ((System.Drawing.Image)(resources.GetObject("BtnBrowsImg.Image")));
            this.BtnBrowsImg.Location = new System.Drawing.Point(245, 108);
            this.BtnBrowsImg.Name = "BtnBrowsImg";
            this.BtnBrowsImg.Size = new System.Drawing.Size(134, 38);
            this.BtnBrowsImg.StyleController = this.dataLayoutControl1;
            this.BtnBrowsImg.TabIndex = 12;
            this.BtnBrowsImg.Text = "انتخاب تصویر";
            this.BtnBrowsImg.Click += new System.EventHandler(this.BtnBrowsImg_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.companyBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(12, 197);
            this.gridControl1.MainView = this.grd;
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1269, 338);
            this.gridControl1.TabIndex = 9;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grd});
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataSource = typeof(Model.Company);
            // 
            // grd
            // 
            this.grd.Appearance.FocusedCell.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedCell.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grd.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grd.Appearance.FocusedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grd.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grd.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grd.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.SelectedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grd.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grd.ColumnPanelRowHeight = 30;
            this.grd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colTitle,
            this.colCode,
            this.colConnectionString,
            this.colEnabled,
            this.colLogo,
            this.colLocalConnectionString});
            this.grd.GridControl = this.gridControl1;
            this.grd.Name = "grd";
            this.grd.OptionsBehavior.Editable = false;
            this.grd.OptionsFind.AlwaysVisible = true;
            this.grd.OptionsFind.FindNullPrompt = "جست و جو ....";
            this.grd.OptionsView.ShowGroupPanel = false;
            this.grd.PreviewFieldName = "LocalConnectionString";
            this.grd.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grd_RowCellClick);
            this.grd.DoubleClick += new System.EventHandler(this.grd_DoubleClick);
            // 
            // colID
            // 
            this.colID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colID.AppearanceCell.Options.UseFont = true;
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colID.AppearanceHeader.Options.UseFont = true;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.Caption = "#";
            this.colID.FieldName = "ID";
            this.colID.MaxWidth = 45;
            this.colID.Name = "colID";
            this.colID.Visible = true;
            this.colID.VisibleIndex = 0;
            this.colID.Width = 45;
            // 
            // colTitle
            // 
            this.colTitle.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTitle.AppearanceCell.Options.UseFont = true;
            this.colTitle.AppearanceCell.Options.UseTextOptions = true;
            this.colTitle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTitle.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTitle.AppearanceHeader.Options.UseFont = true;
            this.colTitle.AppearanceHeader.Options.UseTextOptions = true;
            this.colTitle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTitle.Caption = "عنوان";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 1;
            this.colTitle.Width = 277;
            // 
            // colCode
            // 
            this.colCode.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCode.AppearanceCell.Options.UseFont = true;
            this.colCode.AppearanceCell.Options.UseTextOptions = true;
            this.colCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCode.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCode.AppearanceHeader.Options.UseFont = true;
            this.colCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCode.Caption = "کد";
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            this.colCode.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Code", "{0}")});
            this.colCode.Visible = true;
            this.colCode.VisibleIndex = 2;
            this.colCode.Width = 152;
            // 
            // colConnectionString
            // 
            this.colConnectionString.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colConnectionString.AppearanceCell.Options.UseFont = true;
            this.colConnectionString.AppearanceCell.Options.UseTextOptions = true;
            this.colConnectionString.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colConnectionString.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colConnectionString.AppearanceHeader.Options.UseFont = true;
            this.colConnectionString.AppearanceHeader.Options.UseTextOptions = true;
            this.colConnectionString.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colConnectionString.Caption = "رشته اتصال اینترنتی";
            this.colConnectionString.FieldName = "ConnectionString";
            this.colConnectionString.Name = "colConnectionString";
            this.colConnectionString.Visible = true;
            this.colConnectionString.VisibleIndex = 3;
            this.colConnectionString.Width = 397;
            // 
            // colEnabled
            // 
            this.colEnabled.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colEnabled.AppearanceCell.Options.UseFont = true;
            this.colEnabled.AppearanceCell.Options.UseTextOptions = true;
            this.colEnabled.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnabled.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colEnabled.AppearanceHeader.Options.UseFont = true;
            this.colEnabled.AppearanceHeader.Options.UseTextOptions = true;
            this.colEnabled.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnabled.Caption = "وضعیت";
            this.colEnabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colEnabled.FieldName = "Enabled";
            this.colEnabled.MaxWidth = 55;
            this.colEnabled.Name = "colEnabled";
            this.colEnabled.Visible = true;
            this.colEnabled.VisibleIndex = 5;
            this.colEnabled.Width = 55;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.PictureChecked = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit1.PictureChecked")));
            this.repositoryItemCheckEdit1.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit1.PictureGrayed")));
            this.repositoryItemCheckEdit1.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit1.PictureUnchecked")));
            // 
            // colLogo
            // 
            this.colLogo.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colLogo.AppearanceHeader.Options.UseFont = true;
            this.colLogo.AppearanceHeader.Options.UseTextOptions = true;
            this.colLogo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLogo.Caption = "تصویر شرکت";
            this.colLogo.FieldName = "Logo";
            this.colLogo.Name = "colLogo";
            // 
            // colLocalConnectionString
            // 
            this.colLocalConnectionString.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colLocalConnectionString.AppearanceCell.Options.UseFont = true;
            this.colLocalConnectionString.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colLocalConnectionString.AppearanceHeader.Options.UseFont = true;
            this.colLocalConnectionString.AppearanceHeader.Options.UseTextOptions = true;
            this.colLocalConnectionString.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colLocalConnectionString.Caption = "رشته اتصال لوکال";
            this.colLocalConnectionString.FieldName = "LocalConnectionString";
            this.colLocalConnectionString.Name = "colLocalConnectionString";
            this.colLocalConnectionString.Visible = true;
            this.colLocalConnectionString.VisibleIndex = 4;
            this.colLocalConnectionString.Width = 325;
            // 
            // TitleTextEdit
            // 
            this.TitleTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyBindingSource, "Title", true));
            this.TitleTextEdit.EnterMoveNextControl = true;
            this.TitleTextEdit.Location = new System.Drawing.Point(736, 12);
            this.TitleTextEdit.MenuManager = this.ribbon;
            this.TitleTextEdit.Name = "TitleTextEdit";
            this.TitleTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.TitleTextEdit.Properties.Appearance.Options.UseFont = true;
            this.TitleTextEdit.Size = new System.Drawing.Size(426, 28);
            this.TitleTextEdit.StyleController = this.dataLayoutControl1;
            this.TitleTextEdit.TabIndex = 5;
            conditionValidationRule4.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule4.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.TitleTextEdit, conditionValidationRule4);
            this.TitleTextEdit.Validated += new System.EventHandler(this.EnabledCheckEdit_Validated);
            // 
            // CodeTextEdit
            // 
            this.CodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyBindingSource, "Code", true));
            this.CodeTextEdit.EnterMoveNextControl = true;
            this.CodeTextEdit.Location = new System.Drawing.Point(245, 12);
            this.CodeTextEdit.MenuManager = this.ribbon;
            this.CodeTextEdit.Name = "CodeTextEdit";
            this.CodeTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.CodeTextEdit.Properties.Appearance.Options.UseFont = true;
            this.CodeTextEdit.Size = new System.Drawing.Size(456, 28);
            this.CodeTextEdit.StyleController = this.dataLayoutControl1;
            this.CodeTextEdit.TabIndex = 6;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.CodeTextEdit, conditionValidationRule1);
            this.CodeTextEdit.Validated += new System.EventHandler(this.EnabledCheckEdit_Validated);
            // 
            // ConnectionStringTextEdit
            // 
            this.ConnectionStringTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyBindingSource, "ConnectionString", true));
            this.ConnectionStringTextEdit.EnterMoveNextControl = true;
            this.ConnectionStringTextEdit.Location = new System.Drawing.Point(245, 44);
            this.ConnectionStringTextEdit.MenuManager = this.ribbon;
            this.ConnectionStringTextEdit.Name = "ConnectionStringTextEdit";
            this.ConnectionStringTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ConnectionStringTextEdit.Properties.Appearance.Options.UseFont = true;
            this.ConnectionStringTextEdit.Size = new System.Drawing.Size(917, 28);
            this.ConnectionStringTextEdit.StyleController = this.dataLayoutControl1;
            this.ConnectionStringTextEdit.TabIndex = 7;
            // 
            // EnabledCheckEdit
            // 
            this.EnabledCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyBindingSource, "Enabled", true));
            this.EnabledCheckEdit.Location = new System.Drawing.Point(383, 108);
            this.EnabledCheckEdit.MenuManager = this.ribbon;
            this.EnabledCheckEdit.Name = "EnabledCheckEdit";
            this.EnabledCheckEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.EnabledCheckEdit.Properties.Appearance.Options.UseFont = true;
            this.EnabledCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.EnabledCheckEdit.Properties.OffText = "غیر فعال";
            this.EnabledCheckEdit.Properties.OnText = "فعال";
            this.EnabledCheckEdit.Size = new System.Drawing.Size(779, 33);
            this.EnabledCheckEdit.StyleController = this.dataLayoutControl1;
            this.EnabledCheckEdit.TabIndex = 8;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.EnabledCheckEdit, conditionValidationRule2);
            this.EnabledCheckEdit.Validated += new System.EventHandler(this.EnabledCheckEdit_Validated);
            // 
            // LogoPictureEdit
            // 
            this.LogoPictureEdit.BackgroundImage = global::Test.Properties.Resources.logo;
            this.LogoPictureEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LogoPictureEdit.Cursor = System.Windows.Forms.Cursors.Default;
            this.LogoPictureEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyBindingSource, "Logo", true));
            this.LogoPictureEdit.EditValue = global::Test.Properties.Resources.logo;
            this.LogoPictureEdit.Location = new System.Drawing.Point(12, 28);
            this.LogoPictureEdit.MenuManager = this.ribbon;
            this.LogoPictureEdit.Name = "LogoPictureEdit";
            this.LogoPictureEdit.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.LogoPictureEdit.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            this.LogoPictureEdit.Properties.ZoomAccelerationFactor = 1D;
            this.LogoPictureEdit.Size = new System.Drawing.Size(229, 165);
            this.LogoPictureEdit.StyleController = this.dataLayoutControl1;
            this.LogoPictureEdit.TabIndex = 10;
            // 
            // LocalConnectionStringtextEdit
            // 
            this.LocalConnectionStringtextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.companyBindingSource, "LocalConnectionString", true));
            this.LocalConnectionStringtextEdit.EnterMoveNextControl = true;
            this.LocalConnectionStringtextEdit.Location = new System.Drawing.Point(245, 76);
            this.LocalConnectionStringtextEdit.MenuManager = this.ribbonControl1;
            this.LocalConnectionStringtextEdit.Name = "LocalConnectionStringtextEdit";
            this.LocalConnectionStringtextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.LocalConnectionStringtextEdit.Properties.Appearance.Options.UseFont = true;
            this.LocalConnectionStringtextEdit.Size = new System.Drawing.Size(917, 28);
            this.LocalConnectionStringtextEdit.StyleController = this.dataLayoutControl1;
            this.LocalConnectionStringtextEdit.TabIndex = 7;
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5});
            this.ribbonControl1.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl1.MaxItemId = 6;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage2});
            this.ribbonControl1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl1.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbonControl1.ShowToolbarCustomizeItem = false;
            this.ribbonControl1.Size = new System.Drawing.Size(1138, 140);
            this.ribbonControl1.StatusBar = this.ribbonStatusBar1;
            this.ribbonControl1.Toolbar.ShowCustomizeItem = false;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "ذخیره";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "خالی کردن";
            this.barButtonItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.Glyph")));
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.barButtonItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.LargeGlyph")));
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "بروزرسانی";
            this.barButtonItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.Glyph")));
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.barButtonItem3.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.LargeGlyph")));
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "حذف";
            this.barButtonItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.Glyph")));
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Delete);
            this.barButtonItem4.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.LargeGlyph")));
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "بستن";
            this.barButtonItem5.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.Glyph")));
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.barButtonItem5.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.LargeGlyph")));
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // ribbonPage2
            // 
            this.ribbonPage2.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup4,
            this.ribbonPageGroup5,
            this.ribbonPageGroup6});
            this.ribbonPage2.Name = "ribbonPage2";
            this.ribbonPage2.Text = "ذخیره اطلاعات";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "ذخیره";
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.ShowCaptionButton = false;
            this.ribbonPageGroup5.Text = "بروزرسانی داده ها";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.barButtonItem5);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.ShowCaptionButton = false;
            this.ribbonPageGroup6.Text = "بستن";
            // 
            // ribbonStatusBar1
            // 
            this.ribbonStatusBar1.Location = new System.Drawing.Point(0, 646);
            this.ribbonStatusBar1.Name = "ribbonStatusBar1";
            this.ribbonStatusBar1.Ribbon = this.ribbonControl1;
            this.ribbonStatusBar1.Size = new System.Drawing.Size(1138, 28);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1293, 547);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForTitle,
            this.ItemForConnectionString,
            this.layoutControlItem1,
            this.ItemForCode,
            this.ItemForEnabled,
            this.emptySpaceItem1,
            this.ItemForLogo,
            this.layoutControlItem3,
            this.ItemForConnectionString1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1273, 527);
            // 
            // ItemForTitle
            // 
            this.ItemForTitle.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForTitle.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForTitle.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForTitle.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForTitle.Control = this.TitleTextEdit;
            this.ItemForTitle.Location = new System.Drawing.Point(724, 0);
            this.ItemForTitle.Name = "ItemForTitle";
            this.ItemForTitle.Size = new System.Drawing.Size(549, 32);
            this.ItemForTitle.Text = "عنوان :";
            this.ItemForTitle.TextSize = new System.Drawing.Size(116, 24);
            // 
            // ItemForConnectionString
            // 
            this.ItemForConnectionString.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForConnectionString.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForConnectionString.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForConnectionString.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForConnectionString.Control = this.ConnectionStringTextEdit;
            this.ItemForConnectionString.Location = new System.Drawing.Point(233, 32);
            this.ItemForConnectionString.Name = "ItemForConnectionString";
            this.ItemForConnectionString.Size = new System.Drawing.Size(1040, 32);
            this.ItemForConnectionString.Text = "رشته اتصال اینترنتی :";
            this.ItemForConnectionString.TextSize = new System.Drawing.Size(116, 24);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 185);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1273, 342);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // ItemForCode
            // 
            this.ItemForCode.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForCode.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForCode.Control = this.CodeTextEdit;
            this.ItemForCode.Location = new System.Drawing.Point(233, 0);
            this.ItemForCode.Name = "ItemForCode";
            this.ItemForCode.Size = new System.Drawing.Size(491, 32);
            this.ItemForCode.Text = " کد :";
            this.ItemForCode.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForCode.TextSize = new System.Drawing.Size(26, 24);
            this.ItemForCode.TextToControlDistance = 5;
            // 
            // ItemForEnabled
            // 
            this.ItemForEnabled.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForEnabled.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForEnabled.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForEnabled.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForEnabled.Control = this.EnabledCheckEdit;
            this.ItemForEnabled.Location = new System.Drawing.Point(371, 96);
            this.ItemForEnabled.Name = "ItemForEnabled";
            this.ItemForEnabled.Size = new System.Drawing.Size(902, 42);
            this.ItemForEnabled.Text = "وضعیت ";
            this.ItemForEnabled.TextSize = new System.Drawing.Size(116, 24);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(233, 138);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(1040, 47);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForLogo
            // 
            this.ItemForLogo.Control = this.LogoPictureEdit;
            this.ItemForLogo.Location = new System.Drawing.Point(0, 0);
            this.ItemForLogo.Name = "ItemForLogo";
            this.ItemForLogo.Size = new System.Drawing.Size(233, 185);
            this.ItemForLogo.StartNewLine = true;
            this.ItemForLogo.Text = "تصویر شرکت";
            this.ItemForLogo.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForLogo.TextSize = new System.Drawing.Size(116, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.BtnBrowsImg;
            this.layoutControlItem3.Location = new System.Drawing.Point(233, 96);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(138, 42);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(138, 42);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(138, 42);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // ItemForConnectionString1
            // 
            this.ItemForConnectionString1.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForConnectionString1.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForConnectionString1.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForConnectionString1.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForConnectionString1.Control = this.LocalConnectionStringtextEdit;
            this.ItemForConnectionString1.CustomizationFormText = "رشته اتصال اینترنتی :";
            this.ItemForConnectionString1.Location = new System.Drawing.Point(233, 64);
            this.ItemForConnectionString1.Name = "ItemForConnectionString1";
            this.ItemForConnectionString1.Size = new System.Drawing.Size(1040, 32);
            this.ItemForConnectionString1.Text = "رشته اتصال لوکال :";
            this.ItemForConnectionString1.TextSize = new System.Drawing.Size(116, 24);
            // 
            // frmticketCompany
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1293, 717);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Name = "frmticketCompany";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmticketCompany_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TitleTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConnectionStringTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnabledCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LogoPictureEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LocalConnectionStringtextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTitle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConnectionString)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForConnectionString1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem BtnSave;
        private DevExpress.XtraBars.BarButtonItem BtnNew;
        private DevExpress.XtraBars.BarButtonItem BtnRefresh;
        private DevExpress.XtraBars.BarButtonItem BtnDelete;
        private DevExpress.XtraBars.BarButtonItem Btnclose;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView grd;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colConnectionString;
        private DevExpress.XtraGrid.Columns.GridColumn colEnabled;
        private DevExpress.XtraEditors.TextEdit TitleTextEdit;
        private DevExpress.XtraEditors.TextEdit CodeTextEdit;
        private DevExpress.XtraEditors.TextEdit ConnectionStringTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTitle;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForConnectionString;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEnabled;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.ToggleSwitch EnabledCheckEdit;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private DevExpress.XtraEditors.PictureEdit LogoPictureEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForLogo;
        private DevExpress.XtraGrid.Columns.GridColumn colLogo;
        private DevExpress.XtraEditors.SimpleButton BtnBrowsImg;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.TextEdit LocalConnectionStringtextEdit;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForConnectionString1;
        private DevExpress.XtraGrid.Columns.GridColumn colLocalConnectionString;
    }
}