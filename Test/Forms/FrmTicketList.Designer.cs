﻿namespace Test
{
    partial class FrmTicketList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTicketList));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnReport = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.Btn_Del = new DevExpress.XtraBars.BarButtonItem();
            this.BtnbackSale = new DevExpress.XtraBars.BarButtonItem();
            this.btnCancelPose = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barCreditUser = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.btnLoadRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dgv = new DevExpress.XtraGrid.GridControl();
            this.ticketLocalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTripID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChairsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSumDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaleDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemXtraFADateEdit1 = new FarsiLibrary.Win.DevExpress.RepositoryItemXtraFADateEdit();
            this.colNumChairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStateticket = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCityName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCityID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reCompany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.companyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colCashierName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReturnUserID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusPaidBack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReturnUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidBackDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidBackTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTicket = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFundPriceValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMoveTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaleType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.col2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ColPayback = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ReBtnPayback = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPrintFactor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reBtnPrintticket = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPayPose = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reBtnPayPose = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colPaycredit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reBtnPayCredit = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colCodeCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOnlinePrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colClosed = new DevExpress.XtraGrid.Columns.GridColumn();
            this.toolTip = new DevExpress.Utils.ToolTipController(this.components);
            this.cityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.bgpose = new System.ComponentModel.BackgroundWorker();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Test.WaitForm1), true, true);
            this.chSale = new DevExpress.XtraEditors.CheckEdit();
            this.LayoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.chRezerv = new DevExpress.XtraEditors.CheckEdit();
            this.ChCompany = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.chAll = new DevExpress.XtraEditors.CheckEdit();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.lblNameDay = new DevExpress.XtraEditors.LabelControl();
            this.btnshowReport = new DevExpress.XtraEditors.SimpleButton();
            this.btnNextDate = new DevExpress.XtraEditors.SimpleButton();
            this.btnNowDate = new DevExpress.XtraEditors.SimpleButton();
            this.btnBackDate = new DevExpress.XtraEditors.SimpleButton();
            this.txtFromDate = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.navigationPane1 = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketLocalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReBtnPayback)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPrintticket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPayPose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPayCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSale.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).BeginInit();
            this.LayoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chRezerv.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).BeginInit();
            this.navigationPane1.SuspendLayout();
            this.navigationPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnReport,
            this.barButtonItem1,
            this.barButtonItem2,
            this.Btn_Del,
            this.BtnbackSale,
            this.btnCancelPose,
            this.btnRefresh,
            this.barCreditUser,
            this.barButtonItem4,
            this.btnLoadRefresh});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ribbon.MaxItemId = 12;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1372, 146);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // btnReport
            // 
            this.btnReport.Caption = "گزارش";
            this.btnReport.Glyph = ((System.Drawing.Image)(resources.GetObject("btnReport.Glyph")));
            this.btnReport.Id = 1;
            this.btnReport.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6);
            this.btnReport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnReport.LargeGlyph")));
            this.btnReport.Name = "btnReport";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "چاپ";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 2;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "بستن";
            this.barButtonItem2.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.Glyph")));
            this.barButtonItem2.Id = 3;
            this.barButtonItem2.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem2.LargeGlyph")));
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // Btn_Del
            // 
            this.Btn_Del.Caption = "کنسل بلیط";
            this.Btn_Del.Glyph = ((System.Drawing.Image)(resources.GetObject("Btn_Del.Glyph")));
            this.Btn_Del.Id = 4;
            this.Btn_Del.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("Btn_Del.LargeGlyph")));
            this.Btn_Del.Name = "Btn_Del";
            // 
            // BtnbackSale
            // 
            this.BtnbackSale.Caption = "استرداد";
            this.BtnbackSale.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnbackSale.Glyph")));
            this.BtnbackSale.Id = 5;
            this.BtnbackSale.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F9);
            this.BtnbackSale.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnbackSale.LargeGlyph")));
            this.BtnbackSale.Name = "BtnbackSale";
            toolTipTitleItem1.Text = "استرداد";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "بازگشت هزینه بلیط به موجودی حساب صندوقدار";
            toolTipTitleItem2.LeftIndent = 6;
            toolTipTitleItem2.Text = "F9";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipSeparatorItem1);
            superToolTip1.Items.Add(toolTipTitleItem2);
            this.BtnbackSale.SuperTip = superToolTip1;
            this.BtnbackSale.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnbackSale_ItemClick);
            // 
            // btnCancelPose
            // 
            this.btnCancelPose.Caption = "لغو ارسال";
            this.btnCancelPose.Enabled = false;
            this.btnCancelPose.Id = 6;
            this.btnCancelPose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F8);
            this.btnCancelPose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnCancelPose.LargeGlyph")));
            this.btnCancelPose.Name = "btnCancelPose";
            this.btnCancelPose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancelPose_ItemClick);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Id = 10;
            this.btnRefresh.Name = "btnRefresh";
            // 
            // barCreditUser
            // 
            this.barCreditUser.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barCreditUser.Caption = "اعتبار حساب کاربر";
            this.barCreditUser.Edit = this.repositoryItemTextEdit1;
            this.barCreditUser.EditWidth = 200;
            this.barCreditUser.Id = 8;
            this.barCreditUser.Name = "barCreditUser";
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.repositoryItemTextEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit1.Appearance.Options.UseTextOptions = true;
            this.repositoryItemTextEdit1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.DisplayFormat.FormatString = "c0";
            this.repositoryItemTextEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.EditFormat.FormatString = "c0";
            this.repositoryItemTextEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.repositoryItemTextEdit1.Mask.EditMask = "c0";
            this.repositoryItemTextEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.ReadOnly = true;
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 9;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // btnLoadRefresh
            // 
            this.btnLoadRefresh.Caption = "بروز رسانی لیست";
            this.btnLoadRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("btnLoadRefresh.Glyph")));
            this.btnLoadRefresh.Id = 11;
            this.btnLoadRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btnLoadRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnLoadRefresh.LargeGlyph")));
            this.btnLoadRefresh.Name = "btnLoadRefresh";
            this.btnLoadRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnLoadRefresh_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "عملیات";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.BtnbackSale);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnCancelPose);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnLoadRefresh);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "عملیات";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem2);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barCreditUser);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 725);
            this.ribbonStatusBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1107, 21);
            // 
            // dgv
            // 
            this.dgv.DataSource = this.ticketLocalBindingSource;
            this.dgv.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            gridLevelNode1.RelationName = "Level1";
            this.dgv.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.dgv.Location = new System.Drawing.Point(12, 52);
            this.dgv.MainView = this.grd;
            this.dgv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv.MenuManager = this.ribbon;
            this.dgv.Name = "dgv";
            this.dgv.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ReBtnPayback,
            this.repositoryItemXtraFADateEdit1,
            this.reBtnPrintticket,
            this.reBtnPayPose,
            this.reBtnPayCredit,
            this.reCompany});
            this.dgv.Size = new System.Drawing.Size(1083, 515);
            this.dgv.TabIndex = 31;
            this.dgv.ToolTipController = this.toolTip;
            this.dgv.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grd});
            // 
            // ticketLocalBindingSource
            // 
            this.ticketLocalBindingSource.DataSource = typeof(Model.TicketLocal);
            // 
            // grd
            // 
            this.grd.Appearance.FocusedCell.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedCell.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grd.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grd.Appearance.FocusedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grd.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grd.Appearance.FooterPanel.Font = new System.Drawing.Font("Tornado Tahoma", 8.25F);
            this.grd.Appearance.FooterPanel.Options.UseFont = true;
            this.grd.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grd.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.GroupFooter.Font = new System.Drawing.Font("Tornado Tahoma", 8.25F);
            this.grd.Appearance.GroupFooter.Options.UseFont = true;
            this.grd.Appearance.GroupFooter.Options.UseTextOptions = true;
            this.grd.Appearance.GroupFooter.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.SelectedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grd.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grd.ColumnPanelRowHeight = 30;
            this.grd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colTripID,
            this.colMan,
            this.colFullname,
            this.colNo,
            this.colChairsName,
            this.colPrice,
            this.colSumDiscount,
            this.colSaleDate,
            this.colNumChairs,
            this.colStateticket,
            this.colCityName,
            this.colCityID,
            this.colCompanyCode,
            this.colCashierName,
            this.colReturnUserID,
            this.colStatusPaidBack,
            this.colReturnUserName,
            this.colPaidBackDate,
            this.colPaidBackTime,
            this.colTotalTicket,
            this.colTimeSale,
            this.colFundPriceValue,
            this.colTime,
            this.colMoveTime,
            this.colSaleType,
            this.col1,
            this.col2,
            this.ColPayback,
            this.colPrintFactor,
            this.colPayPose,
            this.colPaycredit,
            this.colCodeCompany,
            this.colOnlinePrice,
            this.colClosed});
            this.grd.GridControl = this.dgv;
            this.grd.Name = "grd";
            this.grd.OptionsFind.AlwaysVisible = true;
            this.grd.OptionsFind.FindDelay = 100;
            this.grd.OptionsFind.FindFilterColumns = "Fullname";
            this.grd.OptionsFind.FindNullPrompt = "جست و جو ..";
            this.grd.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grd.OptionsView.ShowFooter = true;
            this.grd.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.grd_RowStyle);
            this.grd.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.grd_CustomRowCellEdit);
            this.grd.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.grd_CustomUnboundColumnData);
            this.grd.MouseMove += new System.Windows.Forms.MouseEventHandler(this.grd_MouseMove);
            // 
            // colID
            // 
            this.colID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colID.AppearanceCell.Options.UseFont = true;
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colID.AppearanceHeader.Options.UseFont = true;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.Caption = "#";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.OptionsColumn.ReadOnly = true;
            this.colID.Width = 50;
            // 
            // colTripID
            // 
            this.colTripID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTripID.AppearanceCell.Options.UseFont = true;
            this.colTripID.AppearanceCell.Options.UseTextOptions = true;
            this.colTripID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTripID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTripID.AppearanceHeader.Options.UseFont = true;
            this.colTripID.AppearanceHeader.Options.UseTextOptions = true;
            this.colTripID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTripID.Caption = "کد سرویس";
            this.colTripID.FieldName = "TripID";
            this.colTripID.Name = "colTripID";
            this.colTripID.OptionsColumn.ReadOnly = true;
            this.colTripID.Width = 94;
            // 
            // colMan
            // 
            this.colMan.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colMan.AppearanceCell.Options.UseFont = true;
            this.colMan.AppearanceCell.Options.UseTextOptions = true;
            this.colMan.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMan.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colMan.AppearanceHeader.Options.UseFont = true;
            this.colMan.AppearanceHeader.Options.UseTextOptions = true;
            this.colMan.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMan.Caption = "جنسیت";
            this.colMan.FieldName = "Man";
            this.colMan.Name = "colMan";
            this.colMan.OptionsColumn.ReadOnly = true;
            this.colMan.Width = 62;
            // 
            // colFullname
            // 
            this.colFullname.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colFullname.AppearanceCell.Options.UseFont = true;
            this.colFullname.AppearanceCell.Options.UseTextOptions = true;
            this.colFullname.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullname.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colFullname.AppearanceHeader.Options.UseFont = true;
            this.colFullname.AppearanceHeader.Options.UseTextOptions = true;
            this.colFullname.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullname.Caption = "نام مسافر";
            this.colFullname.FieldName = "Fullname";
            this.colFullname.Name = "colFullname";
            this.colFullname.OptionsColumn.ReadOnly = true;
            this.colFullname.Visible = true;
            this.colFullname.VisibleIndex = 2;
            this.colFullname.Width = 119;
            // 
            // colNo
            // 
            this.colNo.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colNo.AppearanceCell.Options.UseFont = true;
            this.colNo.AppearanceCell.Options.UseTextOptions = true;
            this.colNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colNo.AppearanceHeader.Options.UseFont = true;
            this.colNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.Caption = "شماره بلیط";
            this.colNo.FieldName = "No";
            this.colNo.Name = "colNo";
            this.colNo.OptionsColumn.ReadOnly = true;
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 4;
            this.colNo.Width = 90;
            // 
            // colChairsName
            // 
            this.colChairsName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colChairsName.AppearanceCell.Options.UseFont = true;
            this.colChairsName.AppearanceCell.Options.UseTextOptions = true;
            this.colChairsName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colChairsName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colChairsName.AppearanceHeader.Options.UseFont = true;
            this.colChairsName.AppearanceHeader.Options.UseTextOptions = true;
            this.colChairsName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colChairsName.Caption = "صندلی ها";
            this.colChairsName.FieldName = "ChairsName";
            this.colChairsName.Name = "colChairsName";
            this.colChairsName.OptionsColumn.ReadOnly = true;
            this.colChairsName.Visible = true;
            this.colChairsName.VisibleIndex = 5;
            this.colChairsName.Width = 64;
            // 
            // colPrice
            // 
            this.colPrice.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPrice.AppearanceCell.Options.UseFont = true;
            this.colPrice.AppearanceCell.Options.UseTextOptions = true;
            this.colPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPrice.AppearanceHeader.Options.UseFont = true;
            this.colPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.Caption = "قیمت";
            this.colPrice.DisplayFormat.FormatString = "c0";
            this.colPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrice.FieldName = "Price";
            this.colPrice.Name = "colPrice";
            this.colPrice.OptionsColumn.ReadOnly = true;
            this.colPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Price", "{0:c0}")});
            this.colPrice.Width = 37;
            // 
            // colSumDiscount
            // 
            this.colSumDiscount.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSumDiscount.AppearanceCell.Options.UseFont = true;
            this.colSumDiscount.AppearanceCell.Options.UseTextOptions = true;
            this.colSumDiscount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumDiscount.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSumDiscount.AppearanceHeader.Options.UseFont = true;
            this.colSumDiscount.AppearanceHeader.Options.UseTextOptions = true;
            this.colSumDiscount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSumDiscount.Caption = "تخفیف";
            this.colSumDiscount.DisplayFormat.FormatString = "c0";
            this.colSumDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colSumDiscount.FieldName = "SumDiscount";
            this.colSumDiscount.Name = "colSumDiscount";
            this.colSumDiscount.OptionsColumn.ReadOnly = true;
            this.colSumDiscount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Discount", " {0:c0}")});
            this.colSumDiscount.Visible = true;
            this.colSumDiscount.VisibleIndex = 7;
            this.colSumDiscount.Width = 52;
            // 
            // colSaleDate
            // 
            this.colSaleDate.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSaleDate.AppearanceCell.Options.UseFont = true;
            this.colSaleDate.AppearanceCell.Options.UseTextOptions = true;
            this.colSaleDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSaleDate.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colSaleDate.AppearanceHeader.Options.UseFont = true;
            this.colSaleDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colSaleDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSaleDate.Caption = "ت فروش";
            this.colSaleDate.FieldName = "SaleDate";
            this.colSaleDate.Name = "colSaleDate";
            this.colSaleDate.OptionsColumn.ReadOnly = true;
            this.colSaleDate.Width = 82;
            // 
            // repositoryItemXtraFADateEdit1
            // 
            this.repositoryItemXtraFADateEdit1.AutoHeight = false;
            this.repositoryItemXtraFADateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemXtraFADateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemXtraFADateEdit1.Name = "repositoryItemXtraFADateEdit1";
            // 
            // colNumChairs
            // 
            this.colNumChairs.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colNumChairs.AppearanceCell.Options.UseFont = true;
            this.colNumChairs.AppearanceCell.Options.UseTextOptions = true;
            this.colNumChairs.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumChairs.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colNumChairs.AppearanceHeader.Options.UseFont = true;
            this.colNumChairs.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumChairs.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumChairs.Caption = "ت صندلی";
            this.colNumChairs.FieldName = "NumChairs";
            this.colNumChairs.Name = "colNumChairs";
            this.colNumChairs.OptionsColumn.ReadOnly = true;
            this.colNumChairs.Visible = true;
            this.colNumChairs.VisibleIndex = 6;
            this.colNumChairs.Width = 76;
            // 
            // colStateticket
            // 
            this.colStateticket.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colStateticket.AppearanceCell.Options.UseFont = true;
            this.colStateticket.AppearanceCell.Options.UseTextOptions = true;
            this.colStateticket.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateticket.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colStateticket.AppearanceHeader.Options.UseFont = true;
            this.colStateticket.AppearanceHeader.Options.UseTextOptions = true;
            this.colStateticket.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStateticket.Caption = "وضعیت بلیط";
            this.colStateticket.FieldName = "Stateticket";
            this.colStateticket.Name = "colStateticket";
            this.colStateticket.OptionsColumn.ReadOnly = true;
            this.colStateticket.Visible = true;
            this.colStateticket.VisibleIndex = 9;
            this.colStateticket.Width = 84;
            // 
            // colCityName
            // 
            this.colCityName.Caption = "شهر مقصد";
            this.colCityName.FieldName = "CityName";
            this.colCityName.Name = "colCityName";
            this.colCityName.Visible = true;
            this.colCityName.VisibleIndex = 1;
            // 
            // colCityID
            // 
            this.colCityID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCityID.AppearanceCell.Options.UseFont = true;
            this.colCityID.AppearanceCell.Options.UseTextOptions = true;
            this.colCityID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCityID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCityID.AppearanceHeader.Options.UseFont = true;
            this.colCityID.AppearanceHeader.Options.UseTextOptions = true;
            this.colCityID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCityID.Caption = "کد شهر مقصد";
            this.colCityID.FieldName = "CityID";
            this.colCityID.Name = "colCityID";
            this.colCityID.OptionsColumn.ReadOnly = true;
            this.colCityID.Width = 59;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCompanyCode.AppearanceCell.Options.UseFont = true;
            this.colCompanyCode.AppearanceCell.Options.UseTextOptions = true;
            this.colCompanyCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyCode.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCompanyCode.AppearanceHeader.Options.UseFont = true;
            this.colCompanyCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCompanyCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyCode.Caption = "نام تعاونی";
            this.colCompanyCode.ColumnEdit = this.reCompany;
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.OptionsColumn.ReadOnly = true;
            this.colCompanyCode.Visible = true;
            this.colCompanyCode.VisibleIndex = 0;
            this.colCompanyCode.Width = 64;
            // 
            // reCompany
            // 
            this.reCompany.AutoHeight = false;
            this.reCompany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.reCompany.DataSource = this.companyBindingSource;
            this.reCompany.DisplayMember = "Title";
            this.reCompany.Name = "reCompany";
            this.reCompany.NullText = "نامشخص";
            this.reCompany.ValueMember = "Code";
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataSource = typeof(Model.Company);
            // 
            // colCashierName
            // 
            this.colCashierName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCashierName.AppearanceCell.Options.UseFont = true;
            this.colCashierName.AppearanceCell.Options.UseTextOptions = true;
            this.colCashierName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCashierName.AppearanceHeader.Options.UseFont = true;
            this.colCashierName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCashierName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierName.Caption = "کاربر صندوق";
            this.colCashierName.FieldName = "CashierName";
            this.colCashierName.Name = "colCashierName";
            this.colCashierName.OptionsColumn.ReadOnly = true;
            this.colCashierName.Visible = true;
            this.colCashierName.VisibleIndex = 10;
            this.colCashierName.Width = 57;
            // 
            // colReturnUserID
            // 
            this.colReturnUserID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colReturnUserID.AppearanceCell.Options.UseFont = true;
            this.colReturnUserID.AppearanceCell.Options.UseTextOptions = true;
            this.colReturnUserID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnUserID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colReturnUserID.AppearanceHeader.Options.UseFont = true;
            this.colReturnUserID.AppearanceHeader.Options.UseTextOptions = true;
            this.colReturnUserID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnUserID.Caption = "کد کاربر";
            this.colReturnUserID.FieldName = "ReturnUserID";
            this.colReturnUserID.Name = "colReturnUserID";
            this.colReturnUserID.OptionsColumn.ReadOnly = true;
            this.colReturnUserID.Width = 27;
            // 
            // colStatusPaidBack
            // 
            this.colStatusPaidBack.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colStatusPaidBack.AppearanceCell.Options.UseFont = true;
            this.colStatusPaidBack.AppearanceCell.Options.UseTextOptions = true;
            this.colStatusPaidBack.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStatusPaidBack.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colStatusPaidBack.AppearanceHeader.Options.UseFont = true;
            this.colStatusPaidBack.AppearanceHeader.Options.UseTextOptions = true;
            this.colStatusPaidBack.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStatusPaidBack.Caption = "وضعیت برگشت";
            this.colStatusPaidBack.FieldName = "StatusPaidBack";
            this.colStatusPaidBack.Name = "colStatusPaidBack";
            this.colStatusPaidBack.OptionsColumn.ReadOnly = true;
            this.colStatusPaidBack.Visible = true;
            this.colStatusPaidBack.VisibleIndex = 11;
            this.colStatusPaidBack.Width = 80;
            // 
            // colReturnUserName
            // 
            this.colReturnUserName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colReturnUserName.AppearanceCell.Options.UseFont = true;
            this.colReturnUserName.AppearanceCell.Options.UseTextOptions = true;
            this.colReturnUserName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnUserName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colReturnUserName.AppearanceHeader.Options.UseFont = true;
            this.colReturnUserName.AppearanceHeader.Options.UseTextOptions = true;
            this.colReturnUserName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnUserName.Caption = "کاربر برگشت دهنده";
            this.colReturnUserName.FieldName = "ReturnUserName";
            this.colReturnUserName.Name = "colReturnUserName";
            this.colReturnUserName.OptionsColumn.ReadOnly = true;
            this.colReturnUserName.Width = 81;
            // 
            // colPaidBackDate
            // 
            this.colPaidBackDate.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPaidBackDate.AppearanceCell.Options.UseFont = true;
            this.colPaidBackDate.AppearanceCell.Options.UseTextOptions = true;
            this.colPaidBackDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackDate.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPaidBackDate.AppearanceHeader.Options.UseFont = true;
            this.colPaidBackDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colPaidBackDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackDate.Caption = "ت برگشت";
            this.colPaidBackDate.FieldName = "PaidBackDate";
            this.colPaidBackDate.Name = "colPaidBackDate";
            this.colPaidBackDate.OptionsColumn.ReadOnly = true;
            this.colPaidBackDate.Width = 48;
            // 
            // colPaidBackTime
            // 
            this.colPaidBackTime.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPaidBackTime.AppearanceCell.Options.UseFont = true;
            this.colPaidBackTime.AppearanceCell.Options.UseTextOptions = true;
            this.colPaidBackTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackTime.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPaidBackTime.AppearanceHeader.Options.UseFont = true;
            this.colPaidBackTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colPaidBackTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackTime.Caption = "س برگشت";
            this.colPaidBackTime.DisplayFormat.FormatString = "HH:mm:tt";
            this.colPaidBackTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPaidBackTime.FieldName = "PaidBackDate";
            this.colPaidBackTime.Name = "colPaidBackTime";
            this.colPaidBackTime.Width = 57;
            // 
            // colTotalTicket
            // 
            this.colTotalTicket.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTotalTicket.AppearanceCell.Options.UseFont = true;
            this.colTotalTicket.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalTicket.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalTicket.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTotalTicket.AppearanceHeader.Options.UseFont = true;
            this.colTotalTicket.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalTicket.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalTicket.Caption = "جمع کل";
            this.colTotalTicket.DisplayFormat.FormatString = "c0";
            this.colTotalTicket.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalTicket.FieldName = "TotalTicket";
            this.colTotalTicket.Name = "colTotalTicket";
            this.colTotalTicket.OptionsColumn.ReadOnly = true;
            this.colTotalTicket.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalTicket", "ج: {0:c0}")});
            this.colTotalTicket.Visible = true;
            this.colTotalTicket.VisibleIndex = 8;
            this.colTotalTicket.Width = 102;
            // 
            // colTimeSale
            // 
            this.colTimeSale.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTimeSale.AppearanceCell.Options.UseFont = true;
            this.colTimeSale.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeSale.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeSale.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTimeSale.AppearanceHeader.Options.UseFont = true;
            this.colTimeSale.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeSale.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeSale.Caption = "س فروش";
            this.colTimeSale.FieldName = "TimeSale";
            this.colTimeSale.Name = "colTimeSale";
            this.colTimeSale.OptionsColumn.ReadOnly = true;
            this.colTimeSale.Width = 79;
            // 
            // colFundPriceValue
            // 
            this.colFundPriceValue.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colFundPriceValue.AppearanceCell.Options.UseFont = true;
            this.colFundPriceValue.AppearanceCell.Options.UseTextOptions = true;
            this.colFundPriceValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFundPriceValue.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colFundPriceValue.AppearanceHeader.Options.UseFont = true;
            this.colFundPriceValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colFundPriceValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFundPriceValue.Caption = "سهم صندوق";
            this.colFundPriceValue.DisplayFormat.FormatString = "c0";
            this.colFundPriceValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFundPriceValue.FieldName = "FundPriceValue";
            this.colFundPriceValue.Name = "colFundPriceValue";
            this.colFundPriceValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FundPrice", "ج {0:c0}")});
            this.colFundPriceValue.Width = 42;
            // 
            // colTime
            // 
            this.colTime.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTime.AppearanceCell.Options.UseFont = true;
            this.colTime.AppearanceCell.Options.UseTextOptions = true;
            this.colTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colTime.AppearanceHeader.Options.UseFont = true;
            this.colTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTime.Caption = "زمان";
            this.colTime.FieldName = "Time";
            this.colTime.Name = "colTime";
            this.colTime.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.colTime.Visible = true;
            this.colTime.VisibleIndex = 3;
            this.colTime.Width = 78;
            // 
            // colMoveTime
            // 
            this.colMoveTime.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colMoveTime.AppearanceCell.Options.UseFont = true;
            this.colMoveTime.AppearanceCell.Options.UseTextOptions = true;
            this.colMoveTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMoveTime.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colMoveTime.AppearanceHeader.Options.UseFont = true;
            this.colMoveTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colMoveTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colMoveTime.Caption = "س حرکت";
            this.colMoveTime.DisplayFormat.FormatString = "HH:mm";
            this.colMoveTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colMoveTime.FieldName = "MoveTime";
            this.colMoveTime.Name = "colMoveTime";
            this.colMoveTime.Visible = true;
            this.colMoveTime.VisibleIndex = 12;
            this.colMoveTime.Width = 107;
            // 
            // colSaleType
            // 
            this.colSaleType.FieldName = "SaleType";
            this.colSaleType.Name = "colSaleType";
            // 
            // col1
            // 
            this.col1.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.col1.AppearanceCell.Options.UseFont = true;
            this.col1.AppearanceCell.Options.UseTextOptions = true;
            this.col1.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col1.MaxWidth = 80;
            this.col1.Name = "col1";
            this.col1.Visible = true;
            this.col1.VisibleIndex = 14;
            this.col1.Width = 80;
            // 
            // col2
            // 
            this.col2.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.col2.AppearanceCell.Options.UseFont = true;
            this.col2.AppearanceCell.Options.UseTextOptions = true;
            this.col2.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.col2.MaxWidth = 80;
            this.col2.Name = "col2";
            this.col2.Visible = true;
            this.col2.VisibleIndex = 13;
            this.col2.Width = 80;
            // 
            // ColPayback
            // 
            this.ColPayback.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.ColPayback.AppearanceCell.Options.UseFont = true;
            this.ColPayback.AppearanceCell.Options.UseTextOptions = true;
            this.ColPayback.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColPayback.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ColPayback.AppearanceHeader.Options.UseFont = true;
            this.ColPayback.AppearanceHeader.Options.UseTextOptions = true;
            this.ColPayback.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ColPayback.ColumnEdit = this.ReBtnPayback;
            this.ColPayback.MaxWidth = 60;
            this.ColPayback.MinWidth = 50;
            this.ColPayback.Name = "ColPayback";
            this.ColPayback.Width = 60;
            // 
            // ReBtnPayback
            // 
            this.ReBtnPayback.AutoHeight = false;
            this.ReBtnPayback.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "استرداد", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F9), serializableAppearanceObject1, "", null, null, true)});
            this.ReBtnPayback.Name = "ReBtnPayback";
            this.ReBtnPayback.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.ReBtnPayback.Click += new System.EventHandler(this.ReBtnPayback_Click);
            // 
            // colPrintFactor
            // 
            this.colPrintFactor.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPrintFactor.AppearanceCell.Options.UseFont = true;
            this.colPrintFactor.AppearanceCell.Options.UseTextOptions = true;
            this.colPrintFactor.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrintFactor.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPrintFactor.AppearanceHeader.Options.UseFont = true;
            this.colPrintFactor.AppearanceHeader.Options.UseTextOptions = true;
            this.colPrintFactor.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrintFactor.ColumnEdit = this.reBtnPrintticket;
            this.colPrintFactor.MaxWidth = 68;
            this.colPrintFactor.MinWidth = 50;
            this.colPrintFactor.Name = "colPrintFactor";
            this.colPrintFactor.Width = 50;
            // 
            // reBtnPrintticket
            // 
            this.reBtnPrintticket.AutoHeight = false;
            this.reBtnPrintticket.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "چاپ", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F10), serializableAppearanceObject2, "", null, null, true)});
            this.reBtnPrintticket.Name = "reBtnPrintticket";
            this.reBtnPrintticket.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.reBtnPrintticket.Click += new System.EventHandler(this.reBtnPrintticket_Click);
            // 
            // colPayPose
            // 
            this.colPayPose.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPayPose.AppearanceCell.Options.UseFont = true;
            this.colPayPose.AppearanceCell.Options.UseTextOptions = true;
            this.colPayPose.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPayPose.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPayPose.AppearanceHeader.Options.UseFont = true;
            this.colPayPose.AppearanceHeader.Options.UseTextOptions = true;
            this.colPayPose.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPayPose.ColumnEdit = this.reBtnPayPose;
            this.colPayPose.MaxWidth = 75;
            this.colPayPose.MinWidth = 50;
            this.colPayPose.Name = "colPayPose";
            this.colPayPose.Width = 50;
            // 
            // reBtnPayPose
            // 
            this.reBtnPayPose.AutoHeight = false;
            this.reBtnPayPose.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "پرداخت POS", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F7), serializableAppearanceObject3, "", null, null, true)});
            this.reBtnPayPose.Name = "reBtnPayPose";
            this.reBtnPayPose.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.reBtnPayPose.Click += new System.EventHandler(this.reBtnPayPose_Click);
            // 
            // colPaycredit
            // 
            this.colPaycredit.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPaycredit.AppearanceCell.Options.UseFont = true;
            this.colPaycredit.AppearanceCell.Options.UseTextOptions = true;
            this.colPaycredit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaycredit.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPaycredit.AppearanceHeader.Options.UseFont = true;
            this.colPaycredit.AppearanceHeader.Options.UseTextOptions = true;
            this.colPaycredit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaycredit.ColumnEdit = this.reBtnPayCredit;
            this.colPaycredit.MaxWidth = 88;
            this.colPaycredit.MinWidth = 70;
            this.colPaycredit.Name = "colPaycredit";
            this.colPaycredit.Width = 70;
            // 
            // reBtnPayCredit
            // 
            this.reBtnPayCredit.AutoHeight = false;
            this.reBtnPayCredit.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "پرداخت اعتباری", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.F2), serializableAppearanceObject4, "", null, null, true)});
            this.reBtnPayCredit.Name = "reBtnPayCredit";
            this.reBtnPayCredit.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.reBtnPayCredit.Click += new System.EventHandler(this.reBtnPayCredit_Click);
            // 
            // colCodeCompany
            // 
            this.colCodeCompany.Caption = "CodeCompany";
            this.colCodeCompany.FieldName = "CompanyCode";
            this.colCodeCompany.Name = "colCodeCompany";
            // 
            // colOnlinePrice
            // 
            this.colOnlinePrice.FieldName = "OnlinePrice";
            this.colOnlinePrice.Name = "colOnlinePrice";
            // 
            // colClosed
            // 
            this.colClosed.FieldName = "Closed";
            this.colClosed.Name = "colClosed";
            // 
            // toolTip
            // 
            this.toolTip.AutoPopDelay = 20;
            this.toolTip.CloseOnClick = DevExpress.Utils.DefaultBoolean.True;
            this.toolTip.InitialDelay = 20;
            this.toolTip.KeepWhileHovered = true;
            this.toolTip.ReshowDelay = 10;
            this.toolTip.Rounded = true;
            this.toolTip.RoundRadius = 3;
            this.toolTip.ShowBeak = true;
            this.toolTip.ToolTipLocation = DevExpress.Utils.ToolTipLocation.BottomCenter;
            this.toolTip.GetActiveObjectInfo += new DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTip_GetActiveObjectInfo);
            // 
            // cityBindingSource
            // 
            this.cityBindingSource.DataSource = typeof(Model.City);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "چاپ";
            this.barButtonItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.Glyph")));
            this.barButtonItem3.Id = 2;
            this.barButtonItem3.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.LargeGlyph")));
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // bgpose
            // 
            this.bgpose.WorkerReportsProgress = true;
            this.bgpose.WorkerSupportsCancellation = true;
            this.bgpose.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgpose_DoWork);
            this.bgpose.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgpose_RunWorkerCompleted);
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // chSale
            // 
            this.chSale.Location = new System.Drawing.Point(18, 494);
            this.chSale.MenuManager = this.ribbon;
            this.chSale.Name = "chSale";
            this.chSale.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.chSale.Properties.Appearance.Options.UseFont = true;
            this.chSale.Properties.Caption = "فروخته شده ها";
            this.chSale.Size = new System.Drawing.Size(159, 28);
            this.chSale.StyleController = this.LayoutControl2;
            this.chSale.TabIndex = 8;
            this.chSale.CheckedChanged += new System.EventHandler(this.chSale_CheckedChanged);
            // 
            // LayoutControl2
            // 
            this.LayoutControl2.Controls.Add(this.chRezerv);
            this.LayoutControl2.Controls.Add(this.ChCompany);
            this.LayoutControl2.Controls.Add(this.chAll);
            this.LayoutControl2.Controls.Add(this.chSale);
            this.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl2.Name = "LayoutControl2";
            this.LayoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1001, 355, 250, 350);
            this.LayoutControl2.OptionsView.RightToLeftMirroringApplied = true;
            this.LayoutControl2.Root = this.LayoutControlGroup2;
            this.LayoutControl2.Size = new System.Drawing.Size(195, 540);
            this.LayoutControl2.TabIndex = 0;
            this.LayoutControl2.Text = "LayoutControl2";
            // 
            // chRezerv
            // 
            this.chRezerv.Location = new System.Drawing.Point(18, 462);
            this.chRezerv.MenuManager = this.ribbon;
            this.chRezerv.Name = "chRezerv";
            this.chRezerv.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.chRezerv.Properties.Appearance.Options.UseFont = true;
            this.chRezerv.Properties.Caption = "رزرو شده ها";
            this.chRezerv.Size = new System.Drawing.Size(159, 28);
            this.chRezerv.StyleController = this.LayoutControl2;
            this.chRezerv.TabIndex = 6;
            this.chRezerv.CheckedChanged += new System.EventHandler(this.chRezerv_CheckedChanged);
            // 
            // ChCompany
            // 
            this.ChCompany.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.ChCompany.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ChCompany.Appearance.Options.UseBackColor = true;
            this.ChCompany.Appearance.Options.UseFont = true;
            this.ChCompany.CheckOnClick = true;
            this.ChCompany.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChCompany.DataSource = this.companyBindingSource;
            this.ChCompany.DisplayMember = "Title";
            this.ChCompany.Location = new System.Drawing.Point(18, 37);
            this.ChCompany.MultiColumn = true;
            this.ChCompany.Name = "ChCompany";
            this.ChCompany.Size = new System.Drawing.Size(159, 346);
            this.ChCompany.StyleController = this.LayoutControl2;
            this.ChCompany.TabIndex = 5;
            this.ChCompany.ValueMember = "Code";
            this.ChCompany.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.ChCompany_ItemCheck);
            // 
            // chAll
            // 
            this.chAll.Location = new System.Drawing.Point(18, 430);
            this.chAll.MenuManager = this.ribbon;
            this.chAll.Name = "chAll";
            this.chAll.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.chAll.Properties.Appearance.Options.UseFont = true;
            this.chAll.Properties.Caption = "نمایش همه";
            this.chAll.Size = new System.Drawing.Size(159, 28);
            this.chAll.StyleController = this.LayoutControl2;
            this.chAll.TabIndex = 7;
            this.chAll.CheckedChanged += new System.EventHandler(this.chAll_CheckedChanged);
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup2.GroupBordersVisible = false;
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup4,
            this.LayoutControlGroup6});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup2.Name = "Root";
            this.LayoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.LayoutControlGroup2.Size = new System.Drawing.Size(195, 540);
            this.LayoutControlGroup2.TextVisible = false;
            // 
            // LayoutControlGroup4
            // 
            this.LayoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Size = new System.Drawing.Size(187, 393);
            this.LayoutControlGroup4.Text = "شرکت ها";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ChCompany;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(54, 4);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(163, 350);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // LayoutControlGroup6
            // 
            this.LayoutControlGroup6.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup6.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem1,
            this.layoutControlItem4});
            this.LayoutControlGroup6.Location = new System.Drawing.Point(0, 393);
            this.LayoutControlGroup6.Name = "LayoutControlGroup6";
            this.LayoutControlGroup6.Size = new System.Drawing.Size(187, 139);
            this.LayoutControlGroup6.Text = "وضعیت بلیط ها";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.chAll;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(163, 32);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.chRezerv;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(163, 32);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.chSale;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 64);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(163, 32);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.dgv);
            this.dataLayoutControl1.Controls.Add(this.lblNameDay);
            this.dataLayoutControl1.Controls.Add(this.btnshowReport);
            this.dataLayoutControl1.Controls.Add(this.btnNextDate);
            this.dataLayoutControl1.Controls.Add(this.btnNowDate);
            this.dataLayoutControl1.Controls.Add(this.btnBackDate);
            this.dataLayoutControl1.Controls.Add(this.txtFromDate);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 146);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(556, 470, 250, 350);
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1107, 579);
            this.dataLayoutControl1.TabIndex = 35;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // lblNameDay
            // 
            this.lblNameDay.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.lblNameDay.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.lblNameDay.Location = new System.Drawing.Point(663, 12);
            this.lblNameDay.Name = "lblNameDay";
            this.lblNameDay.Size = new System.Drawing.Size(144, 36);
            this.lblNameDay.StyleController = this.dataLayoutControl1;
            this.lblNameDay.TabIndex = 38;
            this.lblNameDay.Text = "نام روز";
            // 
            // btnshowReport
            // 
            this.btnshowReport.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnshowReport.Appearance.Options.UseFont = true;
            this.btnshowReport.Image = ((System.Drawing.Image)(resources.GetObject("btnshowReport.Image")));
            this.btnshowReport.Location = new System.Drawing.Point(163, 12);
            this.btnshowReport.Name = "btnshowReport";
            this.btnshowReport.Size = new System.Drawing.Size(130, 36);
            this.btnshowReport.StyleController = this.dataLayoutControl1;
            this.btnshowReport.TabIndex = 37;
            this.btnshowReport.Text = "جست و جو F6";
            this.btnshowReport.Click += new System.EventHandler(this.btnshowReport_Click);
            // 
            // btnNextDate
            // 
            this.btnNextDate.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnNextDate.Appearance.Options.UseFont = true;
            this.btnNextDate.Image = ((System.Drawing.Image)(resources.GetObject("btnNextDate.Image")));
            this.btnNextDate.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnNextDate.Location = new System.Drawing.Point(621, 12);
            this.btnNextDate.Name = "btnNextDate";
            this.btnNextDate.Size = new System.Drawing.Size(38, 36);
            this.btnNextDate.StyleController = this.dataLayoutControl1;
            this.btnNextDate.TabIndex = 34;
            this.btnNextDate.Click += new System.EventHandler(this.btnNextDate_Click);
            // 
            // btnNowDate
            // 
            this.btnNowDate.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnNowDate.Appearance.Options.UseFont = true;
            this.btnNowDate.Location = new System.Drawing.Point(297, 12);
            this.btnNowDate.Name = "btnNowDate";
            this.btnNowDate.Size = new System.Drawing.Size(76, 36);
            this.btnNowDate.StyleController = this.dataLayoutControl1;
            this.btnNowDate.TabIndex = 35;
            this.btnNowDate.Text = "امروز";
            this.btnNowDate.Click += new System.EventHandler(this.btnNowDate_Click);
            // 
            // btnBackDate
            // 
            this.btnBackDate.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnBackDate.Appearance.Options.UseFont = true;
            this.btnBackDate.Image = ((System.Drawing.Image)(resources.GetObject("btnBackDate.Image")));
            this.btnBackDate.Location = new System.Drawing.Point(377, 12);
            this.btnBackDate.Name = "btnBackDate";
            this.btnBackDate.Size = new System.Drawing.Size(41, 36);
            this.btnBackDate.StyleController = this.dataLayoutControl1;
            this.btnBackDate.TabIndex = 36;
            this.btnBackDate.Click += new System.EventHandler(this.btnBackDate_Click);
            // 
            // txtFromDate
            // 
            this.txtFromDate.EditValue = null;
            this.txtFromDate.Location = new System.Drawing.Point(422, 12);
            this.txtFromDate.MenuManager = this.ribbon;
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 12F);
            this.txtFromDate.Properties.Appearance.Options.UseFont = true;
            this.txtFromDate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFromDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Size = new System.Drawing.Size(150, 34);
            this.txtFromDate.StyleController = this.dataLayoutControl1;
            this.txtFromDate.TabIndex = 33;
            this.txtFromDate.EditValueChanged += new System.EventHandler(this.txtFromDate_EditValueChanged);
            this.txtFromDate.Click += new System.EventHandler(this.txtFromDate_Click);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem8,
            this.layoutControlItem10,
            this.layoutControlItem7,
            this.layoutControlItem12,
            this.layoutControlItem11,
            this.layoutControlItem9,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1107, 579);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.dgv;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 40);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(104, 24);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(1087, 519);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lblNameDay;
            this.layoutControlItem8.Location = new System.Drawing.Point(651, 0);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(40, 28);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(148, 40);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnNextDate;
            this.layoutControlItem10.Location = new System.Drawing.Point(609, 0);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(42, 40);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.txtFromDate;
            this.layoutControlItem7.Location = new System.Drawing.Point(410, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(199, 40);
            this.layoutControlItem7.Text = " تاریخ :";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(40, 24);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.btnBackDate;
            this.layoutControlItem12.Location = new System.Drawing.Point(365, 0);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(45, 40);
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnNowDate;
            this.layoutControlItem11.Location = new System.Drawing.Point(285, 0);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(40, 35);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(80, 40);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnshowReport;
            this.layoutControlItem9.Location = new System.Drawing.Point(151, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(134, 40);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(799, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(288, 40);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(151, 40);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // navigationPane1
            // 
            this.navigationPane1.Controls.Add(this.navigationPage1);
            this.navigationPane1.Dock = System.Windows.Forms.DockStyle.Right;
            this.navigationPane1.Location = new System.Drawing.Point(1107, 146);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.PageProperties.AppearanceCaption.Options.UseTextOptions = true;
            this.navigationPane1.PageProperties.AppearanceCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.navigationPane1.PageProperties.ShowExpandButton = false;
            this.navigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.Image;
            this.navigationPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.navigationPage1});
            this.navigationPane1.RegularSize = new System.Drawing.Size(265, 600);
            this.navigationPane1.SelectedPage = this.navigationPage1;
            this.navigationPane1.Size = new System.Drawing.Size(265, 600);
            this.navigationPane1.TabIndex = 39;
            this.navigationPane1.Text = "navigationPane1";
            // 
            // navigationPage1
            // 
            this.navigationPage1.Caption = "جستجو پیشرفته";
            this.navigationPage1.Controls.Add(this.LayoutControl2);
            this.navigationPage1.Image = ((System.Drawing.Image)(resources.GetObject("navigationPage1.Image")));
            this.navigationPage1.ImageUri.Uri = "Zoom";
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(195, 540);
            // 
            // FrmTicketList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1372, 746);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.navigationPane1);
            this.Controls.Add(this.ribbon);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "FrmTicketList";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "بلیط ها";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmReserve_FormClosing);
            this.Load += new System.EventHandler(this.FrmTicketList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketLocalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ReBtnPayback)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPrintticket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPayPose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reBtnPayCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chSale.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).EndInit();
            this.LayoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chRezerv.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).EndInit();
            this.navigationPane1.ResumeLayout(false);
            this.navigationPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem btnReport;
        private DevExpress.XtraGrid.GridControl dgv;
        private DevExpress.XtraGrid.Views.Grid.GridView grd;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colTripID;
        private DevExpress.XtraGrid.Columns.GridColumn colMan;
        private DevExpress.XtraGrid.Columns.GridColumn colFullname;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Columns.GridColumn colChairsName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colSaleDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNumChairs;
        private DevExpress.XtraGrid.Columns.GridColumn colSumDiscount;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarButtonItem Btn_Del;
        private DevExpress.XtraGrid.Columns.GridColumn ColPayback;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ReBtnPayback;
        private System.Windows.Forms.BindingSource ticketLocalBindingSource;
        private FarsiLibrary.Win.DevExpress.RepositoryItemXtraFADateEdit repositoryItemXtraFADateEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colStateticket;
        private DevExpress.XtraGrid.Columns.GridColumn colCityID;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private DevExpress.XtraGrid.Columns.GridColumn colCashierName;
        private DevExpress.XtraGrid.Columns.GridColumn colPrintFactor;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reBtnPrintticket;
        private DevExpress.XtraGrid.Columns.GridColumn colPayPose;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reBtnPayPose;
        private DevExpress.XtraGrid.Columns.GridColumn colPaycredit;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit reBtnPayCredit;
        private DevExpress.XtraBars.BarButtonItem BtnbackSale;
        private System.ComponentModel.BackgroundWorker bgpose;
        private DevExpress.XtraBars.BarButtonItem btnCancelPose;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit reCompany;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnUserID;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusPaidBack;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidBackDate;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTicket;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeSale;
        private DevExpress.XtraGrid.Columns.GridColumn col1;
        private DevExpress.XtraGrid.Columns.GridColumn col2;
        private DevExpress.XtraGrid.Columns.GridColumn colCodeCompany;
        private DevExpress.XtraBars.BarEditItem barCreditUser;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidBackTime;
        private DevExpress.XtraGrid.Columns.GridColumn colFundPriceValue;
        internal DevExpress.XtraEditors.CheckedListBoxControl ChCompany;
        private DevExpress.XtraEditors.CheckEdit chSale;
        private DevExpress.XtraEditors.CheckEdit chAll;
        private DevExpress.XtraEditors.CheckEdit chRezerv;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private FarsiLibrary.Win.DevExpress.XtraFADateEdit txtFromDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraEditors.LabelControl lblNameDay;
        private DevExpress.XtraEditors.SimpleButton btnshowReport;
        private DevExpress.XtraEditors.SimpleButton btnNextDate;
        private DevExpress.XtraEditors.SimpleButton btnNowDate;
        private DevExpress.XtraEditors.SimpleButton btnBackDate;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.Navigation.NavigationPane navigationPane1;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        internal DevExpress.XtraLayout.LayoutControl LayoutControl2;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private System.Windows.Forms.BindingSource cityBindingSource;
        private DevExpress.Utils.ToolTipController toolTip;
        private DevExpress.XtraGrid.Columns.GridColumn colMoveTime;
        private DevExpress.XtraGrid.Columns.GridColumn colTime;
        private DevExpress.XtraGrid.Columns.GridColumn colOnlinePrice;
        private DevExpress.XtraGrid.Columns.GridColumn colClosed;
        private DevExpress.XtraBars.BarButtonItem btnLoadRefresh;
        private DevExpress.XtraGrid.Columns.GridColumn colSaleType;
        private DevExpress.XtraGrid.Columns.GridColumn colCityName;
    }
}