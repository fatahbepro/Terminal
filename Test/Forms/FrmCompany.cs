﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Model;
using BL;
using DevExpress.XtraEditors;
using System.Reflection;

namespace Test
{
    public partial class frmticketCompany : DevExpress.XtraBars.Ribbon.RibbonForm
    {





        public frmticketCompany()
        {
            InitializeComponent();

            GetList();
            Refresh_frmticket();
        }



        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    Btnclose_ItemClick(null, null);
                    break;
                case Keys.F2:
                    BtnSave_ItemClick(null, null);
                    break;
                case Keys.F3:
                    BtnNew_ItemClick(null, null);
                    break;
                case Keys.F4:
                    Btnclose_ItemClick(null, null);
                    break;
                case Keys.F5:
                    BtnRefresh_ItemClick(null, null);
                    break;
                case Keys.Delete:
                    BtnDelete_ItemClick(null, null);
                    break;

            }

            return base.ProcessCmdKey(ref msg, keyData);
        }


        bool EditValue;

        void GetList()
        {
            try
            {
                gridControl1.DataSource = CompanysServices.GetAll(true);

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        /// <summary>
        /// خالی کردن فرم
        /// </summary>
        void Refresh_frmticket()
        {
            try
            {
                EditValue = false;

                CodeTextEdit.EditValue = string.Empty;
                TitleTextEdit.EditValue = string.Empty;
                ConnectionStringTextEdit.EditValue = string.Empty;
                EnabledCheckEdit.EditValue = false;
                companyBindingSource.DataSource = new Company();

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        // ثبت تعاونی
        private void BtnSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (!dxValidationProvider1.Validate())
                    return;

                companyBindingSource.EndEdit();
                Company company = (Company)companyBindingSource.Current;
                if (!EditValue)
                {
                    Company search = CompanysServices.FindByCode(company.Code);
                    if (search != null)
                    {
                        XtraMessageBox.Show("تعاونی با این کد وجود دارد", "پیام", MessageBoxButtons.OK);
                        return;
                    }
                }

                ImageConverter _imageConverter = new ImageConverter();
                byte[] xByte = (byte[])_imageConverter.ConvertTo(LogoPictureEdit.Image, typeof(byte[]));
                company.Logo = xByte;
                int Resut = 0;
                if (EditValue)
                {
                    Resut = CompanysServices.Update(company);
                }
                else
                {
                    Resut = CompanysServices.Insert(company);
                }


                if (Resut == 1)
                {
                    XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GetList();
                    Refresh_frmticket();
                }
                else
                {
                    XtraMessageBox.Show("اشکال در اجرای عملیات", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }

        }

        private void BtnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            GetList();
        }

        // حذف تعاونی
        private void BtnDelete_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                if (XtraMessageBox.Show("آیا مایل به حذف شرکت می باشید؟", "پیام", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {

                    int id = (int)grd.GetFocusedRowCellValue(colID);
                    Company co = CompanysServices.FindByID(id);
                    co.Enabled = false;
                    int Resut = CompanysServices.Update(co);
                    if (Resut == 1)
                    {
                        Refresh_frmticket();
                        GetList();
                        XtraMessageBox.Show("عملیات با موفقیت انجام شد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    else
                    {
                        XtraMessageBox.Show("اشکال در اجرای عملیات", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }
                }


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        private void Btnclose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void grd_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {

        }

        private void BtnNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            Refresh_frmticket();
        }

        private void EnabledCheckEdit_Validated(object sender, EventArgs e)
        {
            dxValidationProvider1.Validate();
        }


        // نمایش اطلاعات تعاونی در کادر مورد نظر
        private void grd_DoubleClick(object sender, EventArgs e)
        {

            try
            {
                int id = (int)grd.GetFocusedRowCellValue(colID);
                companyBindingSource.DataSource = CompanysServices.FindByID(id);
                EditValue = true;


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }


        }

        private void frmticketCompany_FormClosing(object sender, FormClosingEventArgs e)
        {
            EditValue = false;
            e.Cancel = true;
            this.Hide();
        }

        private DevExpress.XtraEditors.Controls.PictureMenu GetMenu(DevExpress.XtraEditors.PictureEdit edit)
        {
            PropertyInfo pi = typeof(DevExpress.XtraEditors.PictureEdit).GetProperty("Menu", BindingFlags.NonPublic | BindingFlags.Instance);
            if (pi != null)
                return pi.GetValue(edit, null) as DevExpress.XtraEditors.Controls.PictureMenu;
            return null;
        }
        private void InvokeMenuMethod(DevExpress.XtraEditors.Controls.PictureMenu menu, String name)
        {
            MethodInfo mi = typeof(DevExpress.XtraEditors.Controls.PictureMenu).GetMethod(name, BindingFlags.NonPublic | BindingFlags.Instance);
            if (mi != null && menu != null)
                mi.Invoke(menu, new object[] { menu, new EventArgs() });
        }
        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void BtnBrowsImg_Click(object sender, EventArgs e)
        {
            InvokeMenuMethod(GetMenu(LogoPictureEdit), "OnClickedLoad");
        }
    }
}