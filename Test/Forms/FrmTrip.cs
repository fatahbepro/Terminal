﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Model;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Columns;
using BL;
using System.Threading.Tasks;
using System.Threading;
using System.Data;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;


namespace Test
{
    public partial class FrmTrips : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public FrmTrips()
        {
            InitializeComponent();
            ribbon.Minimized = true;
            LoadData();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Escape:
                    barButtonItem1_ItemClick(null, null);
                    break;

                case Keys.F6:
                    barButtonItem2_ItemClick(null, null);
                    break;
                case Keys.F4:
                    barButtonItem1_ItemClick(null, null);
                    break;
                case Keys.F5:
                    btnRefresh_ItemClick(null, null);
                    break;

                case Keys.F9:
                    ShowChairTrip();
                    break;

                case Keys.F10:
                    ShowTicketService();
                    break;

            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        /// <summary>
        /// لیست سرویس ها
        /// </summary>
        List<GetTripsReport> TripsReportList = new List<GetTripsReport>();


        List<Company> AccessCompanyUser;
        /// <summary>
        /// لیست مسیرهای سفرها
        /// </summary>
        List<PathTrip> listPath;
        /// <summary>
        /// نمایش لیست سفرها
        /// </summary>
        void LoadData()
        {
            try
            {

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                txtFromDate.DateTime = DateTime.Now;
                if (Userlogin.AccessCompanies == null)
                {
                    XtraMessageBox.Show("کاربر گرامی سطح دسترسی شما تعریف نشده است", "پیام", MessageBoxButtons.OK);
                    return;
                }
                AccessCompanyUser = CompanysServices.GetCompaniesByListCode(Userlogin.AccessCompanies); //  لیست شرکت های سطح دسترسی
                ChCompany.DataSource = AccessCompanyUser;
                barCreditUser.EditValue = Userlogin.Credit;
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }

        }
        /// <summary>
        /// تابع گزارش لیست سفرها
        /// </summary>
        /// <param name="fromdate">تاریخ شروع</param>
        async void GetListTrip(DateTime fromdate)
        {
            try
            {
                if (AccessCompanyUser == null)
                {
                    XtraMessageBox.Show("کاربر گرامی سطح دسترسی شما تعریف نشده است", "پیام", MessageBoxButtons.OK);
                    return;
                }

                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                lblNameDay.Text = fromdate.GetNameOfDayDate();
                string Overtime = SettingLogin.GetValue("ServiceOvertime");
                int ServiceOvertime = 0;
                if (!string.IsNullOrEmpty(Overtime))
                    ServiceOvertime = int.Parse(Overtime);
                //List<GetTripsReport> listBefore = new List<GetTripsReport>();
                listPath = new List<PathTrip>();
                List<Sp_GetCitiesTrip> listCities = new List<Sp_GetCitiesTrip>();
                DateTime _from = DateTime.Parse(fromdate.ToShortDateString());
                DateTime _todate = _from;
                _todate = _todate.AddHours(23).AddMinutes(59).AddSeconds(59);

                await Task.Run(() =>
                            {
                                TripsReportList = TripServices.GetTripsDateEF(_from, _todate, out listPath, AccessCompanyUser, out listCities, SettingLogin.GetValue("ServiceOvertime").ToInt(),Userlogin.IsLoacConnection);
                            });

                if (Convert.ToDateTime(fromdate.ToShortDateString()) < Convert.ToDateTime(DateTime.Now.ToShortDateString()))
                {
                    DateTime filterTime = Convert.ToDateTime(DateTime.Now.AddMinutes(-ServiceOvertime).ToString("HH:mm"));
                    TripsReportList.RemoveAll(c => Convert.ToDateTime(c.DepartureTime2) < filterTime);
                }
                int indexrow = grd.FocusedRowHandle;

                gridControl1.DataSource = TripsReportList.OrderBy(c => c.Date);
                grd.RefreshData();
                grd.FocusedRowHandle = indexrow;

                ChPath.DataSource = listPath;   // نمایش فقط مسیر های لیست فیلتر شده
                barCreditUser.EditValue = Userlogin.Credit;
                //listCities = listCities.GroupBy(i => i.ID).Select(y => y.FirstOrDefault()).ToList();
                cmbCities.Properties.DataSource = listCities;
                grd.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
               new DevExpress.XtraGrid.Columns.GridColumnSortInfo(colDateSH, DevExpress.Data.ColumnSortOrder.Ascending)          });
            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }
            finally
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
        }
   
        /// <summary>
        /// ارجاع به عملیات مورد نظر فروش و لیست بلیط ها
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void grd_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            try
            {
                if (AccessCompanyUser == null)
                {
                    XtraMessageBox.Show("کاربر گرامی سطح دسترسی شما تعریف نشده است", "پیام", MessageBoxButtons.OK);
                    return;
                }

                if (e.Column == colSale)
                {
                    string Overtime = SettingLogin.GetValue("ServiceOvertime");
                    int ServiceOvertime = 0;
                    if (!string.IsNullOrEmpty(Overtime))
                        ServiceOvertime = int.Parse(Overtime);
                    DateTime timeservice = Convert.ToDateTime(grd.GetFocusedRowCellValue(colDepartureTime2));
                    DateTime DateTicket = Convert.ToDateTime(Convert.ToDateTime(grd.GetFocusedRowCellValue(colDateSH)).ToString("yyyy/MM/dd"));
                    timeservice = timeservice.AddMinutes(ServiceOvertime);
                    if (TimeSpan.Parse(timeservice.ToString("HH:mm")) <= TimeSpan.Parse(DateTime.Now.ToString("HH:mm")) && DateTicket < Convert.ToDateTime(DateTime.Now.ToString("yyyy/MM/dd")))
                    {
                        XtraMessageBox.Show("کاربر گرامی ساعت حرکت سرویس گذشته است و امکان فروش بلیط امکان پذیر نمی باشد", "پیام", MessageBoxButtons.OK);
                        return;
                    }
                    //  فروش
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                    ShowChairTrip();
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                }
                if (e.Column == colTicket)
                {
                    // بلیط ها
                    ShowTicketService();
                }

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }

        }


        /// <summary>
        /// تابع ارجاع به فرم فروش و فروش بلیط
        /// </summary>
        void ShowChairTrip()
        {
            try
            {
                string companycode = grd.GetFocusedRowCellValue(colCompanyCode).ToString();
                string TripID = grd.GetFocusedRowCellValue(colTripID).ToString();
                Company conCompany = new Company();
                DateTime MoveDate = Convert.ToDateTime(grd.GetFocusedRowCellValue(colDateSH).ToString()).AddHours(-12);
                DateTime MoveTime = Convert.ToDateTime(grd.GetFocusedRowCellValue(colDepartureTime2));
                MoveDate = MoveDate.AddHours(MoveTime.Hour).AddMinutes(MoveTime.Minute).AddSeconds(0);

                foreach (Company com in AccessCompanyUser)
                {
                    if (com.Code == companycode)
                    {
                        conCompany = com;
                    }
                }
                string ConnectionCompany = string.Empty;
                if (Userlogin.IsLoacConnection)
                    ConnectionCompany = conCompany.LocalConnectionString;
                else
                    ConnectionCompany = conCompany.ConnectionString;
                Trip _trip = TripServices.FindByID(int.Parse(TripID), ConnectionCompany);
                if (_trip.Locked == true)
                {
                    XtraMessageBox.Show("کاربر گرامی سرویس مورد نظر قفل می باشد", "پیام", MessageBoxButtons.OK);
                    return;
                }
                int idservice = (int)grd.GetFocusedRowCellValue(colServiceID);
                string _way = grd.GetFocusedRowCellValue(colPathTitle).ToString();
                int _layoutId = (int)grd.GetFocusedRowCellValue(colLayoutID);
                string companyTitle = grd.GetFocusedRowCellValue(colCompanyName).ToString();
                string serviceName = grd.GetFocusedRowCellValue(colCarTypeTitle).ToString();
                string idp = grd.GetFocusedRowCellValue(colPathTitle).ToString();

                  var infoTrip = TripsReportList.Where(c => c.TripID == TripID.ToInt() && c.CompanyCode == conCompany.Code ).SingleOrDefault();

                City city = CityService.FindByID(infoTrip.IDCity2, ConnectionCompany);
                string citycode = "0";
                if (city != null)
                    citycode = city.Code;
                FrmTickets fr = new FrmTickets();
                fr.Refresh_frmticket();
                fr.CodeTrip = TripID;
                fr.TripWay = _way;
                fr.DayTrip = grd.GetFocusedRowCellValue(colDay).ToString();
                fr.DateTripPersian = grd.GetFocusedRowCellDisplayText(colDateSH).ToString();
                fr.TimeTrip = grd.GetFocusedRowCellValue(colDepartureTime2).ToString();
                fr.TypeTrip = serviceName;
                fr.Logo = conCompany.Logo;
                fr.MoveTimeDate = MoveDate;
                var numtickets = grd.GetFocusedRowCellValue(colNumTickets);
                if (numtickets == null)
                    numtickets = 0;
                fr.FullChair = numtickets.ToInt();
                var chairrezerv = grd.GetFocusedRowCellValue(colNumReserves);
                if (chairrezerv == null)
                    chairrezerv = 0;
                fr.RezervChair = chairrezerv.ToInt();
                fr.ConnectionCompany = ConnectionCompany;
                fr.CodeCompany = companycode;
                fr.CompanyName = companyTitle;
                fr.ServicesID = idservice;
                fr.LayoutID = _layoutId;
                fr.IDEndCity = infoTrip.IDCity2;
                fr.CodeEndCity = citycode;
                fr.ShowEdit();
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                fr.ShowDialog();
                GetListTrip(txtFromDate.DateTime);

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }

        }


        /// <summary>
        /// تابع مشاهده بلیط های یک سرویس
        /// </summary>
        void ShowTicketService()
        {
            try
            {

                string companycode = grd.GetFocusedRowCellValue(colCompanyCode).ToString();
                string TripID = grd.GetFocusedRowCellValue(colTripID).ToString();
                Company conCompany = new Company();

                foreach (Company com in AccessCompanyUser)
                {
                    if (com.Code == companycode)
                    {
                        conCompany = com;
                        break;
                    }
                }


                string ConnectionCompany = string.Empty;

                if (Userlogin.IsLoacConnection)
                    ConnectionCompany = conCompany.LocalConnectionString;
                else
                    ConnectionCompany = conCompany.ConnectionString;



                Trip _trip = TripServices.FindByID(int.Parse(TripID), ConnectionCompany);
                string _way = grd.GetFocusedRowCellValue(colPathTitle).ToString();
                string serviceName = grd.GetFocusedRowCellValue(colCarTypeTitle).ToString();

                DateTime MoveDate = Convert.ToDateTime(grd.GetFocusedRowCellValue(colDateSH).ToString()).AddHours(-12);
                DateTime MoveTime = Convert.ToDateTime(grd.GetFocusedRowCellValue(colDepartureTime2));
                MoveDate = MoveDate.AddHours(MoveTime.Hour).AddMinutes(MoveTime.Minute).AddSeconds(0);
                if (_trip.Locked == true)
                {
                    XtraMessageBox.Show("کاربر گرامی سرویس مورد نظر قفل می باشد", "پیام", MessageBoxButtons.OK);
                    return;
                }


                int idservice = (int)grd.GetFocusedRowCellValue(colServiceID);


                Form frmticket = Application.OpenForms["FrmReserve"];
                if (frmticket == null)
                {
                    FrmTicketsTrips fr = new FrmTicketsTrips();
                    fr.TripID = int.Parse(TripID);
                    fr.CodeCompany = conCompany.Code;
                    fr.ConnectionCompany = ConnectionCompany;
                    fr.TitleCompany = conCompany.Title;
                    fr.Logo = conCompany.Logo;
                    fr.Owner = this;
                    fr.IDService = idservice;
                    fr.CodeTrip = TripID;
                    fr.TripWay = _way;
                    fr.DayTrip = grd.GetFocusedRowCellValue(colDay).ToString();
                    fr.DateTripPersian = grd.GetFocusedRowCellDisplayText(colDateSH).ToString();
                    fr.TimeTrip = grd.GetFocusedRowCellValue(colDepartureTime2).ToString();
                    fr.TypeTrip = serviceName;
                    fr.MoveTime = MoveTime;
                    fr.LoadList();
                    fr.ShowDialog(this);

                    GetListTrip(txtFromDate.DateTime);
                    //fr.Show();
                    //fr.BringToFront();
                }
                else
                {
                    FrmTicketsTrips fm = (FrmTicketsTrips)frmticket;
                    fm.TripID = int.Parse(TripID);
                    fm.CodeCompany = conCompany.Code;
                    fm.ConnectionCompany = ConnectionCompany;
                    fm.TitleCompany = conCompany.Title;
                    fm.Logo = conCompany.Logo;
                    fm.Owner = this;
                    fm.MoveTime = MoveTime;

                    fm.IDService = idservice;
                    fm.CodeTrip = TripID;
                    fm.TripWay = _way;
                    fm.DayTrip = grd.GetFocusedRowCellValue(colDay).ToString();
                    fm.DateTripPersian = grd.GetFocusedRowCellDisplayText(colDateSH).ToString();
                    fm.TimeTrip = grd.GetFocusedRowCellValue(colDepartureTime2).ToString();
                    fm.TypeTrip = serviceName;

                    fm.LoadList();
                    fm.ShowDialog(this);

                    GetListTrip(txtFromDate.DateTime);

                    //fm.Show();
                    //fm.BringToFront();
                }


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }

        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void grd_CustomDrawRowIndicator(object sender, DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventArgs e)
        {

            if (e.RowHandle >= 0)
                e.Info.DisplayText = (e.RowHandle + 1).ToString();


        }

        private string GetFilterStringBindedColumns(string column, CheckedListBoxControl CheckListBox)
        {

            CheckedListBoxControl clbc = (CheckedListBoxControl)CheckListBox;
            string str = string.Empty;

            for (int i = 0; i <= clbc.CheckedItems.Count - 1; i++)
            {
                if (str != string.Empty)
                {
                    str = str + "OR";
                }
                str = str + "[" + column + "] = '" + clbc.CheckedItems[i].ToString() + "' ";
            }

            return str;

        }

        // مشاهده سرویس های تعاونی انتخاب شده

        private void ChCompany_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            try
            {

                string str = GetFilterStringBindedColumns("CompanyCode", ChCompany);
                grd.Columns["CompanyCode"].FilterInfo = new ColumnFilterInfo(str);

            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }
        }


        // نمایش لیست مسیرها
        private void ChPath_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            try
            {
                string str = GetFilterStringBindedColumns("PathTitle", ChPath);
                grd.Columns["PathTitle"].FilterInfo = new ColumnFilterInfo(str);

            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            GetListTrip(txtFromDate.DateTime);
        }

        private void frmticketTrip_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Hide();
        }

        private void txtFromDate_EditValueChanged(object sender, EventArgs e)
        {
            if (txtFromDate.DateTime.ToShortDateString() != DateTime.Now.ToShortDateString())
                txtFromDate.BackColor = Color.Yellow;
            else
                txtFromDate.BackColor = Color.White;
            lblNameDay.Text = txtFromDate.DateTime.GetNameOfDayDate();

        }

        private void btnNowDate_Click(object sender, EventArgs e)
        {
            txtFromDate.DateTime = DateTime.Now;
            GetListTrip(txtFromDate.DateTime);
        }

        private void btnNextDate_Click(object sender, EventArgs e)
        {
            txtFromDate.DateTime = txtFromDate.DateTime.AddDays(-1);
            lblNameDay.Text = txtFromDate.DateTime.GetNameOfDayDate();

        }

        private void btnBackDate_Click(object sender, EventArgs e)
        {

            txtFromDate.DateTime = txtFromDate.DateTime.AddDays(1);
            lblNameDay.Text = txtFromDate.DateTime.GetNameOfDayDate();
        }

        private void btnshowReport_Click(object sender, EventArgs e)
        {
            GetListTrip(txtFromDate.DateTime);
        }

        private void frmticketTrip_Shown(object sender, EventArgs e)
        {

            //splashScreenManager1.ShowWaitForm();
        }

        private void txtFromDate_Click(object sender, EventArgs e)
        {
            txtFromDate.ShowPopup();
        }

        private void cmbCities_EditValueChanged(object sender, EventArgs e)
        {
            grd.ClearColumnsFilter();
            grd.ActiveFilterString = "CitiesBetween like '%" + cmbCities.Text + "%' ";

        }

        private void BtnTicketTripSelected_Click(object sender, EventArgs e)
        {
            try
            {
                List<Company> SelectedCompanys = new List<Company>();
                if (ChCompany.CheckedItems.Count > 0)
                {
                    foreach (Company _company in ChCompany.CheckedItems)
                    {
                        SelectedCompanys.Add(_company);
                    }
                }
                else
                {
                    SelectedCompanys = AccessCompanyUser;
                }
                if (SelectedCompanys.Count > 0)
                {
                    ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
                    Form frmticket = Application.OpenForms["FrmTicketList"];
                    FrmTicketList fr = new FrmTicketList();
                    fr.Owner = this;
                    fr.AccessCompanyUser = SelectedCompanys;
                    fr.LoadData();
                    fr.LoadListTicket(txtFromDate.DateTime);
                    fr.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }

        }

        #region تغییر رنگ سطر هنگام جابه جایی موس یا هاور موس روی سطره های گرید

        int hotRowHandle = -1;
        private void grd_MouseMove(object sender, MouseEventArgs e)
        {
            GridView view = sender as GridView;
            DevExpress.XtraGrid.Views.Grid.ViewInfo.GridHitInfo hi = view.CalcHitInfo(e.Location);
            if (hi.InRow)
            {
                int prevHandle = hotRowHandle;
                hotRowHandle = hi.RowHandle;
                view.RefreshRow(hotRowHandle);
                view.RefreshRow(prevHandle);
            }
            else
            {
                if (hotRowHandle != -1)
                {
                    int prevHandle = hotRowHandle;
                    hotRowHandle = -1;
                    view.RefreshRow(prevHandle);
                }
            }
        }
        private void grd_RowStyle(object sender, RowStyleEventArgs e)
        {
            // e.HighPriority = true;
            if (e.RowHandle == hotRowHandle)
            {
                e.Appearance.BackColor = Color.DodgerBlue;
                e.Appearance.ForeColor = Color.White;
                return;
            }

        }


        #endregion

        private void btnClearSearchTrip_Click(object sender, EventArgs e)
        {
            grd.ClearColumnsFilter();
        }

        private void frmticketTrip_Load(object sender, EventArgs e)
        {
            GetListTrip(DateTime.Now);
        }
    }
}