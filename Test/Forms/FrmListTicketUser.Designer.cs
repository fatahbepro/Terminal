﻿namespace Test
{
    partial class frmticketListTicketUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmticketListTicketUser));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.btnReport = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.BtnTicketInternet = new DevExpress.XtraBars.BarButtonItem();
            this.btnPayPose = new DevExpress.XtraBars.BarButtonItem();
            this.btnPayCredit = new DevExpress.XtraBars.BarButtonItem();
            this.btnshowAll = new DevExpress.XtraBars.BarButtonItem();
            this.btnStatePaidBack = new DevExpress.XtraBars.BarButtonItem();
            this.btnCredit_SaleBack = new DevExpress.XtraBars.BarButtonItem();
            this.btnRemoveFilter = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.navigationPane1 = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.LayoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.btnShowAllTime = new DevExpress.XtraEditors.SimpleButton();
            this.SearchControl1 = new DevExpress.XtraEditors.SearchControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.ticketLocalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reConpany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.companyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.colSaleDateSH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemXtraFADateEdit1 = new FarsiLibrary.Win.DevExpress.RepositoryItemXtraFADateEdit();
            this.colTimeSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCityName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumChairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTicket = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFundPriceValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPricePaidBack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidBackDateSH = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidBackDateTime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colCashierName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeTicket = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colStatusPaidBack = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserCredit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReturnUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colReturnUserCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTripID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChairsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidBackPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCashierCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnShowAllUser = new DevExpress.XtraEditors.SimpleButton();
            this.txttimeTo = new DevExpress.XtraEditors.TimeEdit();
            this.txtTimeFrom = new DevExpress.XtraEditors.TimeEdit();
            this.ChCompany = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.txtTodate = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.txtFromDate = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.cmbUsers = new DevExpress.XtraEditors.LookUpEdit();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layUsers = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.txtPriceRollBack = new DevExpress.XtraEditors.TextEdit();
            this.txtFundPrice = new DevExpress.XtraEditors.TextEdit();
            this.txtSum = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Test.WaitForm1), true, true);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).BeginInit();
            this.navigationPane1.SuspendLayout();
            this.navigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).BeginInit();
            this.LayoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SearchControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketLocalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reConpany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttimeTo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUsers.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layUsers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPriceRollBack.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFundPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSum.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnRefresh,
            this.barButtonItem2,
            this.btnClose,
            this.btnReport,
            this.btnPrint,
            this.BtnTicketInternet,
            this.btnPayPose,
            this.btnPayCredit,
            this.btnshowAll,
            this.btnStatePaidBack,
            this.btnCredit_SaleBack,
            this.btnRemoveFilter});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 13;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1372, 147);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            this.ribbon.Click += new System.EventHandler(this.ribbon_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "بروز رسانی تعاونی ها";
            this.btnRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Glyph")));
            this.btnRefresh.Id = 1;
            this.btnRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btnRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.LargeGlyph")));
            this.btnRefresh.Name = "btnRefresh";
            toolTipTitleItem1.Text = "تعاونی ها";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "بروز رسانی لیست تعاونی های مورد نظر";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            this.btnRefresh.SuperTip = superToolTip1;
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // btnClose
            // 
            this.btnClose.Caption = "بستن";
            this.btnClose.Glyph = ((System.Drawing.Image)(resources.GetObject("btnClose.Glyph")));
            this.btnClose.Id = 3;
            this.btnClose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnClose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnClose.LargeGlyph")));
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // btnReport
            // 
            this.btnReport.Caption = "بلیط های صندوقدار";
            this.btnReport.Glyph = ((System.Drawing.Image)(resources.GetObject("btnReport.Glyph")));
            this.btnReport.Id = 4;
            this.btnReport.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6);
            this.btnReport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnReport.LargeGlyph")));
            this.btnReport.Name = "btnReport";
            this.btnReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReport_ItemClick);
            // 
            // btnPrint
            // 
            this.btnPrint.Caption = "چاپ گزارش";
            this.btnPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPrint.Glyph")));
            this.btnPrint.Id = 5;
            this.btnPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.btnPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnPrint.LargeGlyph")));
            this.btnPrint.Name = "btnPrint";
            toolTipTitleItem2.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem2.Appearance.Options.UseImage = true;
            toolTipTitleItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem2.Image")));
            toolTipTitleItem2.Text = "چاپ";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "چاپ گزارش با کلید میانبر \r\n";
            toolTipTitleItem3.LeftIndent = 6;
            toolTipTitleItem3.Text = "CTRL+P";
            superToolTip2.Items.Add(toolTipTitleItem2);
            superToolTip2.Items.Add(toolTipItem2);
            superToolTip2.Items.Add(toolTipSeparatorItem1);
            superToolTip2.Items.Add(toolTipTitleItem3);
            this.btnPrint.SuperTip = superToolTip2;
            this.btnPrint.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPrint_ItemClick);
            // 
            // BtnTicketInternet
            // 
            this.BtnTicketInternet.Caption = "بلیط های اینترنتی";
            this.BtnTicketInternet.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnTicketInternet.Glyph")));
            this.BtnTicketInternet.Id = 6;
            this.BtnTicketInternet.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnTicketInternet.LargeGlyph")));
            this.BtnTicketInternet.Name = "BtnTicketInternet";
            this.BtnTicketInternet.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnTicketInternet_ItemClick);
            // 
            // btnPayPose
            // 
            this.btnPayPose.Caption = "پرداخت های کارتخوان";
            this.btnPayPose.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPayPose.Glyph")));
            this.btnPayPose.Id = 7;
            this.btnPayPose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnPayPose.LargeGlyph")));
            this.btnPayPose.Name = "btnPayPose";
            this.btnPayPose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPayPose_ItemClick);
            // 
            // btnPayCredit
            // 
            this.btnPayCredit.Caption = "پرداخت های اعتباری";
            this.btnPayCredit.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPayCredit.Glyph")));
            this.btnPayCredit.Id = 8;
            this.btnPayCredit.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnPayCredit.LargeGlyph")));
            this.btnPayCredit.Name = "btnPayCredit";
            this.btnPayCredit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPayCredit_ItemClick);
            // 
            // btnshowAll
            // 
            this.btnshowAll.Caption = "نمایش کلیه پرداخت ها";
            this.btnshowAll.Glyph = ((System.Drawing.Image)(resources.GetObject("btnshowAll.Glyph")));
            this.btnshowAll.Id = 9;
            this.btnshowAll.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnshowAll.LargeGlyph")));
            this.btnshowAll.Name = "btnshowAll";
            this.btnshowAll.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnshowAll_ItemClick);
            // 
            // btnStatePaidBack
            // 
            this.btnStatePaidBack.Caption = "برگشتی ها";
            this.btnStatePaidBack.Glyph = ((System.Drawing.Image)(resources.GetObject("btnStatePaidBack.Glyph")));
            this.btnStatePaidBack.Id = 10;
            this.btnStatePaidBack.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnStatePaidBack.LargeGlyph")));
            this.btnStatePaidBack.Name = "btnStatePaidBack";
            this.btnStatePaidBack.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnStatePaidBack_ItemClick);
            // 
            // btnCredit_SaleBack
            // 
            this.btnCredit_SaleBack.Caption = "فروش اعتباری و برگشتی";
            this.btnCredit_SaleBack.Glyph = ((System.Drawing.Image)(resources.GetObject("btnCredit_SaleBack.Glyph")));
            this.btnCredit_SaleBack.Id = 11;
            this.btnCredit_SaleBack.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnCredit_SaleBack.LargeGlyph")));
            this.btnCredit_SaleBack.Name = "btnCredit_SaleBack";
            this.btnCredit_SaleBack.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCredit_SaleBack_ItemClick);
            // 
            // btnRemoveFilter
            // 
            this.btnRemoveFilter.Caption = "حذف کلیه فیلترها";
            this.btnRemoveFilter.Glyph = ((System.Drawing.Image)(resources.GetObject("btnRemoveFilter.Glyph")));
            this.btnRemoveFilter.Id = 12;
            this.btnRemoveFilter.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnRemoveFilter.LargeGlyph")));
            this.btnRemoveFilter.Name = "btnRemoveFilter";
            this.btnRemoveFilter.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRemoveFilter_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup4,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "عملیات";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnPrint);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnReport);
            this.ribbonPageGroup1.ItemLinks.Add(this.BtnTicketInternet);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "گزارشات";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnRefresh);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnRemoveFilter);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "بروز رسانی";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.btnPayPose);
            this.ribbonPageGroup4.ItemLinks.Add(this.btnPayCredit);
            this.ribbonPageGroup4.ItemLinks.Add(this.btnStatePaidBack);
            this.ribbonPageGroup4.ItemLinks.Add(this.btnCredit_SaleBack);
            this.ribbonPageGroup4.ItemLinks.Add(this.btnshowAll);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "وضعیت پرداخت";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnClose);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 764);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1330, 23);
            // 
            // navigationPane1
            // 
            this.navigationPane1.Controls.Add(this.navigationPage1);
            this.navigationPane1.Dock = System.Windows.Forms.DockStyle.Right;
            this.navigationPane1.Location = new System.Drawing.Point(1330, 147);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.PageProperties.AppearanceCaption.Options.UseTextOptions = true;
            this.navigationPane1.PageProperties.AppearanceCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.navigationPane1.PageProperties.ShowExpandButton = false;
            this.navigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.Image;
            this.navigationPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.navigationPage1});
            this.navigationPane1.RegularSize = new System.Drawing.Size(277, 640);
            this.navigationPane1.SelectedPage = this.navigationPage1;
            this.navigationPane1.Size = new System.Drawing.Size(42, 640);
            this.navigationPane1.State = DevExpress.XtraBars.Navigation.NavigationPaneState.Collapsed;
            this.navigationPane1.TabIndex = 25;
            this.navigationPane1.Text = "navigationPane1";
            this.navigationPane1.Click += new System.EventHandler(this.navigationPane1_Click);
            // 
            // navigationPage1
            // 
            this.navigationPage1.Caption = "گزارشات";
            this.navigationPage1.Controls.Add(this.LayoutControl2);
            this.navigationPage1.Image = ((System.Drawing.Image)(resources.GetObject("navigationPage1.Image")));
            this.navigationPage1.ImageUri.Uri = "Zoom";
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(0, 0);
            // 
            // LayoutControl2
            // 
            this.LayoutControl2.Controls.Add(this.btnShowAllTime);
            this.LayoutControl2.Controls.Add(this.SearchControl1);
            this.LayoutControl2.Controls.Add(this.btnShowAllUser);
            this.LayoutControl2.Controls.Add(this.txttimeTo);
            this.LayoutControl2.Controls.Add(this.txtTimeFrom);
            this.LayoutControl2.Controls.Add(this.ChCompany);
            this.LayoutControl2.Controls.Add(this.txtTodate);
            this.LayoutControl2.Controls.Add(this.txtFromDate);
            this.LayoutControl2.Controls.Add(this.cmbUsers);
            this.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl2.Name = "LayoutControl2";
            this.LayoutControl2.OptionsView.RightToLeftMirroringApplied = true;
            this.LayoutControl2.Root = this.LayoutControlGroup2;
            this.LayoutControl2.Size = new System.Drawing.Size(0, 0);
            this.LayoutControl2.TabIndex = 0;
            this.LayoutControl2.Text = "LayoutControl2";
            // 
            // btnShowAllTime
            // 
            this.btnShowAllTime.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnShowAllTime.Appearance.Options.UseFont = true;
            this.btnShowAllTime.Location = new System.Drawing.Point(-146, 296);
            this.btnShowAllTime.Name = "btnShowAllTime";
            this.btnShowAllTime.Size = new System.Drawing.Size(128, 29);
            this.btnShowAllTime.StyleController = this.LayoutControl2;
            this.btnShowAllTime.TabIndex = 33;
            this.btnShowAllTime.Text = "نمایش همه ساعت ها";
            this.btnShowAllTime.Click += new System.EventHandler(this.btnShowAllTime_Click);
            // 
            // SearchControl1
            // 
            this.SearchControl1.Client = this.gridControl1;
            this.SearchControl1.Location = new System.Drawing.Point(-158, 6);
            this.SearchControl1.Name = "SearchControl1";
            this.SearchControl1.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.SearchControl1.Properties.Appearance.Options.UseFont = true;
            this.SearchControl1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.SearchControl1.Properties.Client = this.gridControl1;
            this.SearchControl1.Size = new System.Drawing.Size(152, 26);
            this.SearchControl1.StyleController = this.LayoutControl2;
            this.SearchControl1.TabIndex = 16;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.ticketLocalBindingSource;
            this.gridControl1.Location = new System.Drawing.Point(12, 12);
            this.gridControl1.MainView = this.grd;
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemXtraFADateEdit1,
            this.reConpany,
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1306, 565);
            this.gridControl1.TabIndex = 26;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grd});
            this.gridControl1.Click += new System.EventHandler(this.gridControl1_Click);
            // 
            // ticketLocalBindingSource
            // 
            this.ticketLocalBindingSource.DataSource = typeof(Model.TicketLocal);
            // 
            // grd
            // 
            this.grd.Appearance.FilterPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.FilterPanel.Options.UseFont = true;
            this.grd.Appearance.FilterPanel.Options.UseTextOptions = true;
            this.grd.Appearance.FilterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.FocusedCell.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedCell.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grd.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grd.Appearance.FocusedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grd.Appearance.FocusedRow.Options.UseFont = true;
            this.grd.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grd.Appearance.FooterPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.FooterPanel.Options.UseFont = true;
            this.grd.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grd.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.GroupFooter.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.GroupFooter.Options.UseFont = true;
            this.grd.Appearance.GroupFooter.Options.UseTextOptions = true;
            this.grd.Appearance.GroupFooter.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.GroupPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.GroupPanel.Options.UseFont = true;
            this.grd.Appearance.GroupRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.GroupRow.Options.UseFont = true;
            this.grd.Appearance.HeaderPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.HeaderPanel.Options.UseFont = true;
            this.grd.Appearance.Row.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.Row.Options.UseFont = true;
            this.grd.Appearance.SelectedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grd.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grd.Appearance.TopNewRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.Appearance.TopNewRow.Options.UseFont = true;
            this.grd.AppearancePrint.EvenRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.EvenRow.Options.UseFont = true;
            this.grd.AppearancePrint.FilterPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.FilterPanel.Options.UseFont = true;
            this.grd.AppearancePrint.FooterPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.FooterPanel.Options.UseFont = true;
            this.grd.AppearancePrint.GroupFooter.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.GroupFooter.Options.UseFont = true;
            this.grd.AppearancePrint.GroupRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.GroupRow.Options.UseFont = true;
            this.grd.AppearancePrint.HeaderPanel.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.grd.AppearancePrint.Lines.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.Lines.Options.UseFont = true;
            this.grd.AppearancePrint.OddRow.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.OddRow.Options.UseFont = true;
            this.grd.AppearancePrint.Preview.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.Preview.Options.UseFont = true;
            this.grd.AppearancePrint.Row.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.grd.AppearancePrint.Row.Options.UseFont = true;
            this.grd.ColumnPanelRowHeight = 30;
            this.grd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCompanyCode,
            this.colSaleDateSH,
            this.colTimeSale,
            this.colFullname,
            this.colCityName,
            this.colNo,
            this.colNumChairs,
            this.colTotalTicket,
            this.colFundPriceValue,
            this.colPricePaidBack,
            this.colPaidBackDateSH,
            this.colPaidBackDateTime,
            this.colCashierName,
            this.colTypeTicket,
            this.colStatusPaidBack,
            this.colUserCredit,
            this.colReturnUserName,
            this.colReturnUserCode,
            this.colID,
            this.colTripID,
            this.colChairsName,
            this.colTel,
            this.colPrice,
            this.colDiscount,
            this.colPaidBackPercent,
            this.colCashierCode});
            this.grd.GridControl = this.gridControl1;
            this.grd.IndicatorWidth = 30;
            this.grd.Name = "grd";
            this.grd.OptionsBehavior.Editable = false;
            this.grd.OptionsCustomization.AllowColumnMoving = false;
            this.grd.OptionsCustomization.AllowFilter = false;
            this.grd.OptionsCustomization.AllowSort = false;
            this.grd.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            this.grd.OptionsFind.AlwaysVisible = true;
            this.grd.OptionsFind.FindNullPrompt = " جست و جو . . . ";
            this.grd.OptionsPrint.AllowMultilineHeaders = true;
            this.grd.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.grd.OptionsView.ShowFooter = true;
            this.grd.OptionsView.ShowGroupPanel = false;
            this.grd.RowClick += new DevExpress.XtraGrid.Views.Grid.RowClickEventHandler(this.grd_RowClick);
            this.grd.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grd_CustomDrawRowIndicator);
            this.grd.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.grd_CustomUnboundColumnData);
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCompanyCode.AppearanceCell.Options.UseFont = true;
            this.colCompanyCode.AppearanceCell.Options.UseTextOptions = true;
            this.colCompanyCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyCode.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colCompanyCode.AppearanceHeader.Options.UseFont = true;
            this.colCompanyCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCompanyCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyCode.Caption = "شرکت";
            this.colCompanyCode.ColumnEdit = this.reConpany;
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "CompanyCode", "{0}")});
            this.colCompanyCode.Visible = true;
            this.colCompanyCode.VisibleIndex = 0;
            this.colCompanyCode.Width = 108;
            // 
            // reConpany
            // 
            this.reConpany.AutoHeight = false;
            this.reConpany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.reConpany.DataSource = this.companyBindingSource;
            this.reConpany.DisplayMember = "Title";
            this.reConpany.Name = "reConpany";
            this.reConpany.NullText = "نامشخص";
            this.reConpany.ValueMember = "Code";
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataSource = typeof(Model.Company);
            // 
            // colSaleDateSH
            // 
            this.colSaleDateSH.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSaleDateSH.AppearanceCell.Options.UseFont = true;
            this.colSaleDateSH.AppearanceCell.Options.UseTextOptions = true;
            this.colSaleDateSH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSaleDateSH.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colSaleDateSH.AppearanceHeader.Options.UseFont = true;
            this.colSaleDateSH.AppearanceHeader.Options.UseTextOptions = true;
            this.colSaleDateSH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSaleDateSH.Caption = "تاریخ";
            this.colSaleDateSH.FieldName = "SaleDateSH";
            this.colSaleDateSH.Name = "colSaleDateSH";
            this.colSaleDateSH.Visible = true;
            this.colSaleDateSH.VisibleIndex = 1;
            this.colSaleDateSH.Width = 98;
            // 
            // repositoryItemXtraFADateEdit1
            // 
            this.repositoryItemXtraFADateEdit1.AutoHeight = false;
            this.repositoryItemXtraFADateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemXtraFADateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemXtraFADateEdit1.Name = "repositoryItemXtraFADateEdit1";
            // 
            // colTimeSale
            // 
            this.colTimeSale.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTimeSale.AppearanceCell.Options.UseFont = true;
            this.colTimeSale.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeSale.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeSale.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colTimeSale.AppearanceHeader.Options.UseFont = true;
            this.colTimeSale.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeSale.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeSale.Caption = "ساعت";
            this.colTimeSale.DisplayFormat.FormatString = "T";
            this.colTimeSale.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeSale.FieldName = "TimeSale";
            this.colTimeSale.Name = "colTimeSale";
            this.colTimeSale.Visible = true;
            this.colTimeSale.VisibleIndex = 2;
            this.colTimeSale.Width = 65;
            // 
            // colFullname
            // 
            this.colFullname.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colFullname.AppearanceCell.Options.UseFont = true;
            this.colFullname.AppearanceCell.Options.UseTextOptions = true;
            this.colFullname.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullname.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colFullname.AppearanceHeader.Options.UseFont = true;
            this.colFullname.AppearanceHeader.Options.UseTextOptions = true;
            this.colFullname.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullname.Caption = "مشتری";
            this.colFullname.FieldName = "Fullname";
            this.colFullname.Name = "colFullname";
            this.colFullname.Visible = true;
            this.colFullname.VisibleIndex = 3;
            this.colFullname.Width = 105;
            // 
            // colCityName
            // 
            this.colCityName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCityName.AppearanceCell.Options.UseFont = true;
            this.colCityName.AppearanceCell.Options.UseTextOptions = true;
            this.colCityName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCityName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colCityName.AppearanceHeader.Options.UseFont = true;
            this.colCityName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCityName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCityName.Caption = "مقصد";
            this.colCityName.FieldName = "CityName";
            this.colCityName.Name = "colCityName";
            this.colCityName.Visible = true;
            this.colCityName.VisibleIndex = 4;
            this.colCityName.Width = 66;
            // 
            // colNo
            // 
            this.colNo.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colNo.AppearanceCell.Options.UseFont = true;
            this.colNo.AppearanceCell.Options.UseTextOptions = true;
            this.colNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colNo.AppearanceHeader.Options.UseFont = true;
            this.colNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.Caption = "شماره بلیط";
            this.colNo.FieldName = "No";
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 5;
            this.colNo.Width = 99;
            // 
            // colNumChairs
            // 
            this.colNumChairs.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colNumChairs.AppearanceCell.Options.UseFont = true;
            this.colNumChairs.AppearanceCell.Options.UseTextOptions = true;
            this.colNumChairs.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumChairs.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colNumChairs.AppearanceHeader.Options.UseFont = true;
            this.colNumChairs.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumChairs.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumChairs.Caption = "ت صندلی";
            this.colNumChairs.FieldName = "NumChairs";
            this.colNumChairs.Name = "colNumChairs";
            this.colNumChairs.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NumChairs", "{0:0.##}")});
            this.colNumChairs.Visible = true;
            this.colNumChairs.VisibleIndex = 6;
            this.colNumChairs.Width = 80;
            // 
            // colTotalTicket
            // 
            this.colTotalTicket.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTotalTicket.AppearanceCell.Options.UseFont = true;
            this.colTotalTicket.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalTicket.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalTicket.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colTotalTicket.AppearanceHeader.Options.UseFont = true;
            this.colTotalTicket.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalTicket.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalTicket.Caption = "جمع کل";
            this.colTotalTicket.DisplayFormat.FormatString = "c0";
            this.colTotalTicket.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalTicket.FieldName = "TotalTicket";
            this.colTotalTicket.Name = "colTotalTicket";
            this.colTotalTicket.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalTicket", "ج {0:c0}")});
            this.colTotalTicket.Visible = true;
            this.colTotalTicket.VisibleIndex = 7;
            this.colTotalTicket.Width = 85;
            // 
            // colFundPriceValue
            // 
            this.colFundPriceValue.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colFundPriceValue.AppearanceCell.Options.UseFont = true;
            this.colFundPriceValue.AppearanceCell.Options.UseTextOptions = true;
            this.colFundPriceValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFundPriceValue.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colFundPriceValue.AppearanceHeader.Options.UseFont = true;
            this.colFundPriceValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colFundPriceValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFundPriceValue.Caption = "سهم صندوقدار";
            this.colFundPriceValue.DisplayFormat.FormatString = "c0";
            this.colFundPriceValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFundPriceValue.FieldName = "FundPriceValue";
            this.colFundPriceValue.Name = "colFundPriceValue";
            this.colFundPriceValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FundPrice", "{0:c0}")});
            this.colFundPriceValue.Visible = true;
            this.colFundPriceValue.VisibleIndex = 8;
            this.colFundPriceValue.Width = 85;
            // 
            // colPricePaidBack
            // 
            this.colPricePaidBack.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPricePaidBack.AppearanceCell.Options.UseFont = true;
            this.colPricePaidBack.AppearanceCell.Options.UseTextOptions = true;
            this.colPricePaidBack.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPricePaidBack.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colPricePaidBack.AppearanceHeader.Options.UseFont = true;
            this.colPricePaidBack.AppearanceHeader.Options.UseTextOptions = true;
            this.colPricePaidBack.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPricePaidBack.Caption = "مبلغ برگشتی";
            this.colPricePaidBack.DisplayFormat.FormatString = "c0";
            this.colPricePaidBack.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPricePaidBack.FieldName = "PricePaidBack";
            this.colPricePaidBack.Name = "colPricePaidBack";
            this.colPricePaidBack.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PricePaidBack", "{0:c0}")});
            this.colPricePaidBack.Visible = true;
            this.colPricePaidBack.VisibleIndex = 9;
            this.colPricePaidBack.Width = 74;
            // 
            // colPaidBackDateSH
            // 
            this.colPaidBackDateSH.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPaidBackDateSH.AppearanceCell.Options.UseFont = true;
            this.colPaidBackDateSH.AppearanceCell.Options.UseTextOptions = true;
            this.colPaidBackDateSH.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackDateSH.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colPaidBackDateSH.AppearanceHeader.Options.UseFont = true;
            this.colPaidBackDateSH.AppearanceHeader.Options.UseTextOptions = true;
            this.colPaidBackDateSH.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackDateSH.Caption = "ت برگشت";
            this.colPaidBackDateSH.FieldName = "PaidBackDateSH";
            this.colPaidBackDateSH.Name = "colPaidBackDateSH";
            this.colPaidBackDateSH.Visible = true;
            this.colPaidBackDateSH.VisibleIndex = 10;
            // 
            // colPaidBackDateTime
            // 
            this.colPaidBackDateTime.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPaidBackDateTime.AppearanceCell.Options.UseFont = true;
            this.colPaidBackDateTime.AppearanceCell.Options.UseTextOptions = true;
            this.colPaidBackDateTime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackDateTime.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colPaidBackDateTime.AppearanceHeader.Options.UseFont = true;
            this.colPaidBackDateTime.AppearanceHeader.Options.UseTextOptions = true;
            this.colPaidBackDateTime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackDateTime.Caption = "س برگشت";
            this.colPaidBackDateTime.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPaidBackDateTime.DisplayFormat.FormatString = "HH:mm";
            this.colPaidBackDateTime.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPaidBackDateTime.FieldName = "PaidBackDate";
            this.colPaidBackDateTime.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText;
            this.colPaidBackDateTime.Name = "colPaidBackDateTime";
            this.colPaidBackDateTime.Visible = true;
            this.colPaidBackDateTime.VisibleIndex = 11;
            this.colPaidBackDateTime.Width = 98;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            // 
            // colCashierName
            // 
            this.colCashierName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCashierName.AppearanceCell.Options.UseFont = true;
            this.colCashierName.AppearanceCell.Options.UseTextOptions = true;
            this.colCashierName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colCashierName.AppearanceHeader.Options.UseFont = true;
            this.colCashierName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCashierName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierName.Caption = "صندوقدار";
            this.colCashierName.FieldName = "CashierName";
            this.colCashierName.Name = "colCashierName";
            this.colCashierName.Visible = true;
            this.colCashierName.VisibleIndex = 12;
            this.colCashierName.Width = 96;
            // 
            // colTypeTicket
            // 
            this.colTypeTicket.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTypeTicket.AppearanceCell.Options.UseFont = true;
            this.colTypeTicket.AppearanceCell.Options.UseTextOptions = true;
            this.colTypeTicket.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTypeTicket.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colTypeTicket.AppearanceHeader.Options.UseFont = true;
            this.colTypeTicket.AppearanceHeader.Options.UseTextOptions = true;
            this.colTypeTicket.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTypeTicket.Caption = "نوع پرداخت";
            this.colTypeTicket.FieldName = "TypeTicket";
            this.colTypeTicket.Name = "colTypeTicket";
            this.colTypeTicket.Visible = true;
            this.colTypeTicket.VisibleIndex = 13;
            this.colTypeTicket.Width = 96;
            // 
            // colStatusPaidBack
            // 
            this.colStatusPaidBack.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colStatusPaidBack.AppearanceCell.Options.UseFont = true;
            this.colStatusPaidBack.AppearanceCell.Options.UseTextOptions = true;
            this.colStatusPaidBack.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStatusPaidBack.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colStatusPaidBack.AppearanceHeader.Options.UseFont = true;
            this.colStatusPaidBack.AppearanceHeader.Options.UseTextOptions = true;
            this.colStatusPaidBack.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colStatusPaidBack.Caption = "وضعیت برگشت";
            this.colStatusPaidBack.FieldName = "StatusPaidBack";
            this.colStatusPaidBack.Name = "colStatusPaidBack";
            this.colStatusPaidBack.Visible = true;
            this.colStatusPaidBack.VisibleIndex = 14;
            // 
            // colUserCredit
            // 
            this.colUserCredit.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 11F);
            this.colUserCredit.AppearanceCell.Options.UseFont = true;
            this.colUserCredit.AppearanceCell.Options.UseTextOptions = true;
            this.colUserCredit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserCredit.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colUserCredit.AppearanceHeader.Options.UseFont = true;
            this.colUserCredit.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserCredit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserCredit.Caption = "اعتبار مصرفی";
            this.colUserCredit.ColumnEdit = this.repositoryItemTextEdit1;
            this.colUserCredit.FieldName = "UerCredit";
            this.colUserCredit.Name = "colUserCredit";
            this.colUserCredit.UnboundType = DevExpress.Data.UnboundColumnType.Integer;
            // 
            // colReturnUserName
            // 
            this.colReturnUserName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colReturnUserName.AppearanceCell.Options.UseFont = true;
            this.colReturnUserName.AppearanceCell.Options.UseTextOptions = true;
            this.colReturnUserName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnUserName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colReturnUserName.AppearanceHeader.Options.UseFont = true;
            this.colReturnUserName.AppearanceHeader.Options.UseTextOptions = true;
            this.colReturnUserName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colReturnUserName.Caption = "کاربر برگشت دهنده";
            this.colReturnUserName.FieldName = "ReturnUserName";
            this.colReturnUserName.Name = "colReturnUserName";
            // 
            // colReturnUserCode
            // 
            this.colReturnUserCode.Caption = "کد کاربر برگشت دهنده";
            this.colReturnUserCode.FieldName = "ReturnUserCode";
            this.colReturnUserCode.Name = "colReturnUserCode";
            // 
            // colID
            // 
            this.colID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colID.AppearanceCell.Options.UseFont = true;
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colID.AppearanceHeader.Options.UseFont = true;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.Caption = "#";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.Width = 36;
            // 
            // colTripID
            // 
            this.colTripID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTripID.AppearanceCell.Options.UseFont = true;
            this.colTripID.AppearanceCell.Options.UseTextOptions = true;
            this.colTripID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTripID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colTripID.AppearanceHeader.Options.UseFont = true;
            this.colTripID.AppearanceHeader.Options.UseTextOptions = true;
            this.colTripID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTripID.Caption = "سفر";
            this.colTripID.FieldName = "TripID";
            this.colTripID.Name = "colTripID";
            this.colTripID.Width = 70;
            // 
            // colChairsName
            // 
            this.colChairsName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colChairsName.AppearanceCell.Options.UseFont = true;
            this.colChairsName.AppearanceCell.Options.UseTextOptions = true;
            this.colChairsName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colChairsName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colChairsName.AppearanceHeader.Options.UseFont = true;
            this.colChairsName.AppearanceHeader.Options.UseTextOptions = true;
            this.colChairsName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colChairsName.Caption = "صندلی ها";
            this.colChairsName.FieldName = "ChairsName";
            this.colChairsName.Name = "colChairsName";
            this.colChairsName.Width = 56;
            // 
            // colTel
            // 
            this.colTel.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTel.AppearanceCell.Options.UseFont = true;
            this.colTel.AppearanceCell.Options.UseTextOptions = true;
            this.colTel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTel.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colTel.AppearanceHeader.Options.UseFont = true;
            this.colTel.AppearanceHeader.Options.UseTextOptions = true;
            this.colTel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTel.Caption = "تلفن";
            this.colTel.FieldName = "Tel";
            this.colTel.Name = "colTel";
            this.colTel.Width = 98;
            // 
            // colPrice
            // 
            this.colPrice.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPrice.AppearanceCell.Options.UseFont = true;
            this.colPrice.AppearanceCell.Options.UseTextOptions = true;
            this.colPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colPrice.AppearanceHeader.Options.UseFont = true;
            this.colPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.Caption = "هزینه بلیط";
            this.colPrice.DisplayFormat.FormatString = "c0";
            this.colPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrice.FieldName = "Price";
            this.colPrice.Name = "colPrice";
            this.colPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Price", "  {0:c0}")});
            this.colPrice.Width = 63;
            // 
            // colDiscount
            // 
            this.colDiscount.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colDiscount.AppearanceCell.Options.UseFont = true;
            this.colDiscount.AppearanceCell.Options.UseTextOptions = true;
            this.colDiscount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDiscount.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colDiscount.AppearanceHeader.Options.UseFont = true;
            this.colDiscount.AppearanceHeader.Options.UseTextOptions = true;
            this.colDiscount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDiscount.Caption = "تخفیف";
            this.colDiscount.FieldName = "Discount";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Discount", "{0:0.##}")});
            this.colDiscount.Width = 63;
            // 
            // colPaidBackPercent
            // 
            this.colPaidBackPercent.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPaidBackPercent.AppearanceCell.Options.UseFont = true;
            this.colPaidBackPercent.AppearanceCell.Options.UseTextOptions = true;
            this.colPaidBackPercent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackPercent.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colPaidBackPercent.AppearanceHeader.Options.UseFont = true;
            this.colPaidBackPercent.AppearanceHeader.Options.UseTextOptions = true;
            this.colPaidBackPercent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackPercent.Caption = "درصد برگشتی";
            this.colPaidBackPercent.FieldName = "PaidBackPercent";
            this.colPaidBackPercent.Name = "colPaidBackPercent";
            this.colPaidBackPercent.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PaidBackPercent", "{0:0.##}")});
            this.colPaidBackPercent.Width = 73;
            // 
            // colCashierCode
            // 
            this.colCashierCode.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCashierCode.AppearanceCell.Options.UseFont = true;
            this.colCashierCode.AppearanceCell.Options.UseTextOptions = true;
            this.colCashierCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierCode.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colCashierCode.AppearanceHeader.Options.UseFont = true;
            this.colCashierCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCashierCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierCode.Caption = "کدصندوقدار";
            this.colCashierCode.FieldName = "CashierCode";
            this.colCashierCode.Name = "colCashierCode";
            // 
            // btnShowAllUser
            // 
            this.btnShowAllUser.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.btnShowAllUser.Appearance.Options.UseFont = true;
            this.btnShowAllUser.Image = ((System.Drawing.Image)(resources.GetObject("btnShowAllUser.Image")));
            this.btnShowAllUser.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleRight;
            this.btnShowAllUser.Location = new System.Drawing.Point(-155, 173);
            this.btnShowAllUser.Name = "btnShowAllUser";
            this.btnShowAllUser.Size = new System.Drawing.Size(146, 29);
            this.btnShowAllUser.StyleController = this.LayoutControl2;
            this.btnShowAllUser.TabIndex = 32;
            this.btnShowAllUser.Text = "نمایش همه کاربران";
            this.btnShowAllUser.Click += new System.EventHandler(this.btnShowAllUser_Click);
            // 
            // txttimeTo
            // 
            this.txttimeTo.EditValue = new System.DateTime(2019, 5, 14, 0, 0, 0, 0);
            this.txttimeTo.Location = new System.Drawing.Point(-146, 268);
            this.txttimeTo.MenuManager = this.ribbon;
            this.txttimeTo.Name = "txttimeTo";
            this.txttimeTo.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txttimeTo.Properties.Appearance.Options.UseFont = true;
            this.txttimeTo.Properties.Appearance.Options.UseTextOptions = true;
            this.txttimeTo.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txttimeTo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txttimeTo.Properties.DisplayFormat.FormatString = "T";
            this.txttimeTo.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txttimeTo.Properties.EditFormat.FormatString = "T";
            this.txttimeTo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txttimeTo.Size = new System.Drawing.Size(84, 24);
            this.txttimeTo.StyleController = this.LayoutControl2;
            this.txttimeTo.TabIndex = 31;
            this.txttimeTo.EditValueChanged += new System.EventHandler(this.timeEdit2_EditValueChanged);
            // 
            // txtTimeFrom
            // 
            this.txtTimeFrom.EditValue = new System.DateTime(2019, 5, 14, 0, 0, 0, 0);
            this.txtTimeFrom.Location = new System.Drawing.Point(-146, 240);
            this.txtTimeFrom.MenuManager = this.ribbon;
            this.txtTimeFrom.Name = "txtTimeFrom";
            this.txtTimeFrom.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txtTimeFrom.Properties.Appearance.Options.UseFont = true;
            this.txtTimeFrom.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTimeFrom.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTimeFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTimeFrom.Properties.DisplayFormat.FormatString = "T";
            this.txtTimeFrom.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtTimeFrom.Properties.EditFormat.FormatString = "T";
            this.txtTimeFrom.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.txtTimeFrom.Size = new System.Drawing.Size(84, 24);
            this.txtTimeFrom.StyleController = this.LayoutControl2;
            this.txtTimeFrom.TabIndex = 30;
            this.txtTimeFrom.EditValueChanged += new System.EventHandler(this.timeEdit2_EditValueChanged);
            // 
            // ChCompany
            // 
            this.ChCompany.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.ChCompany.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.ChCompany.Appearance.Options.UseBackColor = true;
            this.ChCompany.Appearance.Options.UseFont = true;
            this.ChCompany.CheckOnClick = true;
            this.ChCompany.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChCompany.DataSource = this.companyBindingSource;
            this.ChCompany.DisplayMember = "Title";
            this.ChCompany.Location = new System.Drawing.Point(-155, 363);
            this.ChCompany.MultiColumn = true;
            this.ChCompany.Name = "ChCompany";
            this.ChCompany.Size = new System.Drawing.Size(146, 0);
            this.ChCompany.StyleController = this.LayoutControl2;
            this.ChCompany.TabIndex = 27;
            this.ChCompany.ValueMember = "Code";
            this.ChCompany.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.ChCompany_ItemCheck);
            // 
            // txtTodate
            // 
            this.txtTodate.EditValue = null;
            this.txtTodate.Location = new System.Drawing.Point(-155, 86);
            this.txtTodate.MenuManager = this.ribbon;
            this.txtTodate.Name = "txtTodate";
            this.txtTodate.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txtTodate.Properties.Appearance.Options.UseFont = true;
            this.txtTodate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTodate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTodate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTodate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTodate.Properties.NullValuePrompt = "تا تاریخ";
            this.txtTodate.Size = new System.Drawing.Size(102, 24);
            this.txtTodate.StyleController = this.LayoutControl2;
            this.txtTodate.TabIndex = 4;
            this.txtTodate.Click += new System.EventHandler(this.txtTodate_Click);
            // 
            // txtFromDate
            // 
            this.txtFromDate.EditValue = null;
            this.txtFromDate.Location = new System.Drawing.Point(-155, 58);
            this.txtFromDate.MenuManager = this.ribbon;
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txtFromDate.Properties.Appearance.Options.UseFont = true;
            this.txtFromDate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFromDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Properties.NullValuePrompt = "از تاریخ";
            this.txtFromDate.Size = new System.Drawing.Size(102, 24);
            this.txtFromDate.StyleController = this.LayoutControl2;
            this.txtFromDate.TabIndex = 3;
            this.txtFromDate.Click += new System.EventHandler(this.txtFromDate_Click);
            // 
            // cmbUsers
            // 
            this.cmbUsers.Location = new System.Drawing.Point(-155, 141);
            this.cmbUsers.MenuManager = this.ribbon;
            this.cmbUsers.Name = "cmbUsers";
            this.cmbUsers.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.cmbUsers.Properties.Appearance.Options.UseFont = true;
            this.cmbUsers.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbUsers.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID", "ID", 36, DevExpress.Utils.FormatType.Numeric, "", false, DevExpress.Utils.HorzAlignment.Far),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Code", "کد", 40, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("FullName", "نام کاربر", 75, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Center),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("Credit", "اعتبار حساب", 47, DevExpress.Utils.FormatType.Numeric, "", true, DevExpress.Utils.HorzAlignment.Center)});
            this.cmbUsers.Properties.DataSource = this.userBindingSource;
            this.cmbUsers.Properties.DisplayMember = "FullName";
            this.cmbUsers.Properties.NullText = "";
            this.cmbUsers.Properties.PopupSizeable = false;
            this.cmbUsers.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.cmbUsers.Properties.ValueMember = "Code";
            this.cmbUsers.Size = new System.Drawing.Size(146, 28);
            this.cmbUsers.StyleController = this.LayoutControl2;
            this.cmbUsers.TabIndex = 29;
            this.cmbUsers.EditValueChanged += new System.EventHandler(this.cmbUsers_EditValueChanged);
            this.cmbUsers.Click += new System.EventHandler(this.cmbUsers_Click);
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(Model.User);
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup2.GroupBordersVisible = false;
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup3,
            this.LayoutControlGroup4,
            this.LayoutControlItem3,
            this.layUsers,
            this.layoutControlGroup1});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(-164, 0);
            this.LayoutControlGroup2.Name = "Root";
            this.LayoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.LayoutControlGroup2.Size = new System.Drawing.Size(164, 372);
            this.LayoutControlGroup2.TextVisible = false;
            // 
            // LayoutControlGroup3
            // 
            this.LayoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup3.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.LayoutControlGroup3.AppearanceItemCaption.Options.UseFont = true;
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem1});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 30);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup3.Size = new System.Drawing.Size(156, 81);
            this.LayoutControlGroup3.Text = "گزارش تاریخ";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txtFromDate;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(150, 28);
            this.layoutControlItem5.Text = "از تاریخ";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(41, 24);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtTodate;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(150, 28);
            this.layoutControlItem1.Text = "تا تاریخ";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(41, 24);
            // 
            // LayoutControlGroup4
            // 
            this.LayoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup4.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.LayoutControlGroup4.AppearanceItemCaption.Options.UseFont = true;
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 335);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup4.Size = new System.Drawing.Size(156, 29);
            this.LayoutControlGroup4.Text = "شرکت ها";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ChCompany;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(150, 4);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.SearchControl1;
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(156, 30);
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextVisible = false;
            this.LayoutControlItem3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layUsers
            // 
            this.layUsers.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F, System.Drawing.FontStyle.Bold);
            this.layUsers.AppearanceGroup.Options.UseFont = true;
            this.layUsers.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layUsers.AppearanceItemCaption.Options.UseFont = true;
            this.layUsers.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem6,
            this.layoutControlItem8});
            this.layUsers.Location = new System.Drawing.Point(0, 111);
            this.layUsers.Name = "layUsers";
            this.layUsers.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layUsers.Size = new System.Drawing.Size(156, 92);
            this.layUsers.Text = "کاربران";
            this.layUsers.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.cmbUsers;
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(150, 32);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnShowAllUser;
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(150, 33);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem4,
            this.layoutControlItem10});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 203);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(156, 132);
            this.layoutControlGroup1.Text = "ساعت فروش";
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txttimeTo;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(132, 28);
            this.layoutControlItem7.Text = "تا ساعت";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(41, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtTimeFrom;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(132, 28);
            this.layoutControlItem4.Text = "از ساعت";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(41, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnShowAllTime;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 56);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(132, 33);
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextVisible = false;
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.txtPriceRollBack);
            this.dataLayoutControl1.Controls.Add(this.txtFundPrice);
            this.dataLayoutControl1.Controls.Add(this.txtSum);
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 147);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup5;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1330, 617);
            this.dataLayoutControl1.TabIndex = 29;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // txtPriceRollBack
            // 
            this.txtPriceRollBack.Location = new System.Drawing.Point(12, 581);
            this.txtPriceRollBack.MenuManager = this.ribbon;
            this.txtPriceRollBack.Name = "txtPriceRollBack";
            this.txtPriceRollBack.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txtPriceRollBack.Properties.Appearance.Options.UseFont = true;
            this.txtPriceRollBack.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPriceRollBack.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPriceRollBack.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.txtPriceRollBack.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtPriceRollBack.Properties.DisplayFormat.FormatString = "c0";
            this.txtPriceRollBack.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPriceRollBack.Properties.EditFormat.FormatString = "c0";
            this.txtPriceRollBack.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPriceRollBack.Properties.Mask.EditMask = "c0";
            this.txtPriceRollBack.Properties.ReadOnly = true;
            this.txtPriceRollBack.Size = new System.Drawing.Size(361, 24);
            this.txtPriceRollBack.StyleController = this.dataLayoutControl1;
            this.txtPriceRollBack.TabIndex = 29;
            // 
            // txtFundPrice
            // 
            this.txtFundPrice.Location = new System.Drawing.Point(441, 581);
            this.txtFundPrice.MenuManager = this.ribbon;
            this.txtFundPrice.Name = "txtFundPrice";
            this.txtFundPrice.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txtFundPrice.Properties.Appearance.Options.UseFont = true;
            this.txtFundPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFundPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFundPrice.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.txtFundPrice.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtFundPrice.Properties.DisplayFormat.FormatString = "c0";
            this.txtFundPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtFundPrice.Properties.EditFormat.FormatString = "c0";
            this.txtFundPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtFundPrice.Properties.Mask.EditMask = "c0";
            this.txtFundPrice.Properties.ReadOnly = true;
            this.txtFundPrice.Size = new System.Drawing.Size(378, 24);
            this.txtFundPrice.StyleController = this.dataLayoutControl1;
            this.txtFundPrice.TabIndex = 28;
            // 
            // txtSum
            // 
            this.txtSum.Location = new System.Drawing.Point(899, 581);
            this.txtSum.MenuManager = this.ribbon;
            this.txtSum.Name = "txtSum";
            this.txtSum.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txtSum.Properties.Appearance.Options.UseFont = true;
            this.txtSum.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSum.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtSum.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.txtSum.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtSum.Properties.DisplayFormat.FormatString = "c0";
            this.txtSum.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSum.Properties.EditFormat.FormatString = "c0";
            this.txtSum.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSum.Properties.Mask.EditMask = "c0";
            this.txtSum.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSum.Properties.ReadOnly = true;
            this.txtSum.Size = new System.Drawing.Size(371, 24);
            this.txtSum.StyleController = this.dataLayoutControl1;
            this.txtSum.TabIndex = 27;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup5.GroupBordersVisible = false;
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1330, 617);
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.gridControl1;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(1310, 569);
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.Control = this.txtSum;
            this.layoutControlItem11.Location = new System.Drawing.Point(887, 569);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(423, 28);
            this.layoutControlItem11.Text = " جمع کل :";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(43, 18);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.layoutControlItem12.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem12.Control = this.txtFundPrice;
            this.layoutControlItem12.Location = new System.Drawing.Point(429, 569);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(458, 28);
            this.layoutControlItem12.Text = " سهم صندوقدار :";
            this.layoutControlItem12.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(71, 18);
            this.layoutControlItem12.TextToControlDistance = 5;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.txtPriceRollBack;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 569);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(429, 28);
            this.layoutControlItem13.Text = "مبلغ برگشتی :";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(59, 18);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // frmticketListTicketUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1372, 787);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.navigationPane1);
            this.Controls.Add(this.ribbon);
            this.Name = "frmticketListTicketUser";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "گزارش صندوق";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmticketListTicketUser_FormClosing);
            this.Load += new System.EventHandler(this.frmticketListTicketUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).EndInit();
            this.navigationPane1.ResumeLayout(false);
            this.navigationPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).EndInit();
            this.LayoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SearchControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketLocalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reConpany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttimeTo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbUsers.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layUsers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPriceRollBack.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFundPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSum.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Navigation.NavigationPane navigationPane1;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        internal DevExpress.XtraLayout.LayoutControl LayoutControl2;
        private FarsiLibrary.Win.DevExpress.XtraFADateEdit txtTodate;
        private FarsiLibrary.Win.DevExpress.XtraFADateEdit txtFromDate;
        internal DevExpress.XtraEditors.SearchControl SearchControl1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView grd;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colTripID;
        private DevExpress.XtraGrid.Columns.GridColumn colFullname;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Columns.GridColumn colChairsName;
        private DevExpress.XtraGrid.Columns.GridColumn colCityName;
        private DevExpress.XtraGrid.Columns.GridColumn colTel;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colSaleDateSH;
        private DevExpress.XtraGrid.Columns.GridColumn colNumChairs;
        private DevExpress.XtraGrid.Columns.GridColumn colCashierName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private FarsiLibrary.Win.DevExpress.RepositoryItemXtraFADateEdit repositoryItemXtraFADateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit reConpany;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        internal DevExpress.XtraEditors.CheckedListBoxControl ChCompany;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.BarButtonItem btnReport;
        private DevExpress.XtraGrid.Columns.GridColumn colCashierCode;
        private DevExpress.XtraEditors.LookUpEdit cmbUsers;
        private System.Windows.Forms.BindingSource userBindingSource;
        private DevExpress.XtraLayout.LayoutControlGroup layUsers;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private System.Windows.Forms.BindingSource ticketLocalBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeSale;
        private DevExpress.XtraEditors.TimeEdit txttimeTo;
        private DevExpress.XtraEditors.TimeEdit txtTimeFrom;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidBackPercent;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTicket;
        private DevExpress.XtraGrid.Columns.GridColumn colPricePaidBack;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidBackDateTime;
        private DevExpress.XtraGrid.Columns.GridColumn colFundPriceValue;
        private DevExpress.XtraBars.BarButtonItem BtnTicketInternet;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeTicket;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidBackDateSH;
        private DevExpress.XtraBars.BarButtonItem btnPayPose;
        private DevExpress.XtraBars.BarButtonItem btnPayCredit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private DevExpress.XtraBars.BarButtonItem btnshowAll;
        private DevExpress.XtraGrid.Columns.GridColumn colStatusPaidBack;
        private DevExpress.XtraBars.BarButtonItem btnStatePaidBack;
        private DevExpress.XtraEditors.SimpleButton btnShowAllUser;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnUserCode;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraEditors.TextEdit txtPriceRollBack;
        private DevExpress.XtraEditors.TextEdit txtFundPrice;
        private DevExpress.XtraEditors.TextEdit txtSum;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraBars.BarButtonItem btnCredit_SaleBack;
        private DevExpress.XtraGrid.Columns.GridColumn colUserCredit;
        private DevExpress.XtraGrid.Columns.GridColumn colReturnUserName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraEditors.SimpleButton btnShowAllTime;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraBars.BarButtonItem btnRemoveFilter;
    }
}