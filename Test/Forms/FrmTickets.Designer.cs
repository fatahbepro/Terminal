﻿namespace Test
{
    partial class FrmTickets
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTickets));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip2 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem3 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem2 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem2 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem4 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip3 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem5 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem3 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem3 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem6 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.SuperToolTip superToolTip4 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem7 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem4 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem4 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem8 = new DevExpress.Utils.ToolTipTitleItem();
            Syncfusion.Windows.Forms.Grid.GridBaseStyle gridBaseStyle1 = new Syncfusion.Windows.Forms.Grid.GridBaseStyle();
            Syncfusion.Windows.Forms.Grid.GridBaseStyle gridBaseStyle2 = new Syncfusion.Windows.Forms.Grid.GridBaseStyle();
            Syncfusion.Windows.Forms.Grid.GridBaseStyle gridBaseStyle3 = new Syncfusion.Windows.Forms.Grid.GridBaseStyle();
            Syncfusion.Windows.Forms.Grid.GridBaseStyle gridBaseStyle4 = new Syncfusion.Windows.Forms.Grid.GridBaseStyle();
            Syncfusion.Windows.Forms.Grid.GridCellInfo gridCellInfo1 = new Syncfusion.Windows.Forms.Grid.GridCellInfo();
            Syncfusion.Windows.Forms.Grid.GridCellInfo gridCellInfo2 = new Syncfusion.Windows.Forms.Grid.GridCellInfo();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule4 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule5 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.btnSave = new DevExpress.XtraBars.BarButtonItem();
            this.btnEmpty = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.btnsetchair = new DevExpress.XtraBars.BarButtonItem();
            this.btnPose = new DevExpress.XtraBars.BarButtonItem();
            this.btnCancelPose = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.ShowNumberChair = new DevExpress.XtraBars.BarButtonItem();
            this.btnListTickets = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.dgv = new Syncfusion.Windows.Forms.Grid.GridControl();
            this.picEntekhabZan = new DevExpress.XtraEditors.PictureEdit();
            this.picEntekhabMard = new DevExpress.XtraEditors.PictureEdit();
            this.picRezervZan = new DevExpress.XtraEditors.PictureEdit();
            this.picRezerMard = new DevExpress.XtraEditors.PictureEdit();
            this.picForoshZan = new DevExpress.XtraEditors.PictureEdit();
            this.picForoshMard = new DevExpress.XtraEditors.PictureEdit();
            this.txtSumPrice = new DevExpress.XtraEditors.TextEdit();
            this.txtEmptyChair = new DevExpress.XtraEditors.TextEdit();
            this.txtRezervChair = new DevExpress.XtraEditors.TextEdit();
            this.txtFullChair = new DevExpress.XtraEditors.TextEdit();
            this.txtCountChair = new DevExpress.XtraEditors.TextEdit();
            this.CreditUsertxt = new DevExpress.XtraEditors.TextEdit();
            this.txtStatusPayment = new DevExpress.XtraEditors.TextEdit();
            this.NoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.txtWay = new DevExpress.XtraEditors.TextEdit();
            this.TripIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FullnameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ticketBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ChairsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.TelTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PriceTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.DiscountTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.NumChairsTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PayDateDateEdit = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.PrintedCheckEdit = new DevExpress.XtraEditors.ToggleSwitch();
            this.CityIDTextEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.citiesServicesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grdCitiesServices = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForSaleDate = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDiscount = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPrinted = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTripID = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForChairs = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.ItemForPrice = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFullname = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTel = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCityID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNumChairs = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.BgPose = new System.ComponentModel.BackgroundWorker();
            this.dxValidationProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Test.WaitForm1), true, true);
            this.timerGraphic = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEntekhabZan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEntekhabMard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRezervZan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRezerMard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picForoshZan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picForoshMard.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumPrice.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmptyChair.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRezervChair.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullChair.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountChair.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditUsertxt.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusPayment.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWay.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TripIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FullnameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChairsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriceTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumChairsTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayDateDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayDateDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrintedCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CityIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.citiesServicesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCitiesServices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaleDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDiscount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrinted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTripID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChairs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFullname)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCityID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNumChairs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.barButtonItem1,
            this.btnSave,
            this.btnEmpty,
            this.btnClose,
            this.barButtonItem5,
            this.btnRefresh,
            this.btnsetchair,
            this.btnPose,
            this.btnCancelPose,
            this.barButtonItem2,
            this.ShowNumberChair,
            this.btnListTickets});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 14;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1201, 143);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // btnSave
            // 
            this.btnSave.Caption = " اعتباری F2";
            this.btnSave.Glyph = ((System.Drawing.Image)(resources.GetObject("btnSave.Glyph")));
            this.btnSave.Id = 2;
            this.btnSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnSave.LargeGlyph")));
            this.btnSave.LargeWidth = 75;
            this.btnSave.Name = "btnSave";
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "ثبت";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "با تایید این گزینه بلیط مورد نظر ثبت می گردد";
            toolTipTitleItem2.LeftIndent = 6;
            toolTipTitleItem2.Text = "کلید میانبر F2";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipSeparatorItem1);
            superToolTip1.Items.Add(toolTipTitleItem2);
            this.btnSave.SuperTip = superToolTip1;
            this.btnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSave_ItemClick);
            // 
            // btnEmpty
            // 
            this.btnEmpty.Caption = "نوسازی";
            this.btnEmpty.Glyph = ((System.Drawing.Image)(resources.GetObject("btnEmpty.Glyph")));
            this.btnEmpty.Id = 3;
            this.btnEmpty.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.btnEmpty.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnEmpty.LargeGlyph")));
            this.btnEmpty.Name = "btnEmpty";
            toolTipTitleItem3.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            toolTipTitleItem3.Appearance.Options.UseImage = true;
            toolTipTitleItem3.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem3.Image")));
            toolTipTitleItem3.Text = "فاکتور جدید";
            toolTipItem2.LeftIndent = 6;
            toolTipItem2.Text = "با تایید این گزینه کلیه اطلاعات فرم خالی میشود و برای فاکتور جدید اماده می شود";
            toolTipTitleItem4.LeftIndent = 6;
            toolTipTitleItem4.Text = "کلید میانبر F3";
            superToolTip2.Items.Add(toolTipTitleItem3);
            superToolTip2.Items.Add(toolTipItem2);
            superToolTip2.Items.Add(toolTipSeparatorItem2);
            superToolTip2.Items.Add(toolTipTitleItem4);
            this.btnEmpty.SuperTip = superToolTip2;
            this.btnEmpty.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnEmpty_ItemClick);
            // 
            // btnClose
            // 
            this.btnClose.Caption = "بستن";
            this.btnClose.Glyph = ((System.Drawing.Image)(resources.GetObject("btnClose.Glyph")));
            this.btnClose.Id = 4;
            this.btnClose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnClose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnClose.LargeGlyph")));
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "barButtonItem5";
            this.barButtonItem5.Id = 5;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "بروزرسانی";
            this.btnRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Glyph")));
            this.btnRefresh.Id = 7;
            this.btnRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btnRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.LargeGlyph")));
            this.btnRefresh.Name = "btnRefresh";
            // 
            // btnsetchair
            // 
            this.btnsetchair.Caption = "btnsetchair";
            this.btnsetchair.Id = 8;
            this.btnsetchair.Name = "btnsetchair";
            // 
            // btnPose
            // 
            this.btnPose.Caption = "کارتخوان F7";
            this.btnPose.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPose.Glyph")));
            this.btnPose.Id = 9;
            this.btnPose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F7);
            this.btnPose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnPose.LargeGlyph")));
            this.btnPose.LargeWidth = 75;
            this.btnPose.Name = "btnPose";
            toolTipTitleItem5.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            toolTipTitleItem5.Appearance.Options.UseImage = true;
            toolTipTitleItem5.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem5.Image")));
            toolTipTitleItem5.Text = "پرداخت کارتخوان";
            toolTipItem3.LeftIndent = 6;
            toolTipItem3.Text = "ارسال مبلغ کل بلیط ها به دستگاه کارتخوان مورد نظر";
            toolTipTitleItem6.LeftIndent = 6;
            toolTipTitleItem6.Text = "کلید میانبر F3";
            superToolTip3.Items.Add(toolTipTitleItem5);
            superToolTip3.Items.Add(toolTipItem3);
            superToolTip3.Items.Add(toolTipSeparatorItem3);
            superToolTip3.Items.Add(toolTipTitleItem6);
            this.btnPose.SuperTip = superToolTip3;
            this.btnPose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPose_ItemClick);
            // 
            // btnCancelPose
            // 
            this.btnCancelPose.Caption = "لغو درخواست F8";
            this.btnCancelPose.Id = 10;
            this.btnCancelPose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F8);
            this.btnCancelPose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnCancelPose.LargeGlyph")));
            this.btnCancelPose.Name = "btnCancelPose";
            toolTipTitleItem7.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            toolTipTitleItem7.Appearance.Options.UseImage = true;
            toolTipTitleItem7.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem7.Image")));
            toolTipTitleItem7.Text = "لغو";
            toolTipItem4.LeftIndent = 6;
            toolTipItem4.Text = "لغو عملیات پرداخت کارتخوان و قطع ارتباط دستگاه با سیستم";
            toolTipTitleItem8.LeftIndent = 6;
            toolTipTitleItem8.Text = "کلید میانبر F6";
            superToolTip4.Items.Add(toolTipTitleItem7);
            superToolTip4.Items.Add(toolTipItem4);
            superToolTip4.Items.Add(toolTipSeparatorItem4);
            superToolTip4.Items.Add(toolTipTitleItem8);
            this.btnCancelPose.SuperTip = superToolTip4;
            this.btnCancelPose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCancelPose_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 11;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // ShowNumberChair
            // 
            this.ShowNumberChair.Caption = "نمایش شماره صندلی";
            this.ShowNumberChair.Glyph = ((System.Drawing.Image)(resources.GetObject("ShowNumberChair.Glyph")));
            this.ShowNumberChair.Id = 12;
            this.ShowNumberChair.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("ShowNumberChair.LargeGlyph")));
            this.ShowNumberChair.Name = "ShowNumberChair";
            // 
            // btnListTickets
            // 
            this.btnListTickets.Caption = "لیست بلیط ها";
            this.btnListTickets.Glyph = ((System.Drawing.Image)(resources.GetObject("btnListTickets.Glyph")));
            this.btnListTickets.Id = 13;
            this.btnListTickets.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6);
            this.btnListTickets.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnListTickets.LargeGlyph")));
            this.btnListTickets.Name = "btnListTickets";
            this.btnListTickets.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnListTickets_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup2,
            this.ribbonPageGroup1,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ذخیره اطلاعات";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnSave);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnPose);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnCancelPose);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnEmpty);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "ذخیره ";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnListTickets);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = " نمایش لیست بلیط ها";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnClose);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 714);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1201, 31);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.dgv);
            this.dataLayoutControl1.Controls.Add(this.picEntekhabZan);
            this.dataLayoutControl1.Controls.Add(this.picEntekhabMard);
            this.dataLayoutControl1.Controls.Add(this.picRezervZan);
            this.dataLayoutControl1.Controls.Add(this.picRezerMard);
            this.dataLayoutControl1.Controls.Add(this.picForoshZan);
            this.dataLayoutControl1.Controls.Add(this.picForoshMard);
            this.dataLayoutControl1.Controls.Add(this.txtSumPrice);
            this.dataLayoutControl1.Controls.Add(this.txtEmptyChair);
            this.dataLayoutControl1.Controls.Add(this.txtRezervChair);
            this.dataLayoutControl1.Controls.Add(this.txtFullChair);
            this.dataLayoutControl1.Controls.Add(this.txtCountChair);
            this.dataLayoutControl1.Controls.Add(this.CreditUsertxt);
            this.dataLayoutControl1.Controls.Add(this.txtStatusPayment);
            this.dataLayoutControl1.Controls.Add(this.NoTextEdit);
            this.dataLayoutControl1.Controls.Add(this.txtWay);
            this.dataLayoutControl1.Controls.Add(this.TripIDTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FullnameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.ChairsTextEdit);
            this.dataLayoutControl1.Controls.Add(this.TelTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PriceTextEdit);
            this.dataLayoutControl1.Controls.Add(this.DiscountTextEdit);
            this.dataLayoutControl1.Controls.Add(this.NumChairsTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PayDateDateEdit);
            this.dataLayoutControl1.Controls.Add(this.PrintedCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.CityIDTextEdit);
            this.dataLayoutControl1.DataSource = this.ticketBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 143);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(423, 98, 250, 350);
            this.dataLayoutControl1.OptionsFocus.MoveFocusRightToLeft = true;
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(1201, 571);
            this.dataLayoutControl1.TabIndex = 27;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // dgv
            // 
            this.dgv.AllowSelection = Syncfusion.Windows.Forms.Grid.GridSelectionFlags.None;
            gridBaseStyle1.Name = "Header";
            gridBaseStyle1.StyleInfo.Borders.Bottom = new Syncfusion.Windows.Forms.Grid.GridBorder(Syncfusion.Windows.Forms.Grid.GridBorderStyle.None);
            gridBaseStyle1.StyleInfo.Borders.Left = new Syncfusion.Windows.Forms.Grid.GridBorder(Syncfusion.Windows.Forms.Grid.GridBorderStyle.None);
            gridBaseStyle1.StyleInfo.Borders.Right = new Syncfusion.Windows.Forms.Grid.GridBorder(Syncfusion.Windows.Forms.Grid.GridBorderStyle.None);
            gridBaseStyle1.StyleInfo.Borders.Top = new Syncfusion.Windows.Forms.Grid.GridBorder(Syncfusion.Windows.Forms.Grid.GridBorderStyle.None);
            gridBaseStyle1.StyleInfo.CellType = "Header";
            gridBaseStyle1.StyleInfo.Font.Bold = true;
            gridBaseStyle1.StyleInfo.Interior = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(199)))), ((int)(((byte)(184))))), System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(234)))), ((int)(((byte)(216))))));
            gridBaseStyle1.StyleInfo.VerticalAlignment = Syncfusion.Windows.Forms.Grid.GridVerticalAlignment.Middle;
            gridBaseStyle2.Name = "Standard";
            gridBaseStyle2.StyleInfo.Font.Facename = "Tahoma";
            gridBaseStyle2.StyleInfo.Interior = new Syncfusion.Drawing.BrushInfo(System.Drawing.SystemColors.Window);
            gridBaseStyle3.Name = "Column Header";
            gridBaseStyle3.StyleInfo.BaseStyle = "Header";
            gridBaseStyle3.StyleInfo.HorizontalAlignment = Syncfusion.Windows.Forms.Grid.GridHorizontalAlignment.Center;
            gridBaseStyle4.Name = "Row Header";
            gridBaseStyle4.StyleInfo.BaseStyle = "Header";
            gridBaseStyle4.StyleInfo.HorizontalAlignment = Syncfusion.Windows.Forms.Grid.GridHorizontalAlignment.Left;
            gridBaseStyle4.StyleInfo.Interior = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Horizontal, System.Drawing.Color.FromArgb(((int)(((byte)(203)))), ((int)(((byte)(199)))), ((int)(((byte)(184))))), System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(234)))), ((int)(((byte)(216))))));
            this.dgv.BaseStylesMap.AddRange(new Syncfusion.Windows.Forms.Grid.GridBaseStyle[] {
            gridBaseStyle1,
            gridBaseStyle2,
            gridBaseStyle3,
            gridBaseStyle4});
            this.dgv.ColCount = 3;
            this.dgv.ColWidthEntries.AddRange(new Syncfusion.Windows.Forms.Grid.GridColWidth[] {
            new Syncfusion.Windows.Forms.Grid.GridColWidth(0, 410),
            new Syncfusion.Windows.Forms.Grid.GridColWidth(1, 410),
            new Syncfusion.Windows.Forms.Grid.GridColWidth(2, 410)});
            this.dgv.ControllerOptions = ((Syncfusion.Windows.Forms.Grid.GridControllerOptions)((((((((Syncfusion.Windows.Forms.Grid.GridControllerOptions.ClickCells | Syncfusion.Windows.Forms.Grid.GridControllerOptions.DragSelectRowOrColumn) 
            | Syncfusion.Windows.Forms.Grid.GridControllerOptions.OleDataSource) 
            | Syncfusion.Windows.Forms.Grid.GridControllerOptions.OleDropTarget) 
            | Syncfusion.Windows.Forms.Grid.GridControllerOptions.SelectCells) 
            | Syncfusion.Windows.Forms.Grid.GridControllerOptions.ExcelLikeSelection) 
            | Syncfusion.Windows.Forms.Grid.GridControllerOptions.ResizeCells) 
            | Syncfusion.Windows.Forms.Grid.GridControllerOptions.DragColumnHeader)));
            this.dgv.DefaultGridBorderStyle = Syncfusion.Windows.Forms.Grid.GridBorderStyle.Solid;
            this.dgv.DefaultRowHeight = 20;
            this.dgv.EnableTouchMode = true;
            this.dgv.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.dgv.ForeColor = System.Drawing.SystemColors.ControlText;
            gridCellInfo1.Col = -1;
            gridCellInfo1.Row = -1;
            gridCellInfo1.StyleInfo.Font.Bold = false;
            gridCellInfo1.StyleInfo.Font.Facename = "IRANSansWeb(FaNum)";
            gridCellInfo1.StyleInfo.Font.Italic = false;
            gridCellInfo1.StyleInfo.Font.Size = 9F;
            gridCellInfo1.StyleInfo.Font.Strikeout = false;
            gridCellInfo1.StyleInfo.Font.Underline = false;
            gridCellInfo1.StyleInfo.Font.Unit = System.Drawing.GraphicsUnit.Point;
            gridCellInfo2.Col = 1;
            gridCellInfo2.Row = 1;
            gridCellInfo2.StyleInfo.CultureInfo = new System.Globalization.CultureInfo("fa-IR");
            this.dgv.GridCells.AddRange(new Syncfusion.Windows.Forms.Grid.GridCellInfo[] {
            gridCellInfo1,
            gridCellInfo2});
            this.dgv.GridOfficeScrollBars = Syncfusion.Windows.Forms.OfficeScrollBars.Office2010;
            this.dgv.GridVisualStyles = Syncfusion.Windows.Forms.GridVisualStyles.Metro;
            this.dgv.Location = new System.Drawing.Point(12, 166);
            this.dgv.Name = "dgv";
            this.dgv.Office2007ScrollBars = true;
            this.dgv.Office2010ScrollBarsColorScheme = Syncfusion.Windows.Forms.Office2010ColorScheme.Black;
            this.dgv.Properties.GridLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(212)))), ((int)(((byte)(212)))), ((int)(((byte)(212)))));
            this.dgv.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dgv.RowCount = 5;
            this.dgv.RowHeightEntries.AddRange(new Syncfusion.Windows.Forms.Grid.GridRowHeight[] {
            new Syncfusion.Windows.Forms.Grid.GridRowHeight(0, 29),
            new Syncfusion.Windows.Forms.Grid.GridRowHeight(1, 22),
            new Syncfusion.Windows.Forms.Grid.GridRowHeight(2, 22),
            new Syncfusion.Windows.Forms.Grid.GridRowHeight(3, 22),
            new Syncfusion.Windows.Forms.Grid.GridRowHeight(4, 22)});
            this.dgv.SerializeCellsBehavior = Syncfusion.Windows.Forms.Grid.GridSerializeCellsBehavior.SerializeIntoCode;
            this.dgv.ShowMessageBoxOnDrop = true;
            this.dgv.Size = new System.Drawing.Size(925, 363);
            this.dgv.SmartSizeBox = false;
            this.dgv.TabIndex = 68;
            this.dgv.ThemesEnabled = true;
            this.dgv.UseRightToLeftCompatibleTextBox = true;
            this.dgv.CellClick += new Syncfusion.Windows.Forms.Grid.GridCellClickEventHandler(this.dgv_CellClick);
            // 
            // picEntekhabZan
            // 
            this.picEntekhabZan.Cursor = System.Windows.Forms.Cursors.Default;
            this.picEntekhabZan.EditValue = global::Test.Properties.Resources.entekhab_zan;
            this.picEntekhabZan.Location = new System.Drawing.Point(997, 307);
            this.picEntekhabZan.MenuManager = this.ribbon;
            this.picEntekhabZan.Name = "picEntekhabZan";
            this.picEntekhabZan.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picEntekhabZan.Properties.Appearance.Options.UseBackColor = true;
            this.picEntekhabZan.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.picEntekhabZan.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picEntekhabZan.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picEntekhabZan.Properties.ZoomAccelerationFactor = 1D;
            this.picEntekhabZan.Size = new System.Drawing.Size(51, 64);
            this.picEntekhabZan.StyleController = this.dataLayoutControl1;
            this.picEntekhabZan.TabIndex = 66;
            // 
            // picEntekhabMard
            // 
            this.picEntekhabMard.Cursor = System.Windows.Forms.Cursors.Default;
            this.picEntekhabMard.EditValue = global::Test.Properties.Resources.entekhab_mard;
            this.picEntekhabMard.Location = new System.Drawing.Point(1052, 307);
            this.picEntekhabMard.MenuManager = this.ribbon;
            this.picEntekhabMard.Name = "picEntekhabMard";
            this.picEntekhabMard.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picEntekhabMard.Properties.Appearance.Options.UseBackColor = true;
            this.picEntekhabMard.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.picEntekhabMard.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picEntekhabMard.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picEntekhabMard.Properties.ZoomAccelerationFactor = 1D;
            this.picEntekhabMard.Size = new System.Drawing.Size(54, 64);
            this.picEntekhabMard.StyleController = this.dataLayoutControl1;
            this.picEntekhabMard.TabIndex = 65;
            // 
            // picRezervZan
            // 
            this.picRezervZan.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRezervZan.EditValue = global::Test.Properties.Resources.rezerv_zan;
            this.picRezervZan.Location = new System.Drawing.Point(997, 235);
            this.picRezervZan.MenuManager = this.ribbon;
            this.picRezervZan.Name = "picRezervZan";
            this.picRezervZan.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picRezervZan.Properties.Appearance.Options.UseBackColor = true;
            this.picRezervZan.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.picRezervZan.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picRezervZan.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picRezervZan.Properties.ZoomAccelerationFactor = 1D;
            this.picRezervZan.Size = new System.Drawing.Size(51, 68);
            this.picRezervZan.StyleController = this.dataLayoutControl1;
            this.picRezervZan.TabIndex = 64;
            // 
            // picRezerMard
            // 
            this.picRezerMard.Cursor = System.Windows.Forms.Cursors.Default;
            this.picRezerMard.EditValue = global::Test.Properties.Resources.rezerv_mard;
            this.picRezerMard.Location = new System.Drawing.Point(1052, 235);
            this.picRezerMard.MenuManager = this.ribbon;
            this.picRezerMard.Name = "picRezerMard";
            this.picRezerMard.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picRezerMard.Properties.Appearance.Options.UseBackColor = true;
            this.picRezerMard.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.picRezerMard.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picRezerMard.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picRezerMard.Properties.ZoomAccelerationFactor = 1D;
            this.picRezerMard.Size = new System.Drawing.Size(52, 68);
            this.picRezerMard.StyleController = this.dataLayoutControl1;
            this.picRezerMard.TabIndex = 63;
            // 
            // picForoshZan
            // 
            this.picForoshZan.Cursor = System.Windows.Forms.Cursors.Default;
            this.picForoshZan.EditValue = global::Test.Properties.Resources.frosh_zan;
            this.picForoshZan.Location = new System.Drawing.Point(997, 166);
            this.picForoshZan.MenuManager = this.ribbon;
            this.picForoshZan.Name = "picForoshZan";
            this.picForoshZan.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picForoshZan.Properties.Appearance.Options.UseBackColor = true;
            this.picForoshZan.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.picForoshZan.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picForoshZan.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picForoshZan.Properties.ZoomAccelerationFactor = 1D;
            this.picForoshZan.Size = new System.Drawing.Size(51, 65);
            this.picForoshZan.StyleController = this.dataLayoutControl1;
            this.picForoshZan.TabIndex = 62;
            // 
            // picForoshMard
            // 
            this.picForoshMard.Cursor = System.Windows.Forms.Cursors.Default;
            this.picForoshMard.EditValue = global::Test.Properties.Resources.frosh_mard;
            this.picForoshMard.Location = new System.Drawing.Point(1052, 166);
            this.picForoshMard.MenuManager = this.ribbon;
            this.picForoshMard.Name = "picForoshMard";
            this.picForoshMard.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.picForoshMard.Properties.Appearance.Options.UseBackColor = true;
            this.picForoshMard.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.UltraFlat;
            this.picForoshMard.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.Auto;
            this.picForoshMard.Properties.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Stretch;
            this.picForoshMard.Properties.ZoomAccelerationFactor = 1D;
            this.picForoshMard.Size = new System.Drawing.Size(52, 65);
            this.picForoshMard.StyleController = this.dataLayoutControl1;
            this.picForoshMard.TabIndex = 61;
            // 
            // txtSumPrice
            // 
            this.txtSumPrice.Enabled = false;
            this.txtSumPrice.Location = new System.Drawing.Point(12, 72);
            this.txtSumPrice.MenuManager = this.ribbon;
            this.txtSumPrice.Name = "txtSumPrice";
            this.txtSumPrice.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.txtSumPrice.Properties.Appearance.Options.UseFont = true;
            this.txtSumPrice.Properties.Appearance.Options.UseTextOptions = true;
            this.txtSumPrice.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtSumPrice.Properties.DisplayFormat.FormatString = "c0";
            this.txtSumPrice.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSumPrice.Properties.EditFormat.FormatString = "c0";
            this.txtSumPrice.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtSumPrice.Properties.Mask.EditMask = "c0";
            this.txtSumPrice.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtSumPrice.Properties.ReadOnly = true;
            this.txtSumPrice.Size = new System.Drawing.Size(520, 26);
            this.txtSumPrice.StyleController = this.dataLayoutControl1;
            this.txtSumPrice.TabIndex = 60;
            // 
            // txtEmptyChair
            // 
            this.txtEmptyChair.Enabled = false;
            this.txtEmptyChair.Location = new System.Drawing.Point(12, 12);
            this.txtEmptyChair.MenuManager = this.ribbon;
            this.txtEmptyChair.Name = "txtEmptyChair";
            this.txtEmptyChair.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.txtEmptyChair.Properties.Appearance.Options.UseFont = true;
            this.txtEmptyChair.Properties.Appearance.Options.UseTextOptions = true;
            this.txtEmptyChair.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtEmptyChair.Properties.ReadOnly = true;
            this.txtEmptyChair.Size = new System.Drawing.Size(62, 26);
            this.txtEmptyChair.StyleController = this.dataLayoutControl1;
            this.txtEmptyChair.TabIndex = 59;
            // 
            // txtRezervChair
            // 
            this.txtRezervChair.Enabled = false;
            this.txtRezervChair.Location = new System.Drawing.Point(106, 12);
            this.txtRezervChair.MenuManager = this.ribbon;
            this.txtRezervChair.Name = "txtRezervChair";
            this.txtRezervChair.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.txtRezervChair.Properties.Appearance.Options.UseFont = true;
            this.txtRezervChair.Properties.Appearance.Options.UseTextOptions = true;
            this.txtRezervChair.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtRezervChair.Properties.ReadOnly = true;
            this.txtRezervChair.Size = new System.Drawing.Size(59, 26);
            this.txtRezervChair.StyleController = this.dataLayoutControl1;
            this.txtRezervChair.TabIndex = 58;
            // 
            // txtFullChair
            // 
            this.txtFullChair.Enabled = false;
            this.txtFullChair.Location = new System.Drawing.Point(192, 12);
            this.txtFullChair.MenuManager = this.ribbon;
            this.txtFullChair.Name = "txtFullChair";
            this.txtFullChair.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.txtFullChair.Properties.Appearance.Options.UseFont = true;
            this.txtFullChair.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFullChair.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFullChair.Properties.ReadOnly = true;
            this.txtFullChair.Size = new System.Drawing.Size(65, 26);
            this.txtFullChair.StyleController = this.dataLayoutControl1;
            this.txtFullChair.TabIndex = 57;
            // 
            // txtCountChair
            // 
            this.txtCountChair.Enabled = false;
            this.txtCountChair.Location = new System.Drawing.Point(275, 12);
            this.txtCountChair.MenuManager = this.ribbon;
            this.txtCountChair.Name = "txtCountChair";
            this.txtCountChair.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.txtCountChair.Properties.Appearance.Options.UseFont = true;
            this.txtCountChair.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCountChair.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtCountChair.Properties.ReadOnly = true;
            this.txtCountChair.Size = new System.Drawing.Size(69, 26);
            this.txtCountChair.StyleController = this.dataLayoutControl1;
            this.txtCountChair.TabIndex = 56;
            // 
            // CreditUsertxt
            // 
            this.CreditUsertxt.EditValue = "";
            this.CreditUsertxt.Location = new System.Drawing.Point(913, 42);
            this.CreditUsertxt.MenuManager = this.ribbon;
            this.CreditUsertxt.Name = "CreditUsertxt";
            this.CreditUsertxt.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.CreditUsertxt.Properties.Appearance.Options.UseFont = true;
            this.CreditUsertxt.Properties.Appearance.Options.UseTextOptions = true;
            this.CreditUsertxt.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CreditUsertxt.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.CreditUsertxt.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.CreditUsertxt.Properties.DisplayFormat.FormatString = "c0";
            this.CreditUsertxt.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CreditUsertxt.Properties.EditFormat.FormatString = "c0";
            this.CreditUsertxt.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CreditUsertxt.Properties.Mask.EditMask = "c0";
            this.CreditUsertxt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CreditUsertxt.Properties.ReadOnly = true;
            this.CreditUsertxt.Size = new System.Drawing.Size(205, 26);
            this.CreditUsertxt.StyleController = this.dataLayoutControl1;
            this.CreditUsertxt.TabIndex = 55;
            // 
            // txtStatusPayment
            // 
            this.txtStatusPayment.EditValue = "وضعیت پرداخت کارتخوان";
            this.txtStatusPayment.Enabled = false;
            this.txtStatusPayment.Location = new System.Drawing.Point(12, 533);
            this.txtStatusPayment.MenuManager = this.ribbon;
            this.txtStatusPayment.Name = "txtStatusPayment";
            this.txtStatusPayment.Properties.Appearance.BackColor = System.Drawing.Color.Gainsboro;
            this.txtStatusPayment.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.txtStatusPayment.Properties.Appearance.ForeColor = System.Drawing.Color.Green;
            this.txtStatusPayment.Properties.Appearance.Options.UseBackColor = true;
            this.txtStatusPayment.Properties.Appearance.Options.UseFont = true;
            this.txtStatusPayment.Properties.Appearance.Options.UseForeColor = true;
            this.txtStatusPayment.Properties.Appearance.Options.UseTextOptions = true;
            this.txtStatusPayment.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtStatusPayment.Size = new System.Drawing.Size(1092, 26);
            this.txtStatusPayment.StyleController = this.dataLayoutControl1;
            this.txtStatusPayment.TabIndex = 30;
            // 
            // NoTextEdit
            // 
            this.NoTextEdit.Location = new System.Drawing.Point(423, 132);
            this.NoTextEdit.MenuManager = this.ribbon;
            this.NoTextEdit.Name = "NoTextEdit";
            this.NoTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.NoTextEdit.Properties.Appearance.Options.UseFont = true;
            this.NoTextEdit.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.NoTextEdit.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.NoTextEdit.Properties.ReadOnly = true;
            this.NoTextEdit.Size = new System.Drawing.Size(153, 26);
            this.NoTextEdit.StyleController = this.dataLayoutControl1;
            this.NoTextEdit.TabIndex = 54;
            this.NoTextEdit.Validated += new System.EventHandler(this.TripIDTextEdit_Validated);
            // 
            // txtWay
            // 
            this.txtWay.EditValue = "";
            this.txtWay.Location = new System.Drawing.Point(366, 12);
            this.txtWay.MenuManager = this.ribbon;
            this.txtWay.Name = "txtWay";
            this.txtWay.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.txtWay.Properties.Appearance.Options.UseFont = true;
            this.txtWay.Properties.Appearance.Options.UseTextOptions = true;
            this.txtWay.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtWay.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.LightSkyBlue;
            this.txtWay.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.White;
            this.txtWay.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtWay.Properties.AppearanceReadOnly.Options.UseForeColor = true;
            this.txtWay.Properties.ReadOnly = true;
            this.txtWay.Size = new System.Drawing.Size(823, 26);
            this.txtWay.StyleController = this.dataLayoutControl1;
            this.txtWay.TabIndex = 52;
            // 
            // TripIDTextEdit
            // 
            this.TripIDTextEdit.Enabled = false;
            this.TripIDTextEdit.EnterMoveNextControl = true;
            this.TripIDTextEdit.Location = new System.Drawing.Point(12, 132);
            this.TripIDTextEdit.MenuManager = this.ribbon;
            this.TripIDTextEdit.Name = "TripIDTextEdit";
            this.TripIDTextEdit.Properties.Appearance.BackColor = System.Drawing.Color.White;
            this.TripIDTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.TripIDTextEdit.Properties.Appearance.Options.UseBackColor = true;
            this.TripIDTextEdit.Properties.Appearance.Options.UseFont = true;
            this.TripIDTextEdit.Properties.Mask.EditMask = "N0";
            this.TripIDTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.TripIDTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.TripIDTextEdit.Size = new System.Drawing.Size(127, 26);
            this.TripIDTextEdit.StyleController = this.dataLayoutControl1;
            this.TripIDTextEdit.TabIndex = 27;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.TripIDTextEdit, conditionValidationRule1);
            this.TripIDTextEdit.Validated += new System.EventHandler(this.TripIDTextEdit_Validated);
            // 
            // FullnameTextEdit
            // 
            this.FullnameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ticketBindingSource, "Fullname", true));
            this.FullnameTextEdit.EnterMoveNextControl = true;
            this.FullnameTextEdit.Location = new System.Drawing.Point(585, 42);
            this.FullnameTextEdit.MenuManager = this.ribbon;
            this.FullnameTextEdit.Name = "FullnameTextEdit";
            this.FullnameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.FullnameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.FullnameTextEdit.Size = new System.Drawing.Size(266, 26);
            this.FullnameTextEdit.StyleController = this.dataLayoutControl1;
            this.FullnameTextEdit.TabIndex = 30;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "لطفا نام مسافر را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.FullnameTextEdit, conditionValidationRule2);
            this.FullnameTextEdit.EditValueChanged += new System.EventHandler(this.FullnameTextEdit_EditValueChanged);
            this.FullnameTextEdit.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.FullnameTextEdit_EditValueChanging);
            this.FullnameTextEdit.Enter += new System.EventHandler(this.FullnameTextEdit_Enter);
            this.FullnameTextEdit.Leave += new System.EventHandler(this.FullnameTextEdit_Leave);
            // 
            // ticketBindingSource
            // 
            this.ticketBindingSource.DataSource = typeof(Model.Ticket);
            // 
            // ChairsTextEdit
            // 
            this.ChairsTextEdit.Enabled = false;
            this.ChairsTextEdit.EnterMoveNextControl = true;
            this.ChairsTextEdit.Location = new System.Drawing.Point(584, 102);
            this.ChairsTextEdit.MenuManager = this.ribbon;
            this.ChairsTextEdit.Name = "ChairsTextEdit";
            this.ChairsTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ChairsTextEdit.Properties.Appearance.Options.UseFont = true;
            this.ChairsTextEdit.Properties.ReadOnly = true;
            this.ChairsTextEdit.Size = new System.Drawing.Size(544, 26);
            this.ChairsTextEdit.StyleController = this.dataLayoutControl1;
            this.ChairsTextEdit.TabIndex = 32;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.ChairsTextEdit, conditionValidationRule3);
            this.ChairsTextEdit.Validated += new System.EventHandler(this.TripIDTextEdit_Validated);
            // 
            // TelTextEdit
            // 
            this.TelTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ticketBindingSource, "Tel", true));
            this.TelTextEdit.EnterMoveNextControl = true;
            this.TelTextEdit.Location = new System.Drawing.Point(347, 42);
            this.TelTextEdit.MenuManager = this.ribbon;
            this.TelTextEdit.Name = "TelTextEdit";
            this.TelTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.TelTextEdit.Properties.Appearance.Options.UseFont = true;
            this.TelTextEdit.Size = new System.Drawing.Size(186, 26);
            this.TelTextEdit.StyleController = this.dataLayoutControl1;
            this.TelTextEdit.TabIndex = 38;
            this.TelTextEdit.Validated += new System.EventHandler(this.TripIDTextEdit_Validated);
            // 
            // PriceTextEdit
            // 
            this.PriceTextEdit.Enabled = false;
            this.PriceTextEdit.EnterMoveNextControl = true;
            this.PriceTextEdit.Location = new System.Drawing.Point(912, 72);
            this.PriceTextEdit.MenuManager = this.ribbon;
            this.PriceTextEdit.Name = "PriceTextEdit";
            this.PriceTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.PriceTextEdit.Properties.Appearance.Options.UseFont = true;
            this.PriceTextEdit.Properties.DisplayFormat.FormatString = "c0";
            this.PriceTextEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PriceTextEdit.Properties.EditFormat.FormatString = "c0";
            this.PriceTextEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.PriceTextEdit.Properties.Mask.EditMask = "c0";
            this.PriceTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.PriceTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.PriceTextEdit.Properties.ReadOnly = true;
            this.PriceTextEdit.Size = new System.Drawing.Size(206, 26);
            this.PriceTextEdit.StyleController = this.dataLayoutControl1;
            this.PriceTextEdit.TabIndex = 39;
            conditionValidationRule4.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule4.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.PriceTextEdit, conditionValidationRule4);
            this.PriceTextEdit.EditValueChanged += new System.EventHandler(this.PriceTextEdit_EditValueChanged);
            this.PriceTextEdit.Validated += new System.EventHandler(this.TripIDTextEdit_Validated);
            // 
            // DiscountTextEdit
            // 
            this.DiscountTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ticketBindingSource, "Discount", true));
            this.DiscountTextEdit.EnterMoveNextControl = true;
            this.DiscountTextEdit.Location = new System.Drawing.Point(584, 72);
            this.DiscountTextEdit.MenuManager = this.ribbon;
            this.DiscountTextEdit.Name = "DiscountTextEdit";
            this.DiscountTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.DiscountTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.DiscountTextEdit.Properties.Appearance.Options.UseFont = true;
            this.DiscountTextEdit.Properties.Mask.EditMask = "N0";
            this.DiscountTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.DiscountTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.DiscountTextEdit.Size = new System.Drawing.Size(266, 26);
            this.DiscountTextEdit.StyleController = this.dataLayoutControl1;
            this.DiscountTextEdit.TabIndex = 42;
            this.DiscountTextEdit.ToolTip = "تخفیف را محاسبه کنید  Enter با فشردن کلید";
            this.DiscountTextEdit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.DiscountTextEdit_KeyDown);
            this.DiscountTextEdit.Leave += new System.EventHandler(this.DiscountTextEdit_Leave);
            // 
            // NumChairsTextEdit
            // 
            this.NumChairsTextEdit.EnterMoveNextControl = true;
            this.NumChairsTextEdit.Location = new System.Drawing.Point(12, 42);
            this.NumChairsTextEdit.MenuManager = this.ribbon;
            this.NumChairsTextEdit.Name = "NumChairsTextEdit";
            this.NumChairsTextEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.NumChairsTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.NumChairsTextEdit.Properties.Appearance.Options.UseFont = true;
            this.NumChairsTextEdit.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.NumChairsTextEdit.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.NumChairsTextEdit.Properties.Mask.EditMask = "N0";
            this.NumChairsTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.NumChairsTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.NumChairsTextEdit.Properties.ReadOnly = true;
            this.NumChairsTextEdit.Size = new System.Drawing.Size(67, 26);
            this.NumChairsTextEdit.StyleController = this.dataLayoutControl1;
            this.NumChairsTextEdit.TabIndex = 45;
            // 
            // PayDateDateEdit
            // 
            this.PayDateDateEdit.EditValue = null;
            this.PayDateDateEdit.EnterMoveNextControl = true;
            this.PayDateDateEdit.Location = new System.Drawing.Point(712, 132);
            this.PayDateDateEdit.MenuManager = this.ribbon;
            this.PayDateDateEdit.Name = "PayDateDateEdit";
            this.PayDateDateEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.PayDateDateEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.PayDateDateEdit.Properties.Appearance.Options.UseFont = true;
            this.PayDateDateEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.PayDateDateEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PayDateDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PayDateDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PayDateDateEdit.Size = new System.Drawing.Size(417, 26);
            this.PayDateDateEdit.StyleController = this.dataLayoutControl1;
            this.PayDateDateEdit.TabIndex = 44;
            // 
            // PrintedCheckEdit
            // 
            this.PrintedCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ticketBindingSource, "Printed", true));
            this.PrintedCheckEdit.Enabled = false;
            this.PrintedCheckEdit.Location = new System.Drawing.Point(196, 132);
            this.PrintedCheckEdit.MenuManager = this.ribbon;
            this.PrintedCheckEdit.Name = "PrintedCheckEdit";
            this.PrintedCheckEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.PrintedCheckEdit.Properties.Appearance.Options.UseFont = true;
            this.PrintedCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.PrintedCheckEdit.Properties.OffText = "چاپ نشده";
            this.PrintedCheckEdit.Properties.OnText = "چاپ شده است";
            this.PrintedCheckEdit.Size = new System.Drawing.Size(193, 30);
            this.PrintedCheckEdit.StyleController = this.dataLayoutControl1;
            this.PrintedCheckEdit.TabIndex = 47;
            // 
            // CityIDTextEdit
            // 
            this.CityIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.ticketBindingSource, "CityID", true));
            this.CityIDTextEdit.EnterMoveNextControl = true;
            this.CityIDTextEdit.Location = new System.Drawing.Point(156, 42);
            this.CityIDTextEdit.MenuManager = this.ribbon;
            this.CityIDTextEdit.Name = "CityIDTextEdit";
            this.CityIDTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.CityIDTextEdit.Properties.Appearance.Options.UseFont = true;
            this.CityIDTextEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.CityIDTextEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.CityIDTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.CityIDTextEdit.Properties.DataSource = this.citiesServicesBindingSource;
            this.CityIDTextEdit.Properties.DisplayMember = "Title";
            this.CityIDTextEdit.Properties.NullText = "";
            this.CityIDTextEdit.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.CityIDTextEdit.Properties.ValueMember = "ID";
            this.CityIDTextEdit.Properties.View = this.grdCitiesServices;
            this.CityIDTextEdit.Size = new System.Drawing.Size(147, 26);
            this.CityIDTextEdit.StyleController = this.dataLayoutControl1;
            this.CityIDTextEdit.TabIndex = 33;
            conditionValidationRule5.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule5.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.CityIDTextEdit, conditionValidationRule5);
            this.CityIDTextEdit.EditValueChanged += new System.EventHandler(this.CityIDTextEdit_EditValueChanged);
            this.CityIDTextEdit.Validated += new System.EventHandler(this.TripIDTextEdit_Validated);
            // 
            // citiesServicesBindingSource
            // 
            this.citiesServicesBindingSource.DataSource = typeof(Model.CitiesServices);
            // 
            // grdCitiesServices
            // 
            this.grdCitiesServices.Appearance.FocusedCell.BackColor = System.Drawing.Color.DodgerBlue;
            this.grdCitiesServices.Appearance.FocusedCell.ForeColor = System.Drawing.Color.White;
            this.grdCitiesServices.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grdCitiesServices.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grdCitiesServices.Appearance.FocusedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grdCitiesServices.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grdCitiesServices.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grdCitiesServices.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grdCitiesServices.Appearance.SelectedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grdCitiesServices.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grdCitiesServices.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grdCitiesServices.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grdCitiesServices.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colTitle,
            this.colPrice,
            this.colCode});
            this.grdCitiesServices.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdCitiesServices.Name = "grdCitiesServices";
            this.grdCitiesServices.OptionsBehavior.Editable = false;
            this.grdCitiesServices.OptionsFind.AlwaysVisible = true;
            this.grdCitiesServices.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdCitiesServices.OptionsView.ShowGroupPanel = false;
            // 
            // colID
            // 
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F);
            this.colID.AppearanceHeader.Options.UseFont = true;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.Caption = "#";
            this.colID.FieldName = "ID";
            this.colID.MaxWidth = 40;
            this.colID.Name = "colID";
            this.colID.Visible = true;
            this.colID.VisibleIndex = 0;
            this.colID.Width = 40;
            // 
            // colTitle
            // 
            this.colTitle.AppearanceCell.Options.UseTextOptions = true;
            this.colTitle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTitle.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F);
            this.colTitle.AppearanceHeader.Options.UseFont = true;
            this.colTitle.AppearanceHeader.Options.UseTextOptions = true;
            this.colTitle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTitle.Caption = "مقصد";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 1;
            this.colTitle.Width = 172;
            // 
            // colPrice
            // 
            this.colPrice.AppearanceCell.Options.UseTextOptions = true;
            this.colPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.AppearanceHeader.Font = new System.Drawing.Font("Tahoma", 9F);
            this.colPrice.AppearanceHeader.Options.UseFont = true;
            this.colPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.Caption = "مبلغ";
            this.colPrice.DisplayFormat.FormatString = "c0";
            this.colPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrice.FieldName = "Price";
            this.colPrice.Name = "colPrice";
            this.colPrice.Visible = true;
            this.colPrice.VisibleIndex = 2;
            this.colPrice.Width = 172;
            // 
            // colCode
            // 
            this.colCode.AppearanceCell.Options.UseTextOptions = true;
            this.colCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCode.Caption = "کد شهر";
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            this.colCode.Visible = true;
            this.colCode.VisibleIndex = 3;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlItem8,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem9});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1201, 571);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForSaleDate,
            this.ItemForDiscount,
            this.layoutControlItem3,
            this.layoutControlItem2,
            this.ItemForPrinted,
            this.ItemForTripID,
            this.layoutControlItem4,
            this.emptySpaceItem1,
            this.layoutControlItem10,
            this.ItemForChairs,
            this.emptySpaceItem3,
            this.emptySpaceItem5,
            this.ItemForPrice,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.ItemForFullname,
            this.ItemForTel,
            this.ItemForCityID,
            this.ItemForNumChairs,
            this.layoutControlItem1});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 30);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1181, 521);
            // 
            // ItemForSaleDate
            // 
            this.ItemForSaleDate.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ItemForSaleDate.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForSaleDate.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForSaleDate.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForSaleDate.Control = this.PayDateDateEdit;
            this.ItemForSaleDate.Location = new System.Drawing.Point(700, 90);
            this.ItemForSaleDate.Name = "ItemForSaleDate";
            this.ItemForSaleDate.Size = new System.Drawing.Size(481, 34);
            this.ItemForSaleDate.Text = "تاریخ بلیط :";
            this.ItemForSaleDate.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForSaleDate.TextSize = new System.Drawing.Size(55, 20);
            this.ItemForSaleDate.TextToControlDistance = 5;
            this.ItemForSaleDate.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // ItemForDiscount
            // 
            this.ItemForDiscount.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ItemForDiscount.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForDiscount.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForDiscount.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForDiscount.Control = this.DiscountTextEdit;
            this.ItemForDiscount.Location = new System.Drawing.Point(572, 30);
            this.ItemForDiscount.Name = "ItemForDiscount";
            this.ItemForDiscount.Size = new System.Drawing.Size(328, 30);
            this.ItemForDiscount.Text = "تخفیف :";
            this.ItemForDiscount.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ItemForDiscount.TextSize = new System.Drawing.Size(53, 20);
            this.ItemForDiscount.TextToControlDistance = 5;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.layoutControlItem3.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem3.Control = this.txtStatusPayment;
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 491);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1181, 30);
            this.layoutControlItem3.Text = "وضعیت پرداخت ";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(82, 20);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.layoutControlItem2.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem2.Control = this.NoTextEdit;
            this.layoutControlItem2.Location = new System.Drawing.Point(411, 90);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(289, 34);
            this.layoutControlItem2.Text = "شماره بلیط با فرمت صندلی";
            this.layoutControlItem2.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(127, 20);
            this.layoutControlItem2.TextToControlDistance = 5;
            this.layoutControlItem2.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // ItemForPrinted
            // 
            this.ItemForPrinted.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ItemForPrinted.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForPrinted.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForPrinted.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForPrinted.Control = this.PrintedCheckEdit;
            this.ItemForPrinted.Location = new System.Drawing.Point(184, 90);
            this.ItemForPrinted.Name = "ItemForPrinted";
            this.ItemForPrinted.Size = new System.Drawing.Size(227, 34);
            this.ItemForPrinted.Text = "چاپ:";
            this.ItemForPrinted.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForPrinted.TextSize = new System.Drawing.Size(25, 20);
            this.ItemForPrinted.TextToControlDistance = 5;
            this.ItemForPrinted.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // ItemForTripID
            // 
            this.ItemForTripID.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 9F);
            this.ItemForTripID.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForTripID.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForTripID.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForTripID.Control = this.TripIDTextEdit;
            this.ItemForTripID.Location = new System.Drawing.Point(0, 90);
            this.ItemForTripID.Name = "ItemForTripID";
            this.ItemForTripID.Size = new System.Drawing.Size(184, 34);
            this.ItemForTripID.Text = "کد سفر :";
            this.ItemForTripID.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForTripID.TextSize = new System.Drawing.Size(48, 14);
            this.ItemForTripID.TextToControlDistance = 5;
            this.ItemForTripID.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.layoutControlItem4.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem4.Control = this.CreditUsertxt;
            this.layoutControlItem4.Location = new System.Drawing.Point(901, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(280, 30);
            this.layoutControlItem4.Text = "اعتبار حساب :";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(66, 20);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.Location = new System.Drawing.Point(985, 333);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(196, 158);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.layoutControlItem10.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem10.Control = this.txtSumPrice;
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 30);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(572, 30);
            this.layoutControlItem10.Text = "جمع کل :";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(43, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // ItemForChairs
            // 
            this.ItemForChairs.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ItemForChairs.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForChairs.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForChairs.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForChairs.Control = this.ChairsTextEdit;
            this.ItemForChairs.Location = new System.Drawing.Point(572, 60);
            this.ItemForChairs.Name = "ItemForChairs";
            this.ItemForChairs.Size = new System.Drawing.Size(609, 30);
            this.ItemForChairs.Text = " صندلی ها :";
            this.ItemForChairs.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForChairs.TextSize = new System.Drawing.Size(56, 20);
            this.ItemForChairs.TextToControlDistance = 5;
            this.ItemForChairs.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 60);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(572, 30);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(929, 124);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(56, 367);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // ItemForPrice
            // 
            this.ItemForPrice.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ItemForPrice.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForPrice.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForPrice.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForPrice.Control = this.PriceTextEdit;
            this.ItemForPrice.Location = new System.Drawing.Point(900, 30);
            this.ItemForPrice.Name = "ItemForPrice";
            this.ItemForPrice.Size = new System.Drawing.Size(281, 30);
            this.ItemForPrice.Text = " قیمت بلیط :";
            this.ItemForPrice.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ItemForPrice.TextSize = new System.Drawing.Size(66, 20);
            this.ItemForPrice.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem11.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem11.Control = this.picForoshMard;
            this.layoutControlItem11.Location = new System.Drawing.Point(1040, 124);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(141, 69);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(141, 69);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(141, 69);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "  فروخته شده ";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(82, 24);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.picForoshZan;
            this.layoutControlItem12.Location = new System.Drawing.Point(985, 124);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(55, 69);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(55, 69);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(55, 69);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem13.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem13.Control = this.picRezerMard;
            this.layoutControlItem13.Location = new System.Drawing.Point(1040, 193);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(141, 72);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(141, 72);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(141, 72);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "  رزرو شده      ";
            this.layoutControlItem13.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem13.TextSize = new System.Drawing.Size(80, 24);
            this.layoutControlItem13.TextToControlDistance = 5;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.picRezervZan;
            this.layoutControlItem14.Location = new System.Drawing.Point(985, 193);
            this.layoutControlItem14.MaxSize = new System.Drawing.Size(55, 72);
            this.layoutControlItem14.MinSize = new System.Drawing.Size(55, 72);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(55, 72);
            this.layoutControlItem14.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem15.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem15.Control = this.picEntekhabMard;
            this.layoutControlItem15.Location = new System.Drawing.Point(1040, 265);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(141, 68);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(141, 68);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(141, 68);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "  انتخاب شده ";
            this.layoutControlItem15.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem15.TextSize = new System.Drawing.Size(78, 24);
            this.layoutControlItem15.TextToControlDistance = 5;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.picEntekhabZan;
            this.layoutControlItem16.Location = new System.Drawing.Point(985, 265);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(55, 68);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(55, 68);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(55, 68);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // ItemForFullname
            // 
            this.ItemForFullname.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ItemForFullname.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForFullname.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForFullname.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForFullname.Control = this.FullnameTextEdit;
            this.ItemForFullname.Location = new System.Drawing.Point(573, 0);
            this.ItemForFullname.Name = "ItemForFullname";
            this.ItemForFullname.Size = new System.Drawing.Size(328, 30);
            this.ItemForFullname.Text = " نام مسافر :";
            this.ItemForFullname.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForFullname.TextSize = new System.Drawing.Size(53, 20);
            this.ItemForFullname.TextToControlDistance = 5;
            // 
            // ItemForTel
            // 
            this.ItemForTel.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ItemForTel.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForTel.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForTel.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForTel.Control = this.TelTextEdit;
            this.ItemForTel.Location = new System.Drawing.Point(335, 0);
            this.ItemForTel.Name = "ItemForTel";
            this.ItemForTel.Size = new System.Drawing.Size(238, 30);
            this.ItemForTel.Text = " موبایل :";
            this.ItemForTel.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.ItemForTel.TextSize = new System.Drawing.Size(43, 20);
            this.ItemForTel.TextToControlDistance = 5;
            // 
            // ItemForCityID
            // 
            this.ItemForCityID.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ItemForCityID.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForCityID.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForCityID.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForCityID.Control = this.CityIDTextEdit;
            this.ItemForCityID.Location = new System.Drawing.Point(144, 0);
            this.ItemForCityID.Name = "ItemForCityID";
            this.ItemForCityID.Size = new System.Drawing.Size(191, 30);
            this.ItemForCityID.Text = "مقصد :";
            this.ItemForCityID.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForCityID.TextSize = new System.Drawing.Size(35, 20);
            this.ItemForCityID.TextToControlDistance = 5;
            // 
            // ItemForNumChairs
            // 
            this.ItemForNumChairs.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.ItemForNumChairs.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForNumChairs.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForNumChairs.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForNumChairs.Control = this.NumChairsTextEdit;
            this.ItemForNumChairs.Location = new System.Drawing.Point(0, 0);
            this.ItemForNumChairs.Name = "ItemForNumChairs";
            this.ItemForNumChairs.Size = new System.Drawing.Size(144, 30);
            this.ItemForNumChairs.Text = "تعداد صندلی :";
            this.ItemForNumChairs.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForNumChairs.TextSize = new System.Drawing.Size(68, 20);
            this.ItemForNumChairs.TextToControlDistance = 5;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.dgv;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 124);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(929, 367);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtWay;
            this.layoutControlItem8.Location = new System.Drawing.Point(354, 0);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(827, 30);
            this.layoutControlItem8.Text = " ";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txtCountChair;
            this.layoutControlItem5.Location = new System.Drawing.Point(263, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(91, 30);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(91, 30);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(91, 30);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "کل";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(13, 20);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.layoutControlItem6.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem6.Control = this.txtFullChair;
            this.layoutControlItem6.Location = new System.Drawing.Point(180, 0);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(83, 30);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(83, 30);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(83, 30);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "پر";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(9, 20);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.layoutControlItem7.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem7.Control = this.txtRezervChair;
            this.layoutControlItem7.Location = new System.Drawing.Point(94, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(86, 30);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(86, 30);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(86, 30);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "رزرو";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(18, 20);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.layoutControlItem9.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem9.Control = this.txtEmptyChair;
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(94, 30);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(94, 30);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(94, 30);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "خالی";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(23, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // BgPose
            // 
            this.BgPose.WorkerReportsProgress = true;
            this.BgPose.WorkerSupportsCancellation = true;
            this.BgPose.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BgPose_DoWork);
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // timerGraphic
            // 
            this.timerGraphic.Enabled = true;
            this.timerGraphic.Interval = 1000;
            this.timerGraphic.Tick += new System.EventHandler(this.timerGraphic_Tick);
            // 
            // FrmTickets
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1201, 745);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Name = "FrmTickets";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "فروش بلیط";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmTickets_FormClosing);
            this.Load += new System.EventHandler(this.FrmTickets_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEntekhabZan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picEntekhabMard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRezervZan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picRezerMard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picForoshZan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picForoshMard.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSumPrice.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmptyChair.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRezervChair.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFullChair.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCountChair.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditUsertxt.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusPayment.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtWay.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TripIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FullnameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChairsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TelTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PriceTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DiscountTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumChairsTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayDateDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PayDateDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrintedCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CityIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.citiesServicesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdCitiesServices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForSaleDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDiscount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrinted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTripID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForChairs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFullname)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCityID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNumChairs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private System.Windows.Forms.BindingSource ticketBindingSource;
        private DevExpress.XtraEditors.TextEdit TripIDTextEdit;
        private DevExpress.XtraEditors.TextEdit FullnameTextEdit;
        private DevExpress.XtraEditors.TextEdit ChairsTextEdit;
        private DevExpress.XtraEditors.TextEdit TelTextEdit;
        private DevExpress.XtraEditors.TextEdit PriceTextEdit;
        private DevExpress.XtraEditors.TextEdit DiscountTextEdit;
        private DevExpress.XtraEditors.TextEdit NumChairsTextEdit;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTripID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFullname;
        private DevExpress.XtraLayout.LayoutControlItem ItemForChairs;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCityID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTel;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrice;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDiscount;
        private DevExpress.XtraLayout.LayoutControlItem ItemForSaleDate;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNumChairs;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrinted;
        private FarsiLibrary.Win.DevExpress.XtraFADateEdit PayDateDateEdit;
        private DevExpress.XtraBars.BarButtonItem btnSave;
        private DevExpress.XtraBars.BarButtonItem btnEmpty;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        //private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraEditors.ToggleSwitch PrintedCheckEdit;
        private DevExpress.XtraEditors.GridLookUpEdit CityIDTextEdit;
        private System.Windows.Forms.BindingSource citiesServicesBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView grdCitiesServices;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraEditors.TextEdit txtWay;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraBars.BarButtonItem btnsetchair;
        private DevExpress.XtraBars.BarButtonItem btnPose;
        private DevExpress.XtraEditors.TextEdit NoTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private System.ComponentModel.BackgroundWorker BgPose;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        public DevExpress.XtraBars.BarButtonItem btnCancelPose;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        public DevExpress.XtraEditors.TextEdit txtStatusPayment;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private DevExpress.XtraEditors.TextEdit CreditUsertxt;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txtEmptyChair;
        private DevExpress.XtraEditors.TextEdit txtRezervChair;
        private DevExpress.XtraEditors.TextEdit txtFullChair;
        private DevExpress.XtraEditors.TextEdit txtCountChair;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit txtSumPrice;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        public DevExpress.XtraEditors.PictureEdit picForoshMard;
        public DevExpress.XtraEditors.PictureEdit picForoshZan;
        public DevExpress.XtraEditors.PictureEdit picRezervZan;
        public DevExpress.XtraEditors.PictureEdit picRezerMard;
        public DevExpress.XtraEditors.PictureEdit picEntekhabZan;
        public DevExpress.XtraEditors.PictureEdit picEntekhabMard;
        private Syncfusion.Windows.Forms.Grid.GridControl dgv;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem ShowNumberChair;
        private System.Windows.Forms.Timer timerGraphic;
        private DevExpress.XtraBars.BarButtonItem btnListTickets;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
    }
}