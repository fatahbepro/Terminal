﻿namespace Test
{
    partial class FrmAddCredit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmAddCredit));
            this.txtPayCredit = new DevExpress.XtraEditors.TextEdit();
            this.txtCreditNow = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClose = new DevExpress.XtraEditors.SimpleButton();
            this.btnPose = new DevExpress.XtraEditors.SimpleButton();
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Test.WaitForm1), true, true);
            this.bgpose = new System.ComponentModel.BackgroundWorker();
            this.btnCancelPose = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayCredit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditNow.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPayCredit
            // 
            this.txtPayCredit.EditValue = "0";
            this.txtPayCredit.Location = new System.Drawing.Point(114, 22);
            this.txtPayCredit.Name = "txtPayCredit";
            this.txtPayCredit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 12F);
            this.txtPayCredit.Properties.Appearance.Options.UseFont = true;
            this.txtPayCredit.Properties.Appearance.Options.UseTextOptions = true;
            this.txtPayCredit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtPayCredit.Properties.DisplayFormat.FormatString = "c0";
            this.txtPayCredit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPayCredit.Properties.EditFormat.FormatString = "c0";
            this.txtPayCredit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtPayCredit.Properties.Mask.EditMask = "c0";
            this.txtPayCredit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtPayCredit.Properties.NullValuePrompt = "مبلغ اعتبار";
            this.txtPayCredit.Size = new System.Drawing.Size(270, 34);
            this.txtPayCredit.TabIndex = 0;
            // 
            // txtCreditNow
            // 
            this.txtCreditNow.EditValue = "0";
            this.txtCreditNow.Location = new System.Drawing.Point(114, 72);
            this.txtCreditNow.Name = "txtCreditNow";
            this.txtCreditNow.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 12F);
            this.txtCreditNow.Properties.Appearance.Options.UseFont = true;
            this.txtCreditNow.Properties.Appearance.Options.UseTextOptions = true;
            this.txtCreditNow.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtCreditNow.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.txtCreditNow.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.txtCreditNow.Properties.DisplayFormat.FormatString = "c0";
            this.txtCreditNow.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCreditNow.Properties.EditFormat.FormatString = "c0";
            this.txtCreditNow.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.txtCreditNow.Properties.Mask.EditMask = "c0";
            this.txtCreditNow.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.txtCreditNow.Properties.ReadOnly = true;
            this.txtCreditNow.Size = new System.Drawing.Size(270, 34);
            this.txtCreditNow.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 12F);
            this.label1.Location = new System.Drawing.Point(390, 75);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(137, 27);
            this.label1.TabIndex = 1;
            this.label1.Text = "اعتبار فعلی حساب :";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 12F);
            this.label2.Location = new System.Drawing.Point(390, 25);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(90, 27);
            this.label2.TabIndex = 2;
            this.label2.Text = "مبلغ اعتبار : ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnClose
            // 
            this.btnClose.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 12F);
            this.btnClose.Appearance.Options.UseFont = true;
            this.btnClose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(114, 122);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(79, 48);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "بستن";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnPose
            // 
            this.btnPose.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 12F);
            this.btnPose.Appearance.Options.UseFont = true;
            this.btnPose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnPose.Image = ((System.Drawing.Image)(resources.GetObject("btnPose.Image")));
            this.btnPose.Location = new System.Drawing.Point(317, 122);
            this.btnPose.Name = "btnPose";
            this.btnPose.Size = new System.Drawing.Size(205, 48);
            this.btnPose.TabIndex = 4;
            this.btnPose.Text = "شارژ اعتبار با کارتخوان";
            this.btnPose.Click += new System.EventHandler(this.btnPose_Click);
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // bgpose
            // 
            this.bgpose.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgpose_DoWork);
            // 
            // btnCancelPose
            // 
            this.btnCancelPose.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 12F);
            this.btnCancelPose.Appearance.Options.UseFont = true;
            this.btnCancelPose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.btnCancelPose.Enabled = false;
            this.btnCancelPose.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelPose.Image")));
            this.btnCancelPose.Location = new System.Drawing.Point(199, 122);
            this.btnCancelPose.Name = "btnCancelPose";
            this.btnCancelPose.Size = new System.Drawing.Size(112, 48);
            this.btnCancelPose.TabIndex = 4;
            this.btnCancelPose.Text = "لغو ارسال";
            this.btnCancelPose.Click += new System.EventHandler(this.btnCancelPose_Click);
            // 
            // FrmAddCredit
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 236);
            this.Controls.Add(this.btnCancelPose);
            this.Controls.Add(this.btnPose);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtCreditNow);
            this.Controls.Add(this.txtPayCredit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmAddCredit";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "افزایش اعتبار حساب";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmAddCredit_FormClosing);
            this.Load += new System.EventHandler(this.FrmAddCredit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtPayCredit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCreditNow.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.TextEdit txtPayCredit;
        private DevExpress.XtraEditors.TextEdit txtCreditNow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.SimpleButton btnClose;
        private DevExpress.XtraEditors.SimpleButton btnPose;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private System.ComponentModel.BackgroundWorker bgpose;
        private DevExpress.XtraEditors.SimpleButton btnCancelPose;
    }
}