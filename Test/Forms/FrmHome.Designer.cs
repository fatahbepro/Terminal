﻿namespace Test
{
    partial class frmticketHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmticketHome));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnCompany = new DevExpress.XtraBars.BarButtonItem();
            this.btnCustomer = new DevExpress.XtraBars.BarButtonItem();
            this.btnSetting = new DevExpress.XtraBars.BarButtonItem();
            this.btnUser = new DevExpress.XtraBars.BarButtonItem();
            this.btnTrip = new DevExpress.XtraBars.BarButtonItem();
            this.btnReportSandogh = new DevExpress.XtraBars.BarButtonItem();
            this.btnExit = new DevExpress.XtraBars.BarButtonItem();
            this.BtnCreditUser = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItem2 = new DevExpress.XtraBars.BarStaticItem();
            this.lblVersion = new DevExpress.XtraBars.BarStaticItem();
            this.BtnReportticket = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.btnPermission = new DevExpress.XtraBars.BarButtonItem();
            this.btnReportTick = new DevExpress.XtraBars.BarButtonItem();
            this.btnCreditReport = new DevExpress.XtraBars.BarButtonItem();
            this.btnReportSales = new DevExpress.XtraBars.BarButtonItem();
            this.lbltime = new DevExpress.XtraBars.BarStaticItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rbnDefination = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ColorScheme = DevExpress.XtraBars.Ribbon.RibbonControlColorScheme.Green;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnCompany,
            this.btnCustomer,
            this.btnSetting,
            this.btnUser,
            this.btnTrip,
            this.btnReportSandogh,
            this.btnExit,
            this.BtnCreditUser,
            this.barStaticItem1,
            this.barStaticItem2,
            this.lblVersion,
            this.BtnReportticket,
            this.barButtonItem2,
            this.btnPermission,
            this.btnReportTick,
            this.btnCreditReport,
            this.btnReportSales,
            this.lbltime});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 19;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1259, 146);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // btnCompany
            // 
            this.btnCompany.Caption = "تعاونی ها";
            this.btnCompany.Glyph = ((System.Drawing.Image)(resources.GetObject("btnCompany.Glyph")));
            this.btnCompany.Id = 1;
            this.btnCompany.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnCompany.LargeGlyph")));
            this.btnCompany.Name = "btnCompany";
            this.btnCompany.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCompany_ItemClick);
            // 
            // btnCustomer
            // 
            this.btnCustomer.Caption = "مشتریان";
            this.btnCustomer.Glyph = ((System.Drawing.Image)(resources.GetObject("btnCustomer.Glyph")));
            this.btnCustomer.Id = 2;
            this.btnCustomer.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnCustomer.LargeGlyph")));
            this.btnCustomer.Name = "btnCustomer";
            // 
            // btnSetting
            // 
            this.btnSetting.Caption = "تنظیمات";
            this.btnSetting.Glyph = ((System.Drawing.Image)(resources.GetObject("btnSetting.Glyph")));
            this.btnSetting.Id = 3;
            this.btnSetting.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnSetting.LargeGlyph")));
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnSetting_ItemClick);
            // 
            // btnUser
            // 
            this.btnUser.Caption = "کاربران";
            this.btnUser.Glyph = ((System.Drawing.Image)(resources.GetObject("btnUser.Glyph")));
            this.btnUser.Id = 4;
            this.btnUser.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnUser.LargeGlyph")));
            this.btnUser.Name = "btnUser";
            this.btnUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnUser_ItemClick);
            // 
            // btnTrip
            // 
            this.btnTrip.Caption = "سفرها";
            this.btnTrip.Glyph = ((System.Drawing.Image)(resources.GetObject("btnTrip.Glyph")));
            this.btnTrip.Id = 5;
            this.btnTrip.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.btnTrip.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnTrip.LargeGlyph")));
            this.btnTrip.Name = "btnTrip";
            this.btnTrip.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnTrip_ItemClick);
            // 
            // btnReportSandogh
            // 
            this.btnReportSandogh.Caption = "گزارش صندوق";
            this.btnReportSandogh.Glyph = ((System.Drawing.Image)(resources.GetObject("btnReportSandogh.Glyph")));
            this.btnReportSandogh.Id = 6;
            this.btnReportSandogh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnReportSandogh.LargeGlyph")));
            this.btnReportSandogh.Name = "btnReportSandogh";
            this.btnReportSandogh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReportSandogh_ItemClick);
            // 
            // btnExit
            // 
            this.btnExit.Caption = "خروج";
            this.btnExit.Glyph = ((System.Drawing.Image)(resources.GetObject("btnExit.Glyph")));
            this.btnExit.Id = 7;
            this.btnExit.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnExit.LargeGlyph")));
            this.btnExit.Name = "btnExit";
            this.btnExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnExit_ItemClick);
            // 
            // BtnCreditUser
            // 
            this.BtnCreditUser.Caption = "افزایش اعتبار";
            this.BtnCreditUser.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnCreditUser.Glyph")));
            this.BtnCreditUser.Id = 8;
            this.BtnCreditUser.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnCreditUser.LargeGlyph")));
            this.BtnCreditUser.Name = "BtnCreditUser";
            this.BtnCreditUser.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnCreditUser_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem1.Caption = "barStaticItem1";
            this.barStaticItem1.Id = 9;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItem2
            // 
            this.barStaticItem2.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barStaticItem2.Caption = "barStaticItem2";
            this.barStaticItem2.Id = 10;
            this.barStaticItem2.Name = "barStaticItem2";
            this.barStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // lblVersion
            // 
            this.lblVersion.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.lblVersion.Caption = "نسخه برنامه ";
            this.lblVersion.Id = 11;
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // BtnReportticket
            // 
            this.BtnReportticket.Caption = "گزارش سفرها";
            this.BtnReportticket.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnReportticket.Glyph")));
            this.BtnReportticket.Id = 12;
            this.BtnReportticket.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnReportticket.LargeGlyph")));
            this.BtnReportticket.Name = "BtnReportticket";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 13;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // btnPermission
            // 
            this.btnPermission.Caption = "تعیین سطح دسترسی";
            this.btnPermission.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPermission.Glyph")));
            this.btnPermission.Id = 14;
            this.btnPermission.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnPermission.LargeGlyph")));
            this.btnPermission.Name = "btnPermission";
            this.btnPermission.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnPermission_ItemClick);
            // 
            // btnReportTick
            // 
            this.btnReportTick.Caption = "آمار بلیط ها";
            this.btnReportTick.Glyph = ((System.Drawing.Image)(resources.GetObject("btnReportTick.Glyph")));
            this.btnReportTick.Id = 15;
            this.btnReportTick.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnReportTick.LargeGlyph")));
            this.btnReportTick.Name = "btnReportTick";
            this.btnReportTick.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReportTick_ItemClick);
            // 
            // btnCreditReport
            // 
            this.btnCreditReport.Caption = "گزارش اعتبار کاربر";
            this.btnCreditReport.Glyph = ((System.Drawing.Image)(resources.GetObject("btnCreditReport.Glyph")));
            this.btnCreditReport.Id = 16;
            this.btnCreditReport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnCreditReport.LargeGlyph")));
            this.btnCreditReport.Name = "btnCreditReport";
            this.btnCreditReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCreditReport_ItemClick);
            // 
            // btnReportSales
            // 
            this.btnReportSales.Caption = "گزارش بلیط های پیش فروش ";
            this.btnReportSales.Glyph = ((System.Drawing.Image)(resources.GetObject("btnReportSales.Glyph")));
            this.btnReportSales.Id = 17;
            this.btnReportSales.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnReportSales.LargeGlyph")));
            this.btnReportSales.Name = "btnReportSales";
            this.btnReportSales.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReportSales_ItemClick);
            // 
            // lbltime
            // 
            this.lbltime.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.lbltime.Caption = "ساعت";
            this.lbltime.Id = 18;
            this.lbltime.Name = "lbltime";
            this.lbltime.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rbnDefination,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3,
            this.ribbonPageGroup1,
            this.ribbonPageGroup4});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "صفحه اصلی";
            // 
            // rbnDefination
            // 
            this.rbnDefination.ItemLinks.Add(this.btnCompany);
            this.rbnDefination.ItemLinks.Add(this.btnUser);
            this.rbnDefination.Name = "rbnDefination";
            this.rbnDefination.ShowCaptionButton = false;
            this.rbnDefination.Text = "تعاریف";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnSetting);
            this.ribbonPageGroup2.ItemLinks.Add(this.BtnCreditUser);
            this.ribbonPageGroup2.ItemLinks.Add(this.btnPermission);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "تنظیمات";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnTrip);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "سفر";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnReportSandogh);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnReportTick);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnCreditReport);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnReportSales);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "گزارشات";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.btnExit);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.ShowCaptionButton = false;
            this.ribbonPageGroup4.Text = "خروج";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.lblVersion);
            this.ribbonStatusBar.ItemLinks.Add(this.lbltime);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 602);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1259, 21);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinMaskColor = System.Drawing.Color.Magenta;
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Office 2016 Colorful";
            // 
            // frmticketHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1259, 623);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.FormBorderEffect = DevExpress.XtraEditors.FormBorderEffect.Shadow;
            this.Name = "frmticketHome";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmticketHome_FormClosing);
            this.Load += new System.EventHandler(this.frmticketHome_Load);
            this.Shown += new System.EventHandler(this.frmticketHome_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rbnDefination;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem btnCompany;
        private DevExpress.XtraBars.BarButtonItem btnCustomer;
        private DevExpress.XtraBars.BarButtonItem btnSetting;
        private DevExpress.XtraBars.BarButtonItem btnUser;
        private DevExpress.XtraBars.BarButtonItem btnTrip;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem btnReportSandogh;
        private DevExpress.XtraBars.BarButtonItem btnExit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraBars.BarButtonItem BtnCreditUser;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItem2;
        private DevExpress.XtraBars.BarStaticItem lblVersion;
        private DevExpress.XtraBars.BarButtonItem BtnReportticket;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.BarButtonItem btnPermission;
        private DevExpress.XtraBars.BarButtonItem btnReportTick;
        private DevExpress.XtraBars.BarButtonItem btnCreditReport;
        private DevExpress.XtraBars.BarButtonItem btnReportSales;
        private DevExpress.XtraBars.BarStaticItem lbltime;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
    }
}