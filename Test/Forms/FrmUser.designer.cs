﻿namespace Test
{
    partial class frmticketUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmticketUser));
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule2 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule3 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule4 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule5 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule6 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule7 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule8 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.BtnSave = new DevExpress.XtraBars.BarButtonItem();
            this.BtnNew = new DevExpress.XtraBars.BarButtonItem();
            this.BtnRefesh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.BtnDelete = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.dataLayoutControl1 = new DevExpress.XtraDataLayout.DataLayoutControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colUserName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPassword = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.colFullName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAccessCompanies = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEnabled = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCredit = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CodeTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.UserNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PasswordTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.FullNameTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.EnabledCheckEdit = new DevExpress.XtraEditors.ToggleSwitch();
            this.AccessCompaniesTextEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.companyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grdcompany = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCodeCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTitle = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CreditTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PrinterUserComboBoxEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.IpPoseTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.PoseNameTextEdit = new DevExpress.XtraEditors.ComboBoxEdit();
            this.StarttimeTimeEdit = new DevExpress.XtraEditors.TimeEdit();
            this.EndtimeTimeEdit = new DevExpress.XtraEditors.TimeEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCode = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForUserName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForAccessCompanies = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFullName = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPassword = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForCredit = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForStarttime = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEndtime = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIpPose = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForEnabled = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPrinterUser = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForPoseName = new DevExpress.XtraLayout.LayoutControlItem();
            this.dxValidationProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).BeginInit();
            this.dataLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FullNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnabledCheckEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessCompaniesTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdcompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrinterUserComboBoxEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IpPoseTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoseNameTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StarttimeTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndtimeTimeEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessCompanies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFullName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCredit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStarttime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndtime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIpPose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEnabled)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrinterUser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoseName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.BtnSave,
            this.BtnNew,
            this.BtnRefesh,
            this.barButtonItem1,
            this.BtnDelete});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ribbon.MaxItemId = 7;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(991, 146);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // BtnSave
            // 
            this.BtnSave.Caption = "ذخیره";
            this.BtnSave.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnSave.Glyph")));
            this.BtnSave.Id = 2;
            this.BtnSave.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F2);
            this.BtnSave.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnSave.LargeGlyph")));
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnSave_ItemClick);
            // 
            // BtnNew
            // 
            this.BtnNew.Caption = "خالی کردن";
            this.BtnNew.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnNew.Glyph")));
            this.BtnNew.Id = 3;
            this.BtnNew.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F3);
            this.BtnNew.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnNew.LargeGlyph")));
            this.BtnNew.Name = "BtnNew";
            this.BtnNew.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnNew_ItemClick);
            // 
            // BtnRefesh
            // 
            this.BtnRefesh.Caption = "بروزرسانی";
            this.BtnRefesh.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnRefesh.Glyph")));
            this.BtnRefesh.Id = 4;
            this.BtnRefesh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.BtnRefesh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnRefesh.LargeGlyph")));
            this.BtnRefesh.Name = "BtnRefesh";
            this.BtnRefesh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnRefesh_ItemClick);
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "بستن";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 5;
            this.barButtonItem1.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Caption = "حذف کردن";
            this.BtnDelete.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnDelete.Glyph")));
            this.BtnDelete.Id = 6;
            this.BtnDelete.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.Delete);
            this.BtnDelete.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnDelete.LargeGlyph")));
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.BtnDelete_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "اطلاعات";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.BtnSave);
            this.ribbonPageGroup1.ItemLinks.Add(this.BtnNew);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "ذخیره";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.BtnRefesh);
            this.ribbonPageGroup2.ItemLinks.Add(this.BtnDelete);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "بروزرسانی داده ها";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 687);
            this.ribbonStatusBar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(991, 21);
            // 
            // dataLayoutControl1
            // 
            this.dataLayoutControl1.Controls.Add(this.gridControl1);
            this.dataLayoutControl1.Controls.Add(this.CodeTextEdit);
            this.dataLayoutControl1.Controls.Add(this.UserNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PasswordTextEdit);
            this.dataLayoutControl1.Controls.Add(this.FullNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.EnabledCheckEdit);
            this.dataLayoutControl1.Controls.Add(this.AccessCompaniesTextEdit);
            this.dataLayoutControl1.Controls.Add(this.CreditTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PrinterUserComboBoxEdit);
            this.dataLayoutControl1.Controls.Add(this.IpPoseTextEdit);
            this.dataLayoutControl1.Controls.Add(this.PoseNameTextEdit);
            this.dataLayoutControl1.Controls.Add(this.StarttimeTimeEdit);
            this.dataLayoutControl1.Controls.Add(this.EndtimeTimeEdit);
            this.dataLayoutControl1.DataSource = this.userBindingSource;
            this.dataLayoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataLayoutControl1.Location = new System.Drawing.Point(0, 146);
            this.dataLayoutControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataLayoutControl1.Name = "dataLayoutControl1";
            this.dataLayoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(105, 182, 207, 293);
            this.dataLayoutControl1.OptionsFocus.MoveFocusRightToLeft = true;
            this.dataLayoutControl1.OptionsView.RightToLeftMirroringApplied = true;
            this.dataLayoutControl1.Root = this.layoutControlGroup1;
            this.dataLayoutControl1.Size = new System.Drawing.Size(991, 541);
            this.dataLayoutControl1.TabIndex = 2;
            this.dataLayoutControl1.Text = "dataLayoutControl1";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.userBindingSource;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl1.Location = new System.Drawing.Point(12, 209);
            this.gridControl1.MainView = this.grd;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemTextEdit1});
            this.gridControl1.Size = new System.Drawing.Size(967, 320);
            this.gridControl1.TabIndex = 14;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grd});
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(Model.User);
            // 
            // grd
            // 
            this.grd.Appearance.FocusedCell.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedCell.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grd.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grd.Appearance.FocusedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grd.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grd.Appearance.SelectedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grd.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grd.ColumnPanelRowHeight = 30;
            this.grd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colUserName,
            this.colPassword,
            this.colFullName,
            this.colAccessCompanies,
            this.colCode,
            this.colEnabled,
            this.colCredit});
            this.grd.GridControl = this.gridControl1;
            this.grd.Name = "grd";
            this.grd.OptionsBehavior.Editable = false;
            this.grd.OptionsFind.AlwaysVisible = true;
            this.grd.OptionsFind.FindNullPrompt = "جست و جو . . .";
            this.grd.OptionsView.ShowGroupPanel = false;
            this.grd.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.grd_RowCellClick);
            this.grd.DoubleClick += new System.EventHandler(this.grd_DoubleClick);
            // 
            // colID
            // 
            this.colID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colID.AppearanceCell.Options.UseFont = true;
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colID.AppearanceHeader.Options.UseFont = true;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.Caption = "#";
            this.colID.FieldName = "ID";
            this.colID.MaxWidth = 65;
            this.colID.Name = "colID";
            this.colID.Visible = true;
            this.colID.VisibleIndex = 0;
            this.colID.Width = 59;
            // 
            // colUserName
            // 
            this.colUserName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colUserName.AppearanceCell.Options.UseFont = true;
            this.colUserName.AppearanceCell.Options.UseTextOptions = true;
            this.colUserName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colUserName.AppearanceHeader.Options.UseFont = true;
            this.colUserName.AppearanceHeader.Options.UseTextOptions = true;
            this.colUserName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colUserName.Caption = "نام کاربری";
            this.colUserName.FieldName = "UserName";
            this.colUserName.Name = "colUserName";
            this.colUserName.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "UserName", "{0}")});
            this.colUserName.Visible = true;
            this.colUserName.VisibleIndex = 3;
            this.colUserName.Width = 139;
            // 
            // colPassword
            // 
            this.colPassword.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPassword.AppearanceCell.Options.UseFont = true;
            this.colPassword.AppearanceCell.Options.UseTextOptions = true;
            this.colPassword.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPassword.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colPassword.AppearanceHeader.Options.UseFont = true;
            this.colPassword.AppearanceHeader.Options.UseTextOptions = true;
            this.colPassword.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPassword.Caption = "رمز";
            this.colPassword.ColumnEdit = this.repositoryItemTextEdit1;
            this.colPassword.FieldName = "Password";
            this.colPassword.Name = "colPassword";
            this.colPassword.Visible = true;
            this.colPassword.VisibleIndex = 4;
            this.colPassword.Width = 103;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.PasswordChar = '*';
            // 
            // colFullName
            // 
            this.colFullName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colFullName.AppearanceCell.Options.UseFont = true;
            this.colFullName.AppearanceCell.Options.UseTextOptions = true;
            this.colFullName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colFullName.AppearanceHeader.Options.UseFont = true;
            this.colFullName.AppearanceHeader.Options.UseTextOptions = true;
            this.colFullName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullName.Caption = "نام کامل";
            this.colFullName.FieldName = "FullName";
            this.colFullName.Name = "colFullName";
            this.colFullName.Visible = true;
            this.colFullName.VisibleIndex = 2;
            this.colFullName.Width = 139;
            // 
            // colAccessCompanies
            // 
            this.colAccessCompanies.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colAccessCompanies.AppearanceCell.Options.UseFont = true;
            this.colAccessCompanies.AppearanceCell.Options.UseTextOptions = true;
            this.colAccessCompanies.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAccessCompanies.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colAccessCompanies.AppearanceHeader.Options.UseFont = true;
            this.colAccessCompanies.AppearanceHeader.Options.UseTextOptions = true;
            this.colAccessCompanies.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colAccessCompanies.Caption = "دسترسی به تعاونی ها";
            this.colAccessCompanies.FieldName = "AccessCompanies";
            this.colAccessCompanies.Name = "colAccessCompanies";
            this.colAccessCompanies.Visible = true;
            this.colAccessCompanies.VisibleIndex = 6;
            this.colAccessCompanies.Width = 238;
            // 
            // colCode
            // 
            this.colCode.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCode.AppearanceCell.Options.UseFont = true;
            this.colCode.AppearanceCell.Options.UseTextOptions = true;
            this.colCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCode.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCode.AppearanceHeader.Options.UseFont = true;
            this.colCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCode.Caption = "کد";
            this.colCode.FieldName = "Code";
            this.colCode.Name = "colCode";
            this.colCode.Visible = true;
            this.colCode.VisibleIndex = 1;
            this.colCode.Width = 76;
            // 
            // colEnabled
            // 
            this.colEnabled.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colEnabled.AppearanceCell.Options.UseFont = true;
            this.colEnabled.AppearanceCell.Options.UseTextOptions = true;
            this.colEnabled.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnabled.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colEnabled.AppearanceHeader.Options.UseFont = true;
            this.colEnabled.AppearanceHeader.Options.UseTextOptions = true;
            this.colEnabled.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colEnabled.Caption = "وضعیت";
            this.colEnabled.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colEnabled.FieldName = "Enabled";
            this.colEnabled.Name = "colEnabled";
            this.colEnabled.Visible = true;
            this.colEnabled.VisibleIndex = 7;
            this.colEnabled.Width = 54;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.UserDefined;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.PictureChecked = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit1.PictureChecked")));
            this.repositoryItemCheckEdit1.PictureGrayed = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit1.PictureGrayed")));
            this.repositoryItemCheckEdit1.PictureUnchecked = ((System.Drawing.Image)(resources.GetObject("repositoryItemCheckEdit1.PictureUnchecked")));
            // 
            // colCredit
            // 
            this.colCredit.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCredit.AppearanceCell.Options.UseFont = true;
            this.colCredit.AppearanceCell.Options.UseTextOptions = true;
            this.colCredit.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCredit.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.colCredit.AppearanceHeader.Options.UseFont = true;
            this.colCredit.AppearanceHeader.Options.UseTextOptions = true;
            this.colCredit.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCredit.Caption = "اعتبار حساب";
            this.colCredit.DisplayFormat.FormatString = "n0";
            this.colCredit.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colCredit.FieldName = "Credit";
            this.colCredit.Name = "colCredit";
            this.colCredit.Visible = true;
            this.colCredit.VisibleIndex = 5;
            this.colCredit.Width = 141;
            // 
            // CodeTextEdit
            // 
            this.CodeTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "Code", true));
            this.CodeTextEdit.EnterMoveNextControl = true;
            this.CodeTextEdit.Location = new System.Drawing.Point(12, 12);
            this.CodeTextEdit.MenuManager = this.ribbon;
            this.CodeTextEdit.Name = "CodeTextEdit";
            this.CodeTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.CodeTextEdit.Properties.Appearance.Options.UseFont = true;
            this.CodeTextEdit.Size = new System.Drawing.Size(363, 28);
            this.CodeTextEdit.StyleController = this.dataLayoutControl1;
            this.CodeTextEdit.TabIndex = 16;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.CodeTextEdit, conditionValidationRule1);
            this.CodeTextEdit.Validated += new System.EventHandler(this.FullNameTextEdit_Validated);
            // 
            // UserNameTextEdit
            // 
            this.UserNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "UserName", true));
            this.UserNameTextEdit.EnterMoveNextControl = true;
            this.UserNameTextEdit.Location = new System.Drawing.Point(504, 44);
            this.UserNameTextEdit.MenuManager = this.ribbon;
            this.UserNameTextEdit.Name = "UserNameTextEdit";
            this.UserNameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.UserNameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.UserNameTextEdit.Size = new System.Drawing.Size(350, 28);
            this.UserNameTextEdit.StyleController = this.dataLayoutControl1;
            this.UserNameTextEdit.TabIndex = 17;
            conditionValidationRule2.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule2.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.UserNameTextEdit, conditionValidationRule2);
            this.UserNameTextEdit.Validated += new System.EventHandler(this.FullNameTextEdit_Validated);
            // 
            // PasswordTextEdit
            // 
            this.PasswordTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "Password", true));
            this.PasswordTextEdit.EnterMoveNextControl = true;
            this.PasswordTextEdit.Location = new System.Drawing.Point(12, 44);
            this.PasswordTextEdit.MenuManager = this.ribbon;
            this.PasswordTextEdit.Name = "PasswordTextEdit";
            this.PasswordTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.PasswordTextEdit.Properties.Appearance.Options.UseFont = true;
            this.PasswordTextEdit.Size = new System.Drawing.Size(363, 28);
            this.PasswordTextEdit.StyleController = this.dataLayoutControl1;
            this.PasswordTextEdit.TabIndex = 18;
            conditionValidationRule3.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule3.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.PasswordTextEdit, conditionValidationRule3);
            this.PasswordTextEdit.Validated += new System.EventHandler(this.FullNameTextEdit_Validated);
            // 
            // FullNameTextEdit
            // 
            this.FullNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "FullName", true));
            this.FullNameTextEdit.EnterMoveNextControl = true;
            this.FullNameTextEdit.Location = new System.Drawing.Point(504, 12);
            this.FullNameTextEdit.MenuManager = this.ribbon;
            this.FullNameTextEdit.Name = "FullNameTextEdit";
            this.FullNameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.FullNameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.FullNameTextEdit.Size = new System.Drawing.Size(350, 28);
            this.FullNameTextEdit.StyleController = this.dataLayoutControl1;
            this.FullNameTextEdit.TabIndex = 19;
            conditionValidationRule4.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule4.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.FullNameTextEdit, conditionValidationRule4);
            this.FullNameTextEdit.Validated += new System.EventHandler(this.FullNameTextEdit_Validated);
            // 
            // EnabledCheckEdit
            // 
            this.EnabledCheckEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "Enabled", true));
            this.EnabledCheckEdit.EnterMoveNextControl = true;
            this.EnabledCheckEdit.Location = new System.Drawing.Point(12, 172);
            this.EnabledCheckEdit.MenuManager = this.ribbon;
            this.EnabledCheckEdit.Name = "EnabledCheckEdit";
            this.EnabledCheckEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.EnabledCheckEdit.Properties.Appearance.Options.UseFont = true;
            this.EnabledCheckEdit.Properties.GlyphAlignment = DevExpress.Utils.HorzAlignment.Default;
            this.EnabledCheckEdit.Properties.OffText = "غیر فعال";
            this.EnabledCheckEdit.Properties.OnText = "فعال";
            this.EnabledCheckEdit.Size = new System.Drawing.Size(404, 33);
            this.EnabledCheckEdit.StyleController = this.dataLayoutControl1;
            this.EnabledCheckEdit.TabIndex = 21;
            conditionValidationRule5.ErrorText = "This value is not valid";
            this.dxValidationProvider1.SetValidationRule(this.EnabledCheckEdit, conditionValidationRule5);
            // 
            // AccessCompaniesTextEdit
            // 
            this.AccessCompaniesTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "AccessCompanies", true));
            this.AccessCompaniesTextEdit.EnterMoveNextControl = true;
            this.AccessCompaniesTextEdit.Location = new System.Drawing.Point(504, 76);
            this.AccessCompaniesTextEdit.MenuManager = this.ribbon;
            this.AccessCompaniesTextEdit.Name = "AccessCompaniesTextEdit";
            this.AccessCompaniesTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.AccessCompaniesTextEdit.Properties.Appearance.Options.UseFont = true;
            this.AccessCompaniesTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AccessCompaniesTextEdit.Properties.DataSource = this.companyBindingSource;
            this.AccessCompaniesTextEdit.Properties.DisplayMember = "Title";
            this.AccessCompaniesTextEdit.Properties.NullText = "";
            this.AccessCompaniesTextEdit.Properties.ValueMember = "Code";
            this.AccessCompaniesTextEdit.Properties.View = this.grdcompany;
            this.AccessCompaniesTextEdit.Size = new System.Drawing.Size(350, 28);
            this.AccessCompaniesTextEdit.StyleController = this.dataLayoutControl1;
            this.AccessCompaniesTextEdit.TabIndex = 20;
            this.AccessCompaniesTextEdit.Click += new System.EventHandler(this.AccessCompaniesTextEdit_Click);
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataSource = typeof(Model.Company);
            // 
            // grdcompany
            // 
            this.grdcompany.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodeCompany,
            this.colTitle});
            this.grdcompany.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.grdcompany.Name = "grdcompany";
            this.grdcompany.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.grdcompany.OptionsSelection.MultiSelect = true;
            this.grdcompany.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect;
            this.grdcompany.OptionsView.ShowGroupPanel = false;
            // 
            // colCodeCompany
            // 
            this.colCodeCompany.AppearanceCell.Options.UseTextOptions = true;
            this.colCodeCompany.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCodeCompany.AppearanceHeader.Options.UseTextOptions = true;
            this.colCodeCompany.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCodeCompany.Caption = "کد";
            this.colCodeCompany.FieldName = "Code";
            this.colCodeCompany.Name = "colCodeCompany";
            this.colCodeCompany.Visible = true;
            this.colCodeCompany.VisibleIndex = 1;
            // 
            // colTitle
            // 
            this.colTitle.AppearanceCell.Options.UseTextOptions = true;
            this.colTitle.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTitle.AppearanceHeader.Options.UseTextOptions = true;
            this.colTitle.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTitle.Caption = "عنوان";
            this.colTitle.FieldName = "Title";
            this.colTitle.Name = "colTitle";
            this.colTitle.Visible = true;
            this.colTitle.VisibleIndex = 2;
            // 
            // CreditTextEdit
            // 
            this.CreditTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "Credit", true));
            this.CreditTextEdit.EditValue = "0";
            this.CreditTextEdit.EnterMoveNextControl = true;
            this.CreditTextEdit.Location = new System.Drawing.Point(12, 76);
            this.CreditTextEdit.MenuManager = this.ribbon;
            this.CreditTextEdit.Name = "CreditTextEdit";
            this.CreditTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.CreditTextEdit.Properties.Appearance.Options.UseFont = true;
            this.CreditTextEdit.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White;
            this.CreditTextEdit.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.CreditTextEdit.Properties.DisplayFormat.FormatString = "c0";
            this.CreditTextEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CreditTextEdit.Properties.EditFormat.FormatString = "c0";
            this.CreditTextEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.CreditTextEdit.Properties.Mask.EditMask = "c0";
            this.CreditTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.CreditTextEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            this.CreditTextEdit.Properties.ReadOnly = true;
            this.CreditTextEdit.Size = new System.Drawing.Size(363, 28);
            this.CreditTextEdit.StyleController = this.dataLayoutControl1;
            this.CreditTextEdit.TabIndex = 22;
            conditionValidationRule6.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule6.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.CreditTextEdit, conditionValidationRule6);
            this.CreditTextEdit.Validated += new System.EventHandler(this.FullNameTextEdit_Validated);
            // 
            // PrinterUserComboBoxEdit
            // 
            this.PrinterUserComboBoxEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "PrinterUser", true));
            this.PrinterUserComboBoxEdit.EnterMoveNextControl = true;
            this.PrinterUserComboBoxEdit.Location = new System.Drawing.Point(505, 140);
            this.PrinterUserComboBoxEdit.MenuManager = this.ribbon;
            this.PrinterUserComboBoxEdit.Name = "PrinterUserComboBoxEdit";
            this.PrinterUserComboBoxEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.PrinterUserComboBoxEdit.Properties.Appearance.Options.UseFont = true;
            this.PrinterUserComboBoxEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PrinterUserComboBoxEdit.Size = new System.Drawing.Size(349, 28);
            this.PrinterUserComboBoxEdit.StyleController = this.dataLayoutControl1;
            this.PrinterUserComboBoxEdit.TabIndex = 23;
            this.PrinterUserComboBoxEdit.Click += new System.EventHandler(this.PrinterUserComboBoxEdit_Click);
            // 
            // IpPoseTextEdit
            // 
            this.IpPoseTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "IpPose", true));
            this.IpPoseTextEdit.EnterMoveNextControl = true;
            this.IpPoseTextEdit.Location = new System.Drawing.Point(505, 172);
            this.IpPoseTextEdit.MenuManager = this.ribbon;
            this.IpPoseTextEdit.Name = "IpPoseTextEdit";
            this.IpPoseTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.IpPoseTextEdit.Properties.Appearance.Options.UseFont = true;
            this.IpPoseTextEdit.Properties.Mask.EditMask = "([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\\.([0-9]|[1-9][0-9]|1[0-9][0-9" +
    "]|2[0-4][0-9]|25[0-5])){3}";
            this.IpPoseTextEdit.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx;
            this.IpPoseTextEdit.Size = new System.Drawing.Size(349, 28);
            this.IpPoseTextEdit.StyleController = this.dataLayoutControl1;
            this.IpPoseTextEdit.TabIndex = 25;
            // 
            // PoseNameTextEdit
            // 
            this.PoseNameTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.userBindingSource, "PoseName", true));
            this.PoseNameTextEdit.EnterMoveNextControl = true;
            this.PoseNameTextEdit.Location = new System.Drawing.Point(12, 140);
            this.PoseNameTextEdit.MenuManager = this.ribbon;
            this.PoseNameTextEdit.Name = "PoseNameTextEdit";
            this.PoseNameTextEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.PoseNameTextEdit.Properties.Appearance.Options.UseFont = true;
            this.PoseNameTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.PoseNameTextEdit.Properties.Items.AddRange(new object[] {
            "سداد",
            "سامان کیش",
            "آسان پرداخت",
            "ملت"});
            this.PoseNameTextEdit.Size = new System.Drawing.Size(364, 28);
            this.PoseNameTextEdit.StyleController = this.dataLayoutControl1;
            this.PoseNameTextEdit.TabIndex = 24;
            this.PoseNameTextEdit.Click += new System.EventHandler(this.PoseNameTextEdit_Click);
            // 
            // StarttimeTimeEdit
            // 
            this.StarttimeTimeEdit.EditValue = "00:00:00";
            this.StarttimeTimeEdit.EnterMoveNextControl = true;
            this.StarttimeTimeEdit.Location = new System.Drawing.Point(504, 108);
            this.StarttimeTimeEdit.MenuManager = this.ribbon;
            this.StarttimeTimeEdit.Name = "StarttimeTimeEdit";
            this.StarttimeTimeEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.StarttimeTimeEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.StarttimeTimeEdit.Properties.Appearance.Options.UseFont = true;
            this.StarttimeTimeEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.StarttimeTimeEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.StarttimeTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.StarttimeTimeEdit.Properties.DisplayFormat.FormatString = "HH:mm";
            this.StarttimeTimeEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.StarttimeTimeEdit.Properties.EditFormat.FormatString = "HH:mm";
            this.StarttimeTimeEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.StarttimeTimeEdit.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.StarttimeTimeEdit.Properties.Mask.EditMask = "HH:mm";
            this.StarttimeTimeEdit.Size = new System.Drawing.Size(350, 28);
            this.StarttimeTimeEdit.StyleController = this.dataLayoutControl1;
            this.StarttimeTimeEdit.TabIndex = 26;
            conditionValidationRule7.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule7.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.StarttimeTimeEdit, conditionValidationRule7);
            // 
            // EndtimeTimeEdit
            // 
            this.EndtimeTimeEdit.EditValue = "00:00:00";
            this.EndtimeTimeEdit.EnterMoveNextControl = true;
            this.EndtimeTimeEdit.Location = new System.Drawing.Point(12, 108);
            this.EndtimeTimeEdit.MenuManager = this.ribbon;
            this.EndtimeTimeEdit.Name = "EndtimeTimeEdit";
            this.EndtimeTimeEdit.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.EndtimeTimeEdit.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.EndtimeTimeEdit.Properties.Appearance.Options.UseFont = true;
            this.EndtimeTimeEdit.Properties.Appearance.Options.UseTextOptions = true;
            this.EndtimeTimeEdit.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.EndtimeTimeEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.EndtimeTimeEdit.Properties.DisplayFormat.FormatString = "HH:mm";
            this.EndtimeTimeEdit.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.EndtimeTimeEdit.Properties.EditFormat.FormatString = "HH:mm";
            this.EndtimeTimeEdit.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.EndtimeTimeEdit.Properties.Mask.EditMask = "HH:mm";
            this.EndtimeTimeEdit.Size = new System.Drawing.Size(363, 28);
            this.EndtimeTimeEdit.StyleController = this.dataLayoutControl1;
            this.EndtimeTimeEdit.TabIndex = 27;
            conditionValidationRule8.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule8.ErrorText = "لطفا مقدار را وارد نمایید";
            this.dxValidationProvider1.SetValidationRule(this.EndtimeTimeEdit, conditionValidationRule8);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup2,
            this.layoutControlItem2,
            this.ItemForIpPose,
            this.ItemForEnabled,
            this.ItemForPrinterUser,
            this.ItemForPoseName});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup1.Size = new System.Drawing.Size(991, 541);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.AllowDrawBackground = false;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCode,
            this.ItemForUserName,
            this.ItemForAccessCompanies,
            this.ItemForFullName,
            this.ItemForPassword,
            this.ItemForCredit,
            this.ItemForStarttime,
            this.ItemForEndtime});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "autoGeneratedGroup0";
            this.layoutControlGroup2.OptionsItemText.TextToControlDistance = 4;
            this.layoutControlGroup2.Size = new System.Drawing.Size(971, 128);
            // 
            // ItemForCode
            // 
            this.ItemForCode.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForCode.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForCode.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForCode.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForCode.Control = this.CodeTextEdit;
            this.ItemForCode.Location = new System.Drawing.Point(0, 0);
            this.ItemForCode.Name = "ItemForCode";
            this.ItemForCode.Size = new System.Drawing.Size(492, 32);
            this.ItemForCode.Text = "کد کاربر :";
            this.ItemForCode.TextSize = new System.Drawing.Size(121, 24);
            // 
            // ItemForUserName
            // 
            this.ItemForUserName.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForUserName.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForUserName.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForUserName.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForUserName.Control = this.UserNameTextEdit;
            this.ItemForUserName.Location = new System.Drawing.Point(492, 32);
            this.ItemForUserName.Name = "ItemForUserName";
            this.ItemForUserName.Size = new System.Drawing.Size(479, 32);
            this.ItemForUserName.Text = "نام کاربری :";
            this.ItemForUserName.TextSize = new System.Drawing.Size(121, 24);
            // 
            // ItemForAccessCompanies
            // 
            this.ItemForAccessCompanies.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForAccessCompanies.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForAccessCompanies.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForAccessCompanies.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForAccessCompanies.Control = this.AccessCompaniesTextEdit;
            this.ItemForAccessCompanies.Location = new System.Drawing.Point(492, 64);
            this.ItemForAccessCompanies.Name = "ItemForAccessCompanies";
            this.ItemForAccessCompanies.Size = new System.Drawing.Size(479, 32);
            this.ItemForAccessCompanies.Text = "دسترسی به شرکت ها:";
            this.ItemForAccessCompanies.TextSize = new System.Drawing.Size(121, 24);
            // 
            // ItemForFullName
            // 
            this.ItemForFullName.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForFullName.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForFullName.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForFullName.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForFullName.Control = this.FullNameTextEdit;
            this.ItemForFullName.Location = new System.Drawing.Point(492, 0);
            this.ItemForFullName.Name = "ItemForFullName";
            this.ItemForFullName.Size = new System.Drawing.Size(479, 32);
            this.ItemForFullName.Text = "نام :";
            this.ItemForFullName.TextSize = new System.Drawing.Size(121, 24);
            // 
            // ItemForPassword
            // 
            this.ItemForPassword.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForPassword.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForPassword.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForPassword.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForPassword.Control = this.PasswordTextEdit;
            this.ItemForPassword.Location = new System.Drawing.Point(0, 32);
            this.ItemForPassword.Name = "ItemForPassword";
            this.ItemForPassword.Size = new System.Drawing.Size(492, 32);
            this.ItemForPassword.Text = "رمز عبور:";
            this.ItemForPassword.TextSize = new System.Drawing.Size(121, 24);
            // 
            // ItemForCredit
            // 
            this.ItemForCredit.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForCredit.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForCredit.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForCredit.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForCredit.Control = this.CreditTextEdit;
            this.ItemForCredit.Location = new System.Drawing.Point(0, 64);
            this.ItemForCredit.Name = "ItemForCredit";
            this.ItemForCredit.Size = new System.Drawing.Size(492, 32);
            this.ItemForCredit.Text = "اعتبار حساب :";
            this.ItemForCredit.TextSize = new System.Drawing.Size(121, 24);
            // 
            // ItemForStarttime
            // 
            this.ItemForStarttime.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForStarttime.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForStarttime.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForStarttime.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForStarttime.Control = this.StarttimeTimeEdit;
            this.ItemForStarttime.Location = new System.Drawing.Point(492, 96);
            this.ItemForStarttime.Name = "ItemForStarttime";
            this.ItemForStarttime.Size = new System.Drawing.Size(479, 32);
            this.ItemForStarttime.Text = " ساعت شروع کاری :";
            this.ItemForStarttime.TextSize = new System.Drawing.Size(121, 24);
            // 
            // ItemForEndtime
            // 
            this.ItemForEndtime.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForEndtime.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForEndtime.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForEndtime.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForEndtime.Control = this.EndtimeTimeEdit;
            this.ItemForEndtime.Location = new System.Drawing.Point(0, 96);
            this.ItemForEndtime.Name = "ItemForEndtime";
            this.ItemForEndtime.Size = new System.Drawing.Size(492, 32);
            this.ItemForEndtime.Text = " ساعت اتمام کاری :";
            this.ItemForEndtime.TextSize = new System.Drawing.Size(121, 24);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.gridControl1;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 197);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(971, 324);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // ItemForIpPose
            // 
            this.ItemForIpPose.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForIpPose.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForIpPose.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForIpPose.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForIpPose.Control = this.IpPoseTextEdit;
            this.ItemForIpPose.Location = new System.Drawing.Point(493, 160);
            this.ItemForIpPose.Name = "ItemForIpPose";
            this.ItemForIpPose.Size = new System.Drawing.Size(478, 37);
            this.ItemForIpPose.Text = " IP دستگاه :";
            this.ItemForIpPose.TextSize = new System.Drawing.Size(121, 24);
            // 
            // ItemForEnabled
            // 
            this.ItemForEnabled.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForEnabled.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForEnabled.Control = this.EnabledCheckEdit;
            this.ItemForEnabled.Location = new System.Drawing.Point(0, 160);
            this.ItemForEnabled.Name = "ItemForEnabled";
            this.ItemForEnabled.Size = new System.Drawing.Size(493, 37);
            this.ItemForEnabled.Text = "وضعیت کاربر :";
            this.ItemForEnabled.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForEnabled.TextSize = new System.Drawing.Size(80, 24);
            this.ItemForEnabled.TextToControlDistance = 5;
            // 
            // ItemForPrinterUser
            // 
            this.ItemForPrinterUser.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForPrinterUser.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForPrinterUser.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForPrinterUser.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForPrinterUser.Control = this.PrinterUserComboBoxEdit;
            this.ItemForPrinterUser.Location = new System.Drawing.Point(493, 128);
            this.ItemForPrinterUser.Name = "ItemForPrinterUser";
            this.ItemForPrinterUser.Size = new System.Drawing.Size(478, 32);
            this.ItemForPrinterUser.Text = " پرینتر پیش فرض :";
            this.ItemForPrinterUser.TextSize = new System.Drawing.Size(121, 24);
            // 
            // ItemForPoseName
            // 
            this.ItemForPoseName.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.ItemForPoseName.AppearanceItemCaption.Options.UseFont = true;
            this.ItemForPoseName.AppearanceItemCaption.Options.UseTextOptions = true;
            this.ItemForPoseName.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.ItemForPoseName.Control = this.PoseNameTextEdit;
            this.ItemForPoseName.Location = new System.Drawing.Point(0, 128);
            this.ItemForPoseName.Name = "ItemForPoseName";
            this.ItemForPoseName.Size = new System.Drawing.Size(493, 32);
            this.ItemForPoseName.Text = "نام دستگاه :";
            this.ItemForPoseName.TextSize = new System.Drawing.Size(121, 24);
            // 
            // frmticketUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(991, 708);
            this.Controls.Add(this.dataLayoutControl1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmticketUser";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "کاربران";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmticketUser_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataLayoutControl1)).EndInit();
            this.dataLayoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodeTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UserNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PasswordTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FullNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EnabledCheckEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AccessCompaniesTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdcompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PrinterUserComboBoxEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IpPoseTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PoseNameTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StarttimeTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EndtimeTimeEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForUserName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForAccessCompanies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFullName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCredit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForStarttime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEndtime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIpPose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForEnabled)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPrinterUser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForPoseName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraDataLayout.DataLayoutControl dataLayoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;

        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem BtnSave;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private DevExpress.XtraBars.BarButtonItem BtnNew;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView grd;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colUserName;
        private DevExpress.XtraGrid.Columns.GridColumn colPassword;
        private DevExpress.XtraGrid.Columns.GridColumn colFullName;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.BarButtonItem BtnRefesh;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem BtnDelete;
        private System.Windows.Forms.BindingSource userBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colAccessCompanies;
        private DevExpress.XtraGrid.Columns.GridColumn colCode;
        private DevExpress.XtraGrid.Columns.GridColumn colEnabled;
        private DevExpress.XtraEditors.TextEdit CodeTextEdit;
        private DevExpress.XtraEditors.TextEdit UserNameTextEdit;
        private DevExpress.XtraEditors.TextEdit PasswordTextEdit;
        private DevExpress.XtraEditors.TextEdit FullNameTextEdit;
        private DevExpress.XtraEditors.ToggleSwitch EnabledCheckEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCode;
        private DevExpress.XtraLayout.LayoutControlItem ItemForUserName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForAccessCompanies;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFullName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPassword;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEnabled;
        private DevExpress.XtraEditors.GridLookUpEdit AccessCompaniesTextEdit;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private DevExpress.XtraGrid.Views.Grid.GridView grdcompany;
        private DevExpress.XtraGrid.Columns.GridColumn colCodeCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colTitle;
        private DevExpress.XtraGrid.Columns.GridColumn colCredit;
        private DevExpress.XtraEditors.TextEdit CreditTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCredit;
        private DevExpress.XtraEditors.ComboBoxEdit PrinterUserComboBoxEdit;
        private DevExpress.XtraEditors.TextEdit IpPoseTextEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPrinterUser;
        private DevExpress.XtraLayout.LayoutControlItem ItemForPoseName;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIpPose;
        private DevExpress.XtraEditors.ComboBoxEdit PoseNameTextEdit;
        private DevExpress.XtraEditors.TimeEdit StarttimeTimeEdit;
        private DevExpress.XtraEditors.TimeEdit EndtimeTimeEdit;
        private DevExpress.XtraLayout.LayoutControlItem ItemForStarttime;
        private DevExpress.XtraLayout.LayoutControlItem ItemForEndtime;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
    }
}