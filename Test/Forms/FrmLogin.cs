﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraEditors;

using BL;
using Model;
using System.Globalization;


namespace Test
{
    public partial class FrmLogin : XtraForm
    {
        public FrmLogin()
        {
            InitializeComponent();
        }
        bool Exitfom;

        /// <summary>
        /// ورو به برنامه
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(Txt_username.Text == string.Empty) || !(Txt_password.Text == string.Empty))
                {
                    User _user = UserServices.FindByUser(Txt_username.Text, Txt_password.Text);
                    if (_user != null && _user.Enabled == true)
                    {

                        TimeSpan tt = TimeSpan.Parse(DateTime.Now.ToString("HH:mm"));
                        Boolean islogin = UserServices.LoginTime(tt, _user.ID);
                        if (!islogin)
                        {
                            XtraMessageBox.Show("کاربر گرامی با توجه به زمان تعیین شده امکان ورد به برنامه امکان پذیر نمی باشد", "پیام", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }

                        Userlogin.Code = _user.Code;
                        Userlogin.FullName = _user.FullName;
                        Userlogin.Credit = _user.Credit;
                        Userlogin.ID = _user.ID;
                        Userlogin.AccessCompanies = _user.AccessCompanies;
                        Userlogin.Password = _user.Password;
                        Userlogin.UserName = _user.UserName;
                        Userlogin.isAdmin = _user.isAdmin;
                        Userlogin.PrinterUser = _user.PrinterUser;
                        Userlogin.PoseName = _user.PoseName;
                        Userlogin.IpPose = _user.IpPose;
                        Userlogin.Enabled = _user.Enabled;
                        Userlogin.Starttime = _user.Starttime;
                        Userlogin.Endtime = _user.Endtime;
                        Userlogin.IsLoacConnection = rdLocal.Checked;
                        SettingLogin.ListSettig = SettingServices.GetAll();
                        ReturnStateTickets(_user);
                        Exitfom = true;
                        this.DialogResult = DialogResult.OK;
                        this.Close();

                    }
                    else
                    {
                        Txt_password.EditValue = string.Empty;
                        lblError.Visible = true;
                    }

                }
                else
                {
                    XtraMessageBox.Show("لطفا نام کاربری و کلمه عبور را وارد کنید", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Txt_username.Focus();
                    Txt_password.EditValue = string.Empty;
                    return;
                }

            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
        }

        private void frmticketLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Exitfom)
            {
                e.Cancel = true;
                this.DialogResult = DialogResult.OK;
                this.Hide();
                new frmticketHome().Show();
            }
            else
            {
                Application.ExitThread();
            }



        }

        private void Txt_password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                button1_Click(null, null);
        }


        /// <summary>
        /// برگشت بلیط هایی که هنگام فروش با خطا مواجه شده اند به حالت اولیه
        /// </summary>
        void ReturnStateTickets(User _user)
        {
            try
            {
                Utilitise.ShowWating(this, Utilitise.StateWating.Show);
                if (_user.AccessCompanies != null)
                {
                    List<Company> AccessCompanyUser = new List<Company>();  //  لیست شرکت های سطح دسترسی
                    AccessCompanyUser =CompanysServices.GetCompaniesByListCode(_user.AccessCompanies);
                    string connectionString = "";
                    foreach (Company company in AccessCompanyUser)
                    {
                        if (Userlogin.IsLoacConnection)
                        {
                            connectionString = company.LocalConnectionString;
                        }
                        else
                        {
                            connectionString = company.ConnectionString;
                        }
                        List<Ticket> liticket = TicketServices.GetTicketPaylockRollBack(connectionString, _user.Code);  // خواندن بلیط ها ناموفق از تعاونی ها
                        if (liticket.Count > 0)
                        {
                            foreach (Ticket _ticket in liticket)
                            {
                                if (_ticket.Expire != null)
                                {
                                    _ticket.Price = _ticket.Price + _ticket.FundPrice.ToInt();
                                    _ticket.PayLock = null;
                                    _ticket.FundPrice = 0;
                                    _ticket.PayDate = null;
                                    _ticket.CashierCode = null;
                                    _ticket.CashierName = null;
                                    _ticket.Printed = null;
                                    _ticket.Discount = 0;
                                    _ticket.FundID = 1;
                                    _ticket.SaleType = 1;
                                    int result = TicketServices.UpdateTicketCompany(_ticket, connectionString); // بازگشت بلیط ناموفق به حالت رزروی در سیستم تعاونی
                                }
                                else
                                {
                                    TicketServices.DeleteTickets(_ticket.ID, connectionString);      // حذف بلیط فروش مستقیم ناموفق از صندوق و تعاونی
                                }
                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }
            finally
            {
                Utilitise.ShowWating(this, Utilitise.StateWating.Show);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.ExitThread();
        }
    }
}