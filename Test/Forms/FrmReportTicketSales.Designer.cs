﻿namespace Test
{
    partial class FrmReportTicketSales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmReportTicketSales));
            DevExpress.Utils.SuperToolTip superToolTip1 = new DevExpress.Utils.SuperToolTip();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem1 = new DevExpress.Utils.ToolTipTitleItem();
            DevExpress.Utils.ToolTipItem toolTipItem1 = new DevExpress.Utils.ToolTipItem();
            DevExpress.Utils.ToolTipSeparatorItem toolTipSeparatorItem1 = new DevExpress.Utils.ToolTipSeparatorItem();
            DevExpress.Utils.ToolTipTitleItem toolTipTitleItem2 = new DevExpress.Utils.ToolTipTitleItem();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.btnRefresh = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.btnClose = new DevExpress.XtraBars.BarButtonItem();
            this.btnReport = new DevExpress.XtraBars.BarButtonItem();
            this.btnPrint = new DevExpress.XtraBars.BarButtonItem();
            this.BtnTicketInternet = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.navigationPane1 = new DevExpress.XtraBars.Navigation.NavigationPane();
            this.navigationPage1 = new DevExpress.XtraBars.Navigation.NavigationPage();
            this.LayoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.timeEdit2 = new DevExpress.XtraEditors.TimeEdit();
            this.timeEdit1 = new DevExpress.XtraEditors.TimeEdit();
            this.ChCompany = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.companyBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtTodate = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.txtFromDate = new FarsiLibrary.Win.DevExpress.XtraFADateEdit();
            this.SearchControl1 = new DevExpress.XtraEditors.SearchControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.ticketLocalBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.grd = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTripID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFullname = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colChairsName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCityName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTimeSale = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSaleDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemXtraFADateEdit1 = new FarsiLibrary.Win.DevExpress.RepositoryItemXtraFADateEdit();
            this.colPrice = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNumChairs = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCashierName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.reConpany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colCashierCode = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidBackPercent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPriceDiscount = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPaidBackDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTotalTicket = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFundPriceValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeTicket = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPayDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.Coltime = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LayoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.LayoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.LayoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.userBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.splashScreenManager1 = new DevExpress.XtraSplashScreen.SplashScreenManager(this, typeof(global::Test.WaitForm1), true, true);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).BeginInit();
            this.navigationPane1.SuspendLayout();
            this.navigationPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).BeginInit();
            this.LayoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketLocalBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reConpany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.btnRefresh,
            this.barButtonItem2,
            this.btnClose,
            this.btnReport,
            this.btnPrint,
            this.BtnTicketInternet});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 7;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbon.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False;
            this.ribbon.ShowToolbarCustomizeItem = false;
            this.ribbon.Size = new System.Drawing.Size(1384, 146);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Toolbar.ShowCustomizeItem = false;
            // 
            // btnRefresh
            // 
            this.btnRefresh.Caption = "بروز رسانی تعاونی ها";
            this.btnRefresh.Glyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.Glyph")));
            this.btnRefresh.Id = 1;
            this.btnRefresh.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F5);
            this.btnRefresh.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnRefresh.LargeGlyph")));
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnRefresh_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // btnClose
            // 
            this.btnClose.Caption = "بستن";
            this.btnClose.Glyph = ((System.Drawing.Image)(resources.GetObject("btnClose.Glyph")));
            this.btnClose.Id = 3;
            this.btnClose.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F4);
            this.btnClose.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnClose.LargeGlyph")));
            this.btnClose.Name = "btnClose";
            this.btnClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnClose_ItemClick);
            // 
            // btnReport
            // 
            this.btnReport.Caption = "گزارش";
            this.btnReport.Glyph = ((System.Drawing.Image)(resources.GetObject("btnReport.Glyph")));
            this.btnReport.Id = 4;
            this.btnReport.ItemShortcut = new DevExpress.XtraBars.BarShortcut(System.Windows.Forms.Keys.F6);
            this.btnReport.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnReport.LargeGlyph")));
            this.btnReport.Name = "btnReport";
            this.btnReport.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReport_ItemClick);
            // 
            // btnPrint
            // 
            this.btnPrint.Caption = "چاپ گزارش";
            this.btnPrint.Glyph = ((System.Drawing.Image)(resources.GetObject("btnPrint.Glyph")));
            this.btnPrint.Id = 5;
            this.btnPrint.ItemShortcut = new DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P));
            this.btnPrint.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("btnPrint.LargeGlyph")));
            this.btnPrint.Name = "btnPrint";
            toolTipTitleItem1.Appearance.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            toolTipTitleItem1.Appearance.Options.UseImage = true;
            toolTipTitleItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolTipTitleItem1.Image")));
            toolTipTitleItem1.Text = "چاپ";
            toolTipItem1.LeftIndent = 6;
            toolTipItem1.Text = "چاپ گزارش با کلید میانبر \r\n";
            toolTipTitleItem2.LeftIndent = 6;
            toolTipTitleItem2.Text = "CTRL+P";
            superToolTip1.Items.Add(toolTipTitleItem1);
            superToolTip1.Items.Add(toolTipItem1);
            superToolTip1.Items.Add(toolTipSeparatorItem1);
            superToolTip1.Items.Add(toolTipTitleItem2);
            this.btnPrint.SuperTip = superToolTip1;
            // 
            // BtnTicketInternet
            // 
            this.BtnTicketInternet.Caption = "بلیط های اینترنتی";
            this.BtnTicketInternet.Glyph = ((System.Drawing.Image)(resources.GetObject("BtnTicketInternet.Glyph")));
            this.BtnTicketInternet.Id = 6;
            this.BtnTicketInternet.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("BtnTicketInternet.LargeGlyph")));
            this.BtnTicketInternet.Name = "BtnTicketInternet";
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup2,
            this.ribbonPageGroup3});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "عملیات";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnPrint);
            this.ribbonPageGroup1.ItemLinks.Add(this.btnReport);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.ShowCaptionButton = false;
            this.ribbonPageGroup1.Text = "گزارشات";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.btnRefresh);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.ShowCaptionButton = false;
            this.ribbonPageGroup2.Text = "بروز رسانی";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.btnClose);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.ShowCaptionButton = false;
            this.ribbonPageGroup3.Text = "بستن";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 766);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1384, 21);
            // 
            // navigationPane1
            // 
            this.navigationPane1.Controls.Add(this.navigationPage1);
            this.navigationPane1.Dock = System.Windows.Forms.DockStyle.Right;
            this.navigationPane1.Location = new System.Drawing.Point(1332, 146);
            this.navigationPane1.Name = "navigationPane1";
            this.navigationPane1.PageProperties.AppearanceCaption.Options.UseTextOptions = true;
            this.navigationPane1.PageProperties.AppearanceCaption.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            this.navigationPane1.PageProperties.ShowExpandButton = false;
            this.navigationPane1.PageProperties.ShowMode = DevExpress.XtraBars.Navigation.ItemShowMode.Image;
            this.navigationPane1.Pages.AddRange(new DevExpress.XtraBars.Navigation.NavigationPageBase[] {
            this.navigationPage1});
            this.navigationPane1.RegularSize = new System.Drawing.Size(271, 620);
            this.navigationPane1.SelectedPage = this.navigationPage1;
            this.navigationPane1.Size = new System.Drawing.Size(52, 620);
            this.navigationPane1.State = DevExpress.XtraBars.Navigation.NavigationPaneState.Collapsed;
            this.navigationPane1.TabIndex = 25;
            this.navigationPane1.Text = "navigationPane1";
            // 
            // navigationPage1
            // 
            this.navigationPage1.Caption = "جستجو پیشرفته";
            this.navigationPage1.Controls.Add(this.LayoutControl2);
            this.navigationPage1.Image = ((System.Drawing.Image)(resources.GetObject("navigationPage1.Image")));
            this.navigationPage1.ImageUri.Uri = "Zoom";
            this.navigationPage1.Name = "navigationPage1";
            this.navigationPage1.Size = new System.Drawing.Size(201, 560);
            // 
            // LayoutControl2
            // 
            this.LayoutControl2.Controls.Add(this.timeEdit2);
            this.LayoutControl2.Controls.Add(this.timeEdit1);
            this.LayoutControl2.Controls.Add(this.ChCompany);
            this.LayoutControl2.Controls.Add(this.txtTodate);
            this.LayoutControl2.Controls.Add(this.txtFromDate);
            this.LayoutControl2.Controls.Add(this.SearchControl1);
            this.LayoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LayoutControl2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControl2.Name = "LayoutControl2";
            this.LayoutControl2.OptionsView.RightToLeftMirroringApplied = true;
            this.LayoutControl2.Root = this.LayoutControlGroup2;
            this.LayoutControl2.Size = new System.Drawing.Size(201, 560);
            this.LayoutControl2.TabIndex = 0;
            this.LayoutControl2.Text = "LayoutControl2";
            // 
            // timeEdit2
            // 
            this.timeEdit2.EditValue = new System.DateTime(2019, 5, 14, 0, 0, 0, 0);
            this.timeEdit2.Location = new System.Drawing.Point(9, 167);
            this.timeEdit2.MenuManager = this.ribbon;
            this.timeEdit2.Name = "timeEdit2";
            this.timeEdit2.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.timeEdit2.Properties.Appearance.Options.UseFont = true;
            this.timeEdit2.Properties.Appearance.Options.UseTextOptions = true;
            this.timeEdit2.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.timeEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timeEdit2.Properties.DisplayFormat.FormatString = "T";
            this.timeEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeEdit2.Properties.EditFormat.FormatString = "T";
            this.timeEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeEdit2.Size = new System.Drawing.Size(139, 24);
            this.timeEdit2.StyleController = this.LayoutControl2;
            this.timeEdit2.TabIndex = 31;
            this.timeEdit2.EditValueChanged += new System.EventHandler(this.timeEdit2_EditValueChanged);
            // 
            // timeEdit1
            // 
            this.timeEdit1.EditValue = new System.DateTime(2019, 5, 14, 0, 0, 0, 0);
            this.timeEdit1.Location = new System.Drawing.Point(9, 139);
            this.timeEdit1.MenuManager = this.ribbon;
            this.timeEdit1.Name = "timeEdit1";
            this.timeEdit1.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.timeEdit1.Properties.Appearance.Options.UseFont = true;
            this.timeEdit1.Properties.Appearance.Options.UseTextOptions = true;
            this.timeEdit1.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.timeEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.timeEdit1.Properties.DisplayFormat.FormatString = "T";
            this.timeEdit1.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeEdit1.Properties.EditFormat.FormatString = "T";
            this.timeEdit1.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.timeEdit1.Size = new System.Drawing.Size(139, 24);
            this.timeEdit1.StyleController = this.LayoutControl2;
            this.timeEdit1.TabIndex = 30;
            this.timeEdit1.EditValueChanged += new System.EventHandler(this.timeEdit2_EditValueChanged);
            // 
            // ChCompany
            // 
            this.ChCompany.Appearance.BackColor = System.Drawing.SystemColors.Control;
            this.ChCompany.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.ChCompany.Appearance.Options.UseBackColor = true;
            this.ChCompany.Appearance.Options.UseFont = true;
            this.ChCompany.CheckOnClick = true;
            this.ChCompany.Cursor = System.Windows.Forms.Cursors.Default;
            this.ChCompany.DataSource = this.companyBindingSource;
            this.ChCompany.DisplayMember = "Title";
            this.ChCompany.Location = new System.Drawing.Point(9, 220);
            this.ChCompany.MultiColumn = true;
            this.ChCompany.Name = "ChCompany";
            this.ChCompany.Size = new System.Drawing.Size(183, 331);
            this.ChCompany.StyleController = this.LayoutControl2;
            this.ChCompany.TabIndex = 27;
            this.ChCompany.ValueMember = "Code";
            this.ChCompany.ItemCheck += new DevExpress.XtraEditors.Controls.ItemCheckEventHandler(this.ChCompany_ItemCheck);
            // 
            // companyBindingSource
            // 
            this.companyBindingSource.DataSource = typeof(Model.Company);
            // 
            // txtTodate
            // 
            this.txtTodate.EditValue = null;
            this.txtTodate.Location = new System.Drawing.Point(9, 86);
            this.txtTodate.MenuManager = this.ribbon;
            this.txtTodate.Name = "txtTodate";
            this.txtTodate.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txtTodate.Properties.Appearance.Options.UseFont = true;
            this.txtTodate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtTodate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtTodate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTodate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTodate.Properties.NullValuePrompt = "تا تاریخ";
            this.txtTodate.Size = new System.Drawing.Size(139, 24);
            this.txtTodate.StyleController = this.LayoutControl2;
            this.txtTodate.TabIndex = 4;
            this.txtTodate.Click += new System.EventHandler(this.txtTodate_Click);
            // 
            // txtFromDate
            // 
            this.txtFromDate.EditValue = null;
            this.txtFromDate.Location = new System.Drawing.Point(9, 58);
            this.txtFromDate.MenuManager = this.ribbon;
            this.txtFromDate.Name = "txtFromDate";
            this.txtFromDate.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.txtFromDate.Properties.Appearance.Options.UseFont = true;
            this.txtFromDate.Properties.Appearance.Options.UseTextOptions = true;
            this.txtFromDate.Properties.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.txtFromDate.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtFromDate.Properties.NullValuePrompt = "از تاریخ";
            this.txtFromDate.Size = new System.Drawing.Size(139, 24);
            this.txtFromDate.StyleController = this.LayoutControl2;
            this.txtFromDate.TabIndex = 3;
            this.txtFromDate.Click += new System.EventHandler(this.txtFromDate_Click);
            // 
            // SearchControl1
            // 
            this.SearchControl1.Client = this.gridControl1;
            this.SearchControl1.Location = new System.Drawing.Point(6, 6);
            this.SearchControl1.Name = "SearchControl1";
            this.SearchControl1.Properties.Appearance.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F);
            this.SearchControl1.Properties.Appearance.Options.UseFont = true;
            this.SearchControl1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Repository.ClearButton(),
            new DevExpress.XtraEditors.Repository.SearchButton()});
            this.SearchControl1.Properties.Client = this.gridControl1;
            this.SearchControl1.Size = new System.Drawing.Size(189, 26);
            this.SearchControl1.StyleController = this.LayoutControl2;
            this.SearchControl1.TabIndex = 16;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.ticketLocalBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 146);
            this.gridControl1.MainView = this.grd;
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemXtraFADateEdit1,
            this.reConpany});
            this.gridControl1.Size = new System.Drawing.Size(1332, 620);
            this.gridControl1.TabIndex = 26;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grd});
            // 
            // ticketLocalBindingSource
            // 
            this.ticketLocalBindingSource.DataSource = typeof(Model.TicketLocal);
            // 
            // grd
            // 
            this.grd.Appearance.FilterPanel.Font = new System.Drawing.Font("Tornado Tahoma", 8.25F);
            this.grd.Appearance.FilterPanel.Options.UseFont = true;
            this.grd.Appearance.FilterPanel.Options.UseTextOptions = true;
            this.grd.Appearance.FilterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.FocusedCell.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedCell.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedCell.Options.UseBackColor = true;
            this.grd.Appearance.FocusedCell.Options.UseForeColor = true;
            this.grd.Appearance.FocusedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.FocusedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.FocusedRow.Options.UseBackColor = true;
            this.grd.Appearance.FocusedRow.Options.UseForeColor = true;
            this.grd.Appearance.FooterPanel.Font = new System.Drawing.Font("Tornado Tahoma", 8.25F);
            this.grd.Appearance.FooterPanel.Options.UseFont = true;
            this.grd.Appearance.FooterPanel.Options.UseTextOptions = true;
            this.grd.Appearance.FooterPanel.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.GroupFooter.Font = new System.Drawing.Font("Tornado Tahoma", 8.25F);
            this.grd.Appearance.GroupFooter.Options.UseFont = true;
            this.grd.Appearance.GroupFooter.Options.UseTextOptions = true;
            this.grd.Appearance.GroupFooter.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.grd.Appearance.SelectedRow.BackColor = System.Drawing.Color.DodgerBlue;
            this.grd.Appearance.SelectedRow.ForeColor = System.Drawing.Color.White;
            this.grd.Appearance.SelectedRow.Options.UseBackColor = true;
            this.grd.Appearance.SelectedRow.Options.UseForeColor = true;
            this.grd.ColumnPanelRowHeight = 30;
            this.grd.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colTripID,
            this.colFullname,
            this.colNo,
            this.colChairsName,
            this.colCityName,
            this.colTel,
            this.colTimeSale,
            this.colSaleDate,
            this.colPrice,
            this.colDiscount,
            this.colNumChairs,
            this.colCashierName,
            this.colCompanyCode,
            this.colCashierCode,
            this.colPaidBackPercent,
            this.colPriceDiscount,
            this.colPaidBackDate,
            this.colTotalTicket,
            this.colFundPriceValue,
            this.colTypeTicket,
            this.colPayDate,
            this.Coltime});
            this.grd.GridControl = this.gridControl1;
            this.grd.IndicatorWidth = 30;
            this.grd.Name = "grd";
            this.grd.OptionsBehavior.Editable = false;
            this.grd.OptionsFind.AlwaysVisible = true;
            this.grd.OptionsFind.FindNullPrompt = " جست و جو . . . ";
            this.grd.OptionsView.ShowFooter = true;
            this.grd.OptionsView.ShowGroupPanel = false;
            this.grd.CustomDrawRowIndicator += new DevExpress.XtraGrid.Views.Grid.RowIndicatorCustomDrawEventHandler(this.grd_CustomDrawRowIndicator);
            this.grd.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.grd_CustomUnboundColumnData);
            // 
            // colID
            // 
            this.colID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colID.AppearanceCell.Options.UseFont = true;
            this.colID.AppearanceCell.Options.UseTextOptions = true;
            this.colID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colID.AppearanceHeader.Options.UseFont = true;
            this.colID.AppearanceHeader.Options.UseTextOptions = true;
            this.colID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colID.Caption = "#";
            this.colID.FieldName = "ID";
            this.colID.Name = "colID";
            this.colID.Width = 36;
            // 
            // colTripID
            // 
            this.colTripID.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTripID.AppearanceCell.Options.UseFont = true;
            this.colTripID.AppearanceCell.Options.UseTextOptions = true;
            this.colTripID.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTripID.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colTripID.AppearanceHeader.Options.UseFont = true;
            this.colTripID.AppearanceHeader.Options.UseTextOptions = true;
            this.colTripID.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTripID.Caption = "سفر";
            this.colTripID.FieldName = "TripID";
            this.colTripID.Name = "colTripID";
            this.colTripID.Width = 70;
            // 
            // colFullname
            // 
            this.colFullname.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colFullname.AppearanceCell.Options.UseFont = true;
            this.colFullname.AppearanceCell.Options.UseTextOptions = true;
            this.colFullname.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullname.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colFullname.AppearanceHeader.Options.UseFont = true;
            this.colFullname.AppearanceHeader.Options.UseTextOptions = true;
            this.colFullname.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFullname.Caption = "مشتری";
            this.colFullname.FieldName = "Fullname";
            this.colFullname.Name = "colFullname";
            this.colFullname.Visible = true;
            this.colFullname.VisibleIndex = 2;
            this.colFullname.Width = 105;
            // 
            // colNo
            // 
            this.colNo.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colNo.AppearanceCell.Options.UseFont = true;
            this.colNo.AppearanceCell.Options.UseTextOptions = true;
            this.colNo.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colNo.AppearanceHeader.Options.UseFont = true;
            this.colNo.AppearanceHeader.Options.UseTextOptions = true;
            this.colNo.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNo.Caption = "شماره بلیط";
            this.colNo.FieldName = "No";
            this.colNo.Name = "colNo";
            this.colNo.Visible = true;
            this.colNo.VisibleIndex = 4;
            this.colNo.Width = 99;
            // 
            // colChairsName
            // 
            this.colChairsName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colChairsName.AppearanceCell.Options.UseFont = true;
            this.colChairsName.AppearanceCell.Options.UseTextOptions = true;
            this.colChairsName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colChairsName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colChairsName.AppearanceHeader.Options.UseFont = true;
            this.colChairsName.AppearanceHeader.Options.UseTextOptions = true;
            this.colChairsName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colChairsName.Caption = "صندلی ها";
            this.colChairsName.FieldName = "ChairsName";
            this.colChairsName.Name = "colChairsName";
            this.colChairsName.Width = 56;
            // 
            // colCityName
            // 
            this.colCityName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCityName.AppearanceCell.Options.UseFont = true;
            this.colCityName.AppearanceCell.Options.UseTextOptions = true;
            this.colCityName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCityName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colCityName.AppearanceHeader.Options.UseFont = true;
            this.colCityName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCityName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCityName.Caption = "مقصد";
            this.colCityName.FieldName = "CityName";
            this.colCityName.Name = "colCityName";
            this.colCityName.Visible = true;
            this.colCityName.VisibleIndex = 3;
            this.colCityName.Width = 66;
            // 
            // colTel
            // 
            this.colTel.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTel.AppearanceCell.Options.UseFont = true;
            this.colTel.AppearanceCell.Options.UseTextOptions = true;
            this.colTel.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTel.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colTel.AppearanceHeader.Options.UseFont = true;
            this.colTel.AppearanceHeader.Options.UseTextOptions = true;
            this.colTel.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTel.Caption = "تلفن";
            this.colTel.FieldName = "Tel";
            this.colTel.Name = "colTel";
            this.colTel.Width = 98;
            // 
            // colTimeSale
            // 
            this.colTimeSale.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTimeSale.AppearanceCell.Options.UseFont = true;
            this.colTimeSale.AppearanceCell.Options.UseTextOptions = true;
            this.colTimeSale.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeSale.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colTimeSale.AppearanceHeader.Options.UseFont = true;
            this.colTimeSale.AppearanceHeader.Options.UseTextOptions = true;
            this.colTimeSale.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTimeSale.Caption = "ساعت";
            this.colTimeSale.DisplayFormat.FormatString = "T";
            this.colTimeSale.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colTimeSale.FieldName = "TimeSale";
            this.colTimeSale.Name = "colTimeSale";
            this.colTimeSale.Width = 65;
            // 
            // colSaleDate
            // 
            this.colSaleDate.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colSaleDate.AppearanceCell.Options.UseFont = true;
            this.colSaleDate.AppearanceCell.Options.UseTextOptions = true;
            this.colSaleDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSaleDate.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colSaleDate.AppearanceHeader.Options.UseFont = true;
            this.colSaleDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colSaleDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colSaleDate.Caption = "تاریخ";
            this.colSaleDate.FieldName = "SaleDate";
            this.colSaleDate.Name = "colSaleDate";
            this.colSaleDate.Width = 98;
            // 
            // repositoryItemXtraFADateEdit1
            // 
            this.repositoryItemXtraFADateEdit1.AutoHeight = false;
            this.repositoryItemXtraFADateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemXtraFADateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemXtraFADateEdit1.Name = "repositoryItemXtraFADateEdit1";
            // 
            // colPrice
            // 
            this.colPrice.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPrice.AppearanceCell.Options.UseFont = true;
            this.colPrice.AppearanceCell.Options.UseTextOptions = true;
            this.colPrice.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colPrice.AppearanceHeader.Options.UseFont = true;
            this.colPrice.AppearanceHeader.Options.UseTextOptions = true;
            this.colPrice.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPrice.Caption = "هزینه بلیط";
            this.colPrice.DisplayFormat.FormatString = "c0";
            this.colPrice.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPrice.FieldName = "Price";
            this.colPrice.Name = "colPrice";
            this.colPrice.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Price", "  {0:c0}")});
            this.colPrice.Width = 63;
            // 
            // colDiscount
            // 
            this.colDiscount.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colDiscount.AppearanceCell.Options.UseFont = true;
            this.colDiscount.AppearanceCell.Options.UseTextOptions = true;
            this.colDiscount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDiscount.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colDiscount.AppearanceHeader.Options.UseFont = true;
            this.colDiscount.AppearanceHeader.Options.UseTextOptions = true;
            this.colDiscount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colDiscount.Caption = "تخفیف";
            this.colDiscount.FieldName = "Discount";
            this.colDiscount.Name = "colDiscount";
            this.colDiscount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Discount", "{0:0.##}")});
            this.colDiscount.Visible = true;
            this.colDiscount.VisibleIndex = 6;
            this.colDiscount.Width = 63;
            // 
            // colNumChairs
            // 
            this.colNumChairs.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colNumChairs.AppearanceCell.Options.UseFont = true;
            this.colNumChairs.AppearanceCell.Options.UseTextOptions = true;
            this.colNumChairs.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumChairs.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colNumChairs.AppearanceHeader.Options.UseFont = true;
            this.colNumChairs.AppearanceHeader.Options.UseTextOptions = true;
            this.colNumChairs.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colNumChairs.Caption = "ت صندلی";
            this.colNumChairs.FieldName = "NumChairs";
            this.colNumChairs.Name = "colNumChairs";
            this.colNumChairs.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "NumChairs", "{0:0.##}")});
            this.colNumChairs.Visible = true;
            this.colNumChairs.VisibleIndex = 5;
            this.colNumChairs.Width = 80;
            // 
            // colCashierName
            // 
            this.colCashierName.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCashierName.AppearanceCell.Options.UseFont = true;
            this.colCashierName.AppearanceCell.Options.UseTextOptions = true;
            this.colCashierName.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierName.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colCashierName.AppearanceHeader.Options.UseFont = true;
            this.colCashierName.AppearanceHeader.Options.UseTextOptions = true;
            this.colCashierName.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierName.Caption = "صندوقدار";
            this.colCashierName.FieldName = "CashierName";
            this.colCashierName.Name = "colCashierName";
            this.colCashierName.Visible = true;
            this.colCashierName.VisibleIndex = 8;
            this.colCashierName.Width = 96;
            // 
            // colCompanyCode
            // 
            this.colCompanyCode.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCompanyCode.AppearanceCell.Options.UseFont = true;
            this.colCompanyCode.AppearanceCell.Options.UseTextOptions = true;
            this.colCompanyCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyCode.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colCompanyCode.AppearanceHeader.Options.UseFont = true;
            this.colCompanyCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCompanyCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCompanyCode.Caption = "شرکت";
            this.colCompanyCode.ColumnEdit = this.reConpany;
            this.colCompanyCode.FieldName = "CompanyCode";
            this.colCompanyCode.Name = "colCompanyCode";
            this.colCompanyCode.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "CompanyCode", "{0}")});
            this.colCompanyCode.Visible = true;
            this.colCompanyCode.VisibleIndex = 0;
            this.colCompanyCode.Width = 108;
            // 
            // reConpany
            // 
            this.reConpany.AutoHeight = false;
            this.reConpany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.reConpany.DataSource = this.companyBindingSource;
            this.reConpany.DisplayMember = "Title";
            this.reConpany.Name = "reConpany";
            this.reConpany.NullText = "نامشخص";
            this.reConpany.ValueMember = "Code";
            // 
            // colCashierCode
            // 
            this.colCashierCode.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colCashierCode.AppearanceCell.Options.UseFont = true;
            this.colCashierCode.AppearanceCell.Options.UseTextOptions = true;
            this.colCashierCode.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierCode.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colCashierCode.AppearanceHeader.Options.UseFont = true;
            this.colCashierCode.AppearanceHeader.Options.UseTextOptions = true;
            this.colCashierCode.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colCashierCode.Caption = "کدصندوقدار";
            this.colCashierCode.FieldName = "CashierCode";
            this.colCashierCode.Name = "colCashierCode";
            // 
            // colPaidBackPercent
            // 
            this.colPaidBackPercent.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPaidBackPercent.AppearanceCell.Options.UseFont = true;
            this.colPaidBackPercent.AppearanceCell.Options.UseTextOptions = true;
            this.colPaidBackPercent.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackPercent.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colPaidBackPercent.AppearanceHeader.Options.UseFont = true;
            this.colPaidBackPercent.AppearanceHeader.Options.UseTextOptions = true;
            this.colPaidBackPercent.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackPercent.Caption = "درصد برگشتی";
            this.colPaidBackPercent.FieldName = "PaidBackPercent";
            this.colPaidBackPercent.Name = "colPaidBackPercent";
            this.colPaidBackPercent.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PaidBackPercent", "{0:0.##}")});
            this.colPaidBackPercent.Width = 73;
            // 
            // colPriceDiscount
            // 
            this.colPriceDiscount.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPriceDiscount.AppearanceCell.Options.UseFont = true;
            this.colPriceDiscount.AppearanceCell.Options.UseTextOptions = true;
            this.colPriceDiscount.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceDiscount.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colPriceDiscount.AppearanceHeader.Options.UseFont = true;
            this.colPriceDiscount.AppearanceHeader.Options.UseTextOptions = true;
            this.colPriceDiscount.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPriceDiscount.Caption = "مبلغ برگشتی";
            this.colPriceDiscount.DisplayFormat.FormatString = "c0";
            this.colPriceDiscount.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPriceDiscount.FieldName = "PricePaidBack";
            this.colPriceDiscount.Name = "colPriceDiscount";
            this.colPriceDiscount.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "PriceDiscount", "{0:c0}")});
            this.colPriceDiscount.Width = 74;
            // 
            // colPaidBackDate
            // 
            this.colPaidBackDate.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPaidBackDate.AppearanceCell.Options.UseFont = true;
            this.colPaidBackDate.AppearanceCell.Options.UseTextOptions = true;
            this.colPaidBackDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackDate.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colPaidBackDate.AppearanceHeader.Options.UseFont = true;
            this.colPaidBackDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colPaidBackDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPaidBackDate.Caption = "س برگشت";
            this.colPaidBackDate.DisplayFormat.FormatString = "HH:mm:tt";
            this.colPaidBackDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colPaidBackDate.FieldName = "PaidBackDate";
            this.colPaidBackDate.Name = "colPaidBackDate";
            this.colPaidBackDate.Width = 98;
            // 
            // colTotalTicket
            // 
            this.colTotalTicket.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTotalTicket.AppearanceCell.Options.UseFont = true;
            this.colTotalTicket.AppearanceCell.Options.UseTextOptions = true;
            this.colTotalTicket.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalTicket.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colTotalTicket.AppearanceHeader.Options.UseFont = true;
            this.colTotalTicket.AppearanceHeader.Options.UseTextOptions = true;
            this.colTotalTicket.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTotalTicket.Caption = "جمع کل";
            this.colTotalTicket.DisplayFormat.FormatString = "c0";
            this.colTotalTicket.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colTotalTicket.FieldName = "TotalTicket";
            this.colTotalTicket.Name = "colTotalTicket";
            this.colTotalTicket.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "TotalTicket", "ج {0:c0}")});
            this.colTotalTicket.Visible = true;
            this.colTotalTicket.VisibleIndex = 7;
            this.colTotalTicket.Width = 85;
            // 
            // colFundPriceValue
            // 
            this.colFundPriceValue.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colFundPriceValue.AppearanceCell.Options.UseFont = true;
            this.colFundPriceValue.AppearanceCell.Options.UseTextOptions = true;
            this.colFundPriceValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFundPriceValue.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colFundPriceValue.AppearanceHeader.Options.UseFont = true;
            this.colFundPriceValue.AppearanceHeader.Options.UseTextOptions = true;
            this.colFundPriceValue.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colFundPriceValue.Caption = "سهم صندوقدار";
            this.colFundPriceValue.DisplayFormat.FormatString = "c0";
            this.colFundPriceValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colFundPriceValue.FieldName = "FundPriceValue";
            this.colFundPriceValue.Name = "colFundPriceValue";
            this.colFundPriceValue.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "FundPrice", "{0:c0}")});
            this.colFundPriceValue.Width = 85;
            // 
            // colTypeTicket
            // 
            this.colTypeTicket.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colTypeTicket.AppearanceCell.Options.UseFont = true;
            this.colTypeTicket.AppearanceCell.Options.UseTextOptions = true;
            this.colTypeTicket.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTypeTicket.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colTypeTicket.AppearanceHeader.Options.UseFont = true;
            this.colTypeTicket.AppearanceHeader.Options.UseTextOptions = true;
            this.colTypeTicket.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colTypeTicket.Caption = "نوع پرداخت";
            this.colTypeTicket.FieldName = "TypeTicket";
            this.colTypeTicket.Name = "colTypeTicket";
            this.colTypeTicket.Visible = true;
            this.colTypeTicket.VisibleIndex = 9;
            this.colTypeTicket.Width = 96;
            // 
            // colPayDate
            // 
            this.colPayDate.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.colPayDate.AppearanceCell.Options.UseFont = true;
            this.colPayDate.AppearanceCell.Options.UseTextOptions = true;
            this.colPayDate.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPayDate.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.colPayDate.AppearanceHeader.Options.UseFont = true;
            this.colPayDate.AppearanceHeader.Options.UseTextOptions = true;
            this.colPayDate.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.colPayDate.Caption = "ت برگشت";
            this.colPayDate.FieldName = "PayDate";
            this.colPayDate.Name = "colPayDate";
            // 
            // Coltime
            // 
            this.Coltime.AppearanceCell.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.Coltime.AppearanceCell.Options.UseFont = true;
            this.Coltime.AppearanceCell.Options.UseTextOptions = true;
            this.Coltime.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Coltime.AppearanceHeader.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.Coltime.AppearanceHeader.Options.UseFont = true;
            this.Coltime.AppearanceHeader.Options.UseTextOptions = true;
            this.Coltime.AppearanceHeader.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.Coltime.Caption = "زمان";
            this.Coltime.FieldName = "Time";
            this.Coltime.Name = "Coltime";
            this.Coltime.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.Coltime.Visible = true;
            this.Coltime.VisibleIndex = 1;
            // 
            // LayoutControlGroup2
            // 
            this.LayoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.LayoutControlGroup2.GroupBordersVisible = false;
            this.LayoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.LayoutControlGroup3,
            this.LayoutControlGroup4,
            this.LayoutControlItem3,
            this.layoutControlGroup1});
            this.LayoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlGroup2.Name = "Root";
            this.LayoutControlGroup2.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 4, 4, 4);
            this.LayoutControlGroup2.Size = new System.Drawing.Size(201, 560);
            this.LayoutControlGroup2.TextVisible = false;
            // 
            // LayoutControlGroup3
            // 
            this.LayoutControlGroup3.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup3.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup3.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 8.25F);
            this.LayoutControlGroup3.AppearanceItemCaption.Options.UseFont = true;
            this.LayoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem5,
            this.layoutControlItem1});
            this.LayoutControlGroup3.Location = new System.Drawing.Point(0, 30);
            this.LayoutControlGroup3.Name = "LayoutControlGroup3";
            this.LayoutControlGroup3.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup3.Size = new System.Drawing.Size(193, 81);
            this.LayoutControlGroup3.Text = "گزارش تاریخ";
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem5.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem5.Control = this.txtFromDate;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(187, 28);
            this.layoutControlItem5.Text = "از تاریخ";
            this.layoutControlItem5.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(41, 24);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.layoutControlItem1.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem1.Control = this.txtTodate;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(187, 28);
            this.layoutControlItem1.Text = "تا تاریخ";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(41, 24);
            // 
            // LayoutControlGroup4
            // 
            this.LayoutControlGroup4.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.LayoutControlGroup4.AppearanceGroup.Options.UseFont = true;
            this.LayoutControlGroup4.AppearanceItemCaption.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 10F);
            this.LayoutControlGroup4.AppearanceItemCaption.Options.UseFont = true;
            this.LayoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2});
            this.LayoutControlGroup4.Location = new System.Drawing.Point(0, 192);
            this.LayoutControlGroup4.Name = "LayoutControlGroup4";
            this.LayoutControlGroup4.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.LayoutControlGroup4.Size = new System.Drawing.Size(193, 360);
            this.LayoutControlGroup4.Text = "شرکت ها";
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.ChCompany;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(187, 335);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // LayoutControlItem3
            // 
            this.LayoutControlItem3.Control = this.SearchControl1;
            this.LayoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.LayoutControlItem3.Name = "LayoutControlItem3";
            this.LayoutControlItem3.Size = new System.Drawing.Size(193, 30);
            this.LayoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.LayoutControlItem3.TextVisible = false;
            this.LayoutControlItem3.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.AppearanceGroup.Font = new System.Drawing.Font("IRANSansWeb(FaNum)", 9F, System.Drawing.FontStyle.Bold);
            this.layoutControlGroup1.AppearanceGroup.Options.UseFont = true;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem7,
            this.layoutControlItem4});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 111);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(193, 81);
            this.layoutControlGroup1.Text = "ساعت فروش";
            this.layoutControlGroup1.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.timeEdit2;
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 28);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(187, 28);
            this.layoutControlItem7.Text = "تا ساعت";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(41, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.timeEdit1;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(187, 28);
            this.layoutControlItem4.Text = "از ساعت";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(41, 13);
            // 
            // userBindingSource
            // 
            this.userBindingSource.DataSource = typeof(Model.User);
            // 
            // splashScreenManager1
            // 
            this.splashScreenManager1.ClosingDelay = 500;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Office 2016 Colorful";
            // 
            // FrmReportTicketSales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1384, 787);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.navigationPane1);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Name = "FrmReportTicketSales";
            this.Ribbon = this.ribbon;
            this.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "گزارش بلیط های پیش فروش";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmticketListTicketUser_FormClosing);
            this.Load += new System.EventHandler(this.frmticketListTicketUser_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationPane1)).EndInit();
            this.navigationPane1.ResumeLayout(false);
            this.navigationPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControl2)).EndInit();
            this.LayoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ChCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.companyBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTodate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFromDate.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SearchControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ticketLocalBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemXtraFADateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reConpany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.userBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.Navigation.NavigationPane navigationPane1;
        private DevExpress.XtraBars.Navigation.NavigationPage navigationPage1;
        internal DevExpress.XtraLayout.LayoutControl LayoutControl2;
        private FarsiLibrary.Win.DevExpress.XtraFADateEdit txtTodate;
        private FarsiLibrary.Win.DevExpress.XtraFADateEdit txtFromDate;
        internal DevExpress.XtraEditors.SearchControl SearchControl1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup2;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        internal DevExpress.XtraLayout.LayoutControlGroup LayoutControlGroup4;
        internal DevExpress.XtraLayout.LayoutControlItem LayoutControlItem3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView grd;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colTripID;
        private DevExpress.XtraGrid.Columns.GridColumn colFullname;
        private DevExpress.XtraGrid.Columns.GridColumn colNo;
        private DevExpress.XtraGrid.Columns.GridColumn colChairsName;
        private DevExpress.XtraGrid.Columns.GridColumn colCityName;
        private DevExpress.XtraGrid.Columns.GridColumn colTel;
        private DevExpress.XtraGrid.Columns.GridColumn colPrice;
        private DevExpress.XtraGrid.Columns.GridColumn colDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colSaleDate;
        private DevExpress.XtraGrid.Columns.GridColumn colNumChairs;
        private DevExpress.XtraGrid.Columns.GridColumn colCashierName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyCode;
        private FarsiLibrary.Win.DevExpress.RepositoryItemXtraFADateEdit repositoryItemXtraFADateEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit reConpany;
        private System.Windows.Forms.BindingSource companyBindingSource;
        private DevExpress.XtraBars.BarButtonItem btnRefresh;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem btnClose;
        internal DevExpress.XtraEditors.CheckedListBoxControl ChCompany;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraBars.BarButtonItem btnReport;
        private DevExpress.XtraGrid.Columns.GridColumn colCashierCode;
        private System.Windows.Forms.BindingSource userBindingSource;
        private System.Windows.Forms.BindingSource ticketLocalBindingSource;
        private DevExpress.XtraGrid.Columns.GridColumn colTimeSale;
        private DevExpress.XtraEditors.TimeEdit timeEdit2;
        private DevExpress.XtraEditors.TimeEdit timeEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraBars.BarButtonItem btnPrint;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidBackPercent;
        private DevExpress.XtraSplashScreen.SplashScreenManager splashScreenManager1;
        private DevExpress.XtraGrid.Columns.GridColumn colTotalTicket;
        private DevExpress.XtraGrid.Columns.GridColumn colPriceDiscount;
        private DevExpress.XtraGrid.Columns.GridColumn colPaidBackDate;
        private DevExpress.XtraGrid.Columns.GridColumn colFundPriceValue;
        private DevExpress.XtraBars.BarButtonItem BtnTicketInternet;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeTicket;
        private DevExpress.XtraGrid.Columns.GridColumn colPayDate;
        private DevExpress.XtraGrid.Columns.GridColumn Coltime;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
    }
}