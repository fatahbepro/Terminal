﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Model;
using DevExpress.XtraEditors;
using BL;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraPrinting;
using System.Globalization;

namespace Test
{
    public partial class FrmReportTicket : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public FrmReportTicket()
        {
            InitializeComponent();
            LoadData();
            GetTripSummery(DateTime.Now, DateTime.Now);
        }


        List<PathTrip> listPath;
        List<Company> AccessCompanyUser;
        List<GetTripsResultReport> list = null;

        void LoadData()
        {
           try
            {
                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);
                if (Userlogin.AccessCompanies == null)
                {
                    XtraMessageBox.Show("کاربر گرامی سطح دسترسی شما تعریف نشده است", "پیام", MessageBoxButtons.OK);
                    return;
                }
                AccessCompanyUser = CompanysServices.GetCompaniesByListCode(Userlogin.AccessCompanies);  //  لیست شرکت های سطح دسترسی
                ChCompany.DataSource = AccessCompanyUser;
                reCompany.DataSource = AccessCompanyUser;
            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }
            finally
            {
                ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);
            }

        }

        /// <summary>
        /// گزارش بلیط های فروخته شده
        /// </summary>
        /// <param name="_Fromdate"></param>
        /// <param name="todate"></param>
        async void GetTripSummery(DateTime _Fromdate, DateTime todate, bool SaleTicket = false)
        {

            try
            {

                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Show, splashScreenManager1);


                listPath = new List<PathTrip>();

                await Task.Run(() =>
                {
                    if (AccessCompanyUser == null)
                    {
                        XtraMessageBox.Show("کاربر گرامی سطح دسترسی شما تعریف نشده است", "پیام", MessageBoxButtons.OK);
                        return;
                    }

                    DateTime _from = DateTime.Parse(_Fromdate.ToShortDateString());
                    DateTime _To = DateTime.Parse(todate.ToShortDateString());


                    if (SaleTicket)
                    {
                        var ft = DateTime.Parse((Convert.ToDateTime(TimeStarttimeEdit.EditValue)).ToString("HH:mm"));
                        var tt = DateTime.Parse((Convert.ToDateTime(TimeEndtimeEdit.EditValue)).ToString("HH:mm"));
                        _from = _from.AddHours(ft.Hour).AddMinutes(ft.Minute).AddSeconds(0);
                        _To = _To.AddHours(tt.Hour).AddMinutes(tt.Minute).AddSeconds(0);
                        list = TripServices.GetTripSummerySaleTicket(_from, _To, out listPath, AccessCompanyUser,Userlogin.IsLoacConnection);
                    }
                    else
                    {
                        _To = _To.AddHours(23).AddMinutes(59).AddSeconds(59);
                        list = TripServices.GetTripSummery(_from, _To, out listPath, AccessCompanyUser,Userlogin.IsLoacConnection);
                    }



                });


                gridControl1.DataSource = list.GroupBy(c => c.CompanyCode).Select(x => new { CompanyCode = x.Select(c => c.CompanyCode).FirstOrDefault(), SumPrice = x.Sum(c => c.SumPrice), SumFundPrice = x.Sum(c => c.SumFundPrice), sumNumberchair = x.Sum(c => c.sumNumberchair), SumOnlinePrice = x.Sum(c => c.SumOnlinePrice), Summery = x.Sum(c => c.Summery), CountTrip = x.Sum(c=> c.CountTrip) });

                ChPath.DataSource = listPath;   // نمایش فقط مسیر های لیست فیلتر شده


            }
            catch (Exception ex)
            {

               GetError.WriteError(ex,this);
                
            }
            finally
            {

                 ExtencionFunction.WaiteForm(ExtencionFunction.WaiteFromType.Close, splashScreenManager1);

            }


        }


        private void btnGetTrip_ItemClick(object sender, ItemClickEventArgs e)
        {
            GetTripSummery(txtFromDate.DateTime, txtTodate.DateTime);
        }

        private void btnClose_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void btnRefresh_ItemClick(object sender, ItemClickEventArgs e)
        {
            LoadData();
        }

        private void FrmReportTicket_Load(object sender, EventArgs e)
        {
            txtTodate.DateTime = DateTime.Now;
            txtFromDate.DateTime = DateTime.Now;
        }


        private string GetFilterStringBindedColumns(string column, CheckedListBoxControl CheckListBox)
        {

            CheckedListBoxControl clbc = (CheckedListBoxControl)CheckListBox;
            string str = string.Empty;

            for (int i = 0; i <= clbc.CheckedItems.Count - 1; i++)
            {
                if (str != string.Empty)
                {
                    str = str + "OR";
                }
                str = str + "[" + column + "] = '" + clbc.CheckedItems[i].ToString() + "' ";
            }

            return str;

        }
        private void ChCompany_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            try
            {

                string str = GetFilterStringBindedColumns("CompanyCode", ChCompany);
                grd.Columns["CompanyCode"].FilterInfo = new ColumnFilterInfo(str);

            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }
        }

        private void ChPath_ItemCheck(object sender, DevExpress.XtraEditors.Controls.ItemCheckEventArgs e)
        {
            try
            {
                if (ChPath.CheckedItems.Count > 0)
                {

                    foreach (PathTrip _company in ChPath.CheckedItems)
                    {
                        gridControl1.DataSource = list;
                    }
                }
                else
                {
                    gridControl1.DataSource = list.GroupBy(c => c.CompanyCode).Select(x => new { CompanyCode = x.Select(c => c.CompanyCode).FirstOrDefault(), sumNumberchair = x.Sum(c => c.sumNumberchair), SumPrice = x.Sum(c => c.SumPrice), SumFundPrice = x.Sum(c => c.SumFundPrice), SumOnlinePrice = x.Sum(c => c.SumOnlinePrice), Summery = x.Sum(c => c.Summery) });

                }

                string str = GetFilterStringBindedColumns("PathTitle", ChPath);
                grd.Columns["PathTitle"].FilterInfo = new ColumnFilterInfo(str);

            }
            catch (Exception ex)
            {
               GetError.WriteError(ex,this);
            }
        }

        private void btnClearfilter_Click(object sender, EventArgs e)
        {
            grd.ClearColumnsFilter();
            gridControl1.DataSource = list.GroupBy(c => c.CompanyCode).Select(x => new { CompanyCode = x.Select(c => c.CompanyCode).FirstOrDefault(), SumPrice = x.Sum(c => c.SumPrice), sumNumberchair = x.Sum(c => c.sumNumberchair), SumFundPrice = x.Sum(c => c.SumFundPrice), SumOnlinePrice = x.Sum(c => c.SumOnlinePrice), Summery = x.Sum(c => c.Summery) });


        }

        private void btnPrintReport_ItemClick(object sender, ItemClickEventArgs e)
        {

            if (!gridControl1.IsPrintingAvailable)
            {
                XtraMessageBox.Show("امکان چاپ وجود ندارد", "خطا", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }


            colSummery.VisibleIndex = 0;
            colSumOnlinePrice.VisibleIndex = 1;
            colSumFundPrice.VisibleIndex = 2;
            colSumPrice.VisibleIndex = 3;
            colsumNumberchair.VisibleIndex = 4;
            colCompanyCode.VisibleIndex = 5;

            gridControl1.ShowPrintPreview();

            colSummery.VisibleIndex = 5;
            colSumOnlinePrice.VisibleIndex = 4;
            colSumFundPrice.VisibleIndex = 3;
            colSumPrice.VisibleIndex = 2;
            colsumNumberchair.VisibleIndex = 1;
            colCompanyCode.VisibleIndex = 0;




        }

        private void btnSaleTicket_ItemClick(object sender, ItemClickEventArgs e)
        {
            GetTripSummery(txtFromDate.DateTime, txtTodate.DateTime, true);
        }
    }
}