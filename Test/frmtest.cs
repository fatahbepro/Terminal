﻿using DevExpress.XtraBars.Alerter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test
{
    public partial class frmtest : Form
    {
        public frmtest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            alertControl1.Show(this, "خطا در عملیات", "اشکال در ارتباط با سرور.لطفا وضعیت اتصال را بررسی نمایید");
        }

        private void alertControl1_AlertClick(object sender, AlertClickEventArgs e)
        {
            MessageBox.Show("Test");
        }

  
    }
}
