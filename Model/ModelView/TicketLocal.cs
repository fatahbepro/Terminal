﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilClass;
namespace Model
{
    public class TicketLocal
    {
        public int ID { get; set; }
        public int TripID { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public bool Man { get; set; }
        public string Fullname { get; set; }
        public string No { get; set; }
        public string Chairs { get; set; }
        public string ChairsName
        {
            get { return this.Chairs.Replace("f", string.Empty).Replace("m", string.Empty); }
        }
        public int CityID { get; set; }
        public short SaleType { get; set; }
        public int FundID { get; set; }
        public Nullable<System.DateTime> Expire { get; set; }
        public string ExpireSH
        {
            get
            {

                if (this.Expire.HasValue)
                    return Utilitise.JulianToPersianDate(this.Expire.Value);
                else
                    return "";
            }
        }
        public Nullable<int> BranchID { get; set; }
        public string Tel { get; set; }
        public int Price { get; set; }
        public int PaidBack { get; set; }
        public int PaidBackPercent { get; set; }
        public Nullable<int> Discount { get; set; }
        public int SumDiscount
        {
            get
            {
                if (this.Discount.HasValue)
                    return this.NumChairs.Value * this.Discount.Value;
                else
                    return 0;
            }
        }
        public Nullable<int> SaleUserID { get; set; }
        public Nullable<System.DateTime> SaleDate { get; set; }
        public string SaleDateSH
        {
            get
            {
                if (this.SaleDate.HasValue)
                    return Utilitise.JulianToPersianDate(this.SaleDate.Value);
                else
                    return "";
            }
        }
        public Nullable<int> NumChairs { get; set; }
        public Nullable<bool> Archive { get; set; }
        public Nullable<bool> Printed { get; set; }
        public Nullable<int> FundUserID { get; set; }
        public Nullable<int> ReturnUserID { get; set; }
        public Nullable<System.DateTime> PaidBackDate { get; set; }
        public string PaidBackDateSH
        {
            get
            {
                if (this.PaidBackDate.HasValue)
                    return Utilitise.JulianToPersianDate(this.PaidBackDate.Value);
                else
                    return "";
            }
        }
        public string Address { get; set; }
        public string CityCode { get; set; }
        public string CashierCode { get; set; }
        public string CashierName { get; set; }
        public Nullable<System.DateTime> PayDate { get; set; }
        public string PayDateSH
        {
            get
            {
                if (this.PayDate.HasValue)
                    return Utilitise.JulianToPersianDate(this.PayDate.Value);
                else
                    return "";
            }
        }
        public string CompanyCode { get; set; }
        public string CityName { get; set; }
        public string Stateticket
        {
            get
            {
                return (System.Int64)this.SaleType == 1 ? "رزرو" :
                    (System.Int64)this.SaleType == 0 ? "فروش صندوق" :
                    (System.Int64)this.SaleType == 2 ? "فروش تعاونی" :
                    (System.Int64)this.SaleType == 3 ? "فروش دفتر" : null;
            }
        }
        public string ReturnUserCode { get; set; }
        public string ReturnUserName { get; set; }
        public string StatusPaidBack
        {
            get
            {

                return (System.Int64)this.PaidBack == 1 ? "برگشتی صندوق" :
                    (System.Int64)this.PaidBack == 0 ? "برگشت نشده" :
                    (System.Int64)this.PaidBack == 2 ? "برگشتی تعاونی" : null;
            }
        }

        public int TotalTicket
        {
            get {

                int totalticket = (this.NumChairs.ToInt() * (this.Price + this.OnlinePrice + this.FundPrice.ToInt())) - (this.NumChairs.ToInt() * this.Discount.ToInt());
                return totalticket;
            }
        }
        public double? PricePaidBack { get; set; }
        public string TimeSale
        {
            get
            {
                return this.SaleDate.Value == null ? string.Empty : this.SaleDate.ToString();
            }
        }
        public Nullable<int> FundPrice { get; set; }
        public int FundPriceValue
        {
            get
            {
                return this.FundPrice.ToInt() * this.NumChairs.ToInt();
            }
        }

        public string TypeTicket { get; set; }
        public Nullable<bool> PayLock { get; set; }
        public Nullable<System.DateTime> MoveTime { get { return Convert.ToDateTime(this.DepartureTime); } }
        public string MoveTimeSH
        {
            get
            {
                if (this.MoveTime.HasValue)
                    return Utilitise.JulianToPersianDateTime(this.MoveTime.Value);
                else
                    return "";

            }
        }
        public Nullable<long> PaidBackPrice { get; set; }
        public int OnlinePrice { get; set; }
        public bool Closed { get; set; }
        public string DepartureTime { get; set; }
    }


}
