﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;

namespace Model
{
    public partial class testNetEntities
    {

      static  string Connectionnew;
        public testNetEntities(string nameOrConnectionString)
             : base("name=testNetEntities")
        {

            this.Database.Connection.ConnectionString = Connectionnew;

        }

        public static testNetEntities Create(string providerConnectionString)
        {



            //if (string.IsNullOrEmpty(providerConnectionString) || string.IsNullOrWhiteSpace(providerConnectionString))
            //{
            //    providerConnectionString = "Data Source=192.168.3.10;Initial Catalog=s_t11;Integrated Security=False; User Id=m;Password=1234;MultipleActiveResultSets=True";
            //}
            Connectionnew = providerConnectionString;

            var entityBuilder = new EntityConnectionStringBuilder();
            entityBuilder.ProviderConnectionString = providerConnectionString;
            entityBuilder.Provider = "System.Data.SqlClient";
            //  metadata=res://*/DBTestNet.csdl|res://*/DBTestNet.ssdl|res://*/DBTestNet.msl;provider=System.Data.SqlClient;provider connection string=&quot;Data Source=10.249.27.57;Initial Catalog=s_t11;Integrated Security=False; User Id=test;Password=test;MultipleActiveResultSets=True;App=EntityFramework&quot;" providerName="System.Data.EntityClient" />
            // "metadata=res://*/DBTestNet.csdl|res://*/DBTestNet.ssdl|res://*/DBTestNet.msl;provider=System.Data.SqlClient;provider connection string=\"Data Source=10.249.27.57;Initial Catalog=s_t11;Integrated Security=False; User Id=test;Password=test;MultipleActiveResultSets=True;App=EntityFramework\""

            entityBuilder.Metadata = @"res://*/DBTestNet.csdl|res://*/DBTestNet.ssdl|res://*/DBTestNet.msl";

            SetConnectionString(providerConnectionString, entityBuilder.ConnectionString);

          
            return  new testNetEntities(entityBuilder.ConnectionString); 



        }

        private static void SetConnectionString(string con,string buil)
        {
          Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
         string connow= config.ConnectionStrings.ConnectionStrings["testNetEntities"].ConnectionString;
            config.ConnectionStrings.ConnectionStrings["testNetEntities"].ConnectionString = buil;
            config.ConnectionStrings.ConnectionStrings["testNetEntities"].ProviderName = "System.Data.EntityClient";config.Save(ConfigurationSaveMode.Modified);
            connow = config.ConnectionStrings.ConnectionStrings["testNetEntities"].ConnectionString;
        }

    }



}


