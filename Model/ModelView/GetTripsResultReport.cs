﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class GetTripsResultReport
    {

        public string PathTitle { get; set; }
        public int sumNumberchair { get; set; }
        public string CompanyCode { get; set; }
        public decimal SumPrice { get; set; }
        public decimal SumFundPrice { get; set; }
        public decimal SumOnlinePrice { get; set; }
        public decimal Summery { get; set; }
        public decimal SumTollPrice { get; set; }
        public int CountTrip { get; set; }
        public int TripID { get; set; }


    }
}
