﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    //public  class TicketLocal
    //  {
    //  }


    public class TicketLocal
    {
        public int ID { get; set; }
        public int TripID { get; set; }
        public Nullable<int> CustomerID { get; set; }
        public bool Man { get; set; }
        public string Fullname { get; set; }
        public string No { get; set; }
        private string _chairs;
        public string Chairs
        {
            get { return _chairs; }
            set { _chairs = value.Replace("f", string.Empty).Replace("m", string.Empty); }
        }

        public int CityID { get; set; }
        public short SaleType { get; set; }
        public int FundID { get; set; }
        public Nullable<System.DateTime> Expire { get; set; }
        public string ExpireSH
        {
            get
            {

                if (this.Expire.HasValue)
                    return Utilitise.JulianToPersianDate(this.Expire.Value);
                else
                    return "";
            }
        }
        public Nullable<int> BranchID { get; set; }
        public string Tel { get; set; }
        public int Price { get; set; }
        public int PaidBack { get; set; }
        public int PaidBackPercent { get; set; }
        public Nullable<int> Discount { get { return (this.NumChairs * (this.Discount == null ? 0 : this.Discount.Value)); } }
        public Nullable<int> SaleUserID { get; set; }
        public Nullable<System.DateTime> SaleDate { get; set; }
        public string SaleDateSH
        {
            get
            {
                if (this.SaleDate.HasValue)
                    return Utilitise.JulianToPersianDate(this.SaleDate.Value);
                else
                    return "";
            }
        }
        public Nullable<int> NumChairs { get; set; }
        public Nullable<bool> Archive { get; set; }
        public Nullable<bool> Printed { get; set; }
        public Nullable<int> FundUserID { get; set; }
        public Nullable<int> ReturnUserID { get; set; }
        public Nullable<System.DateTime> PaidBackDate { get; set; }
        public string PaidBackDateSH
        {
            get
            {
                if (this.PaidBackDate.HasValue)
                    return Utilitise.JulianToPersianDate(this.PaidBackDate.Value);
                else
                    return "";
            }
        }
        public string Address { get; set; }
        public string CityCode { get; set; }
        public string CashierCode { get; set; }
        public string CashierName { get; set; }
        public Nullable<System.DateTime> PayDate { get; set; }
        public string PayDateSH
        {
            get
            {
                if (this.PayDate.HasValue)
                    return Utilitise.JulianToPersianDate(this.PayDate.Value);
                else
                    return "";
            }
        }
        public string CompanyCode { get; set; }
        public string CityName { get; set; }
        public string Stateticket
        {
            get { return this.SaleType == 1 ? "رزرو" : this.SaleType == 0 ? "فروش صندوق" : this.SaleType == 2 ? "فروش تعاونی" : this.SaleType == 3 ? "فروش دفتر" : null; }
        }
        public string ReturnUserCode { get; set; }
        public string ReturnUserName { get; set; }

        string paidbackState;
        public string StatusPaidBack
        {
            get
            {
                return (this.PaidBack == 1 ? "برگشتی صندوق" : this.PaidBack == 0 ? "برگشت نشده" : this.PaidBack == 2 ? "برگشتی تعاونی" : null); ;
            }

        }
        public int TotalTicket
        {
            get
            {
                return (NumChairs.Value * (Price + OnlinePrice + FundPrice.Value)) - (NumChairs.Value * Discount.Value);
            }
        }
        public double PricePaidBack { get; set; }
        public string TimeSale
        {
            get
            {
                if (this.SaleDate.HasValue)
                    return this.SaleDate.Value.ToString("HH:mm");
                else
                    return string.Empty;
            }

        }
        int? _fundprice;
        public Nullable<int> FundPrice
        {
            get
            {
                if (_fundprice.HasValue)
                    return _fundprice;
                else
                    return this.FundPrice * (this.NumChairs == null ? 0 : this.NumChairs.Value);
            }
            set { _fundprice = value; }
        }
        public string TypeTicket { get; set; }
        public Nullable<bool> PayLock { get; set; }
        public Nullable<System.DateTime> MoveTime
        {
            get { return Convert.ToDateTime(DepartureTime); }
        }
        public string MoveTimeSH
        {
            get
            {
                if (this.MoveTime.HasValue)
                    return Utilitise.JulianToPersianDateTime(this.MoveTime.Value);
                else
                    return "";

            }
        }
        public Nullable<long> PaidBackPrice { get; set; }
        public int OnlinePrice { get; set; }
        public bool Closed { get; set; }
        public string DepartureTime { get; set; }
    }


}
