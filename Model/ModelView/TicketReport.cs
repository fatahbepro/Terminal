﻿using System;

public class TicketReport
{
    public int ID { get; set; }
    public int TripID { get; set; }
    public bool Man { get; set; }
    public string Fullname { get; set; }
    public string No { get; set; }
    public string Chairs { get; set; }
    public string ChairsName { get { return this.Chairs.Replace("f", string.Empty).Replace("m", string.Empty); } }
    public int CityID { get; set; }
    public string Tel { get; set; }
    public int Price { get; set; }
    public string SaleDate { get; set; }
    public string PrintDate { get; set; }
    public Nullable<int> NumChairs { get; set; }
    public string CashierCode { get; set; }
    public string CashierName { get; set; }
    public string CompanyCode { get; set; }
    public string CompanyTitle { get; set; }
    public int TotalPrice { get; set; }
    public string CityNameEnd { get; set; }
    public string CityNameStart { get; set; }
    public string TypeBus { get; set; }
    public byte[] Logo { get; set; }
    public Nullable<bool> PayLock { get; set; }

}

