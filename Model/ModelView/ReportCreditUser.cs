﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilClass;

namespace Model
{
    public class ReportCreditUser
    {
        public int ID { get; set; }
        public string UserCode { get; set; }
        public long Recived { get; set; }
        public long Payment { get; set; }
        public string ActionType { get; set; }
        public Nullable<System.DateTime> ActionDate { get; set; }
        public string ActionDateSH
        {
            get
            {

                if (this.ActionDate.HasValue)
                    return Utilitise.JulianToPersianDate(this.ActionDate.Value);
                else
                    return "";
            }
        }

        public long Mode { get; set; }
    }
}
