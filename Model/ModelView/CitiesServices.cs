﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class CitiesServices
    {

        public int ID { get; set; }
        public string Title { get; set; }
        public string Code { get; set; }
        public bool Enabled { get; set; }
        public int Price { get; set; }


    }
}
