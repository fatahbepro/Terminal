﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilClass;

namespace Model
{
    public class GetTripsReport
    {
        public int TripID { get; set; }
        public int ServiceID { get; set; }
        public System.DateTime Date { get; set; }
        public Nullable<int> CarID { get; set; }
        public Nullable<int> DriverID1 { get; set; }
        public Nullable<int> DriverID2 { get; set; }
        public Nullable<int> DriverID3 { get; set; }
        public int No { get; set; }
        public bool Closed { get; set; }
        public Nullable<bool> Locked { get; set; }
        public Nullable<int> CarTypeID { get; set; }
        public string DepartureTime { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public Nullable<int> CloseUserID { get; set; }
        public string CompanyUser { get; set; }
        public Nullable<System.DateTime> DateMove { get; set; }
        public string TimeMove { get; set; }
        public string DepartureTime2 { get; set; }
        public int ServiceType { get; set; }
        public Nullable<int> LayoutID { get; set; }
        public string CarTypeTitle { get; set; }
        public string PathTitle { get; set; }
        public int PathID { get; set; }
        public string IDCities { get; set; }
        public int IDCity1 { get; set; }
        public int IDCity2 { get; set; }
        public Nullable<int> NumTickets { get; set; }
        public Nullable<int> NumReserves { get; set; }
        public string CompanyName { get; set; }
        public string CompanyCode { get; set; }
        public string DateSH { get { return Utilitise.JulianToPersianDate(this.Date); } }
        public string Day { get; set; }
        public int ID { get; set; }
        public Nullable<int> TotalPrice { get; set; }
        public int CityID { get; set; }
        public string CitiesBetween { get; set; }
    }


    public class TripCountRezervTicketsReport
    {
        public int NumTickets { get; set; }
        public int NumReserves { get; set; }
        public String DepartureTime2 { get; set; }
        public DateTime DepartureTime { get { return Convert.ToDateTime(this.DepartureTime2); } }
        public int TripID { get; set; }
    }
}
