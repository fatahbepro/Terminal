﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Model
{
    public class PathTrip
    {

        public string PathTitle { get; set; }
        public int IDCity1 { get; set; }
        public int IDCity2 { get; set; }
        public string IDCities { get; set; }
        public int TripID { get; set; }

    }

    public partial class Sp_GetCitiesTrip
    {
        public string Title { get; set; }
        public string Code { get; set; }
        public int ID { get; set; }
        public bool Enabled { get; set; }
        public int ServiceID { get; set; }
    }

}
