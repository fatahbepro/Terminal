﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using ModelLayer;
using Model;

namespace BL
{
    public static class LayoutServices
    {
        public static ILayout RepositoryLayout;

        static LayoutServices()
        {
            RepositoryLayout = new LayoutRipository();
        }


        public static Layout FindByID(int id, string _connectionCompany)
        {
            return RepositoryLayout.FindByID(id, _connectionCompany);

        }


    }
}
