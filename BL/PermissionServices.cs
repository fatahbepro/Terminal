﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelLayer;
using Model;

namespace BL
{
  public static  class PermissionServices 
    {
        public static IPermission _repository;

        static PermissionServices()
        {
            _repository = new PermissionRepository();
        }



        public static int Delete(Permission permission)
        {
            return _repository.Delete(permission);
        }

        public static Permission FindByName(string name,string usercode)
        {
            return _repository.FindByName(name,usercode);
        }

        public static List<Permission> GetAll()
        {
            return _repository.GetAll();
        }

        public static int Insert(Permission permission)
        {
            return _repository.Insert(permission);
        }

        public static Permission FindByID(int ID)
        {
          return _repository.FindByID(ID);
        }

        public static int Update(Permission permission)
        {
            return _repository.Update(permission);
        }
    }
}
