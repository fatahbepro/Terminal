﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;
using ModelLayer;


namespace BL
{

    public static class UserCreditReportServices
    {

        public static IUserCreditReport repository;

        static UserCreditReportServices()
        {
            repository = new UserCreditReportRepository();
        }

        public static int Insert(UserCreditReport user)
        {

            return repository.Insert(user);
        }
        public static List<ReportCreditUser> ReportUser(DateTime FromdDate, DateTime Todate, string UserCode)
        {
            return repository.ReportUser(FromdDate, Todate, UserCode);
        }
    }
}
