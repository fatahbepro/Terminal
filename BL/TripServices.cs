﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;

using ModelLayer;
using Model;
using System.Data;

namespace BL
{
    public static class TripServices
    {
        public static ITrip RepositoryTrip;

        static TripServices()
        {
            RepositoryTrip = new TripRepository();
        }

        public static Trip FindByID(int id, string ConnectionCompany)
        {
            return RepositoryTrip.FindByID(id, ConnectionCompany);
        }

        /// <summary>
        /// لیست سرویس های یک تاریخ
        /// </summary>
        /// <param name="_fromDate">از تاریخ</param>
        /// <param name="_todate">تا تاریخ</param>
        /// <param name="_Path">لیست مسیر های این سرویس ها</param>
        /// <param name="AccessCompanyUser">تعاونی های مورد نظر</param>
        /// <param name="listCities">لیست شهر های بین راهی این سرویس ها</param>
        /// <param name="ServiceOvertime">مدت زمان فروش تا سرویس</param>
        /// <param name="IsLoacConnection">اتصال لوکال یا شبکه</param>
        /// <returns></returns>
        public static List<GetTripsReport> GetTripsDateEF(DateTime _fromDate, DateTime _todate, out List<PathTrip> _Path, List<Company> AccessCompanyUser,
         out List<Sp_GetCitiesTrip> listCities, int ServiceOvertime, bool IsLoacConnection)
        {
            return RepositoryTrip.GetTripsDateEF(_fromDate, _todate, out _Path, AccessCompanyUser, out listCities, ServiceOvertime, IsLoacConnection);
        }
        public static DataTable GetTripsByAdo(string connection, DateTime fromdate, DateTime todate)
        {
            SqlConnection con = new SqlConnection(connection);
            string sql = string.Format(@"
-- Region Parameters
DECLARE @p0 NVarChar(1000) = ' -> '
DECLARE @p1 NVarChar(1000) = ' -> '
DECLARE @p2 Int = 0
DECLARE @p3 Int = 0
DECLARE @p4 Int = 2
DECLARE @p5 Int = 0
DECLARE @p6 Int = 1
-- EndRegion
SELECT
[t0].[ID]AS[TripID],[t0].[ServiceID],[t0].[Date],[t0].[CarID],[t0].[DriverID1],
[t0].[DriverID2],[t0].[DriverID3],[t0].[No],[t0].[Closed],[t0].[Locked],[t0].[CarTypeID],[t0].[DepartureTime],[t0].[CloseDate],[t0].[CloseUserID],
		  [t0].[CompanyUser], [t0].[DateMove], [t0].[TimeMove], COALESCE([t0].[DepartureTime]	   
		   ,[t1].[DepartureTime]) AS [DepartureTime2], [t1].[ServiceType],
		    [t5].[LayoutID] AS [LayoutID], [t5].[Title] AS [CarTypeTitle], 
			([t3].[Title] + @p0) + [t4].[Title] AS [PathTitle], [t2].[ID] AS [PathID],
			 ((CONVERT(NVarChar,[t3].[ID])) + @p1) + (CONVERT(NVarChar,[t4].[ID])) AS [IDCities], 
			 [t3].[ID] AS [IDCity1], [t4].[ID] AS [IDCity2], (
    SELECT SUM([t6].[NumChairs])
    FROM [Tickets] AS [t6]
    WHERE ([t6].[TripID] = [t0].[ID]) AND ([t6].[PaidBack] = @p2) AND (([t6].[SaleType] = @p3) OR ([t6].[SaleType] = @p4))
    ) AS [NumTickets], (
    SELECT SUM([t7].[NumChairs])
    FROM [Tickets] AS [t7]
    WHERE ([t7].[TripID] = [t0].[ID]) AND ([t7].[PaidBack] = @p5) AND ([t7].[SaleType] = @p6)
    ) AS [NumReserves]

FROM [Trips] AS [t0]
INNER JOIN [Services] AS [t1] ON [t0].[ServiceID] = [t1].[ID]
INNER JOIN [Paths] AS [t2] ON [t1].[PathID] = [t2].[ID]
INNER JOIN [Cities] AS [t3] ON [t2].[SrcCityID] = [t3].[ID]
INNER JOIN [Cities] AS [t4] ON [t2].[DestCityID] = [t4].[ID]
LEFT OUTER JOIN [CarTypes] AS [t5] ON ([t0].[CarTypeID]) = [t5].[ID]
WHERE (ISNULL(Closed,0)<>1) AND (ISNULL(Locked,0)<>1)
AND ([t0].[Date] BETWEEN '{0}' AND '{1}')", fromdate, todate);

            //SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataAdapter da = new SqlDataAdapter(sql, con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

        /// <summary>
        /// مجموع بلیط های فروخته شده
        /// </summary>
        /// <param name="_fromdate"></param>
        /// <param name="_todate"></param>
        /// <param name="listPath"></param>
        /// <param name="AccessCompanyUser"></param>
        /// <returns></returns>
        public static List<GetTripsResultReport> GetTripSummery(DateTime _fromdate, DateTime _todate, out List<PathTrip> listPath, List<Company> AccessCompanyUser, bool IsLoacConnection)
        {
            return RepositoryTrip.GetTripSummery(_fromdate, _todate, out listPath, AccessCompanyUser, IsLoacConnection);
        }

        /// <summary>
        /// مجموع بلیط های پیش فروش
        /// </summary>
        /// <param name="_fromdate"></param>
        /// <param name="_todate"></param>
        /// <param name="listPath"></param>
        /// <param name="AccessCompanyUser"></param>
        /// <returns></returns>
        public static List<GetTripsResultReport> GetTripSummerySaleTicket(DateTime _fromdate, DateTime _todate, out List<PathTrip> listPath, List<Company> AccessCompanyUser, bool IsLoacConnection)
        {
            return RepositoryTrip.GetTripSummerySaleTicket(_fromdate, _todate, out listPath, AccessCompanyUser, IsLoacConnection);
        }

        public static int GetNumberRezervTickets(string ConnectionCompany, int TripID, out int FullChair, int ServiceOvertime)
        {
            return RepositoryTrip.GetNumberRezervTickets(ConnectionCompany, TripID, out FullChair, ServiceOvertime);
        }
    }
}

