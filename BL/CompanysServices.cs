﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelLayer;
using Model;


namespace BL
{
    public static class CompanysServices
    {
        public static ICompany repositoryCompany;
        static List<Company> _ListCompanyuserCashing;
        static List<Company> _ListCompany;
        static CompanysServices()
        {
            repositoryCompany = new CompanyRepository();
        }

        public static List<Company> GetAll(bool Getimg=false)
        {
            if (_ListCompany == null)
                _ListCompany = repositoryCompany.GetAll(Getimg);
            return _ListCompany;
        }

        public static Company FindByID(int id)
        {
            return repositoryCompany.FindByID(id);

        }
        public static Company FindByCode(string code)
        {
            return repositoryCompany.FindByCode(code);
        }
        public static List<Company> GetCompaniesByListCode(string CodesCompanyUser)
        {
            if (_ListCompanyuserCashing == null || _ListCompanyuserCashing.Count == 0)
            {
                _ListCompanyuserCashing = new List<Company>();
                List<string> licom = CodesCompanyUser.Split(';').ToList();
                licom.RemoveAll(c => string.IsNullOrEmpty(c));
                string codeCom = string.Join(",", licom);
                _ListCompanyuserCashing = repositoryCompany.GetCompaniesByListCode(codeCom);
            }
            return _ListCompanyuserCashing;
        }
        public static Company FindByCodeEnable(string Code, bool _Enable)
        {
            return repositoryCompany.FindByCodeEnable(Code, _Enable);
        }
        //public static List<Company> GetCompanyUser(string CodesCompanyUser)
        //{
        //    if (_ListCompanyuserCashing == null || _ListCompanyuserCashing.Count == 0)
        //    {
        //        _ListCompanyuserCashing = new List<Company>();
        //        List<string> _CodeCompanyUser = CodesCompanyUser.Split(';').ToList();
        //        foreach (string item in _CodeCompanyUser)
        //        {
        //            if (!string.IsNullOrEmpty(item) || !string.IsNullOrWhiteSpace(item))
        //            {
        //                Company co = CompanysServices.FindByCodeEnable(item, true);
        //                if (co != null)
        //                {
        //                    _ListCompanyuserCashing.Add(co);
        //                }
        //            }
        //        }
        //    }
        //    return _ListCompanyuserCashing;
        //}
        public static int Insert(Company cu)
        {
            _ListCompany = null;
            return repositoryCompany.Insert(cu);
        }
        public static int Update(Company cu)
        {
            _ListCompany = null;
            return repositoryCompany.Update(cu);

        }
    }
}
