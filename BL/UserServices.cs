﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;
using ModelLayer;

namespace BL
{
    public static class UserServices
    {
        public static IUser RepositoryUser;

        static UserServices()
        {
            RepositoryUser = new UserRipository();
        }

        public static List<User> GetAll()
        {
            return RepositoryUser.GetAll();
        }

        public static User FindByUser(string Uname, string Upass)
        {
            return RepositoryUser.FindByUser(Uname, Upass);
        }


        public static User FindByID(int id)
        {
            return RepositoryUser.FindByID(id);
        }

        public static int Insert(User user)
        {
            return RepositoryUser.Insert(user);
        }

        public static int Update(User user)
        {
            return RepositoryUser.Update(user);

        }

        public static bool LoginTime(TimeSpan _now, int _ID)
        {
            return RepositoryUser.LoginTime(_now, _ID);
        }
        public static User FindByCode(string _code, string _uname)
        {
            return RepositoryUser.FindByCode(_code, _uname);
        }



    }
}


