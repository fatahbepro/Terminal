﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;
using ModelLayer;

namespace BL
{
    public static class TicketServices
    {
        public static ITicket RepositoryTicket;
        static TicketServices()
        {
            RepositoryTicket = new TicketRepository();
        }


        public static List<TicketLocal> GetListByTripID(int _tripID, string ConnectionCompany, string CodeCompany)
        {
            return RepositoryTicket.GetListByTripID(_tripID, ConnectionCompany, CodeCompany);
        }
        public static List<Ticket> FindByIDTrip(int idtrip, string ConnectionCompany)
        {
            return RepositoryTicket.FindByIDTrip(idtrip, ConnectionCompany);
        }
        public static object GetTripbyID(int id, string connection)
        {
            return RepositoryTicket.GetTripbyID(id, connection);
        }

        public static List<TicketLocal> GetTicketByDate(DateTime fromdt, DateTime todate, bool ISAdmin, string CashierCode, List<Company> Companis, bool localConnection)
        {
            return RepositoryTicket.GetTicketByDate(fromdt, todate, ISAdmin, CashierCode, Companis, localConnection);
        }

        public static List<TicketLocal> GetTicketRollBakByDate(DateTime fromdt, DateTime todate, bool ISAdmin, string CashierCode, List<Company> licompany, bool localConnection)
        {
            return RepositoryTicket.GetTicketRollBakByDate(fromdt, todate, ISAdmin, CashierCode, licompany, localConnection);
        }

        public static List<TicketLocal> GetTicketNetByDate(DateTime fromdt, DateTime todate, List<Company> listcompany, bool localConnection)
        {
            return RepositoryTicket.GetTicketNetByDate(fromdt, todate, listcompany, localConnection);
        }

        public static int Insert(TicketLocal ticket, string connectionCompany)
        {
            return RepositoryTicket.Insert(ticket, connectionCompany);

        }

        public static int UpSertTickets(Ticket ticket, string connectionCompany)
        {
            return RepositoryTicket.UpSertTickets(ticket, connectionCompany);

        }
        public static int Update(Ticket ticketnet, string ConnectionCompany)
        {
            return RepositoryTicket.Update(ticketnet, ConnectionCompany);
        }
        public static Ticket FindByIDNet(int id, string ConnectionCompany)
        {
            return RepositoryTicket.FindByIDNet(id, ConnectionCompany);
        }

        public static Ticket FindByIDNetSaly(int id, string ConnectionCompany)

        {
            return RepositoryTicket.FindByIDNetSaly(id, ConnectionCompany);
        }

        public static int UpdateticketNet(Ticket ticketnet, string ConnectionCompany)
        {
            return RepositoryTicket.UpdateticketNet(ticketnet, ConnectionCompany);
        }
        public static int DeleteTickets(int idticketCompany, string ConnectionCompany)
        {
            return RepositoryTicket.DeleteTickets(idticketCompany, ConnectionCompany);
        }

        public static List<Ticket> GetTicketPaylockRollBack(string ConnectionCompany, string _cashierCode)
        {
            return RepositoryTicket.GetTicketPaylockRollBack(ConnectionCompany, _cashierCode);
        }
        public static int UpdateTicketCompany(Ticket _ticket, string ConnectionCompany)
        {
            return RepositoryTicket.UpdateTicketCompany(_ticket, ConnectionCompany);
        }
        public static List<TicketLocal> GetListTicketDate(DateTime fromdt, DateTime todate, List<Company> listcompany, bool localConnection, int Saletype)
        {
            return RepositoryTicket.GetListTicketDate(fromdt, todate, listcompany, localConnection, Saletype);
        }
        public static bool SearchTicketByNo(string NO, string ConnectionCompany)
        {
            return RepositoryTicket.SearchTicketByNo(NO, ConnectionCompany);
        }
        public static int SuccessPayLockPos(int IDTicketCompany, string ConnectionCompany)
        {
            return RepositoryTicket.SuccessPayLockPos(IDTicketCompany, ConnectionCompany);
        }
        public static List<TicketLocal> GetTicketSales(DateTime fromdt, DateTime todate, List<Company> Companis, bool localConnection)
        {
            return RepositoryTicket.GetTicketSales(fromdt, todate, Companis, localConnection);
        }
    }
}
