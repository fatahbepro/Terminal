﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Model;
using ModelLayer;
using System.Data.SqlClient;
using System.Data;

namespace BL
{
    public static class CityService

    {
        static CityService()
        {
            RepositoryCity = new CityRepository();
        }

        public static ICity RepositoryCity;

        public static List<CitiesServices> FindByIDService(int idService, string Connection)
        {
            return RepositoryCity.FindByIDService(idService, Connection);
        }
        public static City FindByID(int id, string _Connection)
        {
            return RepositoryCity.FindByID(id, _Connection);
        }

        public static City FindByCode(string code, string _Connection)
        {
            return RepositoryCity.FindByCode(code, _Connection);
        }

        public static DataTable GetCitiesTripsByAdo(string connection, string ServiceID)
        {
            SqlConnection con = new SqlConnection(connection);
            string sql = string.Format(@"
                                        SELECT 
                                        [Cities].[Title],[Cities].[Code],[Cities].[ID],[Cities].[Enabled]
                                         FROM [dbo].[Cities]
                                        INNER JOIN [dbo].[ServiceDests] ON [Cities].[ID] = [ServiceDests].[CityID]
                                        WHERE [ServiceDests].[ServiceID]={0}
                                        ", ServiceID);
            SqlDataAdapter da = new SqlDataAdapter(sql, con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            return dt;
        }

    }
}
