﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelLayer;
using Model;


namespace BL
{
    public static class SettingServices
    {
        public static ISetting repositorysetting;

        static SettingServices()
        {
            repositorysetting = new SettinRipository();
        }

        public static List<Setting> GetAll()
        {    
            return repositorysetting.GetAll();
        }

        public static Setting FindByName(string name)
        {
            return repositorysetting.FindByName(name);

        }
        public static int Insert(Setting cu)
        {
            return repositorysetting.Insert(cu);

        }

        public static int Update(Setting cu)
        {
            return repositorysetting.Update(cu);

        }

        public static int Remove(string name)
        {
            return repositorysetting.RemoveByName(name);

        }

    }
}
