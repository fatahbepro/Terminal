﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ModelLayer;
using Model;


namespace BL
{
    public static  class companyservices
    {
        public static ICompany repositoryCompany;

        static companyservices()
        {
            repositoryCompany = new CompanyRepository();
        }

        public static List<Company> GetAll()
        {
            return repositoryCompany.GetAll();
        }

        public static Company FindByID(int id)
        {
            return repositoryCompany.FindByID(id);

        }
        public static Company FindByCode(string code)
        {
            return repositoryCompany.FindByCode(code);

        }

        public static int Insert(Company cu)
        {
            return repositoryCompany.Insert(cu);

        }

        public static int Update(Company cu)
        {
            return repositoryCompany.Update(cu);

        }
        public static int Delete(Company cu)
        {
            return repositoryCompany.Delete(cu);

        }

        public static int Remove(int id)
        {
            return repositoryCompany.RemoveByID(id);

        }
        public static int Remove(string code)
        {
            return repositoryCompany.RemoveByCode(code);

        }



    }
}
